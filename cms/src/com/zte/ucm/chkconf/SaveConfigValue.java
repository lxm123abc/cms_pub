package com.zte.ucm.chkconf;

import com.zte.ucm.Constants;
import com.zte.ucm.Resultlog;
import com.zte.ucm.config.Config;
import com.zte.ucm.log.Logger;
import com.zte.ucm.log.LoggerFactory;

import java.io.PrintWriter;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

public class SaveConfigValue
{
  Logger log = LoggerFactory.getLogger(SaveConfigValue.class);

  public void saveToMap(Map<String, String> paramMap, Config paramConfig)
  {
    if ((paramConfig == null) || (paramConfig.getCode() == null)) {
      return;
    }
    if (paramMap.containsKey(paramConfig.getCode()))
    {
      String str = (String)paramMap.get(paramConfig.getCode());
      if (!str.equals(paramConfig.getValue()))
      {
        paramMap.put(paramConfig.getCode(), str + "||" + paramConfig.getValue());
      }
    }
    else
    {
      paramMap.put(paramConfig.getCode(), paramConfig.getValue());
    }
  }

  public void outputMap(Map<String, String> paramMap)
  {
    PrintWriter localPrintWriter = null;
    try
    {
      localPrintWriter = new PrintWriter(Constants.CONFIGFILEPATH + "config_check_result.log", "utf-8");

      Set localSet = paramMap.keySet();
      Iterator localIterator = localSet.iterator();
      while (localIterator.hasNext())
      {
        String str = (String)localIterator.next();
        localPrintWriter.write(str + "=" + (String)paramMap.get(str) + "\n");
      }
      this.log.info("In the end, output the checked config value");
    }
    catch (Exception localException)
    {
      localException.printStackTrace();
      this.log.error("Error occurs when output the config value!!!");
      new Resultlog().appendLog("error");
    }
    finally
    {
      localPrintWriter.close();
    }
  }
}
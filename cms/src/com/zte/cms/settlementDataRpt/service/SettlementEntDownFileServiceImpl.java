package com.zte.cms.settlementDataRpt.service;

import com.zte.cms.cpContractManagement.dao.ContractMapDao;
import com.zte.cms.cpContractManagement.model.ContractMap;
import com.zte.cms.cpsp.service.FtpUploadService;
import com.zte.cms.notice.util.FtpAttachment;
import com.zte.cms.settlementDataRpt.dao.SettlementEntDownFileMapper;
import com.zte.cms.settlementDataRpt.model.SettlementEntDownFile;
import com.zte.ssb.framework.exception.exceptions.DAOException;
import com.zte.umap.common.dynamicobj.DynamicObjectBaseDS;
import org.apache.commons.lang.StringUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * @Author: Liangxiaomin
 * @Date Created in 15:23 2019/6/28
 * @Description:
 */
public class SettlementEntDownFileServiceImpl extends DynamicObjectBaseDS implements SettlementEntDownFileService {

    private SettlementEntDownFileMapper dao = null;

    private ContractMapDao contractMapDao = null;

    private FtpUploadService ftpUploadService;

    public void setFtpUploadService(FtpUploadService ftpUploadService) {
        this.ftpUploadService = ftpUploadService;
    }

    public void setContractMapDao(ContractMapDao contractMapDao) { this.contractMapDao = contractMapDao; }

    public void setDao(SettlementEntDownFileMapper dao) {
        this.dao = dao;
    }


    private SettlementEntDownFile getSettlementEntDownFileById(long id) {
        SettlementEntDownFile settlementEntDownFile = new SettlementEntDownFile();
        settlementEntDownFile.setId(id);
        return settlementEntDownFile;
    }

    private SettlementEntDownFile getSettlementEntDownFileBySpid(String spid) {
        SettlementEntDownFile settlementEntDownFile = new SettlementEntDownFile();
        settlementEntDownFile.setSpid(spid);
        return settlementEntDownFile;
    }

    @Override
    public void insertSettlementEntDownFile(SettlementEntDownFile settlementEntDownFile) throws DAOException {

        Long index = (Long) this.getPrimaryKeyGenerator().getPrimarykey("settlementEntDownFile_index");
        //上传
        String nameList = ftpUploadService.upload(index);

        settlementEntDownFile.setFilename(nameList);
        settlementEntDownFile.setId(index);
        dao.insertSettlementEntDownFile(settlementEntDownFile);
    }

    @Override
    public void updateSettlementEntDownFile(SettlementEntDownFile settlementEntDownFile) throws DAOException {
        //上传
        String nameList = ftpUploadService.upload(settlementEntDownFile.getId());
        if (StringUtils.isNotEmpty(settlementEntDownFile.getFilename())){
            settlementEntDownFile.setFilename(settlementEntDownFile.getFilename()+nameList);
        }else {
            settlementEntDownFile.setFilename(nameList);
        }

        dao.updateSettlementEntDownFile(settlementEntDownFile);
    }

    @Override
    public SettlementEntDownFile selectSettlementEntDownFileBySpid(String spid) throws DAOException {
        SettlementEntDownFile settlementEntDownFile = getSettlementEntDownFileBySpid(spid);
        Integer cnt = dao.selectSettlementEntDownFileCountBySpid(settlementEntDownFile);
        if (cnt != null && cnt.intValue()!=0){
            settlementEntDownFile = dao.selectSettlementEntDownFileBySpid(settlementEntDownFile);
            if (StringUtils.isNotEmpty(settlementEntDownFile.getFilename())){
                String[] split = settlementEntDownFile.getFilename().split("\\|");
                List<String> newName = new ArrayList<String>();
                List<String> oldName = new ArrayList<String>();
                for (String id : split) {
                    ContractMap attachInfoById = contractMapDao.getAttachInfoById(id.substring(0,id.indexOf(".")));
                    newName.add(attachInfoById.getAttachurl());
                    oldName.add(attachInfoById.getAttachname());
                }
                settlementEntDownFile.setNewname(newName.toArray(new String[newName.size()]));
                settlementEntDownFile.setOldname(oldName.toArray(new String[oldName.size()]));
            }
        }
        return settlementEntDownFile;
    }

    @Override
    public void deleteAttach(SettlementEntDownFile settlementEntDownFile) throws DAOException {
        String operfilename = settlementEntDownFile.getOperfilename();
        String substring = operfilename.substring(0, operfilename.lastIndexOf("."));
        ftpUploadService.delete(Integer.parseInt(substring),operfilename);
    }


}

/*******************************************************************内容新增页面************************************************************/
	/***
	   提交 
	***/
	var pattern='[!@#$%^&*;|?()\\[\\]\\{\\}\\<\\>\'\"]';
	var showpattern="!@#$%^&*;|?()[]{}<>' \"";
	var pattern2=/[!@#$%^&*\-_+=;:,.'"|?()\[\]\{\}\<\>\\\/]/;
	var showpattern2="!@#$%^&*()-_+=[]{}|\\;:'\",.\/<>?";
	function doConfirm(){   
	
			//清空所有的提示
			$("label[id$='_msg']").text("");


	        /***/
            //var namecn=getHtmlCtrlTrimValue("namecn");
            //内容名称 为空、特殊字符效验  
            if(checkEmptyWideVal("namecn")){
                setMsgCtrlTextAndFocus('namecn',$res_entry("msg.info.mustfill",""))   ;
                return;
            }else if (!isSafe($("#namecn").val(),new RegExp('[!@#$%^*;|?()\\[\\]\\{\\}\\<\\>\'\"]','g'))){
                setMsgCtrlText("namecn",$res_entry("msg.cms.content.42053","")+"!@#$%^*;|?()[]{}<>' \"");
                return;
            }
		   error = IsMaxLength(document.getElementById('namecn'),'namecn_msg','128');
		   if(error == false){
		       return false;
		   }
           
            //原名 非空时的特殊字符效验  
            //var nameen=getHtmlCtrlTrimValue("nameen");
            if(!checkEmptyWideVal("nameen")){
                 if (!isSafe($("#nameen").val(),new RegExp(pattern,'g'))){
                 setMsgCtrlText("nameen",$res_entry("msg.cms.content.42053","")+showpattern);
                 return;
                 }
                 
                error = IsMaxLength(document.getElementById('nameen'),'nameen_msg',128);
			    if(error == false){
			         return false;
			    }
            
          }
            
            //关键字 非空、特殊字符效验
            //var keywords=getHtmlCtrlTrimValue("keywords");
             if(checkEmptyWideVal("keywords")){
                setMsgCtrlTextAndFocus('keywords',$res_entry("msg.info.mustfill",""))   ;
                return;
            }else if (!isSafe($("#keywords").val(),new RegExp(pattern,'g'))){
                setMsgCtrlText("keywords",$res_entry("msg.cms.content.42053","")+showpattern);
                return;
            }
           error =  IsMaxLength(document.getElementById('keywords'),'keywords_msg',256);
           if(error == false){
           return false;
		   }
		   
		   
             //有效开始时间<有效结束时间
             /*
             if(!checkEmptyWideVal("effectiveStartdate")){
            	 validatedate("effectivedate","effectiveStartdate","effectiveStartdate_msg");
            	
             }
             if(!checkEmptyWideVal("expiryEnddate")){
             	 validatedate("expirydate","expiryEnddate","expiryEnddate_msg");
             	
             }
             */
            
             
            var check=betweenTimeValidate("effectivedate","effectiveStartdate","expiryEnddate","effectiveStartdate_msg");
            if(check==0){
                return;
            } 

            
            
            /**产地/国家地区为空、特殊字符效验
            var country=getHtmlCtrlTrimValue("country");
            if(!(checkEmptyWideVal(country))){
                setMsgCtrlTextAndFocus('country',$res_entry("msg.info.mustfill",""))   ;
                return;
            }else if (!isSafe($("#country").val(),new RegExp(pattern,'g'))||$("#country").val().indexOf("\\")!=-1){
                setMsgCtrlText("country",$res_entry("msg.cms.content.42053","")+'!@#$%^&*;|?()\[\]\{\}\<\>');
                return;
            }else{
                setMsgCtrlText('country','');
            }**/
            //导演 非空时特殊字符效验
            //var director=getHtmlCtrlTrimValue("director");
            if(!checkEmptyWideVal("director")){
              if (!isSafe($("#director").val(),new RegExp(pattern,'g'))){
                setMsgCtrlText("director",$res_entry("msg.cms.content.42053","")+showpattern);
                return;
            }
           error =  IsMaxLength(document.getElementById('director'),'director_msg',256);
           if(error == false){
		        return false;
		   }
		   }
            //演员 非空时特殊字符效验
            //var actor=getHtmlCtrlTrimValue("actor");
            if(!checkEmptyWideVal("actor")){
             if (!isSafe($("#actor").val(),new RegExp(pattern,'g'))){
                setMsgCtrlText("actor",$res_entry("msg.cms.content.42053","")+showpattern);
                return;
            } 
           error =  IsMaxLength(document.getElementById('actor'),'actor_msg',256);
           if(error == false){
		        return false;
		   }
           }
            //制片人 非空时特殊字符效验
            //var producer=getHtmlCtrlTrimValue("producer");
            if(!checkEmptyWideVal("producer")){
                if (!isSafe($("#producer").val(),new RegExp(pattern,'g'))){
                setMsgCtrlText("producer",$res_entry("msg.cms.content.42053","")+showpattern);
                return;
            }
           error =  IsMaxLength(document.getElementById('producer'),'producer_msg',64);
           if(error == false){
		       return false;
		   }
          }
            
            //首播日期
           /*
            if(!checkEmptyWideVal("premieredateTime")){
            	 //validatedate("premieredate","premieredateTime","premieredate_msg");
            	 document.getElementById("premieredateTime").value=document.getElementById("premieredateTime").value.replace(/[-]/g,"");
            }
          */
          
           
           
           
           
           
           
           
		  //推荐星级 空 特殊字符 中文字符 0开头 1-10格式
			var pattrec=/^([(1-9)]|10)$/;
			//var pattrecnum=/^[0]\d*$/;
			//var pattrecabc=/[a-zA-Z]+/;
			 var pattrespace=/\s+/;//是否含有空格
             if(!(checkEmptyWideVal("recommendedstar"))){
             	
            	 if(!pattrec.test($("#recommendedstar").val())){
            	 	$("#recommendedstar_msg").html($res_entry("msg.info.content.145"));
            	 	recommendedstar.onfocus();
            	 	return false;
            	 }
            }
            
            /*播放时长 空 特殊字符 中文字符 0开头 格式
            var playtime = document.getElementById("playtime").value;
            if(playtime !=null&&playtime!=""){  
                   var model=/^[0-9]{2}([0-5][0-9]){2}[0-9]{2}$/;
    				if(!model.test(playtime)||playtime=='00000000'){
    				$("#playtime_msg").html("格式不正确,请重新输入");
    				document.getElementById("playtime").focus();
    				return false;
    				}else{
    					$("#playtime_msg").html("");
    				}
                }else{
                	$("#playtime_msg").html("");
                }
             */


             if(checkEmptyWideVal("playtime")){
              	setMsgCtrlTextAndFocus('playtime',$res_entry("msg.info.mustfill",""));
               	 return;
             }else if(pattern2.test($("#playtime").val())){
           	 	setMsgCtrlText("playtime",$res_entry("msg.cms.content.42053","")+showpattern2);
           	 	playtime.onfocus();
           	 	return false;
           	 }else if($("#playtime").val().charAt(0)=="0"||isNaN($("#playtime").val())){
           	 	$("#playtime_msg").html($res_entry("msg.info.channel.025"));
           	 	playtime.onfocus();
           	 	return false;
           	 }
     
            
           error =  IsMaxLength(document.getElementById('playtime'),'playtime_msg',4);
           if(error == false){
		       return false;
		   }
            
            /*上映年份
            var releasedateTime = document.getElementById("releasedateTime").value;
            if(releasedateTime ==""){  
                    setMsgCtrlTextAndFocus('releasedateTime',$res_entry("msg.info.mustfill",""))   ;
                    return;
                }else{
                    setMsgCtrlText('releasedateTime','');
            }  */           
             
              
            /*
            if(!checkEmptyWideVal("releasedateTime")){
            	 validatedate("releasedate","releasedateTime","releasedateTime_msg");
            }
            */
           
           
            
            
            //发行商 为空、特殊字符效验
            //var publisher=getHtmlCtrlTrimValue("publisher");
            if(!(checkEmptyWideVal("publisher"))){
               if (!isSafe($("#publisher").val(),new RegExp(pattern,'g'))){
	                setMsgCtrlText("publisher",$res_entry("msg.cms.content.42053","")+showpattern);
	                return;
                }
                
               error =  IsMaxLength(document.getElementById('publisher'),'publisher_msg',64);
	           if(error == false){
			        return false;
			   }
            }
            
            /**字幕语言 为空、特殊字符效验
            var subtitlelanguage=getHtmlCtrlTrimValue("subtitlelanguage");
            if(!(checkEmptyWideVal("subtitlelanguage"))){
                setMsgCtrlTextAndFocus('subtitlelanguage',$res_entry("msg.info.mustfill",""))   ;
                return;
            }else if (!isSafe($("#subtitlelanguage").val(),new RegExp('[!@#$%^&*;|?()\\[\\]\\{\\}\\<\\>]','g'))){
                setMsgCtrlText("subtitlelanguage",$res_entry("msg.cms.content.42053","")+showpattern);
                return;
            }else{
                setMsgCtrlText('subtitlelanguage','');
            }**/
            /**配音语种 为空、特殊字符效验
            var dubbinglanguage=getHtmlCtrlTrimValue("dubbinglanguage");
            if(!(checkEmptyWideVal("dubbinglanguage"))){
                setMsgCtrlTextAndFocus('dubbinglanguage',$res_entry("msg.info.mustfill",""))   ;
                return;
            }else if (!isSafe($("#dubbinglanguage").val(),new RegExp('[!@#$%^&*;|?()\\[\\]\\{\\}\\<\\>]','g'))){
                setMsgCtrlText("dubbinglanguage",$res_entry("msg.cms.content.42053","")+showpattern);
                return;
            }else{
                setMsgCtrlText('dubbinglanguage','');
            }**/
            /**语言 为空、特殊字符效验
            var languagese=getHtmlCtrlTrimValue("languagese");
            if(!(checkEmptyWideVal("languagese"))){
                setMsgCtrlTextAndFocus('languagese',$res_entry("msg.info.mustfill",""))   ;
                return;
            }else if (!isSafe($("#languagese").val(),new RegExp('[!@#$%^&*;|?()\\[\\]\\{\\}\\<\\>]','g'))){
                setMsgCtrlText("languagese",$res_entry("msg.cms.content.42053","")+showpattern);
                return;
            }else{
                setMsgCtrlText('languagese','');
            }**/
            
             /**
               节目分类
             **/
             if(checkEmptyWideVal("lblcontenttags")){
                setMsgCtrlTextAndFocus('lblcontenttags',$res_entry("msg.info.mustfill",""))   ;
                return;
            }else if (!isSafe($("#lblcontenttags").val(),new RegExp(pattern,'g'))){
                setMsgCtrlText("lblcontenttags",$res_entry("msg.cms.content.42053","")+showpattern);
                return;
            }
             
            //内容简介 为空、特殊字符效验
            if(!(checkEmptyWideVal("introduction"))){
            if (!isSafe($("#introduction").val(),new RegExp('[!@#$%^*;|?()\\[\\]\\{\\}\\<\\>\'\"]','g'))){
              setMsgCtrlText("introduction",$res_entry("msg.cms.content.42053","")+"!@#$%^*;|?()[]{}<>' \"");
              return;
             }
            }
	        
          error = IsMaxLength2(document.getElementById('introduction'),'introduction_old',1024);
           if(error == false){
		       return false;
		   }
	        
	        
	     
           
           
	        var cpcnshortname=getHtmlCtrlTrimValue("cpcnshortname");
	        //内容提供商CP 为空、特殊字符效验  
	        if(checkEmptyWideVal("cpcnshortname")){
	            setMsgCtrlTextAndFocus('cpcnshortname',$res_entry("msg.info.mustfill",""))   ;
	            return;
	        }else if (!isSafe($("#cpcnshortname").val(),new RegExp(pattern,'g'))){
	            setMsgCtrlText("cpcnshortname",$res_entry("msg.cms.content.42053","")+showpattern);
	            return;
	        }else{
	            setMsgCtrlText('cpcnshortname','');
	        }
	       
	       //牌照方
	        var licenseorgan=document.getElementById("licenseorgan").length;	       
            if(licenseorgan<2){
               openMsgBoxSize($res_entry("msg.info.content.146"));
               return;
            }else{
                 var licenseorganValue=document.getElementById("licenseorgan").value;
                 if(licenseorganValue==""){
	                 setMsgCtrlText('licenseorgan',$res_entry("msg.info.basicdata.032"));	
	                 return;                 
                 }else{
	               setMsgCtrlText('licenseorgan','');
	            } 
	        }
	        
	        	       
	        //var provid=getHtmlCtrlTrimValue("provid");
	        //所属区域 为空、特殊字符效验  
	        if(checkEmptyWideVal("provid")){
	            setMsgCtrlTextAndFocus('provid',$res_entry("msg.info.basicdata.032"));
	            return;
	        }
	        
	        
	         // 内容独家有效期
	   		 var pattrecnum2=/^[0]\d+$/;
			 //var pattrec=/[a-zA-Z]+/;
             if(checkEmptyWideVal("exclusivevalidity")){
             	setMsgCtrlTextAndFocus('exclusivevalidity',$res_entry("msg.info.mustfill",""))   ;
                return;
                }else if(pattern2.test($("#exclusivevalidity").val())){
            	 	setMsgCtrlText("exclusivevalidity",$res_entry("msg.cms.content.42053","")+showpattern2);
            	 	exclusivevalidity.onfocus();
            	 	return false;
            	 }else if(isNaN($("#exclusivevalidity").val())){
            	 	$("#exclusivevalidity_msg").html($res_entry("msg.info.content.147"));
            	 	exclusivevalidity.onfocus();
            	 	return false;
            	 }
            	 else if(pattrecnum2.test($("#exclusivevalidity").val())){
            	 	$("#exclusivevalidity_msg").html($res_entry("msg.info.content.147"));
            	 	exclusivevalidity.onfocus();
            	 	return false;
            	 }
            
	         error =  IsMaxLength(document.getElementById('exclusivevalidity'),'exclusivevalidity_msg',3);
	           if(error == false){
			        return false;
			   }
			  
			
			   
	        
	       // 版权失效期 应早于系统日期时间
            var copyrightexpirationTime = document.getElementById("copyrightexpirationTime").value;
          
            if(copyrightexpirationTime ==""){  
                    setMsgCtrlTextAndFocus('copyrightexpirationTime',$res_entry("msg.info.mustfill",""))   ;
                    return;
                }else{
                	
                    validatedate("copyrightexpiration","copyrightexpirationTime","copyrightexpirationTime_msg");
            } 
            
			//牌照方自定义内容下线时间
			
			if(!checkEmptyWideVal("offlinedateTime")){
            	 validatedate("offlinetime","offlinedateTime","offlinedateTime_msg");
            }
           
			//内容删除时间
			
   			if(!checkEmptyWideVal("condeletetime")){
            	 validatedate("deletetime","condeletetime","condeletetime_msg");
            }
           
	        var copyrightcn=getHtmlCtrlTrimValue("copyrightcn");
	        //所属版权 为空、特殊字符效验  
	        if(!checkEmptyWideVal("copyrightcn")){
	           if (!isSafe($("#copyrightcn").val(),new RegExp(pattern,'g'))){
	            setMsgCtrlText("copyrightcn",$res_entry("msg.cms.content.42053","")+showpattern);
	            return;
	            }
		       error = IsMaxLength(document.getElementById('copyrightcn'),'copyrightcn_msg',64);
	           if(error == false){
			       return false;
			   }
	        }
	        

	        
	        
	        
	        
	       

	
	   

	        /**var licensecode=getHtmlCtrlTrimValue("licensecode");
	        //牌照 为空、特殊字符效验  
	        if(checkEmptyWideVal("licensecode")){
	            setMsgCtrlTextAndFocus('licensecode',$res_entry("msg.info.mustfill",""))   ;
	            return;
	        }else if (!isSafe($("#licensecode").val(),new RegExp('[!@#$%^&*;|?()\\[\\]\\{\\}\\<\\>]','g'))){
	            setMsgCtrlText("licensecode",$res_entry("msg.cms.content.42053","")+'!@#$%^&*;|?()\[\]\{\}\<\>\\\/');;
	            return;
	        }else{
	            setMsgCtrlText('licensecode','');
	        }**/
	        
	        
	        /*var buyquality=getHtmlCtrlTrimValue("buyquality");
	        //交易属性 为空、特殊字符效验  
	        if(checkEmptyWideVal("buyquality")){
	            setMsgCtrlTextAndFocus('buyquality',$res_entry("msg.info.mustfill",""))   ;
	            return;
	        }else if (!isSafe($("#buyquality").val(),new RegExp('[!@#$%^&*;|?()\\[\\]\\{\\}\\<\\>]','g'))){
	            setMsgCtrlText("buyquality",$res_entry("msg.cms.content.42053","")+'!@#$%^&*;|?()\[\]\{\}\<\>\\\/');
	            return;
	        }else{
	            setMsgCtrlText('buyquality','');
	        }*/
	        
	        //清空错误提示
	        //clearAllMsgLabel();
	        return true;
	        
	}
	
	
    var CmsCpbind={};
    var licensecodeMap={};
     // 弹出内容提供商选择页面 /****/  
    function popcpsp(){
        var page = "../cpsp/popCpsp.html";
        var sFeatures = "dialogHeight: " + 600 + "px;dialogWidth: " + 800 + "px;";
        var rtnobj = window.showModalDialog(page, "", sFeatures);
        
        if(typeof(rtnobj) != "undefined"){
	        $("#cpcnshortname").val(rtnobj.cpcnshortname);
	        $("#cpindex").val(rtnobj.cpindex);
	        $("#cpid").val(rtnobj.cpid);    
	        //根据cpid获取牌照方 
	        callSid('getCpBase',rtnobj.cpid);
        }
    }  

    
     //弹出节目分类页面
    function popcontenttags(){
        var page = "../basicdata/productData.html";
        var sFeatures = "dialogHeight: " + 600 + "px;dialogWidth: " + 800 + "px;";
        var rtnobj = window.showModalDialog(page, "", sFeatures); 
        if(typeof(rtnobj) != "undefined"){
        $("#typeid").val(rtnobj.typeid);
        $("#lblcontenttags").val(rtnobj.typename);
        }
  }   
    
    
    function resultMsg(errorcode){ 
    
   	
   	
        if(errorcode=='0'){
              openMsg( errorcode + ":" + $res_entry("msg.info.0000010",""), "CMS/content/contentList.html?isLoad=true", true);
              return ;
        }
        if(errorcode=='2'){
              openMsg( "1:"+$res_entry("msg.info.content.097"), "CMS/content/contentList.html?isLoad=true", true);
              return ;
        }
        
        if(errorcode=='3'){
               openMsgBoxSize($res_entry("msg.info.basicdata.078"), "", false);
               return ;
        }
        
        if(errorcode=='4'){
               openMsgBoxSize($res_entry("msg.info.content.148"), "", false);
               return ;
        }
        
        if(errorcode=='5'){
               openMsgBoxSize($res_entry("msg.info.content.149"), "", false);
               return ;
        }
        if(errorcode=='6'){
               openMsgBoxSize($res_entry("msg.info.content.150"), "", false);
               return ;
        }
        if(errorcode=='7'){
               openMsg("1:"+$res_entry("msg.info.content.151"), "CMS/content/contentList.html?isLoad=true", true);
               return ;
        }
        else{
              errorcode = errorcode + ":" + $res_entry("msg.info.0000030","");
              openMsg(errorcode, "CMS/content/vodUploadPage.html?isLoad=true", true);
        }
        
    }
    
    
  function resultMsg2(errorcode){ 
   
        if(errorcode=='0'){
              openMsg(errorcode + ":" + $res_entry("msg.info.0000010",""),  "CMS/content/vodUploadPage.html", true);
              return ;
        }
        
        if(errorcode=='2'){
               openMsgBoxSize($res_entry("msg.info.basicdata.078"), "", false);
               return ;
        }
        if(errorcode=='3'){
               openMsgBoxSize($res_entry("msg.info.content.148"), "", false);
               return ;
        }
        if(errorcode=='4'){
               openMsgBoxSize($res_entry("msg.info.content.149"), "", false);
               return ;
        }
        if(errorcode=='5'){
               openMsgBoxSize($res_entry("msg.info.content.150"), "", false);
               return ;
        }
        else{
              errorcode = errorcode + ":" + $res_entry("msg.info.0000030","");
              openMsg(errorcode, "CMS/content/contentList.html?isLoad=true", true);
        }
         
    }
    
    /***
    返回页面
    ***/
    function toBackPrePage()
    { 
    
     if(typeof(_para.from)!= "undefined"&&_para.from!="undefined")
             document.location.href=_para.from;
           else
             document.location.href="contentList.html?isLoad=true";
             
          
    }
    
/*******************************************************************************************************************************/    
     
    
   
   
    
    
    
   /*******************************************************************内容管理页面************************************************************/
   

       //内容类型转换
     var   keyvalue2;
    function recommendedstarConver(keyvalue){  
         keyvalue.recommendedstar=recommendedstarComm(keyvalue.recommendedstar); 
         return keyvalue;
    } 
    /***/
    var value2;
    function recommendedstarComm(value){
        if((value-0)>=1 && (value-0)<=3){
                 value2 = $res_entry("msg.info.content.009");
            }else if((value-0)>3 && (value-0)<=5){
                value2 = $res_entry("msg.info.content.010");
            }else if((value-0) >=6 && (value-0)<8){
                 value2 = $res_entry("msg.info.content.011");
            }else if((value-0)>=8 && (value-0)<=9){
                value2 = $res_entry("msg.info.content.012");
            }else{
                value2= $res_entry("msg.info.content.013");
            }            
         return value2; 
    
    }
     
    
     function contenttypeConver(keyvalue){   
         keyvalue['contenttype'] = contenttypeMap[keyvalue['contenttype']];
         return keyvalue;
    } 
     function languagesConver(keyvalue){     
         keyvalue['languages'] = languageMap[keyvalue['languages']];
         return keyvalue;
    } 
    
     //获取时间
    function getTime(str){ 
        if(str!=null && str!=""){
	       if(str.length > 6){
	           return str.substr(0,4) + "-" + str.substr(4,2) + "-" + str.substr(6,2) + " " + str.substr(8,2) + ":" + str.substr(10,2) + ":" + str.substr(12,2);
	       }else{ 
	           return str.substr(0,2) + ":" + str.substr(2,2) + ":" + str.substr(4,2);
	       }
	    }
  } 
    
    //定义时间的格式。
    function formatTime(keyvalue) {
      keyvalue['releasedate'] = getTime(keyvalue['releasedate']);
      return keyvalue;
    }
    
    /*******************************************************************************************************************************/
    
     /*******************************************contentList*********************************************************************/
     /****
    平台转换
   ****/
   
   function platformConver(keyvalue){  
         keyvalue['platform'] = platformMap[keyvalue['platform']];
         return keyvalue;
    } 
   
   /****
    删除
   ****/
   var cntObj={};
  
   function  delCntInfo(contentindex,businessindex,contentid){
    	savePageStatus('zteTable',"save_content_view");
        if(confirm($res_entry("msg.info.confirm.del"))){
            cntObj['contentindex']=contentindex;
            cntObj['businessindex']=businessindex;
            cntObj['contentid']=contentid; 
            callSid('delCnt',cntObj);
        }
   } 

   
   
   /****
    同步
   ****/
   var syscnObj={};
   var msg={};
   function commitSyscn(businessindex,contentindex,contentid){
   			savePageStatus('zteTable',"save_content_view");
   			if(confirm($res_entry("msg.info.basicdata.112"))){
			   syscnObj['businessindex']=businessindex;
	           syscnObj['contentindex']=contentindex;
	           syscnObj['contentid']=contentid; 
	           //调用服务
	           callUrl('icmContentBasicinfoLSBufFacade/insetSyscnCon.ssm',syscnObj,'msg',true);
	           //提示消息
	           rtnMsg(msg.rtnValue);
   			}
   }
   
   /****
    提示消息
   ****/

   
   
    function dealResult(errorcode,des)
	 {
	 	   
	 	   var flag=true;
        if(errorcode=='0'){
              errorcode = errorcode + ":" +$res_entry("msg.info.basicdata.028");
              rtnUrl = "CMS/content/contentList.html?isLoad=true";
              openMsg(errorcode, rtnUrl, flag); 
              return ;  
        }else if(errorcode=='2'){
              openMsg("1:"+$res_entry("msg.info.content.097"),'CMS/content/contentList.html?isLoad=true',true); 
              return ;
        } else if(errorcode=='3'){
        	  openMsg("1:"+$res_entry("msg.info.content.151"),'CMS/content/contentList.html?isLoad=true',true);
        	  return ;
        }
        else{
              errorcode = errorcode + ":" + $res_entry("msg.info.0000030","");
              rtnUrl = "CMS/content/contentList.html?isLoad=true";
              openMsg(errorcode, rtnUrl, flag);
        }
         
	
   }  
   /****
    状态转换
   ****/  
   //13:待二审 14:二审中 15:待三审 16:三审中 17:审核中
    function statusConver(keyvalue){ 
    	var status=keyvalue['status'];
    	 if(status=="13" ||  status=="14" ||  status=="15" ||  status=="16" ||  status=="17")
    	 {
         	keyvalue['status']=$res_entry("msg.info.content.026");
         }
         else
         {
         	 keyvalue['status'] = constatusMap[status];
         }
         return keyvalue;
    }
 
 
 
 
 
 
 //显示或隐藏修改链接 10:初始 11:待一审  
function query_upd(i)
{
	if(i>0)
	{
			var status=data[i-1].status;
			if(updRight)
			{
				if(status==10||status==11)
				{
					this.style.display = "";
				}
				else
				{
					this.style.display = "none";
				}
			}
			else
			{
				this.style.display = "none";
			}		
	}
}
 
 
  
//显示或隐藏删除链接 10:初始 11:待一审 20:待上线
function query_del(i)
{
	if(i>0)
	{
		var status=data[i-1].status;
			if(delRight)
			{
				if(status==10 ||status==11 ||status==20)
				{
					this.style.display = "block";
				}
				else
				{
					this.style.display = "none";
				}
			}
			else
			{
				this.style.display = "none";
			}				
	}
}
 
 
//显示或隐藏查看链接
function query_detail(i)
{
	if(i>0)
	{
		var status=data[i-1].status;
			if(detailRight)
			{
				this.style.display = "";
			}
			else
			{
				this.style.display = "none";
			}	
	}
}
 
 //显示或隐藏新增同步链接  17:审核中 25:已删除 90:已销毁 24:已下线 11:待一审 10:初始 43:上线同步到流媒体中
function query_syscn(i)
{
	if(i>0)
	{
		var status=data[i-1].status;
		this.style.display = "none";
		/*
		if(syscnRight)
		{
			if(status==17||status==25||status==90||status==24||status==10||status==11)
			{
				this.style.display = "none";
			}
			else
			{
				this.style.display = "";
			}
		}
		else
		{
			this.style.display = "none";
		}
		*/
			
	}
}
 
 
 	var data={};
  //初始化数据服务之后调用
     function showCnt(tableData, tableId){ 
     
      	data=getTableById(tableId).data;
		noDataTrips(tableData,tableId);
	    ctrlSelectCheckbox(tableId);
		if(resultid=="2")//2:当前登陆用户是牌照方操作员且绑定了牌照方或CP
		{
			addRight=false;
			updRight=false;
			delRight=false;
			applyRight=false;
			syscnRight=false;
			$("a[name='a_sou']").each(function(a_sou){
				this.style.display="none";
			});
		}
    	
     	  document.getElementById("btn_add").style.display = addRight==true?"":"none";
     	  document.getElementById("datch_apply").style.display = applyRight==true&&data.length>=1?"":"none";
     	  $("a[name='a_upd']").each(query_upd);
     	  $("a[name='a_del']").each(query_del);
     	  $("a[name='a_show']").each(query_detail);
  		  $("a[name='a_syscn']").each(query_syscn);
  		  
  		  
  		 
  	    //只有当"待一审"的内容checkbox才让选择
  		for(var i=0;i<data.length;i++)
  		{
  			if(data[i].status=="11")
  			{
  				document.getElementsByName("checkbox1")[i+1].disabled=false;
  			}
  			else
  			{
  				document.getElementsByName("checkbox1")[i+1].disabled=true;
  			}
  		}
     }
  
  
  
  //提审
	function batchDel()
	{   
	    savePageStatus("zteTable","save_content_view");   
	    var cntList = getTable_Td_CheckedValue("zteTable", "1");    
	    if (null != cntList && "" != cntList && cntList.length>0)
	    {
	    	if (confirm($res_entry("msg.info.content.152")))
	        {                
	        	callSid('applyCnt',cntList,"$M.msg",'true');        
	        } 
	    }  
	    else
	    {
	         openMsgBoxSize($res_entry("msg.info.content.153"));
	    }
	} 
	
	//提审结果
	function rtnApplyCntMsg(msg){ 
         var rtnUrl ="CMS/content/contentList.html?isLoad=true";
	  	 rtnUrl = encodeURIComponent(rtnUrl);
	     openMsgBatch(msg,rtnUrl,true);
	}
	


   
    //点击复选框方法
    function onclickBox(objs)
    {
        if(objs.disabled)
		{
			objs.checked=false;
		}
    }
    //清空提示
    function clearData(){ 

        document.getElementById('cntstatus').value="";
        document.getElementById('contenttype').value="";
        document.getElementById('platform').value="";
        document.getElementById('namecn').value="";
        document.getElementById('contentid').value="";
        
        dynChgSelectWidth(document.all.contenttype.id);
    	dynChgSelectWidth(document.all.platform.id);
    	dynChgSelectWidth(document.all.cntstatus.id);
    	
      document.getElementById("endDate2").value="";
      document.getElementById("endDate1").value="";
      document.getElementById('startDate1').value="";
	  document.getElementById('startDate2').value="";
	  document.getElementById('createtime1').value="";
	  document.getElementById('createtime2').value="";
	  document.getElementById('startTime_msg').innerText="";
	  document.getElementById('endTime_msg').innerText="";
	  document.getElementById('startendTime_msg').innerText="";
	  document.getElementById('startime_msg').innerText="";
	  

	  	
    }
     
    
    //查询方法
    function proxyfunc(){
 
    
      document.getElementById('startTime_msg').innerText="";
	  document.getElementById('endTime_msg').innerText="";
	  document.getElementById('startendTime_msg').innerText="";
	  document.getElementById('startime_msg').innerText="";
	  
	   //查询时对于入库时间段的判断
	  	var error3 = true;
	    var createtimeStartTime = document.getElementById('createtime1');
	    var createtimeEndTime = document.getElementById('createtime2');
	    error = beginTimeLEndTime(createtimeStartTime,createtimeEndTime,'startime_msg');
		if(error == false)
		{
		  return;
		}
		
        //查询时对于开始时间段的判断
	    var error = true;
	    var startTimeObj = document.getElementById('startDate1');
	    var endTimeObj = document.getElementById('startDate2');
	    error = beginTimeLEndTime(startTimeObj,endTimeObj,'startTime_msg');
		if(error == false)
		{
		  return;
		}
	
	  
		
		//查询时对于结束时间段的判断
	    var error2 = true;
	    
	    var startTimeObj2 = document.getElementById('endDate1');
	    var endTimeObj2 = document.getElementById('endDate2');
	    
	    error2 = beginTimeLEndTime(startTimeObj2,endTimeObj2,'startendTime_msg');
	    if(error2 == false)
		{
		  return;
		}
		
		//判断开始时间的最小值是否小于结束时间的最大值
		
		error2 = startSmallestEndBiggest(startTimeObj,endTimeObj2,'endTime_msg');
		if(error2 == false)
		{
		  return;
		}
		

	  	
        searchTableData('queryCntList','$M.obj','zteTable');
    }
     
     
   //日期校验,所选日期应早于当前系统日期时间
    function validatedate(dateid,datetarget,date_msg){
             var date=formatDBDate(document.getElementById(datetarget).value,14);
             var nowdate=getTimefromUTC("yyyy-mm-dd hh:mm:ss", new Date());
             var date1=formatDBDate(nowdate,14);
             if((date-date1)<0){
             	$("#"+date_msg).html($res_entry("msg.info.cast.23"));
             	dateid.onfocus();
             	return ;
             }else{
             	$("#"+date_msg).html("");
             }
            }
    
    //开始时间必须小于结束时间      
   function betweenTimeValidate(dateid,startTimeTarget,endTimeTarget,date_msg){
    startTimeTarget=document.getElementById(startTimeTarget).value;
    endTimeTarget=document.getElementById(endTimeTarget).value;
     if(startTimeTarget!="" && endTimeTarget!=""){
        var startTime=formatDBDate(startTimeTarget,14);
     	var endTime=formatDBDate(endTimeTarget,14);
     	if((startTime-endTime)>=0){
	     	$("#"+date_msg).html($res_entry("msg.info.channel.007"));
	     	return 0;
	    }
     }
    }
           
             
  //验证内容简介输入框中字数是否超过最大长度        
 function IsMaxLength2(input,input_msg,len){

  if(utf8_strlen2(trim(input.value)) > len){
      document.getElementById(input_msg).innerText = $res_entry("input.system.maxlength.suggest.13")+len+$res_entry("input.system.maxlength.suggest.15");//"不能超过"+len+ "个字符";
      document.getElementById(input.id).focus();
      return false;
  }else{
       document.getElementById(input_msg).innerText = "";   
       return true;   
  }
}
    
  //设置上映年份  
 function setReleasedate(){
 	
 	var releasedateTime=document.getElementById("releasedateTime") ;
 	releasedateTime.length=0;
 	releasedateTime.options.add(new Option($res_entry("label.selectdefault"),""));
 	for(var i=1900;i<=2050;i++){
 		releasedateTime.options.add(new Option(i,i));
 	}
 }  
           
      /*******************************************************************************************************************************/
    
     
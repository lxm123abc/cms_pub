package com.zte.cms.egptemplt.service;

import java.util.List;

import com.zte.cms.egptemplt.dao.IEpgtempltDAO;
import com.zte.cms.egptemplt.model.Epgtemplt;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;
import com.zte.ssb.framework.exception.exceptions.DAOException;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.SSBBus;
import com.zte.umap.common.dynamicobj.DynamicObjectBaseDS;

public class EpgtempltDS extends DynamicObjectBaseDS implements IEpgtempltDS
{
    // 日志
    private Log log = SSBBus.getLog(getClass());

    private IEpgtempltDAO dao = null;

    public void setDao(IEpgtempltDAO dao)
    {
        this.dao = dao;
    }

    public void insertEpgtemplt(Epgtemplt epgtemplt) throws DomainServiceException
    {
        log.debug("insert epgtemplt starting...");
        try
        {
            Long templateIndex = (Long) this.getPrimaryKeyGenerator().getPrimarykey("cms_epgtemplt_seq");
            epgtemplt.setTempltindex(templateIndex);
            epgtemplt.setTempltid(templateIndex.toString());//改为不从界面输入
            dao.insertEpgtemplt(epgtemplt);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("insert epgtemplt end");
    }

    public void updateEpgtemplt(Epgtemplt epgtemplt) throws DomainServiceException
    {
        log.debug("update epgtemplt by pk starting...");
        try
        {
            dao.updateEpgtemplt(epgtemplt);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("update epgtemplt by pk end");
    }

    public void updateEpgtempltList(List<Epgtemplt> epgtempltList) throws DomainServiceException
    {
        log.debug("update epgtempltList by pk starting...");
        if (null == epgtempltList || epgtempltList.size() == 0)
        {
            log.debug("there is no datas in epgtempltList");
            return;
        }
        try
        {
            for (Epgtemplt epgtemplt : epgtempltList)
            {
                dao.updateEpgtemplt(epgtemplt);
            }
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("update epgtempltList by pk end");
    }

    public void removeEpgtemplt(Epgtemplt epgtemplt) throws DomainServiceException
    {
        log.debug("remove epgtemplt by pk starting...");
        try
        {
            dao.deleteEpgtemplt(epgtemplt);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("remove epgtemplt by pk end");
    }

    public void removeEpgtempltList(List<Epgtemplt> epgtempltList) throws DomainServiceException
    {
        log.debug("remove epgtempltList by pk starting...");
        if (null == epgtempltList || epgtempltList.size() == 0)
        {
            log.debug("there is no datas in epgtempltList");
            return;
        }
        try
        {
            for (Epgtemplt epgtemplt : epgtempltList)
            {
                dao.deleteEpgtemplt(epgtemplt);
            }
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("remove epgtempltList by pk end");
    }

    public Epgtemplt getEpgtemplt(Epgtemplt epgtemplt) throws DomainServiceException
    {
        log.debug("get epgtemplt by pk starting...");
        Epgtemplt rsObj = null;
        try
        {
            rsObj = dao.getEpgtemplt(epgtemplt);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("get epgtempltList by pk end");
        return rsObj;
    }

    public List<Epgtemplt> getEpgtempltByCond(Epgtemplt epgtemplt) throws DomainServiceException
    {
        log.debug("get epgtemplt by condition starting...");
        List<Epgtemplt> rsList = null;
        try
        {
            rsList = dao.getEpgtempltByCond(epgtemplt);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("get epgtemplt by condition end");
        return rsList;
    }

    @SuppressWarnings("unchecked")
    public TableDataInfo pageInfoQuery(Epgtemplt epgtemplt, int start, int pageSize) throws DomainServiceException
    {
        log.debug("get epgtemplt page info by condition starting...");
        PageInfo pageInfo = null;
        try
        {
            pageInfo = dao.pageInfoQuery(epgtemplt, start, pageSize);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        TableDataInfo tableInfo = new TableDataInfo();
        tableInfo.setData((List<Epgtemplt>) pageInfo.getResult());
        tableInfo.setTotalCount((int) pageInfo.getTotalCount());
        log.debug("get epgtemplt page info by condition end");
        return tableInfo;
    }

    @SuppressWarnings("unchecked")
    public TableDataInfo pageInfoQuery(Epgtemplt epgtemplt, int start, int pageSize, PageUtilEntity puEntity)
            throws DomainServiceException
    {
        log.debug("get epgtemplt page info by condition starting...");
        PageInfo pageInfo = null;
        try
        {
            pageInfo = dao.pageInfoQuery(epgtemplt, start, pageSize, puEntity);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        TableDataInfo tableInfo = new TableDataInfo();
        tableInfo.setData((List<Epgtemplt>) pageInfo.getResult());
        tableInfo.setTotalCount((int) pageInfo.getTotalCount());
        log.debug("get epgtemplt page info by condition end");
        return tableInfo;
    }
}

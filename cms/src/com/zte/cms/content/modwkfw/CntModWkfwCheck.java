package com.zte.cms.content.modwkfw;

import java.util.List;

import com.zte.cms.content.common.CntConstants;
import com.zte.cms.content.movie.model.CmsMovie;
import com.zte.cms.content.movie.service.ICmsMovieDS;
import com.zte.cms.content.program.model.CmsProgram;
import com.zte.cms.content.program.model.CmsProgramWkfwhis;
import com.zte.cms.content.program.service.ICmsProgramWkfwhisDS;
import com.zte.ssb.framework.SSBBus;
import com.zte.ssb.framework.common.log.Log;
import com.zte.umap.common.ResourceMgt;

public class CntModWkfwCheck implements ICntModWkfwCheck
{
    private Log log = SSBBus.getLog(getClass());
    private ICmsMovieDS cmsMovieDS = null;
    private ICmsProgramWkfwhisDS cmsProgramWkfwhisDS = null;

    public String checkCntModwkfwApply(CmsProgram cmsProgram) throws Exception
    {
        // 判断内容是否存在
        if (null == cmsProgram)
        {// 内容不存在
            log.error("errorCode=" + CntConstants.CNT_NOT_EXIST
                    + " : content does not exist, cannot submit it for audit");
            return CntConstants.CNT_NOT_EXIST;
        }

        // 判断内容状态是否正确 以下状态可以发起修改工作流0：待发布 20：新增同步成功 50：修改同步成功 60：修改同步失败 80：取消同步成功
        if ((0 != cmsProgram.getStatus().intValue()) && (20 != cmsProgram.getStatus().intValue())
                && (50 != cmsProgram.getStatus().intValue()) && (60 != cmsProgram.getStatus().intValue())
                && (80 != cmsProgram.getStatus().intValue()))
        {// 内容状态不正确
            log.error("errorCode=" + CntConstants.CNT_STATUS_ERROR
                    + " : content status error, cannot submit it for audit");
            return CntConstants.CNT_STATUS_ERROR;
        }

        // 获取内容的所有子内容
        List<CmsMovie> cmsMovieList = null;
        CmsMovie cmsMovie = new CmsMovie();
        cmsMovie.setProgramid(cmsProgram.getProgramid());
        cmsMovieList = cmsMovieDS.getCmsMovieByCond(cmsMovie);
        // 判断内容下是否存在子内容
        if (null == cmsMovieList || 0 == cmsMovieList.size())
        {// 内容没有子内容,不能提审 250006
            log.error("errorCode=" + CntConstants.CNT_HASNO_SUBCNT
                    + " : content has no subcontents, cannot submit it for audit");
            return CntConstants.CNT_HASNO_SUBCNT;
        }

        // 判断内容的子内容状态是否符合发起工作流条件，此处暂时省略

        // 判断内容已被提审 所处工作流是否正确
        if ((0 != cmsProgram.getWorkflow()))
        {// 判断内容已被提交修改申请 "250201"
            log.error("errorCode=" + CntConstants.CNT_WORKFLOW_ERROR
                    + " : content is not in the right workflow, cannot submit it for audit");
            return "250201"; // CntConstants.CNT_WORKFLOW_ERROR;
        }

        log.debug("errorCode=" + CntConstants.CNT_SUCCESS + ": content apply wkfw check success");
        return CntConstants.CNT_SUCCESS; // 内容可以提交审核

    }

    public String checkCntModwkfwAudit(CmsProgram cmsProgram, String processid, String nodeName) throws Exception
    {
        if (null == cmsProgram)
        {// 内容不存在
            log.error("errorCode=" + CntConstants.CNT_NOT_EXIST + " : content does not exist, cannot audit");
            return CntConstants.CNT_NOT_EXIST;
        }

        if (!((CntConstants.CNT_STATUS_ONEAUDITING == cmsProgram.getStatus() && 
        		ResourceMgt.findDefaultText("cnt.first.audit").equals(nodeName))
                || (CntConstants.CNT_STATUS_TWOAUDITING == cmsProgram.getStatus() && 
                		ResourceMgt.findDefaultText("cnt.second.audit").equals(nodeName)) || (CntConstants.CNT_STATUS_THREEAUDITING == cmsProgram
                .getStatus() && 
                ResourceMgt.findDefaultText("cnt.third.audit").equals(nodeName))))
        {
            log.error("errorCode=" + CntConstants.CNT_STATUS_ERROR + " : content status error, cannot audit");
            return CntConstants.CNT_STATUS_ERROR;
        }

        // 判断内容是否处于工作流中
        if (2 != cmsProgram.getWorkflow())
        {// 内容尚未被提交修改申请或审核流程已被处理完成 250205
            log.error("errorCode=" + CntConstants.CNT_AUDITWKFW_ERROR + " : content audit worflow error, cannot audit");
            return "250205"; // CntConstants.CNT_AUDITWKFW_ERROR;
        }

        // 判断当前审核流程是否被其他操作员操作过
        CmsProgramWkfwhis cmsProgramWkfwhis = new CmsProgramWkfwhis();
        List<CmsProgramWkfwhis> cmsProgramWkfwhisList = null;
        cmsProgramWkfwhis.setNodename(nodeName);
        cmsProgramWkfwhis.setProgramid(cmsProgram.getProgramid());
        cmsProgramWkfwhis.setWorkflowindex(Long.parseLong(processid));
        cmsProgramWkfwhisList = cmsProgramWkfwhisDS.getCmsProgramWkfwhisByCond(cmsProgramWkfwhis);
        if (null != cmsProgramWkfwhisList && cmsProgramWkfwhisList.size() > 0)
        {
            log.error("errorCode=" + CntConstants.CNT_HASBEEN_AUDITED + " : content has been audited, cannot audit");
            return CntConstants.CNT_HASBEEN_AUDITED;
        }

        log.debug("errorCode=" + CntConstants.CNT_SUCCESS + ": content aduit wkfw check success");
        return CntConstants.CNT_SUCCESS; // 内容可以审核
    }

    public void setCmsMovieDS(ICmsMovieDS cmsMovieDS)
    {
        this.cmsMovieDS = cmsMovieDS;
    }

    public void setCmsProgramWkfwhisDS(ICmsProgramWkfwhisDS cmsProgramWkfwhisDS)
    {
        this.cmsProgramWkfwhisDS = cmsProgramWkfwhisDS;
    }

}

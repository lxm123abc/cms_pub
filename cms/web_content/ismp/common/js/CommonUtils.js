
/**
 * 计算UTF8字符串占用字节数
 * @param str  要计算长度的字符串
 * @return    字符串的字节长度
 */
function utf8_strlen2(str) 
{ 
	var cnt = 0; 
	for( i=0; i<str.length; i++) 
	{ 
	    var value = str.charCodeAt(i); 
	    if( value < 0x080) 
	    { 
	        cnt += 1; 
	    } 
	    else if( value < 0x0800) 
	    { 
	        cnt += 2; 
	    } 
	    else  if(value < 0x010000)
	    { 
	        cnt += 3; 
	    }
		else
		{
			cnt += 4;
		}
	} 
	return cnt; 
}
/**
 * 检验电话号码,包含（ 数字、字母、-）且长度不大于20
 * @param str     电话号码
 * @return        true 符合条件， false 符合条件
 */
function isMobile(str)
{
	if(str.length > 20)
	{
		return false;
	}
	else
	{
		var regex=/^[0-9a-zA-Z\-]*$/;
		return regex.test(str);
	}
}
/**
 * 检验邮编 ,包含（ 数字、字母、-） 且长度不大于20
 * @param str  邮政编码
 * @return     true  为正确的邮政编码， false  不符合邮政编规则
 */
function isZipCode(str)
{
	if(str.length > 20)
	{
		return false;
	}
	else
	{
		var regex=/^[0-9a-zA-Z\-]*$/;
		return regex.test(str);
	}
}
/**
 * 在做提交前清空所有的提示信息
 * @return
 */
function clearAllMsgLabel()
{
	$("label[@id$='_msg']").html("");
}

/*
function dynChgSelectWidth(id)
{
	var selectObj = document.getElementById(id);
	if(selectObj != undefined && selectObj != null)
	{
		var v  = selectObj.options[selectObj.selectedIndex].text;
		var width=7*utf8_strlen2(v);
		width = (width<130 ? 130 : width);
		selectObj.style.width=width;
	}
}
*/

/**
 * 动态改变select 框的长度
 * @param id   下拉框id
 * @return
 */
function dynChgSelectWidth(id)
{
	var selectObj = document.getElementById(id);
	if(selectObj != undefined && selectObj != null)
	{
		var v  = selectObj.options[selectObj.selectedIndex].text;
		var width = getCharWidth(v);
		width = (width<130 ? 130 : width);
		selectObj.style.width=width;
	}
}

function getCharWidth(str) 
{ 
	var width = 0; 
	var type2 = "\'";
	var type3 = "fijltI! |[]\\:;,./";
	var type4 = "r`()-{}\"";
	var type5 = "vxyz^*";
	var type6 = "ckJ";
	var type7 = "abdeghnopqsu0123456789AFLTVXYZ~#$_+=>?";
	var type8 = "BEKPS&";
	var type9 = "wCDGHMNOQRU";
	var type11 = "mW%";
	var type12 = "@";
	for( i=0; i<str.length; i++) 
	{ 
		var value = str.charCodeAt(i); 
		if( value < 0x080) 
		{ 
			if(type2.indexOf(str.charAt(i)) != -1)
			{
				width += 2; 
			}
			else if(type3.indexOf(str.charAt(i)) != -1)
			{
				width += 3; 
			}
			else if(type4.indexOf(str.charAt(i)) != -1)
			{
				width += 4; 
			}
			else if(type5.indexOf(str.charAt(i)) != -1)
			{
				width += 5; 
			}
			else if(type6.indexOf(str.charAt(i)) != -1)
			{
				width += 6; 
			}
			else if(type7.indexOf(str.charAt(i)) != -1)
			{
				width += 7; 
			}
			else if(type8.indexOf(str.charAt(i)) != -1)
			{
				width += 8; 
			}
			else if(type9.indexOf(str.charAt(i)) != -1)
			{
				width += 9; 
			}
			else if(type11.indexOf(str.charAt(i)) != -1)
			{
				width += 11; 
			}
			else if(type12.indexOf(str.charAt(i)) != -1)
			{
				width += 12; 
			}
			else
			{
				width += 12; 
			}
		} 
		else 
		{ 
			width += 12; 
		}
	} 
	width += 28;
	return width; 
}	

/**
 * 去掉字符串前后的空格
 * @param str  要去掉前后空格的字符串
 * @return     去掉前后空格的字符串
 */
function strTrim(str) 
{
	str = str + " ";
    var first = 0;
    var last = str.length - 1;
    while (str.charAt(first) == " ") { first++; }
    while (str.charAt(last) == " ") { last--; }
	if (first > last) return "";
    return str.substring(first, last + 1);
}

/**
 * 判断输入字符串是否为合法的时间格式(hh:mm:ss)
 * @param str
 * @return
 */
function isTime(str)
{
	var a = str.match(/^(\d{1,2})(:)?(\d{1,2})\2(\d{1,2})$/);
   	if (a == null) {return false;}
   	if (a[1]>=24 || a[3]>=60 || a[4]>=60)
    {
    	return false
     }
     return true;
}

/**
 * 判断字符串是否为null或者 ""(空字符串)
 * @param str     
 * @return  true  字符串为空， false  不为空
 */
function is_empty(str)
{
	if (str == null || str == "") return true;
	if (strTrim(str) == "") return true;
	return false;
}

/**
 * 判断输入字符串是否为合法的日期格式(YYYY-MM-DD)
 * @param str
 * @return
 */
function isDate(str)
{
 	var r = str.match(/^(\d{1,4})(-)(\d{1,2})\2(\d{1,2})$/);
   	if(r==null)return false;
    var d= new Date(r[1], r[3]-1, r[4]);
    return (d.getFullYear()==r[1]&&(d.getMonth()+1)==r[3]&&d.getDate()==r[4]);
}

/**
 * 判断输入字符串是否为合法的日期格式(YYYY-MM-DD HH:MM:SS)(2003-12-05 13:04:06)
 * @param str
 * @return
 */
function isDateTime(str)
{
	var reg = /^(\d{1,4})(-|\/)(\d{1,2})\2(\d{1,2}) (\d{1,2}):(\d{1,2}):(\d{1,2})$/;
    var r = str.match(reg);
    if(r==null)return false;
    var d= new Date(r[1], r[3]-1,r[4],r[5],r[6],r[7]);
    return (d.getFullYear()==r[1]&&(d.getMonth()+1)==r[3]&&d.getDate()==r[4]&&d.getHours()==r[5]&&d.getMinutes()==r[6]&&d.getSeconds()==r[7]);
}
/**
 * 判断待测字符串sCheck是否包含中文字符
 * 
 */
function hasChineseChars(sCheck)
{
	for (nIndex=0; nIndex<sCheck.length; nIndex++)
	{
	   cCheck = sCheck.charCodeAt(nIndex);
	      if (cCheck > 255)
	         return true;
	}
	return false;
}

/**
 *  判断待测字符串sCheck是否包含空格
 *
 */
function hasWhitespace (sCheck)
{
	var whitespace = " \t\n\r";
	var i;
	for (i = 0; i < sCheck.length; i++){
	  var c = sCheck.charAt(i);
	  if (whitespace.indexOf(c) >= 0){
	    return true;
	  }
	}
	return false;
}

/**
 * 检查待测字符串中的字符是否全是合法字符
 * @param sCheck     待测字符串
 * @param ValidChars 指定有效字符集合，如"0123456789;*"
 * @return    true   是
 *            false  否
 */
function isValidChars(sCheck,ValidChars)
{
	 var i,j;
	 if (sCheck.length== 0)
	     return false
	 for (i=0;i<sCheck.length;i++)
	 {
	    j = ValidChars.indexOf(sCheck.charAt(i));
	    if (j==-1)
	      return false;
	 }
	 return true;
}
/**
 * 检查字符串是否全部为数字
 * @param sCheck
 * @return   true  全为数字 
 *           false  不为数字
 */
function checkNum(sCheck)
{

    var regexp= /^\d+$/; 

    var reg = sCheck.match(regexp); 

    if(reg==null){

        return false;

    }

    return true;
}
/**
 * 检查字符串是否全部为数字或者为空字符串
 * @param sCheck
 * @return
 */
function checkNumORNull(sCheck){

    if (strTrim(sCheck) == "") {

        return true;

    }
    return checkNum(sCheck);
}

/**
 * 检验字符串是否为IP地址
 * @param ipaddr
 * @return   true 是IP地址， false 不是IP地址
 */
function checkIP(ipaddr)
{
    if(ipaddr!=""&&ipaddr!=null){
      var reg1=/^(((2[0-4]\d)|(25[0-5])|(1\d{2})|([1-9]\d)|(\d))\.((2[0-4]\d)|(25[0-5])|(1\d{2})|([1-9]\d)|(\d))\.((2[0-4]\d)|(25[0-5])|(1\d{2})|([1-9]\d)|(\d))\.((2[0-4]\d)|(25[0-5])|(1\d{2})|([1-9]\d)|(\d)))$/;
      return reg1.test(ipaddr);
    }
 }

/**
 * 给IP地址补位，不够3位的前面补0 。如 10.129.2.120 补位后 010.129.002.120
 * @param ipaddr
 * @return
 */
function convertIP(ipaddr)
{
    var str = ipaddr.split(".");
    for (var i=0;i<str.length;i++){
       if (str[i].length == 1)
       {
           str[i] = "0" + "0" + str[i];
       }
       else if (str[i].length == 2)
       {
            str[i] = "0" + str[i];
       }
    }
    
    var str1 = str[0]+"."+str[1]+"."+str[2]+"."+str[3];
    return str1;
}
/**
 * IP地址比较大小
 * 
 * @param ipaddr1
 * @param ipaddr2
 * @return  1  ipaddr1 > ipaddr2  
 *          0  ipaddr1 = ipaddr2 
 *         -1  ipaddr1 < ipaddr2  
 */
function compareToIpaddr(ipaddr1,ipaddr2)
{
	if(convertIP(ipaddr1) > convertIP(ipaddr2))
	{
		return 1;
	}
	if(convertIP(ipaddr1) == convertIP(ipaddr2))
	{
		return 0;
	}
	if(convertIP(ipaddr1) < convertIP(ipaddr2))
	{
		return -1;
	}
}
/**
 * 清空input  输入框 
 * @param id  input控件的id
 * @return
 */
function cleanInput(id)
{
	document.getElementById(id).value="";
}

/**
 * 选择下拉框的第一个值，如果下拉框没有值，设置为空
 * @param id      下拉框的id
 * @return
 */
function cleanSelect(id)
{
	var selObj = document.getElementById(id);
	if(selObj.selectedIndex)
	{
		selObj.selectedIndex=0;
	}
	else
	{
	    selObj.value="";
	}
}

/**
 * 字符串长度比较，当字符串长度大于指定的长度时，返回true
 * @param inputStr      要比较的字符串
 * @param length        指定的长度
 * @return              true    字符串长度大于指定的长度
 */
function compareStrLength(inputStr, length)
{
	if(inputStr != undefined && inputStr != null)
	{
		return utf8_strlen2(strTrim(inputStr)) > length;
	}
	else
	{
		return false;
	}
}

/**
 * 二进制转十进制
 * @param binaryStr
 * @return
 */
function binaryToDecimal(binaryStr)
{
	var binaryInt = parseInt(binaryStr,2);
	return binaryInt.toString(10);
}
/**
 * 十进制转二进制
 * @param decimalStr
 * @return
 */
function decimalToBinary(decimalStr)
{
	var decimalInt = parseInt(decimalStr);
	return decimalInt.toString(2);
}
/**
 * 检验url格式
 * @param url
 * @return
 */
function checkCommonURL(url)
{
	var myReg = /^http:\/\//;
  	return myReg.test(url.toLowerCase());
}
/**
 * 检验ftp格式
 * @param ftpurl
 * @return
 */
function checkCommonftp(ftpurl)
{
	var myReg = /^ftp:\/\//;
  	return myReg.test(ftpurl.toLowerCase());
}

/**
 * 判断字符串是否由数字或字母组成
 * @param str
 * @return 
 */
function isNumOrChar(str){
    var pattern = /[^a-zA-Z0-9]/;
	if(pattern.test(str)){
		return false;
	}else{
		return true;
	}
}

/**
 * 日期格式转换成字符串,如：('2009-10-02 10:20:02'--->20091002102002)
 * @param str
 * @param len  返回的字符串包含的长度，如果没有指定该参数，则为最大长度
 * @return
 */
function formatDBDate(str, len) 
{
    str = str.replace(/:/g,"");<!-- replace :     -->
    str = str.replace(/ /g,"");<!-- replace blank -->
    str = str.replace(/-/g,"");<!-- replace -     -->
    if (str == "") {
        str = "00000000000000";
    }
    return str.substr(0, len);
}
/**
 * 判断字符串是否包含以下特殊字符：%'/\:*&?"<>|
 * @param s
 * @return
 */
function hasInvalidChar1(s) 
{
	var invalid = '%\'/\\:*&?"<>|';
	for (i = 0; i < invalid.length; i++)
	{
		o = invalid.charAt(i);
		if (s.indexOf(o) >= 0)
		{
			return true;
		}
	}
	return false;	
}
/**
 * 检验字符串中是否包含用户指定特殊字符
 * @param checkstr         特殊字符集
 * @param userinput        待检测字符串
 * @return    true          包含
 *            false         不包含
 */
function checkstring(checkstr,userinput)
{
	var allValid = false;	
	for (i = 0;i<userinput.length;i++) {
	    ch = userinput.charAt(i);
	    if(checkstr.indexOf(ch) >= 0) {
	        allValid = true;
	        break;
	      }
	  }
	 return allValid;
}

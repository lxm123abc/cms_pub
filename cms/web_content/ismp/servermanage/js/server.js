//校验服务器基本信息
function checkServerInfo(){   
   var servername = document.getElementById("servername");
   if (checkNull(servername.value)) {
      document.getElementById("servername_msg").innerHTML= $res_entry("msg.info.server.item.cannot.null"); //此项不能为空
      servername.focus();    
      return false;
    }
    
    if (CheckSpecialString(servername,"servername_msg") == false){//此项不能包含这些字符 $%'/\\:*&?\"<>|
        return false;
    } 
    if(checkMaxLength(servername,100,"servername_msg") == false){//此项的字符个数超过最大长度限制：100
        return false;
    }
   
   var inipaddress = document.getElementById("inipaddress");
   if (checkNull(inipaddress.value)) {
      document.getElementById("inipaddress_msg").innerHTML= $res_entry("msg.info.server.item.cannot.null"); //此项不能为空
      inipaddress.focus();
      return false;
    }   
   if (checkIpFormat(inipaddress, "inipaddress_msg") == false) {
      return false;
    }
    
   var outipaddress =  document.getElementById("outipaddress");
   if (checkNull(outipaddress.value)) {
      document.getElementById("outipaddress_msg").innerHTML= $res_entry("msg.info.server.item.cannot.null"); //此项不能为空
      outipaddress.focus();
      return false;
    }  
    if (checkIpFormat(outipaddress, "outipaddress_msg") == false){ 
      return false;

    }
    
    var ftpaccount = document.getElementById("ftpaccount");
    if (checkNull(ftpaccount.value)){
        document.getElementById("ftpaccount_msg").innerHTML= $res_entry("msg.info.server.item.cannot.null"); //"请输入用户名";
        ftpaccount.focus();
        return false;
    }
    if(checkMaxLength(ftpaccount,100,"ftpaccount_msg") == false){
        return false;
    }
    
    var ftppassword = document.getElementById("ftppassword");
    if (ftppassword != null && trim(ftppassword.value) == ""){
        document.getElementById("ftppassword_msg").innerHTML= $res_entry("msg.info.server.item.cannot.null"); //"请输入密码";
        ftppassword.focus();
        return false;
    }
    var exp = new RegExp("[A-Za-z0-9-_]");
    var password = ftppassword.value;
    for (var i = 0 ; i < password.length ; i++){
        var reg = password.charAt(i).match(exp);
        if (reg == null) {
           document.getElementById("ftppassword_msg").innerHTML=$res_entry("msg.info.server.ftppassword.invalid");
           ftppassword.focus();
           return false;
        }
    }
    if (checkMaxLength(ftppassword,100,"ftppassword_msg") == false){
        return false;
    }
    
    var ftpport = document.getElementById("ftpport");
    if (checkNull(ftpport.value)) {
       document.getElementById("ftpport_msg").innerHTML= $res_entry("msg.info.server.item.cannot.null"); //此项不能为空
       ftpport.focus();
       return false;
    }
    if (!checkPort(ftpport.value)){
        document.getElementById("ftpport_msg").innerHTML= $res_entry("msg.info.server.ftpport.invalid"); //请输入正确格式的端口号
        document.getElementById("ftpport").focus();
        return false;        
    }
    if(checkMaxLength(ftpport,100,"ftpport_msg") == false){
        return false;
    }
    
    var ftppath = document.getElementById("ftppath");
    if (checkNull(ftppath.value)){
        document.getElementById("ftppath_msg").innerHTML= $res_entry("msg.info.server.item.cannot.null"); //"请输入文件存储路径";
        ftppath.focus();
        return false;
    }
    if(checkMaxLength(ftppath,100,"ftppath_msg") == false){
        return false;
    }    
    if(!checkPath(ftppath.value)){
        document.getElementById("ftppath_msg").innerHTML= $res_entry("msg.info.server.ftppath.invalid"); //文件存储路径必须开头和结尾都使用反斜杠'/'
        document.getElementById("ftppath").focus();
        return false;
    }
    
    var description = document.getElementById("description");
    if(!checkMaxLength(description,255,"description_msg")){
    	return false;
	}
    return true;
}

    function checkMaxLength(input,len,input_msg){	
        if (utf8_strlen2(trim(input.value)) > len){
            document.getElementById(input_msg).innerText = $res_entry("msg.info.server.item.toolong")+len;//"è¶è¿æå¤§é¿åº¦éå¶:";
            document.getElementById(input.id).focus();
            return false;
        }else{
           document.getElementById(input_msg).innerText = "";   
           return true;   
        }
    }

function checkIpFormat(input,input_msg){
  if(trim(input.value) != '' && input.value != null){
    var aa = input.value.split(";");
    for(var i=0; i<aa.length; i++){
     var exp=/^(\d{1,2}|1\d\d|2[0-4]\d|25[0-5])\.(\d{1,2}|1\d\d|2[0-4]\d|25[0-5])\.(\d{1,2}|1\d\d|2[0-4]\d|25[0-5])\.(\d{1,2}|1\d\d|2[0-4]\d|25[0-5])$/;
     var reg = aa[i].match(exp);
     if(reg == null){
        document.getElementById(input_msg).innerText = $res_entry("msg.info.server.ipformat.invalid"); //"IPå°åæ ¼å¼ä¸æ­£ç¡®";
        document.getElementById(input.id).focus();
        return false;         
     }else{
        document.getElementById(input_msg).innerText = ""; 
    }   
    }
   return true; 
  }
}
    function checkPath(path){
        var first = path.charAt(0);
        var last = path.charAt(path.length-1);
        if (first!='/'){
           return false;
        }
        if (last!='/'){
            return false;
        }
        if (path.indexOf('//')!=-1){
            return false;
        }
        return true;
    } 

    function checkPort(port){   
        var newPar=/^\d+$/;   
        return newPar.test(port);
    }
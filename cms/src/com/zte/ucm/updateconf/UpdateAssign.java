package com.zte.ucm.updateconf;

import com.zte.ucm.Constants;
import com.zte.ucm.Resultlog;
import com.zte.ucm.config.Config;
import com.zte.ucm.config.ConfigItem;
import com.zte.ucm.config.ConfigParser;
import com.zte.ucm.config.XMLParser;
import com.zte.ucm.log.Logger;
import com.zte.ucm.log.LoggerFactory;
import com.zte.ucm.util.FileUtil;

import java.io.PrintStream;
import java.util.List;

public class UpdateAssign
{
  private static Logger log = LoggerFactory.getLogger(UpdateAssign.class);

  public void exe()
  {
    try
    {
      log.info("Operation: modify the config items");
      Object localObject1 = Constants.CONFIGAPPLYITEMSXML;

      XMLParser localXMLParser = new XMLParser();
      List<ConfigItem> localList1 = localXMLParser.parserXml((String)localObject1);

      log.info("Start to modify the configs...");
      for (ConfigItem localConfigItem : localList1)
      {
        ConfigParser localConfigParser = new ConfigParser();
        List<Config> localList2 = localConfigParser.getConfig(localConfigItem);
        for (Config localConfig : localList2)
        {
          new BackupConfigFiles().exe(localConfig);
          Object localObject2;
          if (FileUtil.isXml(localConfig.getFile()))
          {
            localObject2 = new ConfigModifier();
            ((ConfigModifier)localObject2).modifyXMLConfig(localConfig);
          }
          else
          {
            localObject2 = new TXTModifier();
            ((TXTModifier)localObject2).modifyTXTConfig(localConfig);
          }
        }
      }

      if (Resultlog.isSucc())
      {
        log.info("The configs have been modified successfully.");
        System.out.println("*#06#");
      }
      else {
        new RecoverConfigFiles().exe();
      }

      localObject1 = new Resultlog();
      ((Resultlog)localObject1).resultLog();
    }
    catch (Exception localException)
    {
      log.error("config modified failure");
      localException.printStackTrace();
    }
    finally
    {
      Resultlog localResultlog1;
      Resultlog localResultlog2 = new Resultlog();
      localResultlog2.resultLog();
    }
  }
}

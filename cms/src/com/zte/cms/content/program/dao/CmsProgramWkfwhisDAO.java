package com.zte.cms.content.program.dao;

import java.util.List;

import com.zte.cms.content.program.dao.ICmsProgramWkfwhisDAO;
import com.zte.cms.content.program.model.CmsProgramWkfwhis;
import com.zte.ssb.dynamicobj.DynamicObjectBaseDao;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DAOException;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.SSBBus;

public class CmsProgramWkfwhisDAO extends DynamicObjectBaseDao implements ICmsProgramWkfwhisDAO
{
    // 日志
    private Log log = SSBBus.getLog(getClass());

    public void insertCmsProgramWkfwhis(CmsProgramWkfwhis cmsProgramWkfwhis) throws DAOException
    {
        log.debug("insert cmsProgramWkfwhis starting...");
        super.insert("insertCmsProgramWkfwhis", cmsProgramWkfwhis);
        log.debug("insert cmsProgramWkfwhis end");
    }

    public void insertCmsProgramWkfwhisList(List<CmsProgramWkfwhis> cmsProgramWkfwhisList) throws DAOException
    {
        log.debug("insert cmsProgramWkfwhisList starting...");
        if (null != cmsProgramWkfwhisList)
        {
            super.insertBatch("insertCmsProgramWkfwhis", cmsProgramWkfwhisList);
        }
        log.debug("insert cmsProgramWkfwhisList end");
    }

    public void updateCmsProgramWkfwhis(CmsProgramWkfwhis cmsProgramWkfwhis) throws DAOException
    {
        log.debug("update cmsProgramWkfwhis by pk starting...");
        super.update("updateCmsProgramWkfwhis", cmsProgramWkfwhis);
        log.debug("update cmsProgramWkfwhis by pk end");
    }

    public void updateCmsProgramWkfwhisList(List<CmsProgramWkfwhis> cmsProgramWkfwhisList) throws DAOException
    {
        log.debug("update cmsProgramWkfwhisList by pk starting...");
        super.updateBatch("updateCmsProgramWkfwhis", cmsProgramWkfwhisList);
        log.debug("update cmsProgramWkfwhisList by pk end");
    }

    public void updateCmsProgramWkfwhisByCond(CmsProgramWkfwhis cmsProgramWkfwhis) throws DAOException
    {
        log.debug("update cmsProgramWkfwhis by conditions starting...");
        super.update("updateCmsProgramWkfwhisByCond", cmsProgramWkfwhis);
        log.debug("update cmsProgramWkfwhis by conditions end");
    }

    public void updateCmsProgramWkfwhisListByCond(List<CmsProgramWkfwhis> cmsProgramWkfwhisList) throws DAOException
    {
        log.debug("update cmsProgramWkfwhisList by conditions starting...");
        super.updateBatch("updateCmsProgramWkfwhisByCond", cmsProgramWkfwhisList);
        log.debug("update cmsProgramWkfwhisList by conditions end");
    }

    public void deleteCmsProgramWkfwhis(CmsProgramWkfwhis cmsProgramWkfwhis) throws DAOException
    {
        log.debug("delete cmsProgramWkfwhis by pk starting...");
        super.delete("deleteCmsProgramWkfwhis", cmsProgramWkfwhis);
        log.debug("delete cmsProgramWkfwhis by pk end");
    }

    public void deleteCmsProgramWkfwhisList(List<CmsProgramWkfwhis> cmsProgramWkfwhisList) throws DAOException
    {
        log.debug("delete cmsProgramWkfwhisList by pk starting...");
        super.deleteBatch("deleteCmsProgramWkfwhis", cmsProgramWkfwhisList);
        log.debug("delete cmsProgramWkfwhisList by pk end");
    }

    public void deleteCmsProgramWkfwhisByCond(CmsProgramWkfwhis cmsProgramWkfwhis) throws DAOException
    {
        log.debug("delete cmsProgramWkfwhis by conditions starting...");
        super.delete("deleteCmsProgramWkfwhisByCond", cmsProgramWkfwhis);
        log.debug("delete cmsProgramWkfwhis by conditions end");
    }

    public void deleteCmsProgramWkfwhisListByCond(List<CmsProgramWkfwhis> cmsProgramWkfwhisList) throws DAOException
    {
        log.debug("delete cmsProgramWkfwhisList by conditions starting...");
        super.deleteBatch("deleteCmsProgramWkfwhisByCond", cmsProgramWkfwhisList);
        log.debug("delete cmsProgramWkfwhisList by conditions end");
    }

    public CmsProgramWkfwhis getCmsProgramWkfwhis(CmsProgramWkfwhis cmsProgramWkfwhis) throws DAOException
    {
        log.debug("query cmsProgramWkfwhis starting...");
        CmsProgramWkfwhis resultObj = (CmsProgramWkfwhis) super.queryForObject("getCmsProgramWkfwhis",
                cmsProgramWkfwhis);
        log.debug("query cmsProgramWkfwhis end");
        return resultObj;
    }

    @SuppressWarnings("unchecked")
    public List<CmsProgramWkfwhis> getCmsProgramWkfwhisByCond(CmsProgramWkfwhis cmsProgramWkfwhis) throws DAOException
    {
        log.debug("query cmsProgramWkfwhis by condition starting...");
        List<CmsProgramWkfwhis> rList = (List<CmsProgramWkfwhis>) super.queryForList(
                "queryCmsProgramWkfwhisListByCond", cmsProgramWkfwhis);
        log.debug("query cmsProgramWkfwhis by condition end");
        return rList;
    }

    @SuppressWarnings("unchecked")
    public List<CmsProgramWkfwhis> getCmsProgramWkfwhisByCond(CmsProgramWkfwhis cmsProgramWkfwhis,
            PageUtilEntity puEntity) throws DAOException
    {
        log.debug("query cmsProgramWkfwhis by condition starting...");
        List<CmsProgramWkfwhis> rList = (List<CmsProgramWkfwhis>) super.queryForList(
                "queryCmsProgramWkfwhisListByCond", cmsProgramWkfwhis, puEntity);
        log.debug("query cmsProgramWkfwhis by condition end");
        return rList;
    }

    @SuppressWarnings("unchecked")
    public PageInfo pageInfoQuery(CmsProgramWkfwhis cmsProgramWkfwhis, int start, int pageSize) throws DAOException
    {
        log.debug("page query cmsProgramWkfwhis by condition starting...");
        PageInfo pageInfo = null;
        int totalCnt = ((Integer) super.queryForObject("queryCmsProgramWkfwhisListCntByCond", cmsProgramWkfwhis))
                .intValue();
        if (totalCnt > 0)
        {
            int fetchSize = pageSize > (totalCnt - start) ? (totalCnt - start) : pageSize;
            List<CmsProgramWkfwhis> rsList = (List<CmsProgramWkfwhis>) super.pageQuery(
                    "queryCmsProgramWkfwhisListByCond", cmsProgramWkfwhis, start, fetchSize);
            pageInfo = new PageInfo(start, totalCnt, fetchSize, rsList);
        }
        else
        {
            pageInfo = new PageInfo();
        }
        log.debug("page query cmsProgramWkfwhis by condition end");
        return pageInfo;
    }

    @SuppressWarnings("unchecked")
    public PageInfo pageInfoQuery(CmsProgramWkfwhis cmsProgramWkfwhis, int start, int pageSize, PageUtilEntity puEntity)
            throws DAOException
    {
        return super.indexPageQuery("queryCmsProgramWkfwhisListByCond", "queryCmsProgramWkfwhisListCntByCond",
                cmsProgramWkfwhis, start, pageSize, puEntity);
    }

}
package com.zte.cms.content.apply;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import net.sf.cglib.beans.BeanCopier;

import com.zte.cms.common.ResourceManager;
import com.zte.cms.content.movie.model.CmsMovie;
import com.zte.cms.content.movie.service.ICmsMovieDS;
import com.zte.cms.content.program.model.CmsProgram;
import com.zte.cms.content.program.model.CmsProgramWkfwhis;
import com.zte.cms.content.program.service.ICmsProgramDS;
import com.zte.cms.content.program.service.ICmsProgramWkfwhisDS;
import com.zte.ismp.common.constant.GlobalConstants;
import com.zte.ismp.common.util.StrUtil;
import com.zte.ssb.framework.SSBBus;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;
import com.zte.umap.common.ResourceMgt;
import com.zte.umap.common.wkfw.CommonCallback;
import com.zte.umap.ssb.logmanager.business.model.AppLogInfoEntity;

/**
 * <p>
 * 文件名称: CntApplyCallback.java
 * </p>
 * <p>
 * 文件描述: 内容申请回调类
 * </p>
 * <p>
 * 版权所有: 版权所有(C)2010-2020
 * </p>
 * <p>
 * 公 司: 中兴通讯股份有限公司
 * </p>
 * <p>
 * 内容摘要: 主要提供内容申请审核后的回调方法
 * </p>
 * <p>
 * 其他说明:
 * </p>
 * <p>
 * 完成日期：2011年02月26日
 * </p>
 * <p>
 * 修改记录1:
 * </p>
 * 
 * <pre>
 *    修改日期：
 *    版 本 号：
 *    修 改 人：
 *    修改内容：
 * </pre>
 * 
 * @version ZXIN10 CMSV2.01.01
 * @author 李梅
 */

public class CntApplyCallback extends CommonCallback
{
    // 日志
    private Log log = SSBBus.getLog(getClass());

    protected ICmsProgramDS cmsProgramDS;
    protected ICmsMovieDS cmsMovieDS = null;
    protected ICmsProgramWkfwhisDS cmsProgramWkfwhisDS;

    protected CmsProgram cmsProgram;
    protected CmsProgramWkfwhis cmsProgramWkfwhis;

    @Override
    public AppLogInfoEntity getLogInfo()
    {
        AppLogInfoEntity loginfo = new AppLogInfoEntity();
        ResourceMgt.addDefaultResourceBundle("log_program_resource");

        String optObjecttype = "";
        String optDetail = "";
        String optType = "";
        String optObject = cmsProgram.getProgramid(); // wkflItem.getPboIndex().toString();

        if (wkflItem.getNodeId().longValue() == 1)
        {
            optType = ResourceMgt.findDefaultText("log.opttype.cnt.mgt"); // 点播内容管理
            optObjecttype = ResourceMgt.findDefaultText("log.optobjecttype.cnt.apply"); // 内容提审
            optDetail = ResourceMgt.findDefaultText("log.optdetail.cnt.apply");// 操作员[opername]提审了内容
                                                                                // [programid]，[handleevent]
        }
        else
        {
            optType = ResourceMgt.findDefaultText("log.opttype.cnt.auditmgt"); // 内容审核管理
            optObjecttype = ResourceMgt.findDefaultText("log.optobjecttype.cnt.audit"); // 内容[nodename]
            optDetail = ResourceMgt.findDefaultText("log.optdetail.cnt.audit");// 操作员[opername]对内容
                                                                                // [programid]申请进行[nodename]，[handleevent]
        }

        if (null != optObjecttype)
        {
            optObjecttype = optObjecttype.replaceAll("\\[nodename\\]", wkflItem.getNodeName());
        }

        if (null != optDetail)
        {
            optDetail = optDetail.replaceAll("\\[opername\\]", StrUtil.filterDollarStr(wkflItem.getExcutorName()));
            optDetail = optDetail.replaceAll("\\[programid\\]", String.valueOf(cmsProgram.getProgramid()));
            optDetail = optDetail.replaceAll("\\[nodename\\]", wkflItem.getNodeName());
            optDetail = optDetail.replaceAll("\\[handleevent\\]", wkflItem.getHandleEvent());
        }

        loginfo.setOptType(optType);
        loginfo.setOptObjecttype(optObjecttype);
        loginfo.setOptObject(optObject);
        loginfo.setOptDetail(optDetail);
        loginfo.setServicekey(GlobalConstants.SYSTEM_SERVICE_KEY);
        return loginfo;
    }

    @Override
    public void initData()
    {
        Long programindex = wkflItem.getPboIndex();

        cmsProgram = new CmsProgram();
        cmsProgram.setProgramindex(programindex);
        // 查询订单对象
        try
        {
            cmsProgram = cmsProgramDS.getCmsProgram(cmsProgram);
        }
        catch (DomainServiceException dsEx)
        {
            log.error("domain exception:" + dsEx);
            throw new RuntimeException(dsEx);
        }

        // 更新订单工作流状态
        cmsProgram.setWorkflowlife(wkflItem.getPboState());

        // 工作流对象处理，设置生效时间
        wkflItem.setEffectiveTime("0"); // cmsProgram.getEffectivedate()
        wkflItem.setPboId(String.valueOf(cmsProgram.getProgramindex()));

        // 创建订单工作流历史对象
        cmsProgramWkfwhis = new CmsProgramWkfwhis();
        BeanCopier copier = BeanCopier.create(CmsProgram.class, CmsProgramWkfwhis.class, false);
        copier.copy(cmsProgram, cmsProgramWkfwhis, null);

        cmsProgramWkfwhis.setWorkflowindex(Long.valueOf(wkflItem.getFlowInstId()));
        cmsProgramWkfwhis.setTaskindex(Long.valueOf(wkflItem.getTaskId()));
        cmsProgramWkfwhis.setNodeid(wkflItem.getNodeId().longValue());
        cmsProgramWkfwhis.setNodename(wkflItem.getNodeName());
        cmsProgramWkfwhis.setOpername(wkflItem.getExcutorId());
        cmsProgramWkfwhis.setOpopinion(wkflItem.getComments());
        cmsProgramWkfwhis.setTemplateindex(wkflItem.getTemplateSeqNo().longValue());
        cmsProgramWkfwhis.setHandleevent(wkflItem.getHandleEvent());

        SimpleDateFormat dateformat = new SimpleDateFormat("yyyyMMddHHmmss");
        if (wkflItem.getStartTime() != null)
        {
            String workflowstarttime = dateformat.format(wkflItem.getStartTime());
            cmsProgramWkfwhis.setWorkflowstarttime(workflowstarttime);
        }
        if (wkflItem.getFinishTime() != null)
        {
            String workflowendtime = dateformat.format(wkflItem.getFinishTime());
            cmsProgramWkfwhis.setWorkflowendtime(workflowendtime);
        }
    }


    @Override
    public void operate()
    {
        try
        {
            if (wkflItem.getPboState().intValue() == -1)
            {// 审核不通过，分三种情况 一审、二审、三审，内容状态分别更新为一审失败、二审失败、三审失败或回收站
                cmsProgram.setWorkflow(0); // 0:未启动
                SimpleDateFormat format = new SimpleDateFormat("yyyyMMddHHmmss");
                cmsProgram.setEffectivedate(format.format(new Date()));// 将生效时间字段设为当前时间

                //if ("一审".equals(wkflItem.getNodeName()))
                if (ResourceMgt.findDefaultText("cnt.first.audit").equals(wkflItem.getNodeName()))
                {
                    cmsProgram.setStatus(130); // 130:一审失败
                }
                //else if ("二审".equals(wkflItem.getNodeName()))
                else if (ResourceMgt.findDefaultText("cnt.second.audit").equals(wkflItem.getNodeName()))
                	
                	
                {
                    //if ("不通过".equals(wkflItem.getHandleEvent()))
                	if (ResourceMgt.findDefaultText("cnt.not.pass").equals(wkflItem.getHandleEvent()))
                    {
                        cmsProgram.setStatus(150); // 150:二审失败
                    }
                    //else if ("进回收站".equals(wkflItem.getHandleEvent()))
                	else if (ResourceMgt.findDefaultText("cnt.to.recycle").equals(wkflItem.getHandleEvent()))
                    {
                        cmsProgram.setStatus(160); // 160:回收站
                    }
                }
                //else if ("三审".equals(wkflItem.getNodeName()))
                else if (ResourceMgt.findDefaultText("cnt.third.audit").equals(wkflItem.getNodeName()))
                		 
                {
                    cmsProgram.setStatus(180); // 180:三审失败
                }
            }// 否则，分三种情况申请、一审、二审
            else
            {
                String route = cmsProgramDS.getRoute(cmsProgram);
                int r1 = Integer.valueOf(route.substring(4, 5)).intValue();
                int r2 = Integer.valueOf(route.substring(9, 10)).intValue();
                if (wkflItem.getNodeId().longValue() == 1)
                {// 提审
                    if (r1 == 0 && r2 == 0)
                    {// 免一审并且免二审，置为三审中170
                        cmsProgram.setStatus(170);
                    }
                    if (r1 == 0 && r2 == 1)
                    {// 免一审且不免二审，置为二审中140
                        cmsProgram.setStatus(140);
                    }
                    if (r1 == 1 && r2 == 0)
                    {// 走一审且免二审，置为一审中120
                        cmsProgram.setStatus(120);
                    }
                    if (r1 == 1 && r2 == 1)
                    {// 一审、二审都不免 置为一审中120
                        cmsProgram.setStatus(120);
                    }
                }
                else
                {
                    //if ("一审".equals(wkflItem.getNodeName()))
                	if (ResourceMgt.findDefaultText("cnt.first.audit").equals(wkflItem.getNodeName()))
                    {
                        if (r2 == 0)
                        {// 免二审，置为三审中170
                            cmsProgram.setStatus(170); // 170:三审中
                        }
                        else
                        {// 不免二审，置为二审中140
                            cmsProgram.setStatus(140); // 140:二审中
                        }
                    }
                    //else if ("二审".equals(wkflItem.getNodeName()))
                    else if (ResourceMgt.findDefaultText("cnt.second.audit").equals(wkflItem.getNodeName()))
                    {
                        cmsProgram.setStatus(170); // 170:三审中
                    }
                }
            }

            log.debug("before updateCmsProgram");
            cmsProgramDS.updateCmsProgram(cmsProgram);
            log.debug("after updateCmsProgram");

            // 更新内容下的所有子内容为内容的状态
            CmsMovie cmsMovie = new CmsMovie();
            List<CmsMovie> cmsMovieList = null;
            cmsMovie.setProgramid(cmsProgram.getProgramid());
            cmsMovieList = cmsMovieDS.getCmsMovieByCond(cmsMovie);
            if (null != cmsMovieList && cmsMovieList.size() > 0)
            {
                for (int i = 0; i < cmsMovieList.size(); i++)
                {
                    cmsMovieList.get(i).setStatus(cmsProgram.getStatus());
                }
                cmsMovieDS.updateCmsMovieList(cmsMovieList);
            }

            log.debug("before insertCmsProgramWkfwhis");
            cmsProgramWkfwhisDS.insertCmsProgramWkfwhis(cmsProgramWkfwhis);
            log.debug("after insertCmsProgramWkfwhis");
        }
        catch (DomainServiceException dsEx)
        {
            log.error("domain exception:" + dsEx);
            throw new RuntimeException(dsEx);
        }
    }

    public void setCmsProgramDS(ICmsProgramDS cmsProgramDS)
    {
        this.cmsProgramDS = cmsProgramDS;
    }

    public void setCmsMovieDS(ICmsMovieDS cmsMovieDS)
    {
        this.cmsMovieDS = cmsMovieDS;
    }

    public void setCmsProgramWkfwhisDS(ICmsProgramWkfwhisDS cmsProgramWkfwhisDS)
    {
        this.cmsProgramWkfwhisDS = cmsProgramWkfwhisDS;
    }

}

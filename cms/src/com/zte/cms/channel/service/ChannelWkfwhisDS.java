package com.zte.cms.channel.service;

import java.util.List;

import com.zte.cms.channel.dao.IChannelWkfwhisDAO;
import com.zte.cms.channel.model.ChannelWkfwhis;
import com.zte.cms.service.ls.ServiceConstants;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;
import com.zte.ssb.framework.exception.exceptions.DAOException;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.SSBBus;
import com.zte.umap.common.dynamicobj.DynamicObjectBaseDS;

public class ChannelWkfwhisDS extends DynamicObjectBaseDS implements IChannelWkfwhisDS
{
    // 日志
    private Log log = SSBBus.getLog(getClass());

    private IChannelWkfwhisDAO dao = null;

    public void setDao(IChannelWkfwhisDAO dao)
    {
        this.dao = dao;
    }

    public void insertChannelWkfwhis(ChannelWkfwhis channelWkfwhis) throws DomainServiceException
    {
        log.debug("insert channelWkfwhis starting...");
        Long index = (Long) this.getPrimaryKeyGenerator().getPrimarykey("ucdn_channel_wkfw_his_index");
        channelWkfwhis.setHistoryindex(index);
        String historydate = ServiceConstants.getCurrentTimeString();
        channelWkfwhis.setHistorydate(historydate);
        try
        {

            dao.insertChannelWkfwhis(channelWkfwhis);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("insert channelWkfwhis end");
    }

    public void updateChannelWkfwhis(ChannelWkfwhis channelWkfwhis) throws DomainServiceException
    {
        log.debug("update channelWkfwhis by pk starting...");
        try
        {
            dao.updateChannelWkfwhis(channelWkfwhis);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("update channelWkfwhis by pk end");
    }

    public void updateChannelWkfwhisList(List<ChannelWkfwhis> channelWkfwhisList) throws DomainServiceException
    {
        log.debug("update channelWkfwhisList by pk starting...");
        if (null == channelWkfwhisList || channelWkfwhisList.size() == 0)
        {
            log.debug("there is no datas in channelWkfwhisList");
            return;
        }
        try
        {
            for (ChannelWkfwhis channelWkfwhis : channelWkfwhisList)
            {
                dao.updateChannelWkfwhis(channelWkfwhis);
            }
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("update channelWkfwhisList by pk end");
    }

    public void removeChannelWkfwhis(ChannelWkfwhis channelWkfwhis) throws DomainServiceException
    {
        log.debug("remove channelWkfwhis by pk starting...");
        try
        {
            dao.deleteChannelWkfwhis(channelWkfwhis);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("remove channelWkfwhis by pk end");
    }

    public void removeChannelWkfwhisList(List<ChannelWkfwhis> channelWkfwhisList) throws DomainServiceException
    {
        log.debug("remove channelWkfwhisList by pk starting...");
        if (null == channelWkfwhisList || channelWkfwhisList.size() == 0)
        {
            log.debug("there is no datas in channelWkfwhisList");
            return;
        }
        try
        {
            for (ChannelWkfwhis channelWkfwhis : channelWkfwhisList)
            {
                dao.deleteChannelWkfwhis(channelWkfwhis);
            }
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("remove channelWkfwhisList by pk end");
    }

    public ChannelWkfwhis getChannelWkfwhis(ChannelWkfwhis channelWkfwhis) throws DomainServiceException
    {
        log.debug("get channelWkfwhis by pk starting...");
        ChannelWkfwhis rsObj = null;
        try
        {
            rsObj = dao.getChannelWkfwhis(channelWkfwhis);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("get channelWkfwhisList by pk end");
        return rsObj;
    }

    public List<ChannelWkfwhis> getChannelWkfwhisByCond(ChannelWkfwhis channelWkfwhis) throws DomainServiceException
    {
        log.debug("get channelWkfwhis by condition starting...");
        List<ChannelWkfwhis> rsList = null;
        try
        {
            rsList = dao.getChannelWkfwhisByCond(channelWkfwhis);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("get channelWkfwhis by condition end");
        return rsList;
    }

    @SuppressWarnings("unchecked")
    public TableDataInfo pageInfoQuery(ChannelWkfwhis channelWkfwhis, int start, int pageSize)
            throws DomainServiceException
    {
        log.debug("get channelWkfwhis page info by condition starting...");
        PageInfo pageInfo = null;
        try
        {
            pageInfo = dao.pageInfoQuery(channelWkfwhis, start, pageSize);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        TableDataInfo tableInfo = new TableDataInfo();
        tableInfo.setData((List<ChannelWkfwhis>) pageInfo.getResult());
        tableInfo.setTotalCount((int) pageInfo.getTotalCount());
        log.debug("get channelWkfwhis page info by condition end");
        return tableInfo;
    }

    @SuppressWarnings("unchecked")
    public TableDataInfo pageInfoQuery(ChannelWkfwhis channelWkfwhis, int start, int pageSize, PageUtilEntity puEntity)
            throws DomainServiceException
    {
        log.debug("get channelWkfwhis page info by condition starting...");
        PageInfo pageInfo = null;
        try
        {
            pageInfo = dao.pageInfoQuery(channelWkfwhis, start, pageSize, puEntity);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        TableDataInfo tableInfo = new TableDataInfo();
        tableInfo.setData((List<ChannelWkfwhis>) pageInfo.getResult());
        tableInfo.setTotalCount((int) pageInfo.getTotalCount());
        log.debug("get channelWkfwhis page info by condition end");
        return tableInfo;
    }
}

package com.zte.cms.content.wkfw.dao;

import java.util.List;

import com.zte.cms.content.wkfw.model.IcmContentWkfwCondition;
import com.zte.cms.content.wkfw.model.IcmContentWkfwTaskResult;
import com.zte.ssb.dynamicobj.DynamicObjectBaseDao;
import com.zte.ssb.framework.SSBBus;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.exception.exceptions.DAOException;

public class IcmContentWkfwTaskHisResultDAO extends DynamicObjectBaseDao implements IIcmContentWkfwTaskHisResultDAO
{
    // 日志
    private Log log = SSBBus.getLog(getClass());

    @SuppressWarnings("unchecked")
    public PageInfo pageInfoQuery(IcmContentWkfwCondition contentWkfwCondition, int start, int pageSize)
            throws DAOException
    {
        log.debug("page query IcmContentWkfwTaskResult by condition starting...");
        PageInfo pageInfo = null;
        int totalCnt = ((Integer) super.queryForObject("queryIcmContentWkfwTaskHisResultListCntByCond",
                contentWkfwCondition)).intValue();
        if (totalCnt > 0)
        {
            int fetchSize = pageSize > (totalCnt - start) ? (totalCnt - start) : pageSize;
            List<IcmContentWkfwTaskResult> rsList = (List<IcmContentWkfwTaskResult>) super.pageQuery(
                    "queryIcmContentWkfwTaskHisResultListByCond", contentWkfwCondition, start, fetchSize);
            pageInfo = new PageInfo(start, totalCnt, fetchSize, rsList);
        }
        else
        {
            pageInfo = new PageInfo();
        }
        log.debug("page query IcmContentWkfwTaskResult by condition end");
        return pageInfo;
    }

    public long getMyHaveHandleWorkflowItemListCount(IcmContentWkfwCondition contentWkfwCondition) throws DAOException
    {
        long result = super.getTotalCount("queryIcmContentWkfwTaskHisResultListCntByCond", contentWkfwCondition);
        return result;
    }

    public PageInfo pageInfoQuery(IcmContentWkfwCondition contentWkfwCondition, int start, int pageSize,
            PageUtilEntity puEntity) throws DAOException
    {
        return super.indexPageQuery("queryIcmContentWkfwTaskHisResultListByCond",
                "queryIcmContentWkfwTaskHisResultListCntByCond", contentWkfwCondition, start, pageSize, puEntity);
    }

}
package com.zte.cms.settlementDataRpt.service;

import com.zte.cms.settlementDataRpt.model.OperInformation;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;

/**
 * @Author: Liangxiaomin
 * @Date Created in 15:00 2019/6/27
 * @Description:
 */
public interface OperatorsService {

    TableDataInfo pageInfoQuery(OperInformation operInformation);

}

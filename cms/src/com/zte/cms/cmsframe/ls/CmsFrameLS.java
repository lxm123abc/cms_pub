package com.zte.cms.cmsframe.ls;

import java.util.List;

import com.zte.cms.cmsframe.model.CmsFrame;
import com.zte.cms.cmsframe.service.ICmsFrameDS;
import com.zte.cms.common.log.CommonLogConstant;
import com.zte.cms.common.log.CommonLogUtil;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.SSBBus;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;
import com.zte.umap.common.EspecialCharMgt;
import com.zte.umap.common.ResourceMgt;

public class CmsFrameLS implements ICmsFrameLS
{
    // 日志
    private Log log = SSBBus.getLog(getClass());

    private ICmsFrameDS cmsFrameDS = null;

    public void setCmsFrameDS(ICmsFrameDS cmsFrameDS)
    {
        this.cmsFrameDS = cmsFrameDS;
    }

    public String addCmsFrame(String checkAll, String frameName, String frameDesc) throws DomainServiceException
    {
        log.debug("insert cmsFrame starting...");
        CmsFrame cmsframe = new CmsFrame();
        cmsframe.setFramname(frameName);

        // 先判断是否有重复的名称
        List list = cmsFrameDS.getCmsFrameByName(cmsframe);
        int count = (Integer) list.get(0);
        if (count> 0)
        {
            return ResourceMgt.findDefaultText("same.frame.name.exist");//"1:已经有相同的模板名称存在";
        }
        cmsframe.setFramdesc(frameDesc);
        cmsframe.setFramestatus(0);
        String str[] = checkAll.split(":");

        cmsframe.setContenttypeid(str[7].equals("1") ? 1:0);
        cmsframe.setSourcetype(str[8].equals("1") ? 1:0);
        cmsframe.setOrdernumber(str[9].equals("1") ? 1:0);
        cmsframe.setOriginalnamecn(str[10].equals("1") ? 1:0);
        cmsframe.setSortname(str[11].equals("1") ? 1:0);
        cmsframe.setSearchname(str[12].equals("1") ? 1:0);
        cmsframe.setGenre(str[13].equals("1") ? 1:0);
        cmsframe.setDesccn(str[14].equals("1") ? 1:0);
        cmsframe.setContentversion(str[15].equals("1") ? 1:0);
        cmsframe.setPlatform(str[16].equals("1") ? 1:0);
        cmsframe.setKeywords(str[17].equals("1") ? 1:0);
        cmsframe.setCopyrightcn(str[18].equals("1") ? 1:0);
        cmsframe.setEffectivedate(str[19].equals("1") ? 1:0);
        cmsframe.setExpirydate(str[20].equals("1") ? 1:0);
        cmsframe.setExclusivevalidity(str[21].equals("1") ? 1:0);
        cmsframe.setCopyrightexpiration(str[22].equals("1") ? 1:0);
        cmsframe.setLlicensofflinetime(str[23].equals("1") ? 1:0);
        cmsframe.setDeletetime(str[24].equals("1") ? 1:0);
        cmsframe.setCountry(str[25].equals("1") ? 1:0);
        cmsframe.setCopyproperty(str[26].equals("1") ? 1:0);
        cmsframe.setAuthorizationway(str[27].equals("1") ? 1:0);
        cmsframe.setImportlisence(str[28].equals("1") ? 1:0);
        cmsframe.setReleaselisence(str[29].equals("1") ? 1:0);
        cmsframe.setTradetype(str[30].equals("1") ? 1:0);
        cmsframe.setContenttags(str[31].equals("1") ? 1:0);
        cmsframe.setPricetaxin(str[32].equals("1") ? 1:0);
        cmsframe.setNewcomedays(str[33].equals("1") ? 1:0);
        cmsframe.setRemainingdays(str[34].equals("1") ? 1:0);
        cmsframe.setRecommendstart(str[35].equals("1") ? 1:0);
        cmsframe.setRating(str[36].equals("1") ? 1:0);
        cmsframe.setActors(str[37].equals("1") ? 1:0);
        cmsframe.setWriters(str[38].equals("1") ? 1:0);
        cmsframe.setDirectors(str[39].equals("1") ? 1:0);
        cmsframe.setLanguage(str[40].equals("1") ? 1:0);
        cmsframe.setReleaseyear(str[41].equals("1") ? 1:0);
        cmsframe.setOrgairdate(str[42].equals("1") ? 1:0);
        cmsframe.setLicensingstart(str[43].equals("1") ? 1:0);
        cmsframe.setLicensingend(str[44].equals("1") ? 1:0);
        cmsframe.setMacrovision(str[45].equals("1") ? 1:0);
        cmsframe.setViewpoint(str[46].equals("1") ? 1:0);
        cmsframe.setAwards(str[47].equals("1") ? 1:0);
        cmsframe.setDuration(str[48].equals("1") ? 1:0);
        cmsframe.setCountrytype(str[49].equals("1") ? 1:0);
        cmsframe.setProductcorp(str[50].equals("1") ? 1:0);
        cmsframe.setProducer(str[51].equals("1") ? 1:0);
        cmsframe.setPublisher(str[52].equals("1") ? 1:0);
        cmsframe.setSubtitlelanguage(str[53].equals("1") ? 1:0);
        cmsframe.setDubbinglanguage(str[54].equals("1") ? 1:0);
        cmsframe.setTitle(str[55].equals("1") ? 1:0);

        try
        {
            cmsFrameDS.insertCmsFrame(cmsframe);
        }
        catch (DomainServiceException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("insert cmsFrame end");
        // 写操作日志
        CommonLogUtil.insertOperatorLog(cmsframe.getFramname(), CommonLogConstant.MGTTYPE_PROGRAM,
                CommonLogConstant.OPERTYPE_CMSFRAME_ADD, CommonLogConstant.OPERTYPE_CMSFRAME_ADD_INFO,
                CommonLogConstant.RESOURCE_OPERATION_SUCCESS);
        return ResourceMgt.findDefaultText("add.success");//"0:新增成功";
    }

    public String updateCmsFrame(String checkAll, String frameName, String frameDesc, Long index)
            throws DomainServiceException
    {

        log.debug("update cmsFrame starting...");
        CmsFrame frame = new CmsFrame();
        CmsFrame cmsframe = new CmsFrame();
        frame.setFramname(frameName);
        frame.setFrameindex(index);

        // 先判断是否有重复的名称
        List<CmsFrame> cmsframeList = cmsFrameDS.queryCmsFrameListCntByNameUpdate(frame);
        if (cmsframeList.size() > 0 )
        {
            return ResourceMgt.findDefaultText("same.frame.name.exist");//"1:已经有相同的模板名称存在";
        }
        else
        {
            cmsframe.setFrameindex(index);
            cmsframe = cmsFrameDS.getCmsFrame(cmsframe);

            if (cmsframe == null)
            {
                return ResourceMgt.findDefaultText("frame.not.exist");//"1:模板不存在";
            }
        }
        cmsframe.setFramdesc(frameDesc);
        cmsframe.setFramname(frameName);
        cmsframe.setFramestatus(0);//1是生效，0是失效
        String str[] = checkAll.split(":");
        cmsframe.setContenttypeid(str[7].equals("1") ? 1:0);
        cmsframe.setSourcetype(str[8].equals("1") ? 1:0);
        cmsframe.setOrdernumber(str[9].equals("1") ? 1:0);
        cmsframe.setOriginalnamecn(str[10].equals("1") ? 1:0);
        cmsframe.setSortname(str[11].equals("1") ? 1:0);
        cmsframe.setSearchname(str[12].equals("1") ? 1:0);
        cmsframe.setGenre(str[13].equals("1") ? 1:0);
        cmsframe.setDesccn(str[14].equals("1") ? 1:0);
        cmsframe.setContentversion(str[15].equals("1") ? 1:0);
        cmsframe.setPlatform(str[16].equals("1") ? 1:0);
        cmsframe.setKeywords(str[17].equals("1") ? 1:0);
        cmsframe.setCopyrightcn(str[18].equals("1") ? 1:0);
        cmsframe.setEffectivedate(str[19].equals("1") ? 1:0);
        cmsframe.setExpirydate(str[20].equals("1") ? 1:0);
        cmsframe.setExclusivevalidity(str[21].equals("1") ? 1:0);
        cmsframe.setCopyrightexpiration(str[22].equals("1") ? 1:0);
        cmsframe.setLlicensofflinetime(str[23].equals("1") ? 1:0);
        cmsframe.setDeletetime(str[24].equals("1") ? 1:0);
        cmsframe.setCountry(str[25].equals("1") ? 1:0);
        cmsframe.setCopyproperty(str[26].equals("1") ? 1:0);
        cmsframe.setAuthorizationway(str[27].equals("1") ? 1:0);
        cmsframe.setImportlisence(str[28].equals("1") ? 1:0);
        cmsframe.setReleaselisence(str[29].equals("1") ? 1:0);
        cmsframe.setTradetype(str[30].equals("1") ? 1:0);
        cmsframe.setContenttags(str[31].equals("1") ? 1:0);
        cmsframe.setPricetaxin(str[32].equals("1") ? 1:0);
        cmsframe.setNewcomedays(str[33].equals("1") ? 1:0);
        cmsframe.setRemainingdays(str[34].equals("1") ? 1:0);
        cmsframe.setRecommendstart(str[35].equals("1") ? 1:0);
        cmsframe.setRating(str[36].equals("1") ? 1:0);
        cmsframe.setActors(str[37].equals("1") ? 1:0);
        cmsframe.setWriters(str[38].equals("1") ? 1:0);
        cmsframe.setDirectors(str[39].equals("1") ? 1:0);
        cmsframe.setLanguage(str[40].equals("1") ? 1:0);
        cmsframe.setReleaseyear(str[41].equals("1") ? 1:0);
        cmsframe.setOrgairdate(str[42].equals("1") ? 1:0);
        cmsframe.setLicensingstart(str[43].equals("1") ? 1:0);
        cmsframe.setLicensingend(str[44].equals("1") ? 1:0);
        cmsframe.setMacrovision(str[45].equals("1") ? 1:0);
        cmsframe.setViewpoint(str[46].equals("1") ? 1:0);
        cmsframe.setAwards(str[47].equals("1") ? 1:0);
        cmsframe.setDuration(str[48].equals("1") ? 1:0);
        cmsframe.setCountrytype(str[49].equals("1") ? 1:0);
        cmsframe.setProductcorp(str[50].equals("1") ? 1:0);
        cmsframe.setProducer(str[51].equals("1") ? 1:0);
        cmsframe.setPublisher(str[52].equals("1") ? 1:0);
        cmsframe.setSubtitlelanguage(str[53].equals("1") ? 1:0);
        cmsframe.setDubbinglanguage(str[54].equals("1") ? 1:0);
        cmsframe.setTitle(str[55].equals("1") ? 1:0);

        try
        {
            cmsFrameDS.updateCmsFrame(cmsframe);
        }
        catch (DomainServiceException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("update cmsFrame end");
        // 写操作日志
        CommonLogUtil.insertOperatorLog(cmsframe.getFramname(), CommonLogConstant.MGTTYPE_PROGRAM,
                CommonLogConstant.OPERTYPE_CMSFRAME_UPDATE, CommonLogConstant.OPERTYPE_CMSFRAME_UPDATE_INFO,
                CommonLogConstant.RESOURCE_OPERATION_SUCCESS);
        return ResourceMgt.findDefaultText("modify.success");//"0:修改成功"
    }

    public String removeCmsFrame(CmsFrame cmsFrame) throws DomainServiceException
    {
        log.debug("remove cmsFrame by pk starting...");

        // 判断状态是否生效中，如果生效，则不能删除
        CmsFrame cmsframe = new CmsFrame();
        cmsframe.setFrameindex(cmsFrame.getFrameindex());
        cmsframe = cmsFrameDS.getCmsFrame(cmsframe);
        if (null == cmsframe)
        {
            return ResourceMgt.findDefaultText("frame.notexist.or.bedeleted");//"1:该模板不存在或者已经被删除"
        }
        else if (cmsframe.getFramestatus() == 1)
        {
            return ResourceMgt.findDefaultText("frame.status.effecting.cannot.delete");//"1:该模板的状态为生效中，不可删除";
        }
        try
        {
            cmsFrameDS.removeCmsFrame(cmsFrame);
        }
        catch (DomainServiceException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("remove cmsFrame by pk end");

        // 写操作日志
        CommonLogUtil.insertOperatorLog(cmsframe.getFramname(), CommonLogConstant.MGTTYPE_PROGRAM,
                CommonLogConstant.OPERTYPE_CMSFRAME_DEL, CommonLogConstant.OPERTYPE_SERVICE_DEL_INFO,
                CommonLogConstant.RESOURCE_OPERATION_SUCCESS);

        return ResourceMgt.findDefaultText("delete.success");//"0:删除成功"
    }

    public String effectFrame(CmsFrame cmsFrame) throws DomainServiceException
    {
        log.debug("remove cmsFrame by pk starting...");

        // 判断状态是否生效中，如果生效，则不能删除
        CmsFrame cmsframe = new CmsFrame();
        cmsframe.setFrameindex(cmsFrame.getFrameindex());
        cmsframe = cmsFrameDS.getCmsFrame(cmsframe);

        if (null == cmsframe)
        {
            return ResourceMgt.findDefaultText("frame.notexist.or.bedeleted");//"1:该模板不存在或者已经被删除"
        }
        
        if (cmsframe.getFramestatus() == 1)
        {
            return ResourceMgt.findDefaultText("frame.status.effecting.cannot.effectFrame");//"1:该模板已经处于生效状态"
        }
        
        try
        {   cmsframe.setFramestatus(1);
            cmsFrameDS.updateCmsFrame(cmsframe);
        }
        catch (DomainServiceException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("remove cmsFrame by pk end");

        // 写操作日志
        CommonLogUtil.insertOperatorLog(cmsframe.getFramname(), CommonLogConstant.MGTTYPE_PROGRAM,
                CommonLogConstant.OPERTYPE_CMSFRAME_EFFECT, CommonLogConstant.OPERTYPE_CMSFRAME_EFFECT_INFO,
                CommonLogConstant.RESOURCE_OPERATION_SUCCESS);
        return ResourceMgt.findDefaultText("effect.success");//"0:生效成功"
    }

    public String uneffectFrame(CmsFrame cmsFrame) throws DomainServiceException
    {
        log.debug("remove cmsFrame by pk starting...");

        // 判断状态是否生效中，如果生效，则不能删除
        CmsFrame cmsframe = new CmsFrame();
        cmsframe.setFrameindex(cmsFrame.getFrameindex());
        cmsframe = cmsFrameDS.getCmsFrame(cmsframe);
        
        if (null == cmsframe)
        {
            return ResourceMgt.findDefaultText("frame.notexist.or.bedeleted");//"1:该模板不存在或者已经被删除"
        }
        
        if (cmsframe.getFramestatus() == 0)
        {
            return ResourceMgt.findDefaultText("frame.status.uneffecting.cannot.uneffectFrame");//"1:该模板已经处于失效状态"
        }
        
        try
        {
        	cmsframe.setFramestatus(0);
            cmsFrameDS.updateCmsFrame(cmsframe);
        }
        catch (DomainServiceException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("remove cmsFrame by pk end");

        // 写操作日志
        CommonLogUtil.insertOperatorLog(cmsframe.getFramname(), CommonLogConstant.MGTTYPE_PROGRAM,
                CommonLogConstant.OPERTYPE_CMSFRAME_UNEFFECT, CommonLogConstant.OPERTYPE_CMSFRAME_UNEFFECT_INFO,
                CommonLogConstant.RESOURCE_OPERATION_SUCCESS);

        return ResourceMgt.findDefaultText("uneffect.success");//"0:失效成功"
    }

    public CmsFrame getCmsFrame(CmsFrame cmsFrame) throws DomainServiceException
    {
        log.debug("get cmsFrame by pk starting...");
        CmsFrame rsObj = null;
        try
        {
            rsObj = cmsFrameDS.getCmsFrame(cmsFrame);
        }
        catch (DomainServiceException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("get cmsFrameList by pk end");
        return rsObj;
    }

    public List<CmsFrame> getCmsFrameByCond(CmsFrame cmsFrame) throws DomainServiceException
    {
        log.debug("get cmsFrame by condition starting...");
        List<CmsFrame> rsList = null;
        try
        {
            rsList = cmsFrameDS.getCmsFrameByCond(cmsFrame);
        }
        catch (DomainServiceException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("get cmsFrame by condition end");
        return rsList;
    }

    @SuppressWarnings("unchecked")
    public TableDataInfo pageInfoQuery(CmsFrame cmsFrame, int start, int pageSize) throws DomainServiceException
    {
        log.debug("get cmsFrame page info by condition starting...");
        cmsFrame.setFramname(EspecialCharMgt.conversion(cmsFrame.getFramname()));

        TableDataInfo tableInfo = null;
        PageUtilEntity puEntity = new PageUtilEntity();
        puEntity.setIsAlwaysSearchSize(true);
        puEntity.setIsAsc(false);
        puEntity.setOrderByColumn("frameindex");
        try
        {
            tableInfo = pageInfoQuery(cmsFrame, start, pageSize, puEntity);
        }
        catch (DomainServiceException daoEx)
        {
            log.error("dao exception:", daoEx);
            throw new DomainServiceException(daoEx);
        }

        log.debug("get cmsFrame page info by condition end");
        return tableInfo;
    }

    @SuppressWarnings("unchecked")
    public TableDataInfo pageInfoQuery(CmsFrame cmsFrame, int start, int pageSize, PageUtilEntity puEntity)
            throws DomainServiceException
    {
        log.debug("get cmsFrame page info by condition starting...");
        TableDataInfo tableInfo = null;
        try
        {
            tableInfo = cmsFrameDS.pageInfoQuery(cmsFrame, start, pageSize, puEntity);
        }
        catch (DomainServiceException daoEx)
        {
            log.error("dao exception:", daoEx);
            throw new DomainServiceException(daoEx);
        }
        log.debug("get cmsFrame page info by condition end");
        return tableInfo;
    }
}


  package com.zte.cms.content.CSplatformSyn.model;

import com.zte.ssb.dynamicobj.DynamicBaseObject;
//加入栏目等信息
public class CSCntPlatformSync extends DynamicBaseObject
{
	private java.lang.Long syncindex;
	private java.lang.Integer objecttype;
	private java.lang.Long objectindex;
	private java.lang.String objectid;
	private java.lang.String elementid;
	private java.lang.String parentid;
	private java.lang.Integer platform;
	private java.lang.Integer status;
	private java.lang.String createtime;
	private java.lang.String publishtime;
	private java.lang.String cancelpubtime;
	private java.lang.Integer isfiledelete;
	private java.lang.Long reserve01;
	private java.lang.String reserve02;
	private java.lang.String categoryname;
	private java.lang.String channelname;
	private java.lang.String channelid;
	private java.lang.String programname;
	
	private java.lang.String createtimeStartDate;
    private java.lang.String createtimeEndDate;
    
	private java.lang.String onlinetimeStartDate;
	private java.lang.String onlinetimeEndDate;
	private java.lang.String cntofflinestarttime;
	private java.lang.String cntofflineendtime;
	
	private java.lang.String operid;
	private java.lang.String cpid;
	private java.lang.String cpcnshortname;
	
    
	
    public java.lang.String getCreatetimeStartDate()
    {
        return createtimeStartDate;
    }

    public void setCreatetimeStartDate(java.lang.String createtimeStartDate)
    {
        this.createtimeStartDate = createtimeStartDate;
    }

    public java.lang.String getCreatetimeEndDate()
    {
        return createtimeEndDate;
    }

    public void setCreatetimeEndDate(java.lang.String createtimeEndDate)
    {
        this.createtimeEndDate = createtimeEndDate;
    }

    public java.lang.String getCreatetime()
    {
        return createtime;
    }

    public void setCreatetime(java.lang.String createtime)
    {
        this.createtime = createtime;
    }

    public java.lang.String getCpid()
    {
        return cpid;
    }

    public void setCpid(java.lang.String cpid)
    {
        this.cpid = cpid;
    }

    public java.lang.String getCpcnshortname()
    {
        return cpcnshortname;
    }

    public void setCpcnshortname(java.lang.String cpcnshortname)
    {
        this.cpcnshortname = cpcnshortname;
    }

    public java.lang.String getOperid()
    {
        return operid;
    }

    public void setOperid(java.lang.String operid)
    {
        this.operid = operid;
    }

    public java.lang.String getOnlinetimeStartDate() {
		return onlinetimeStartDate;
	}

	public void setOnlinetimeStartDate(java.lang.String onlinetimeStartDate) {
		this.onlinetimeStartDate = onlinetimeStartDate;
	}

	public java.lang.String getOnlinetimeEndDate() {
		return onlinetimeEndDate;
	}

	public void setOnlinetimeEndDate(java.lang.String onlinetimeEndDate) {
		this.onlinetimeEndDate = onlinetimeEndDate;
	}

	public java.lang.String getCntofflinestarttime() {
		return cntofflinestarttime;
	}

	public void setCntofflinestarttime(java.lang.String cntofflinestarttime) {
		this.cntofflinestarttime = cntofflinestarttime;
	}

	public java.lang.String getCntofflineendtime() {
		return cntofflineendtime;
	}

	public void setCntofflineendtime(java.lang.String cntofflineendtime) {
		this.cntofflineendtime = cntofflineendtime;
	}

	public java.lang.String getProgramname() {
		return programname;
	}

	public void setProgramname(java.lang.String programname) {
		this.programname = programname;
	}

	public java.lang.String getChannelid() {
		return channelid;
	}

	public void setChannelid(java.lang.String channelid) {
		this.channelid = channelid;
	}

	public java.lang.String getChannelname() {
		return channelname;
	}

	public void setChannelname(java.lang.String channelname) {
		this.channelname = channelname;
	}

	public java.lang.String getCategoryname() {
		return categoryname;
	}

	public void setCategoryname(java.lang.String categoryname) {
		this.categoryname = categoryname;
	}

	public java.lang.Long getSyncindex()
    {
        return syncindex;
    } 
         
    public void setSyncindex(java.lang.Long syncindex) 
    {
        this.syncindex = syncindex;
    }
    
    public java.lang.Integer getObjecttype()
    {
        return objecttype;
    } 
         
    public void setObjecttype(java.lang.Integer objecttype) 
    {
        this.objecttype = objecttype;
    }
    
    public java.lang.Long getObjectindex()
    {
        return objectindex;
    } 
         
    public void setObjectindex(java.lang.Long objectindex) 
    {
        this.objectindex = objectindex;
    }
    
    public java.lang.String getObjectid()
    {
        return objectid;
    } 
         
    public void setObjectid(java.lang.String objectid) 
    {
        this.objectid = objectid;
    }
    
    public java.lang.String getElementid()
    {
        return elementid;
    } 
         
    public void setElementid(java.lang.String elementid) 
    {
        this.elementid = elementid;
    }
    
    public java.lang.String getParentid()
    {
        return parentid;
    } 
         
    public void setParentid(java.lang.String parentid) 
    {
        this.parentid = parentid;
    }
    
    public java.lang.Integer getPlatform()
    {
        return platform;
    } 
         
    public void setPlatform(java.lang.Integer platform) 
    {
        this.platform = platform;
    }
    
    public java.lang.Integer getStatus()
    {
        return status;
    } 
         
    public void setStatus(java.lang.Integer status) 
    {
        this.status = status;
    }
    
    public java.lang.String getPublishtime()
    {
        return publishtime;
    } 
         
    public void setPublishtime(java.lang.String publishtime) 
    {
        this.publishtime = publishtime;
    }
    
    public java.lang.String getCancelpubtime()
    {
        return cancelpubtime;
    } 
         
    public void setCancelpubtime(java.lang.String cancelpubtime) 
    {
        this.cancelpubtime = cancelpubtime;
    }
    
    public java.lang.Integer getIsfiledelete()
    {
        return isfiledelete;
    } 
         
    public void setIsfiledelete(java.lang.Integer isfiledelete) 
    {
        this.isfiledelete = isfiledelete;
    }
    
    public java.lang.Long getReserve01()
    {
        return reserve01;
    } 
         
    public void setReserve01(java.lang.Long reserve01) 
    {
        this.reserve01 = reserve01;
    }
    
    public java.lang.String getReserve02()
    {
        return reserve02;
    } 
         
    public void setReserve02(java.lang.String reserve02) 
    {
        this.reserve02 = reserve02;
    }
    
	@Override
	public void initRelation()
	{
        this.addRelation("syncindex","SYNCINDEX");   
        this.addRelation("objecttype","OBJECTTYPE"); 
        this.addRelation("objectindex","OBJECTINDEX");   
        this.addRelation("objectid","OBJECTID");   
        this.addRelation("elementid","ELEMENTID");   
        this.addRelation("parentid","PARENTID");   
        this.addRelation("platform","PLATFORM");   
        this.addRelation("status","STATUS");           
        this.addRelation("publishtime","PUBLISHTIME");   
        this.addRelation("cancelpubtime","CANCELPUBTIME"); 
        this.addRelation("isfiledelete","ISFILEDELETE");   
        this.addRelation("reserve01","RESERVE01");   
        this.addRelation("reserve02","RESERVE02");  		
	}

 
}

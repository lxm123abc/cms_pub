package com.zte.cms.basicdata.dao;

import java.util.List;

import com.zte.cms.basicdata.dao.ICmsExtractedvafileInfoDAO;
import com.zte.cms.basicdata.model.CmsExtractedvafileInfo;
import com.zte.ssb.dynamicobj.DynamicObjectBaseDao;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DAOException;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.SSBBus;

public class CmsExtractedvafileInfoDAO extends DynamicObjectBaseDao implements ICmsExtractedvafileInfoDAO
{
    // 日志
    private Log log = SSBBus.getLog(getClass());

    public void insertCmsExtractedvafileInfo(CmsExtractedvafileInfo cmsExtractedvafileInfo) throws DAOException
    {
        log.debug("insert cmsExtractedvafileInfo starting...");
        super.insert("insertCmsExtractedvafileInfo", cmsExtractedvafileInfo);
        log.debug("insert cmsExtractedvafileInfo end");
    }

    public void updateCmsExtractedvafileInfo(CmsExtractedvafileInfo cmsExtractedvafileInfo) throws DAOException
    {
        log.debug("update cmsExtractedvafileInfo by pk starting...");
        super.update("updateCmsExtractedvafileInfo", cmsExtractedvafileInfo);
        log.debug("update cmsExtractedvafileInfo by pk end");
    }

    public void deleteCmsExtractedvafileInfo(CmsExtractedvafileInfo cmsExtractedvafileInfo) throws DAOException
    {
        log.debug("delete cmsExtractedvafileInfo by pk starting...");
        super.delete("deleteCmsExtractedvafileInfo", cmsExtractedvafileInfo);
        log.debug("delete cmsExtractedvafileInfo by pk end");
    }

    public CmsExtractedvafileInfo getCmsExtractedvafileInfo(CmsExtractedvafileInfo cmsExtractedvafileInfo)
            throws DAOException
    {
        log.debug("query cmsExtractedvafileInfo starting...");
        CmsExtractedvafileInfo resultObj = (CmsExtractedvafileInfo) super.queryForObject("getCmsExtractedvafileInfo",
                cmsExtractedvafileInfo);
        log.debug("query cmsExtractedvafileInfo end");
        return resultObj;
    }

    @SuppressWarnings("unchecked")
    public List<CmsExtractedvafileInfo> getCmsExtractedvafileInfoByCond(CmsExtractedvafileInfo cmsExtractedvafileInfo)
            throws DAOException
    {
        log.debug("query cmsExtractedvafileInfo by condition starting...");
        List<CmsExtractedvafileInfo> rList = (List<CmsExtractedvafileInfo>) super.queryForList(
                "queryCmsExtractedvafileInfoListByCond", cmsExtractedvafileInfo);
        log.debug("query cmsExtractedvafileInfo by condition end");
        return rList;
    }

    @SuppressWarnings("unchecked")
    public PageInfo pageInfoQuery(CmsExtractedvafileInfo cmsExtractedvafileInfo, int start, int pageSize)
            throws DAOException
    {
        log.debug("page query cmsExtractedvafileInfo by condition starting...");
        PageInfo pageInfo = null;
        int totalCnt = ((Integer) super.queryForObject("queryCmsExtractedvafileInfoListCntByCond",
                cmsExtractedvafileInfo)).intValue();
        if (totalCnt > 0)
        {
            int fetchSize = pageSize > (totalCnt - start) ? (totalCnt - start) : pageSize;
            List<CmsExtractedvafileInfo> rsList = (List<CmsExtractedvafileInfo>) super.pageQuery(
                    "queryCmsExtractedvafileInfoListByCond", cmsExtractedvafileInfo, start, fetchSize);
            pageInfo = new PageInfo(start, totalCnt, fetchSize, rsList);
        }
        else
        {
            pageInfo = new PageInfo();
        }
        log.debug("page query cmsExtractedvafileInfo by condition end");
        return pageInfo;
    }

    @SuppressWarnings("unchecked")
    public PageInfo pageInfoQuery(CmsExtractedvafileInfo cmsExtractedvafileInfo, int start, int pageSize,
            PageUtilEntity puEntity) throws DAOException
    {
        return super.indexPageQuery("queryCmsExtractedvafileInfoListByCond",
                "queryCmsExtractedvafileInfoListCntByCond", cmsExtractedvafileInfo, start, pageSize, puEntity);
    }

}
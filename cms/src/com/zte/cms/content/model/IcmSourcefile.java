package com.zte.cms.content.model;

import com.zte.ssb.dynamicobj.DynamicBaseObject;

public class IcmSourcefile extends DynamicBaseObject
{
    private java.lang.Long fileindex;
    private java.lang.Long cpindex;
    private java.lang.String cpid;
    private java.lang.String namecn;
    private java.lang.String filename;
    private java.lang.Long formatindex;
    private java.lang.Integer status;
    private java.lang.Integer filetype;
    private java.lang.String addresspath;
    private java.lang.String cachepath;
    private java.lang.String addressfile;
    private java.lang.String filesize;
    private java.lang.String sourceurl;
    private java.lang.Integer uploadtype;
    private java.lang.Integer seriesno;
    private java.lang.Integer definition;
    private java.lang.Integer sourcetype;
    private java.lang.Long parentindex;
    private java.lang.String duration;
    private java.lang.String bitrate;
    private java.lang.String bitrates;
    private java.lang.Long numberofframes;
    private java.lang.Long frameheight;
    private java.lang.Long framewidth;
    private java.lang.String aspectratio;
    private java.lang.String framerate;
    private java.lang.String brightness;
    private java.lang.String contrast;
    private java.lang.String videocodec;
    private java.lang.String audiocodec;
    private java.lang.String usemode;
    private java.lang.Integer bitratecode;
    private java.lang.String programname;
    private java.lang.String starttime;
    private java.lang.String endtime;
    private java.lang.Integer encodingformat;
    private java.lang.Integer packageformat;
    private java.lang.Integer resolution;
    private java.lang.Integer adownload;
    private java.lang.Integer ismainfile;
    private java.lang.Integer syncresultstatus;
    private java.lang.String syncerrorcode;
    private java.lang.String syncerrordesc;
    private java.lang.Integer posttype;
    private java.lang.String encryptedfilename;
    private java.lang.String liveurl;
    private java.lang.Integer previewtag;
    private java.lang.Integer drmfiletype;
    private java.lang.Integer snapexist;
    private java.lang.String snapbegintime;
    private java.lang.String snapendtime;
    private java.lang.Long timelength;
    private java.lang.Integer siteno;
    private java.lang.Long playnums;
    private java.lang.String catagorycode;
    private java.lang.Integer publishrate;
    private java.lang.String prop01;
    private java.lang.String prop02;
    private java.lang.String prop03;
    private java.lang.String prop04;
    private java.lang.String prop05;
    private java.lang.String prop06;
    private java.lang.String prop07;
    private java.lang.String prop08;
    private java.lang.String prop09;
    private java.lang.String prop10;
    private java.lang.Long templetid;
    private java.lang.String keywords;
    private java.lang.String country;
    private java.lang.String director;
    private java.lang.String actor;
    private java.lang.String playtime;
    private java.lang.String introduction;
    private java.lang.String subtitle;
    private java.lang.String subtitlelanguage;
    private java.lang.String dubbinglanguage;
    private java.lang.Integer cmsid;
    private java.lang.String servicekey;
    public java.lang.String getOperid()
    {
        return operid;
    }

    public void setOperid(java.lang.String operid)
    {
        this.operid = operid;
    }

    private java.lang.String operid;

    public java.lang.Long getFileindex()
    {
        return fileindex;
    }

    public void setFileindex(java.lang.Long fileindex)
    {
        this.fileindex = fileindex;
    }

    public java.lang.Long getCpindex()
    {
        return cpindex;
    }

    public void setCpindex(java.lang.Long cpindex)
    {
        this.cpindex = cpindex;
    }

    public java.lang.String getCpid()
    {
        return cpid;
    }

    public void setCpid(java.lang.String cpid)
    {
        this.cpid = cpid;
    }

    public java.lang.String getNamecn()
    {
        return namecn;
    }

    public void setNamecn(java.lang.String namecn)
    {
        this.namecn = namecn;
    }

    public java.lang.String getFilename()
    {
        return filename;
    }

    public void setFilename(java.lang.String filename)
    {
        this.filename = filename;
    }

    public java.lang.Long getFormatindex()
    {
        return formatindex;
    }

    public void setFormatindex(java.lang.Long formatindex)
    {
        this.formatindex = formatindex;
    }

    public java.lang.Integer getStatus()
    {
        return status;
    }

    public void setStatus(java.lang.Integer status)
    {
        this.status = status;
    }

    public java.lang.Integer getFiletype()
    {
        return filetype;
    }

    public void setFiletype(java.lang.Integer filetype)
    {
        this.filetype = filetype;
    }

    public java.lang.String getAddresspath()
    {
        return addresspath;
    }

    public void setAddresspath(java.lang.String addresspath)
    {
        this.addresspath = addresspath;
    }

    public java.lang.String getCachepath()
    {
        return cachepath;
    }

    public void setCachepath(java.lang.String cachepath)
    {
        this.cachepath = cachepath;
    }

    public java.lang.String getAddressfile()
    {
        return addressfile;
    }

    public void setAddressfile(java.lang.String addressfile)
    {
        this.addressfile = addressfile;
    }

    public java.lang.String getFilesize()
    {
        return filesize;
    }

    public void setFilesize(java.lang.String filesize)
    {
        this.filesize = filesize;
    }

    public java.lang.String getSourceurl()
    {
        return sourceurl;
    }

    public void setSourceurl(java.lang.String sourceurl)
    {
        this.sourceurl = sourceurl;
    }

    public java.lang.Integer getUploadtype()
    {
        return uploadtype;
    }

    public void setUploadtype(java.lang.Integer uploadtype)
    {
        this.uploadtype = uploadtype;
    }

    public java.lang.Integer getSeriesno()
    {
        return seriesno;
    }

    public void setSeriesno(java.lang.Integer seriesno)
    {
        this.seriesno = seriesno;
    }

    public java.lang.Integer getDefinition()
    {
        return definition;
    }

    public void setDefinition(java.lang.Integer definition)
    {
        this.definition = definition;
    }

    public java.lang.Integer getSourcetype()
    {
        return sourcetype;
    }

    public void setSourcetype(java.lang.Integer sourcetype)
    {
        this.sourcetype = sourcetype;
    }

    public java.lang.Long getParentindex()
    {
        return parentindex;
    }

    public void setParentindex(java.lang.Long parentindex)
    {
        this.parentindex = parentindex;
    }

    public java.lang.String getDuration()
    {
        return duration;
    }

    public void setDuration(java.lang.String duration)
    {
        this.duration = duration;
    }

    public java.lang.String getBitrate()
    {
        return bitrate;
    }

    public void setBitrate(java.lang.String bitrate)
    {
        this.bitrate = bitrate;
    }

    public java.lang.Long getNumberofframes()
    {
        return numberofframes;
    }

    public void setNumberofframes(java.lang.Long numberofframes)
    {
        this.numberofframes = numberofframes;
    }

    public java.lang.Long getFrameheight()
    {
        return frameheight;
    }

    public void setFrameheight(java.lang.Long frameheight)
    {
        this.frameheight = frameheight;
    }

    public java.lang.Long getFramewidth()
    {
        return framewidth;
    }

    public void setFramewidth(java.lang.Long framewidth)
    {
        this.framewidth = framewidth;
    }

    public java.lang.String getAspectratio()
    {
        return aspectratio;
    }

    public void setAspectratio(java.lang.String aspectratio)
    {
        this.aspectratio = aspectratio;
    }

    public java.lang.String getFramerate()
    {
        return framerate;
    }

    public void setFramerate(java.lang.String framerate)
    {
        this.framerate = framerate;
    }

    public java.lang.String getBrightness()
    {
        return brightness;
    }

    public void setBrightness(java.lang.String brightness)
    {
        this.brightness = brightness;
    }

    public java.lang.String getContrast()
    {
        return contrast;
    }

    public void setContrast(java.lang.String contrast)
    {
        this.contrast = contrast;
    }

    public java.lang.String getVideocodec()
    {
        return videocodec;
    }

    public void setVideocodec(java.lang.String videocodec)
    {
        this.videocodec = videocodec;
    }

    public java.lang.String getAudiocodec()
    {
        return audiocodec;
    }

    public void setAudiocodec(java.lang.String audiocodec)
    {
        this.audiocodec = audiocodec;
    }

    public java.lang.String getUsemode()
    {
        return usemode;
    }

    public void setUsemode(java.lang.String usemode)
    {
        this.usemode = usemode;
    }

    public java.lang.Integer getBitratecode()
    {
        return bitratecode;
    }

    public void setBitratecode(java.lang.Integer bitratecode)
    {
        this.bitratecode = bitratecode;
    }

    public java.lang.String getBitrates()
    {
        return bitrates;
    }

    public void setBitrates(java.lang.String bitrates)
    {
        this.bitrates = bitrates;
    }

    public java.lang.String getProgramname()
    {
        return programname;
    }

    public void setProgramname(java.lang.String programname)
    {
        this.programname = programname;
    }

    public java.lang.String getStarttime()
    {
        return starttime;
    }

    public void setStarttime(java.lang.String starttime)
    {
        this.starttime = starttime;
    }

    public java.lang.String getEndtime()
    {
        return endtime;
    }

    public void setEndtime(java.lang.String endtime)
    {
        this.endtime = endtime;
    }

    public java.lang.Integer getEncodingformat()
    {
        return encodingformat;
    }

    public void setEncodingformat(java.lang.Integer encodingformat)
    {
        this.encodingformat = encodingformat;
    }

    public java.lang.Integer getPackageformat()
    {
        return packageformat;
    }

    public void setPackageformat(java.lang.Integer packageformat)
    {
        this.packageformat = packageformat;
    }

    public java.lang.Integer getResolution()
    {
        return resolution;
    }

    public void setResolution(java.lang.Integer resolution)
    {
        this.resolution = resolution;
    }

    public java.lang.Integer getAdownload()
    {
        return adownload;
    }

    public void setAdownload(java.lang.Integer adownload)
    {
        this.adownload = adownload;
    }

    public java.lang.Integer getIsmainfile()
    {
        return ismainfile;
    }

    public void setIsmainfile(java.lang.Integer ismainfile)
    {
        this.ismainfile = ismainfile;
    }

    public java.lang.Integer getSyncresultstatus()
    {
        return syncresultstatus;
    }

    public void setSyncresultstatus(java.lang.Integer syncresultstatus)
    {
        this.syncresultstatus = syncresultstatus;
    }

    public java.lang.String getSyncerrorcode()
    {
        return syncerrorcode;
    }

    public void setSyncerrorcode(java.lang.String syncerrorcode)
    {
        this.syncerrorcode = syncerrorcode;
    }

    public java.lang.String getSyncerrordesc()
    {
        return syncerrordesc;
    }

    public void setSyncerrordesc(java.lang.String syncerrordesc)
    {
        this.syncerrordesc = syncerrordesc;
    }

    public java.lang.Integer getPosttype()
    {
        return posttype;
    }

    public void setPosttype(java.lang.Integer posttype)
    {
        this.posttype = posttype;
    }

    public java.lang.String getEncryptedfilename()
    {
        return encryptedfilename;
    }

    public void setEncryptedfilename(java.lang.String encryptedfilename)
    {
        this.encryptedfilename = encryptedfilename;
    }

    public java.lang.String getLiveurl()
    {
        return liveurl;
    }

    public void setLiveurl(java.lang.String liveurl)
    {
        this.liveurl = liveurl;
    }

    public java.lang.Integer getPreviewtag()
    {
        return previewtag;
    }

    public void setPreviewtag(java.lang.Integer previewtag)
    {
        this.previewtag = previewtag;
    }

    public java.lang.Integer getDrmfiletype()
    {
        return drmfiletype;
    }

    public void setDrmfiletype(java.lang.Integer drmfiletype)
    {
        this.drmfiletype = drmfiletype;
    }

    public java.lang.Integer getSnapexist()
    {
        return snapexist;
    }

    public void setSnapexist(java.lang.Integer snapexist)
    {
        this.snapexist = snapexist;
    }

    public java.lang.String getSnapbegintime()
    {
        return snapbegintime;
    }

    public void setSnapbegintime(java.lang.String snapbegintime)
    {
        this.snapbegintime = snapbegintime;
    }

    public java.lang.String getSnapendtime()
    {
        return snapendtime;
    }

    public void setSnapendtime(java.lang.String snapendtime)
    {
        this.snapendtime = snapendtime;
    }

    public java.lang.Long getTimelength()
    {
        return timelength;
    }

    public void setTimelength(java.lang.Long timelength)
    {
        this.timelength = timelength;
    }

    public java.lang.Integer getSiteno()
    {
        return siteno;
    }

    public void setSiteno(java.lang.Integer siteno)
    {
        this.siteno = siteno;
    }

    public java.lang.Long getPlaynums()
    {
        return playnums;
    }

    public void setPlaynums(java.lang.Long playnums)
    {
        this.playnums = playnums;
    }

    public java.lang.String getCatagorycode()
    {
        return catagorycode;
    }

    public void setCatagorycode(java.lang.String catagorycode)
    {
        this.catagorycode = catagorycode;
    }

    public java.lang.Integer getPublishrate()
    {
        return publishrate;
    }

    public void setPublishrate(java.lang.Integer publishrate)
    {
        this.publishrate = publishrate;
    }

    public java.lang.String getProp01()
    {
        return prop01;
    }

    public void setProp01(java.lang.String prop01)
    {
        this.prop01 = prop01;
    }

    public java.lang.String getProp02()
    {
        return prop02;
    }

    public void setProp02(java.lang.String prop02)
    {
        this.prop02 = prop02;
    }

    public java.lang.String getProp03()
    {
        return prop03;
    }

    public void setProp03(java.lang.String prop03)
    {
        this.prop03 = prop03;
    }

    public java.lang.String getProp04()
    {
        return prop04;
    }

    public void setProp04(java.lang.String prop04)
    {
        this.prop04 = prop04;
    }

    public java.lang.String getProp05()
    {
        return prop05;
    }

    public void setProp05(java.lang.String prop05)
    {
        this.prop05 = prop05;
    }

    public java.lang.String getProp06()
    {
        return prop06;
    }

    public void setProp06(java.lang.String prop06)
    {
        this.prop06 = prop06;
    }

    public java.lang.String getProp07()
    {
        return prop07;
    }

    public void setProp07(java.lang.String prop07)
    {
        this.prop07 = prop07;
    }

    public java.lang.String getProp08()
    {
        return prop08;
    }

    public void setProp08(java.lang.String prop08)
    {
        this.prop08 = prop08;
    }

    public java.lang.String getProp09()
    {
        return prop09;
    }

    public void setProp09(java.lang.String prop09)
    {
        this.prop09 = prop09;
    }

    public java.lang.String getProp10()
    {
        return prop10;
    }

    public void setProp10(java.lang.String prop10)
    {
        this.prop10 = prop10;
    }

    public java.lang.Long getTempletid()
    {
        return templetid;
    }

    public void setTempletid(java.lang.Long templetid)
    {
        this.templetid = templetid;
    }

    public java.lang.String getKeywords()
    {
        return keywords;
    }

    public void setKeywords(java.lang.String keywords)
    {
        this.keywords = keywords;
    }

    public java.lang.String getCountry()
    {
        return country;
    }

    public void setCountry(java.lang.String country)
    {
        this.country = country;
    }

    public java.lang.String getDirector()
    {
        return director;
    }

    public void setDirector(java.lang.String director)
    {
        this.director = director;
    }

    public java.lang.String getActor()
    {
        return actor;
    }

    public void setActor(java.lang.String actor)
    {
        this.actor = actor;
    }

    public java.lang.String getPlaytime()
    {
        return playtime;
    }

    public void setPlaytime(java.lang.String playtime)
    {
        this.playtime = playtime;
    }

    public java.lang.String getIntroduction()
    {
        return introduction;
    }

    public void setIntroduction(java.lang.String introduction)
    {
        this.introduction = introduction;
    }

    public java.lang.String getSubtitle()
    {
        return subtitle;
    }

    public void setSubtitle(java.lang.String subtitle)
    {
        this.subtitle = subtitle;
    }

    public java.lang.String getSubtitlelanguage()
    {
        return subtitlelanguage;
    }

    public void setSubtitlelanguage(java.lang.String subtitlelanguage)
    {
        this.subtitlelanguage = subtitlelanguage;
    }

    public java.lang.String getDubbinglanguage()
    {
        return dubbinglanguage;
    }

    public void setDubbinglanguage(java.lang.String dubbinglanguage)
    {
        this.dubbinglanguage = dubbinglanguage;
    }

    public java.lang.Integer getCmsid()
    {
        return cmsid;
    }

    public void setCmsid(java.lang.Integer cmsid)
    {
        this.cmsid = cmsid;
    }

    public java.lang.String getServicekey()
    {
        return servicekey;
    }

    public void setServicekey(java.lang.String servicekey)
    {
        this.servicekey = servicekey;
    }

    public void initRelation()
    {
        this.addRelation("fileindex", "FILEINDEX");
        this.addRelation("cpindex", "CPINDEX");
        this.addRelation("cpid", "CPID");
        this.addRelation("namecn", "NAMECN");
        this.addRelation("filename", "FILENAME");
        this.addRelation("formatindex", "FORMATINDEX");
        this.addRelation("status", "STATUS");
        this.addRelation("filetype", "FILETYPE");
        this.addRelation("addresspath", "ADDRESSPATH");
        this.addRelation("cachepath", "CACHEPATH");
        this.addRelation("addressfile", "ADDRESSFILE");
        this.addRelation("filesize", "FILESIZE");
        this.addRelation("sourceurl", "SOURCEURL");
        this.addRelation("uploadtype", "UPLOADTYPE");
        this.addRelation("seriesno", "SERIESNO");
        this.addRelation("definition", "DEFINITION");
        this.addRelation("sourcetype", "SOURCETYPE");
        this.addRelation("parentindex", "PARENTINDEX");
        this.addRelation("duration", "DURATION");
        this.addRelation("bitrate", "BITRATE");
        this.addRelation("numberofframes", "NUMBEROFFRAMES");
        this.addRelation("frameheight", "FRAMEHEIGHT");
        this.addRelation("framewidth", "FRAMEWIDTH");
        this.addRelation("aspectratio", "ASPECTRATIO");
        this.addRelation("framerate", "FRAMERATE");
        this.addRelation("brightness", "BRIGHTNESS");
        this.addRelation("contrast", "CONTRAST");
        this.addRelation("videocodec", "VIDEOCODEC");
        this.addRelation("audiocodec", "AUDIOCODEC");
        this.addRelation("usemode", "USEMODE");
        this.addRelation("bitratecode", "BITRATECODE");
        this.addRelation("programname", "PROGRAMNAME");
        this.addRelation("starttime", "STARTTIME");
        this.addRelation("endtime", "ENDTIME");
        this.addRelation("encodingformat", "ENCODINGFORMAT");
        this.addRelation("packageformat", "PACKAGEFORMAT");
        this.addRelation("resolution", "RESOLUTION");
        this.addRelation("adownload", "ADOWNLOAD");
        this.addRelation("ismainfile", "ISMAINFILE");
        this.addRelation("syncresultstatus", "SYNCRESULTSTATUS");
        this.addRelation("syncerrorcode", "SYNCERRORCODE");
        this.addRelation("syncerrordesc", "SYNCERRORDESC");
        this.addRelation("posttype", "POSTTYPE");
        this.addRelation("encryptedfilename", "ENCRYPTEDFILENAME");
        this.addRelation("liveurl", "LIVEURL");
        this.addRelation("previewtag", "PREVIEWTAG");
        this.addRelation("drmfiletype", "DRMFILETYPE");
        this.addRelation("snapexist", "SNAPEXIST");
        this.addRelation("snapbegintime", "SNAPBEGINTIME");
        this.addRelation("snapendtime", "SNAPENDTIME");
        this.addRelation("timelength", "TIMELENGTH");
        this.addRelation("siteno", "SITENO");
        this.addRelation("playnums", "PLAYNUMS");
        this.addRelation("catagorycode", "CATAGORYCODE");
        this.addRelation("publishrate", "PUBLISHRATE");
        this.addRelation("prop01", "PROP01");
        this.addRelation("prop02", "PROP02");
        this.addRelation("prop03", "PROP03");
        this.addRelation("prop04", "PROP04");
        this.addRelation("prop05", "PROP05");
        this.addRelation("prop06", "PROP06");
        this.addRelation("prop07", "PROP07");
        this.addRelation("prop08", "PROP08");
        this.addRelation("prop09", "PROP09");
        this.addRelation("prop10", "PROP10");
        this.addRelation("templetid", "TEMPLETID");
        this.addRelation("keywords", "KEYWORDS");
        this.addRelation("country", "COUNTRY");
        this.addRelation("director", "DIRECTOR");
        this.addRelation("actor", "ACTOR");
        this.addRelation("playtime", "PLAYTIME");
        this.addRelation("introduction", "INTRODUCTION");
        this.addRelation("subtitle", "SUBTITLE");
        this.addRelation("subtitlelanguage", "SUBTITLELANGUAGE");
        this.addRelation("dubbinglanguage", "DUBBINGLANGUAGE");
        this.addRelation("cmsid", "CMSID");
        this.addRelation("servicekey", "SERVICEKEY");
    }
}

package com.zte.cms.cntSyncRecord.service;

import java.util.List;
import com.zte.cms.cntSyncRecord.model.CntSyncRecord;
import com.zte.cms.cntSyncRecord.dao.ICntSyncRecordDAO;
import com.zte.cms.cntSyncRecord.service.ICntSyncRecordDS;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;
import com.zte.ssb.framework.exception.exceptions.DAOException;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.SSBBus;

public class CntSyncRecordDS implements ICntSyncRecordDS
{
    // 日志
    private Log log = SSBBus.getLog(getClass());

    private ICntSyncRecordDAO dao = null;

    public void setDao(ICntSyncRecordDAO dao)
    {
        this.dao = dao;
    }

    public void insertCntSyncRecord(CntSyncRecord cntSyncRecord) throws DomainServiceException
    {
        log.debug("insert cntSyncRecord starting...");
        try
        {
            dao.insertCntSyncRecord(cntSyncRecord);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("insert cntSyncRecord end");
    }

    public void updateCntSyncRecord(CntSyncRecord cntSyncRecord) throws DomainServiceException
    {
        log.debug("update cntSyncRecord by pk starting...");
        try
        {
            dao.updateCntSyncRecord(cntSyncRecord);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("update cntSyncRecord by pk end");
    }

    public void updateCntSyncRecordList(List<CntSyncRecord> cntSyncRecordList) throws DomainServiceException
    {
        log.debug("update cntSyncRecordList by pk starting...");
        if (null == cntSyncRecordList || cntSyncRecordList.size() == 0)
        {
            log.debug("there is no datas in cntSyncRecordList");
            return;
        }
        try
        {
            for (CntSyncRecord cntSyncRecord : cntSyncRecordList)
            {
                dao.updateCntSyncRecord(cntSyncRecord);
            }
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("update cntSyncRecordList by pk end");
    }

    public void removeCntSyncRecord(CntSyncRecord cntSyncRecord) throws DomainServiceException
    {
        log.debug("remove cntSyncRecord by pk starting...");
        try
        {
            dao.deleteCntSyncRecord(cntSyncRecord);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("remove cntSyncRecord by pk end");
    }

    public void removeCntSyncRecordList(List<CntSyncRecord> cntSyncRecordList) throws DomainServiceException
    {
        log.debug("remove cntSyncRecordList by pk starting...");
        if (null == cntSyncRecordList || cntSyncRecordList.size() == 0)
        {
            log.debug("there is no datas in cntSyncRecordList");
            return;
        }
        try
        {
            for (CntSyncRecord cntSyncRecord : cntSyncRecordList)
            {
                dao.deleteCntSyncRecord(cntSyncRecord);
            }
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("remove cntSyncRecordList by pk end");
    }

    public CntSyncRecord getCntSyncRecord(CntSyncRecord cntSyncRecord) throws DomainServiceException
    {
        log.debug("get cntSyncRecord by pk starting...");
        CntSyncRecord rsObj = null;
        try
        {
            rsObj = dao.getCntSyncRecord(cntSyncRecord);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("get cntSyncRecordList by pk end");
        return rsObj;
    }

    public List<CntSyncRecord> getCntSyncRecordByCond(CntSyncRecord cntSyncRecord) throws DomainServiceException
    {
        log.debug("get cntSyncRecord by condition starting...");
        List<CntSyncRecord> rsList = null;
        try
        {
            rsList = dao.getCntSyncRecordByCond(cntSyncRecord);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("get cntSyncRecord by condition end");
        return rsList;
    }

    @SuppressWarnings("unchecked")
    public TableDataInfo pageInfoQuery(CntSyncRecord cntSyncRecord, int start, int pageSize)
            throws DomainServiceException
    {
        log.debug("get cntSyncRecord page info by condition starting...");
        PageInfo pageInfo = null;
        try
        {
            pageInfo = dao.pageInfoQuery(cntSyncRecord, start, pageSize);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        TableDataInfo tableInfo = new TableDataInfo();
        tableInfo.setData((List<CntSyncRecord>) pageInfo.getResult());
        tableInfo.setTotalCount((int) pageInfo.getTotalCount());
        log.debug("get cntSyncRecord page info by condition end");
        return tableInfo;
    }

    @SuppressWarnings("unchecked")
    public TableDataInfo pageInfoQuery(CntSyncRecord cntSyncRecord, int start, int pageSize, PageUtilEntity puEntity)
            throws DomainServiceException
    {
        log.debug("get cntSyncRecord page info by condition starting...");
        PageInfo pageInfo = null;
        try
        {
            pageInfo = dao.pageInfoQuery(cntSyncRecord, start, pageSize, puEntity);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        TableDataInfo tableInfo = new TableDataInfo();
        tableInfo.setData((List<CntSyncRecord>) pageInfo.getResult());
        tableInfo.setTotalCount((int) pageInfo.getTotalCount());
        log.debug("get cntSyncRecord page info by condition end");
        return tableInfo;
    }
}

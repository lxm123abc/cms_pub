package com.zte.cms.content.service;

import java.util.List;
import com.zte.cms.content.model.CmsSplittask;
import com.zte.cms.content.dao.ICmsSplittaskDAO;
import com.zte.cms.content.service.ICmsSplittaskDS;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;
import com.zte.ssb.framework.exception.exceptions.DAOException;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.SSBBus;
import com.zte.umap.common.dynamicobj.DynamicObjectBaseDS;

public class CmsSplittaskDS extends DynamicObjectBaseDS implements ICmsSplittaskDS
{
    // 日志
    private Log log = SSBBus.getLog(getClass());

    private ICmsSplittaskDAO dao = null;

    public void setDao(ICmsSplittaskDAO dao)
    {
        this.dao = dao;
    }

    public void insertCmsSplittask(CmsSplittask cmsSplittask) throws DomainServiceException
    {
        log.debug("insert cmsSplittask starting...");
        try
        {
            Long taskindex;
            if (cmsSplittask.getTaskindex() == null)
            {
                taskindex = (Long) this.getPrimaryKeyGenerator().getPrimarykey("cms_splittask");
                cmsSplittask.setTaskindex(taskindex);
            }

            dao.insertCmsSplittask(cmsSplittask);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("insert cmsSplittask end");
    }

    public void updateCmsSplittask(CmsSplittask cmsSplittask) throws DomainServiceException
    {
        log.debug("update cmsSplittask by pk starting...");
        try
        {
            dao.updateCmsSplittask(cmsSplittask);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("update cmsSplittask by pk end");
    }

    public void updateCmsSplittaskList(List<CmsSplittask> cmsSplittaskList) throws DomainServiceException
    {
        log.debug("update cmsSplittaskList by pk starting...");
        if (null == cmsSplittaskList || cmsSplittaskList.size() == 0)
        {
            log.debug("there is no datas in cmsSplittaskList");
            return;
        }
        try
        {
            for (CmsSplittask cmsSplittask : cmsSplittaskList)
            {
                dao.updateCmsSplittask(cmsSplittask);
            }
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("update cmsSplittaskList by pk end");
    }

    public void updateCmsSplittaskByCond(CmsSplittask cmsSplittask) throws DomainServiceException
    {
        log.debug("update cmsSplittask by condition starting...");
        try
        {
            dao.updateCmsSplittaskByCond(cmsSplittask);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("update cmsSplittask by condition end");
    }

    public void updateCmsSplittaskListByCond(List<CmsSplittask> cmsSplittaskList) throws DomainServiceException
    {
        log.debug("update cmsSplittaskList by condition starting...");
        if (null == cmsSplittaskList || cmsSplittaskList.size() == 0)
        {
            log.debug("there is no datas in cmsSplittaskList");
            return;
        }
        try
        {
            for (CmsSplittask cmsSplittask : cmsSplittaskList)
            {
                dao.updateCmsSplittaskByCond(cmsSplittask);
            }
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("update cmsSplittaskList by condition end");
    }

    public void removeCmsSplittask(CmsSplittask cmsSplittask) throws DomainServiceException
    {
        log.debug("remove cmsSplittask by pk starting...");
        try
        {
            dao.deleteCmsSplittask(cmsSplittask);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("remove cmsSplittask by pk end");
    }

    public void removeCmsSplittaskList(List<CmsSplittask> cmsSplittaskList) throws DomainServiceException
    {
        log.debug("remove cmsSplittaskList by pk starting...");
        if (null == cmsSplittaskList || cmsSplittaskList.size() == 0)
        {
            log.debug("there is no datas in cmsSplittaskList");
            return;
        }
        try
        {
            for (CmsSplittask cmsSplittask : cmsSplittaskList)
            {
                dao.deleteCmsSplittask(cmsSplittask);
            }
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("remove cmsSplittaskList by pk end");
    }

    public void removeCmsSplittaskByCond(CmsSplittask cmsSplittask) throws DomainServiceException
    {
        log.debug("remove cmsSplittask by condition starting...");
        try
        {
            dao.deleteCmsSplittaskByCond(cmsSplittask);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("remove cmsSplittask by condition end");
    }

    public void removeCmsSplittaskListByCond(List<CmsSplittask> cmsSplittaskList) throws DomainServiceException
    {
        log.debug("remove cmsSplittaskList by condition starting...");
        if (null == cmsSplittaskList || cmsSplittaskList.size() == 0)
        {
            log.debug("there is no datas in cmsSplittaskList");
            return;
        }
        try
        {
            for (CmsSplittask cmsSplittask : cmsSplittaskList)
            {
                dao.deleteCmsSplittaskByCond(cmsSplittask);
            }
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("remove cmsSplittaskList by condition end");
    }

    public CmsSplittask getCmsSplittask(CmsSplittask cmsSplittask) throws DomainServiceException
    {
        log.debug("get cmsSplittask by pk starting...");
        CmsSplittask rsObj = null;
        try
        {
            rsObj = dao.getCmsSplittask(cmsSplittask);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("get cmsSplittaskList by pk end");
        return rsObj;
    }

    public List<CmsSplittask> getCmsSplittaskByCond(CmsSplittask cmsSplittask) throws DomainServiceException
    {
        log.debug("get cmsSplittask by condition starting...");
        List<CmsSplittask> rsList = null;
        try
        {
            rsList = dao.getCmsSplittaskByCond(cmsSplittask);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("get cmsSplittask by condition end");
        return rsList;
    }

    @SuppressWarnings("unchecked")
    public TableDataInfo pageInfoQuery(CmsSplittask cmsSplittask, int start, int pageSize)
            throws DomainServiceException
    {
        log.debug("get cmsSplittask page info by condition starting...");
        PageInfo pageInfo = null;
        try
        {
            pageInfo = dao.pageInfoQuery(cmsSplittask, start, pageSize);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        TableDataInfo tableInfo = new TableDataInfo();
        tableInfo.setData((List<CmsSplittask>) pageInfo.getResult());
        tableInfo.setTotalCount((int) pageInfo.getTotalCount());
        log.debug("get cmsSplittask page info by condition end");
        return tableInfo;
    }

    @SuppressWarnings("unchecked")
    public TableDataInfo pageInfoQuery(CmsSplittask cmsSplittask, int start, int pageSize, PageUtilEntity puEntity)
            throws DomainServiceException
    {
        log.debug("get cmsSplittask page info by condition starting...");
        PageInfo pageInfo = null;
        try
        {
            pageInfo = dao.pageInfoQuery(cmsSplittask, start, pageSize, puEntity);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        TableDataInfo tableInfo = new TableDataInfo();
        tableInfo.setData((List<CmsSplittask>) pageInfo.getResult());
        tableInfo.setTotalCount((int) pageInfo.getTotalCount());
        log.debug("get cmsSplittask page info by condition end");
        return tableInfo;
    }

    @SuppressWarnings("unchecked")
    public TableDataInfo pageInfoQuerySplit(CmsSplittask cmsSplittask, int start, int pageSize)
            throws DomainServiceException
    {
        log.debug("pageInfoQuerySqlit1 starting...");
        PageInfo pageInfo = null;
        try
        {
            pageInfo = dao.pageInfoQuerySplit(cmsSplittask, start, pageSize);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        TableDataInfo tableInfo = new TableDataInfo();
        tableInfo.setData((List<CmsSplittask>) pageInfo.getResult());
        tableInfo.setTotalCount((int) pageInfo.getTotalCount());
        log.debug("pageInfoQuerySqlit1 end");
        return tableInfo;
    }

    @SuppressWarnings("unchecked")
    public TableDataInfo pageInfoQuerySplit(CmsSplittask cmsSplittask, int start, int pageSize, PageUtilEntity puEntity)
            throws DomainServiceException
    {
        log.debug("pageInfoQuerySqlit2 starting...");
        PageInfo pageInfo = null;
        try
        {
            pageInfo = dao.pageInfoQuerySplit(cmsSplittask, start, pageSize, puEntity);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        TableDataInfo tableInfo = new TableDataInfo();
        tableInfo.setData((List<CmsSplittask>) pageInfo.getResult());
        tableInfo.setTotalCount((int) pageInfo.getTotalCount());
        log.debug("pageInfoQuerySqlit2 end");
        return tableInfo;
    }
}

package com.zte.cms.castrolemap.model;

import com.zte.ssb.dynamicobj.DynamicBaseObject;

public class Castrolemap extends DynamicBaseObject
{
    private java.lang.Long castrolemapindex;
    private java.lang.String castrolemapid;
    private java.lang.Long castindex;
    private java.lang.String castid;
    private java.lang.String castrole;
    private java.lang.Integer roletype;
    private java.lang.Integer status;
    private java.lang.String createtime;
    private java.lang.String publishtime;
    private java.lang.String description;
    private java.lang.String castrolemapcode;
    private java.lang.String cancelpubtime;
    private java.lang.String castname;

    private java.lang.String statuses;
    
    public java.lang.String getCastname()
    {
        return castname;
    }

    public void setCastname(java.lang.String castname)
    {
        this.castname = castname;
    }

    public java.lang.String getCancelpubtime()
    {
        return cancelpubtime;
    }

    public void setCancelpubtime(java.lang.String cancelpubtime)
    {
        this.cancelpubtime = cancelpubtime;
    }

    public java.lang.Long getCastrolemapindex()
    {
        return castrolemapindex;
    }

    public void setCastrolemapindex(java.lang.Long castrolemapindex)
    {
        this.castrolemapindex = castrolemapindex;
    }

    public java.lang.String getCastrolemapid()
    {
        return castrolemapid;
    }

    public void setCastrolemapid(java.lang.String castrolemapid)
    {
        this.castrolemapid = castrolemapid;
    }

    public java.lang.Long getCastindex()
    {
        return castindex;
    }

    public void setCastindex(java.lang.Long castindex)
    {
        this.castindex = castindex;
    }

    public java.lang.String getCastid()
    {
        return castid;
    }

    public void setCastid(java.lang.String castid)
    {
        this.castid = castid;
    }

    public java.lang.String getCastrole()
    {
        return castrole;
    }

    public void setCastrole(java.lang.String castrole)
    {
        this.castrole = castrole;
    }

    public java.lang.Integer getRoletype()
    {
        return roletype;
    }

    public void setRoletype(java.lang.Integer roletype)
    {
        this.roletype = roletype;
    }

    public java.lang.Integer getStatus()
    {
        return status;
    }

    public void setStatus(java.lang.Integer status)
    {
        this.status = status;
    }

    public java.lang.String getCreatetime()
    {
        return createtime;
    }

    public void setCreatetime(java.lang.String createtime)
    {
        this.createtime = createtime;
    }

    public java.lang.String getPublishtime()
    {
        return publishtime;
    }

    public void setPublishtime(java.lang.String publishtime)
    {
        this.publishtime = publishtime;
    }

    public java.lang.String getDescription()
    {
        return description;
    }

    public void setDescription(java.lang.String description)
    {
        this.description = description;
    }

    public java.lang.String getCastrolemapcode()
    {
        return castrolemapcode;
    }

    public void setCastrolemapcode(java.lang.String castrolemapcode)
    {
        this.castrolemapcode = castrolemapcode;
    }

    public void initRelation()
    {
        this.addRelation("castrolemapindex", "CASTROLEMAPINDEX");
        this.addRelation("castrolemapid", "CASTROLEMAPID");
        this.addRelation("castindex", "CASTINDEX");
        this.addRelation("castid", "CASTID");
        this.addRelation("castrole", "CASTROLE");
        this.addRelation("roletype", "ROLETYPE");
        this.addRelation("status", "STATUS");
        this.addRelation("createtime", "CREATETIME");
        this.addRelation("publishtime", "PUBLISHTIME");
        this.addRelation("description", "DESCRIPTION");
        this.addRelation("castrolemapcode", "CASTROLEMAPCODE");
        this.addRelation("cancelpubtime", "CANCELPUBTIME");
    }

    public java.lang.String getStatuses()
    {
        return statuses;
    }

    public void setStatuses(java.lang.String statuses)
    {
        this.statuses = statuses;
    }
}

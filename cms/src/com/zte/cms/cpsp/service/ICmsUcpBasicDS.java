package com.zte.cms.cpsp.service;

import com.zte.cms.cpsp.model.UcpBasicModel;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;
import com.zte.umap.cpsp.common.model.UcpBasic;

import java.util.List;

public interface ICmsUcpBasicDS {
    /**
     * 根据条件分页查询CmsCpbind对象
     *
     * @param cmsCpbind CmsCpbind对象，作为查询条件的参数
     * @param start     起始行
     * @param pageSize  页面大小
     * @param puEntity  排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws DomainServiceException ds异常
     */
    public TableDataInfo pageInfoQuery(UcpBasic ucpBasic, int start, int pageSize) throws DomainServiceException;

    /**
     * 根据条件分页查询CmsCpbind对象
     *
     * @param ucpBasic CmsCpbind对象，作为查询条件的参数
     * @param start    起始行
     * @param pageSize 页面大小
     * @param puEntity 排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws DomainServiceException ds异常
     */
    public TableDataInfo pageInfoQuery(UcpBasic ucpBasic, int start, int pageSize, PageUtilEntity puEntity)
            throws DomainServiceException;

    public boolean deleteCmsOperPower(String cpid);

    void insertUcpBasic(UcpBasicModel model) throws DomainServiceException;

    UcpBasicModel getUcpBasicByIndex(UcpBasicModel model);

    public void deleteUcpBasicFile(UcpBasicModel model);

    void updateUcpBasic(UcpBasicModel model) throws DomainServiceException;

    List<UcpBasicModel> checkQualificationDoc(String operid);

}
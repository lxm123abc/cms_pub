package com.zte.cms.content.wkfw.dao;

import com.zte.cms.content.wkfw.model.CntWkfwhis;
import com.zte.cms.content.wkfw.model.CntWkfwCondition;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DAOException;

/**
 * 内容订单工作流查询的一系列接口
 * 
 * @author 李梅 li.mei3@zte.com.cn
 * @version ZXCMS
 */
public interface ICntWkfwDAO
{

    /**
     * <p>
     * 根据查询条件分页查询需要处理的内容订单工单信息
     * </p>
     * 
     * @param cntorderWkfwCondition 内容订单工作流查询条件公共类
     * @param start 起始行
     * @param pageSize 页面大小
     * @param puEntity 表格中需要排序的字段实体
     * @return 返回查询结果，Cntorder信息列表
     * @throws DAOException
     * @since ZXCMS
     */
    public PageInfo getMyNeedHandleWorkflowItemList(CntWkfwCondition cntWkfwCondition, int start, int pageSize,
            PageUtilEntity puEntity) throws DAOException;

    /**
     * <p>
     * 根据查询条件查询需要处理的内容订单工单数量
     * </p>
     * 
     * @param cntorderWkfwCondition 内容订单工作流查询条件公共类
     * @return 返回查询结果，需要处理的内容订单工单数量
     * @throws DAOException
     * @since ZXCMS
     */
    public long getMyNeedHandleWorkflowItemListCnt(CntWkfwCondition cntWkfwCondition) throws DAOException;

    /**
     * 根据条件分页查询CmsProgramWkfwhis对象，作为查询条件的参数
     * 
     * @param cmsProgramWkfwhis CmsProgramWkfwhis对象
     * @param start 起始行
     * @param pageSize 页面大小
     * @param puEntity 排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws DAOException dao异常
     */
    public PageInfo pageInfoQuery(CntWkfwhis CntWkfwhis, int start, int pageSize, PageUtilEntity puEntity)
            throws DAOException;

    /**
     * 内容工单流程跟踪
     * 
     * @param cmsProgramWkfwhis
     * @param start
     * @param pageSize
     * @return
     * @throws DAOException
     */
    public PageInfo pageInfoQuery(CntWkfwhis CntWkfwhis, int start, int pageSize) throws DAOException;

    public long getNopassWorkflowCnt(CntWkfwhis cntWkfwis) throws DAOException;

    /**
     * 审核信息查看
     * 
     * @param cmsProgramWkfwhis
     * @return
     */
    public CntWkfwhis getCmsProgramWkfwhis(CntWkfwhis CntWkfwhis) throws DAOException;

}



package com.zte.cms.service.servicepublishmanager.service;

import java.util.List;


import com.zte.cms.service.servicepublishmanager.model.ServicePlatformSyn;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DAOException;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;

public interface IServicePlatformSynDS
{
   
	 
	 /**
	  * 根据条件查询CntTargetsysSyn对象 
	  * 
	  * @param ServicePlatformSyn CntTargetsysSyn对象
	  * @return 满足条件的CntTargetsysSyn对象集
	  * @throws DomainServiceException ds异常
	  */
     public List<ServicePlatformSyn> getCntTargetsysSynByCond( ServicePlatformSyn servicePlatformSyn )throws DomainServiceException;
     /**
      * 根据条件查询CntTargetsysSyn对象 
      * 
      * @param ServicePlatformSyn CntTargetsysSyn对象
      * @return 满足条件的CntTargetsysSyn对象集
      * @throws DomainServiceException ds异常
      */
     public ServicePlatformSyn getServicePlatformSynByIndex( ServicePlatformSyn servicePlatformSyn )throws DomainServiceException;

	 /**
	  * 根据条件分页查询CntTargetsysSyn对象 
	  *
	  * @param ServicePlatformSyn CntTargetsysSyn对象，作为查询条件的参数 
	  * @param start 起始行
	  * @param pageSize 页面大小
	  * @param puEntity 排序空置参数@see PageUtilEntity
	  * @return  查询结果
	  * @throws DomainServiceException ds异常
	  */
     public TableDataInfo pageInfoQuery(ServicePlatformSyn servicePlatformSyn, int start, int pageSize)throws DomainServiceException;
     
	 /**
	  * 根据条件分页查询CntTargetsysSyn对象 
	  *
	  * @param ServicePlatformSyn CntTargetsysSyn对象，作为查询条件的参数 
	  * @param start 起始行
	  * @param pageSize 页面大小
	  * @param puEntity 排序空置参数@see PageUtilEntity
	  * @return  查询结果
	  * @throws DomainServiceException ds异常
	  */
     public TableDataInfo pageInfoQuery(ServicePlatformSyn servicePlatformSyn, int start, int pageSize, PageUtilEntity puEntity)throws DomainServiceException;
     //按查询条件查询
 	 
    public  String  updateServicePlatformSyn(ServicePlatformSyn servicePlatformSyn) throws DomainServiceException;
    
    public TableDataInfo pageInfoQueryforServiceProgramBing(ServicePlatformSyn servicePlatformSyn, int start, int pageSize)throws DomainServiceException;
    public TableDataInfo pageInfoQueryforServiceSeriesBing(ServicePlatformSyn servicePlatformSyn, int start, int pageSize)throws DomainServiceException;
    public TableDataInfo pageInfoQueryforServiceChannelBing(ServicePlatformSyn servicePlatformSyn, int start, int pageSize)throws DomainServiceException;
    //根据parentid和platform来查找相同平台下父节点相同的mapping关系
    public List<ServicePlatformSyn> getSameParentPlatformSyn ( ServicePlatformSyn servicePlatformSyn ) throws DomainServiceException;
    
}
package com.zte.cms.storageArea.model;

import com.zte.ssb.dynamicobj.DynamicBaseObject;

public class CmsStoragearea extends DynamicBaseObject
{
    private java.lang.Long areaindex;
    private java.lang.String areaname;
    private java.lang.String areaid;
    private java.lang.Integer areastatus;
    private java.lang.String storagepercentage;
    private java.lang.String monitorthreshold;
    private java.lang.String description;
    private java.lang.Integer isbindedtospace;
    private java.lang.String varbackup1;
    private java.lang.String varbackup2;
    private java.lang.Long numbackup1; // 再次作为树节点的父Index
    private java.lang.Long numbackup2;
    

	public java.lang.Long getAreaindex()
    {
        return areaindex;
    }

    public void setAreaindex(java.lang.Long areaindex)
    {
        this.areaindex = areaindex;
    }

    public java.lang.String getAreaname()
    {
        return areaname;
    }

    public void setAreaname(java.lang.String areaname)
    {
        this.areaname = areaname;
    }

    public java.lang.String getAreaid()
    {
        return areaid;
    }

    public void setAreaid(java.lang.String areaid)
    {
        this.areaid = areaid;
    }

    public java.lang.Integer getAreastatus()
    {
        return areastatus;
    }

    public void setAreastatus(java.lang.Integer areastatus)
    {
        this.areastatus = areastatus;
    }

    public java.lang.String getStoragepercentage()
    {
        return storagepercentage;
    }

    public void setStoragepercentage(java.lang.String storagepercentage)
    {
        this.storagepercentage = storagepercentage;
    }

    public java.lang.String getMonitorthreshold()
    {
        return monitorthreshold;
    }

    public void setMonitorthreshold(java.lang.String monitorthreshold)
    {
        this.monitorthreshold = monitorthreshold;
    }

    public java.lang.String getDescription()
    {
        return description;
    }

    public void setDescription(java.lang.String description)
    {
        this.description = description;
    }

    public java.lang.Integer getIsbindedtospace()
    {
        return isbindedtospace;
    }

    public void setIsbindedtospace(java.lang.Integer isbindedtospace)
    {
        this.isbindedtospace = isbindedtospace;
    }

    public java.lang.String getVarbackup1()
    {
        return varbackup1;
    }

    public void setVarbackup1(java.lang.String varbackup1)
    {
        this.varbackup1 = varbackup1;
    }

    public java.lang.String getVarbackup2()
    {
        return varbackup2;
    }

    public void setVarbackup2(java.lang.String varbackup2)
    {
        this.varbackup2 = varbackup2;
    }

    public java.lang.Long getNumbackup1()
    {
        return numbackup1;
    }

    public void setNumbackup1(java.lang.Long numbackup1)
    {
        this.numbackup1 = numbackup1;
    }

    public java.lang.Long getNumbackup2()
    {
        return numbackup2;
    }

    public void setNumbackup2(java.lang.Long numbackup2)
    {
        this.numbackup2 = numbackup2;
    }

    public void initRelation()
    {
        this.addRelation("areaindex", "AREAINDEX");
        this.addRelation("areaname", "AREANAME");
        this.addRelation("areaid", "AREAID");
        this.addRelation("areastatus", "AREASTATUS");
        this.addRelation("storagepercentage", "STORAGEPERCENTAGE");
        this.addRelation("monitorthreshold", "MONITORTHRESHOLD");
        this.addRelation("description", "DESCRIPTION");
        this.addRelation("isbindedtospace", "ISBINDEDTOSPACE");
        this.addRelation("varbackup1", "VARBACKUP1");
        this.addRelation("varbackup2", "VARBACKUP2");
        this.addRelation("numbackup1", "NUMBACKUP1");
        this.addRelation("numbackup2", "NUMBACKUP2");
    }
}

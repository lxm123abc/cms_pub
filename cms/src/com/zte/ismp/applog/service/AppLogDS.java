package com.zte.ismp.applog.service;

import java.util.List;

import com.zte.ismp.applog.dao.IAppLogDAO;
import com.zte.ismp.common.constant.GlobalConstants;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.SSBBus;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.exception.exceptions.DAOException;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;
import com.zte.umap.common.LoginMgt;
import com.zte.umap.common.model.OperInfo;
import com.zte.umap.ssb.logmanager.business.model.VSsblog;

public class AppLogDS implements IAppLogDS
{
    // 日志
    private Log log = SSBBus.getLog(getClass());

    private IAppLogDAO appLogDao = null;

    @SuppressWarnings("unchecked")
    public TableDataInfo pageInfoQuery(VSsblog vSsblog, int start, int pageSize) throws DomainServiceException
    {
        log.debug("get vSsblog page info by condition starting...");
        PageInfo pageInfo = null;
        OperInfo operInfo = null;
        try
        {
            operInfo = LoginMgt.getOperInfo(GlobalConstants.SYSTEM_SERVICE_KEY);
            if (Long.parseLong(operInfo.getOperid()) != 1L)
            {
                vSsblog.setUserId(operInfo.getOperid());
            }
            pageInfo = appLogDao.pageInfoQuery(vSsblog, start, pageSize);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        TableDataInfo tableInfo = new TableDataInfo();
        tableInfo.setData((List<VSsblog>) pageInfo.getResult());
        tableInfo.setTotalCount((int) pageInfo.getTotalCount());
        log.debug("get vSsblog page info by condition end");
        return tableInfo;
    }

    @SuppressWarnings("unchecked")
    public TableDataInfo pageInfoQuery(VSsblog vSsblog, int start, int pageSize, PageUtilEntity puEntity)
            throws DomainServiceException
    {
        log.debug("get vSsblog page info by condition starting...");
        PageInfo pageInfo = null;
        OperInfo operInfo = null;
        try
        {
            operInfo = LoginMgt.getOperInfo(GlobalConstants.SYSTEM_SERVICE_KEY);
            if (Long.parseLong(operInfo.getOperid()) != 1L)
            {
                vSsblog.setUserId(operInfo.getOperid());
            }
            pageInfo = appLogDao.pageInfoQuery(vSsblog, start, pageSize, puEntity);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        TableDataInfo tableInfo = new TableDataInfo();
        tableInfo.setData((List<VSsblog>) pageInfo.getResult());
        tableInfo.setTotalCount((int) pageInfo.getTotalCount());
        log.debug("get vSsblog page info by condition end");
        return tableInfo;
    }

    public void setAppLogDao(IAppLogDAO appLogDao)
    {
        this.appLogDao = appLogDao;
    }
}

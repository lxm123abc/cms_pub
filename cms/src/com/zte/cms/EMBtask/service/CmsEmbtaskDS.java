package com.zte.cms.EMBtask.service;

import java.util.List;
import com.zte.cms.EMBtask.model.CmsEmbtask;
import com.zte.cms.EMBtask.dao.ICmsEmbtaskDAO;
import com.zte.cms.EMBtask.service.ICmsEmbtaskDS;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;
import com.zte.ssb.framework.exception.exceptions.DAOException;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.SSBBus;
import com.zte.umap.common.dynamicobj.DynamicObjectBaseDS;

public class CmsEmbtaskDS extends DynamicObjectBaseDS implements ICmsEmbtaskDS
{
    // 日志
    private Log log = SSBBus.getLog(getClass());

    private ICmsEmbtaskDAO dao = null;

    public void setDao(ICmsEmbtaskDAO dao)
    {
        this.dao = dao;
    }

    public void insertCmsEmbtask(CmsEmbtask cmsEmbtask) throws DomainServiceException
    {
        log.debug("insert cmsEmbtask starting...");
        try
        {
            if(cmsEmbtask.getTaskindex() == null)
            {
                Long id = (Long) this.getPrimaryKeyGenerator().getPrimarykey("cms_EMBtask_taskindex");
                cmsEmbtask.setTaskindex(id);
            }
            if (cmsEmbtask.getSourceindex() == null&&cmsEmbtask.getTaskindex() != null)
            {
                cmsEmbtask.setSourceindex(cmsEmbtask.getTaskindex());
            }

            dao.insertCmsEmbtask(cmsEmbtask);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("insert cmsEmbtask end");
    }

    public void updateCmsEmbtask(CmsEmbtask cmsEmbtask) throws DomainServiceException
    {
        log.debug("update cmsEmbtask by pk starting...");
        try
        {
            dao.updateCmsEmbtask(cmsEmbtask);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("update cmsEmbtask by pk end");
    }

    public void updateCmsEmbtaskList(List<CmsEmbtask> cmsEmbtaskList) throws DomainServiceException
    {
        log.debug("update cmsEmbtaskList by pk starting...");
        if (null == cmsEmbtaskList || cmsEmbtaskList.size() == 0)
        {
            log.debug("there is no datas in cmsEmbtaskList");
            return;
        }
        try
        {
            for (CmsEmbtask cmsEmbtask : cmsEmbtaskList)
            {
                dao.updateCmsEmbtask(cmsEmbtask);
            }
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("update cmsEmbtaskList by pk end");
    }

    public void removeCmsEmbtask(CmsEmbtask cmsEmbtask) throws DomainServiceException
    {
        log.debug("remove cmsEmbtask by pk starting...");
        try
        {
            dao.deleteCmsEmbtask(cmsEmbtask);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("remove cmsEmbtask by pk end");
    }

    public void removeCmsEmbtaskList(List<CmsEmbtask> cmsEmbtaskList) throws DomainServiceException
    {
        log.debug("remove cmsEmbtaskList by pk starting...");
        if (null == cmsEmbtaskList || cmsEmbtaskList.size() == 0)
        {
            log.debug("there is no datas in cmsEmbtaskList");
            return;
        }
        try
        {
            for (CmsEmbtask cmsEmbtask : cmsEmbtaskList)
            {
                dao.deleteCmsEmbtask(cmsEmbtask);
            }
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("remove cmsEmbtaskList by pk end");
    }

    public CmsEmbtask getCmsEmbtask(CmsEmbtask cmsEmbtask) throws DomainServiceException
    {
        log.debug("get cmsEmbtask by pk starting...");
        CmsEmbtask rsObj = null;
        try
        {
            rsObj = dao.getCmsEmbtask(cmsEmbtask);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("get cmsEmbtaskList by pk end");
        return rsObj;
    }

    public List<CmsEmbtask> getCmsEmbtaskByCond(CmsEmbtask cmsEmbtask) throws DomainServiceException
    {
        log.debug("get cmsEmbtask by condition starting...");
        List<CmsEmbtask> rsList = null;
        try
        {
            rsList = dao.getCmsEmbtaskByCond(cmsEmbtask);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("get cmsEmbtask by condition end");
        return rsList;
    }

    @SuppressWarnings("unchecked")
    public TableDataInfo pageInfoQuery(CmsEmbtask cmsEmbtask, int start, int pageSize) throws DomainServiceException
    {
        log.debug("get cmsEmbtask page info by condition starting...");
        PageInfo pageInfo = null;
        try
        {
            pageInfo = dao.pageInfoQuery(cmsEmbtask, start, pageSize);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        TableDataInfo tableInfo = new TableDataInfo();
        tableInfo.setData((List<CmsEmbtask>) pageInfo.getResult());
        tableInfo.setTotalCount((int) pageInfo.getTotalCount());
        log.debug("get cmsEmbtask page info by condition end");
        return tableInfo;
    }

    @SuppressWarnings("unchecked")
    public TableDataInfo pageInfoQuerySub(CmsEmbtask cmsEmbtask, int start, int pageSize) throws DomainServiceException
    {
        log.debug("get cmsEmbtask page info by condition starting...");
        PageInfo pageInfo = null;
        try
        {
            pageInfo = dao.pageInfoQuerySub(cmsEmbtask, start, pageSize);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        TableDataInfo tableInfo = new TableDataInfo();
        tableInfo.setData((List<CmsEmbtask>) pageInfo.getResult());
        tableInfo.setTotalCount((int) pageInfo.getTotalCount());
        log.debug("get cmsEmbtask page info by condition end");
        return tableInfo;
    }

    @SuppressWarnings("unchecked")
    public TableDataInfo pageInfoQuerySub(CmsEmbtask cmsEmbtask, int start, int pageSize, PageUtilEntity puEntity)
            throws DomainServiceException
    {
        log.debug("get cmsEmbtask page info by condition starting...");
        PageInfo pageInfo = null;
        try
        {
            pageInfo = dao.pageInfoQuerySub(cmsEmbtask, start, pageSize, puEntity);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        TableDataInfo tableInfo = new TableDataInfo();
        tableInfo.setData((List<CmsEmbtask>) pageInfo.getResult());
        tableInfo.setTotalCount((int) pageInfo.getTotalCount());
        log.debug("get cmsEmbtask page info by condition end");
        return tableInfo;
    }

    @SuppressWarnings("unchecked")
    public TableDataInfo pageInfoQueryEncryption(CmsEmbtask cmsEmbtask, int start, int pageSize, PageUtilEntity puEntity)
            throws DomainServiceException
    {
        log.debug("get cmsEmbtask page info by condition starting...");
        PageInfo pageInfo = null;
        try
        {
            pageInfo = dao.pageInfoQueryEncryption(cmsEmbtask, start, pageSize, puEntity);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        TableDataInfo tableInfo = new TableDataInfo();
        tableInfo.setData((List<CmsEmbtask>) pageInfo.getResult());
        tableInfo.setTotalCount((int) pageInfo.getTotalCount());
        log.debug("get cmsEmbtask page info by condition end");
        return tableInfo;
    }
    
    
    @SuppressWarnings("unchecked")
    public TableDataInfo pageInfoQuery(CmsEmbtask cmsEmbtask, int start, int pageSize, PageUtilEntity puEntity)
            throws DomainServiceException
    {
        log.debug("get cmsEmbtask page info by condition starting...");
        PageInfo pageInfo = null;
        try
        {
            pageInfo = dao.pageInfoQuery(cmsEmbtask, start, pageSize, puEntity);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        TableDataInfo tableInfo = new TableDataInfo();
        tableInfo.setData((List<CmsEmbtask>) pageInfo.getResult());
        tableInfo.setTotalCount((int) pageInfo.getTotalCount());
        log.debug("get cmsEmbtask page info by condition end");
        return tableInfo;
    }
}

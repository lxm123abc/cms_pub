package com.zte.cms.cpsp.service;

import java.util.Date;
import java.util.List;
import java.util.Map;

import com.zte.cms.common.DbUtil;
import com.zte.cms.cppr.model.CmsCppr;
import com.zte.cms.cpsp.dao.ICmsUcpBasicDAO;
import com.zte.cms.cpsp.model.UcpBasicModel;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.SSBBus;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.exception.exceptions.DAOException;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;
import com.zte.umap.common.EspecialCharMgt;
import com.zte.umap.cpsp.common.model.UcpBasic;

public class CmsUcpBasicDS extends com.zte.umap.common.dynamicobj.DynamicObjectBaseDS implements ICmsUcpBasicDS {
    private Log log = SSBBus.getLog(getClass());

    private ICmsUcpBasicDAO dao = null;

    public void setDao(ICmsUcpBasicDAO dao) {
        this.dao = dao;
    }

    /**
     * 根据条件分页查询ucpBasic对象
     *
     * @param ucpBasic ucpBasic对象，作为查询条件的参数
     * @param start    起始行
     * @param pageSize 页面大小
     * @return 查询结果
     * @throws DomainServiceException ds异常
     */
    public TableDataInfo pageInfoQuery(UcpBasic ucpBasic, int start, int pageSize) throws DomainServiceException {
        log.debug("get ucpBasic page info by condition starting...");
        PageInfo pageInfo = null;
        try {
            pageInfo = dao.pageInfoQuery(ucpBasic, start, pageSize);
        } catch (DAOException daoEx) {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        TableDataInfo tableInfo = new TableDataInfo();
        tableInfo.setData((List<UcpBasic>) pageInfo.getResult());
        tableInfo.setTotalCount((int) pageInfo.getTotalCount());
        log.debug("get ucpBasic page info by condition end");
        return tableInfo;
    }

    /**
     * 根据条件分页查询UcpBasic对象
     *
     * @param ucpBasic ucpBasic对象，作为查询条件的参数
     * @param start    起始行
     * @param pageSize 页面大小
     * @param puEntity 排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws DomainServiceException ds异常
     */
    public TableDataInfo pageInfoQuery(UcpBasic ucpBasic, int start, int pageSize, PageUtilEntity puEntity) throws DomainServiceException {
        log.debug("get UcpBasic page info by condition starting...");
        PageInfo pageInfo = null;
        try {
            pageInfo = dao.pageInfoQuery(ucpBasic, start, pageSize, puEntity);
        } catch (DAOException daoEx) {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        TableDataInfo tableInfo = new TableDataInfo();

        tableInfo.setData((List<UcpBasic>) pageInfo.getResult());
        tableInfo.setTotalCount((int) pageInfo.getTotalCount());
        log.debug("get UcpBasic page info by condition end");
        return tableInfo;
    }

    public boolean deleteCmsOperPower(String cpid) {
        CmsCppr cmsCppr = new CmsCppr();
        cmsCppr.setCpid(cpid);
        try {
            dao.deleteCmsOperPower(cmsCppr);
            return true;
        } catch (DAOException e) {
            // TODO Auto-generated catch block
            log.error("dao exception:", e);
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public void insertUcpBasic(UcpBasicModel model) throws DomainServiceException {
        this.log.debug("insert ucpBasic starting...");

        try {
            this.dao.insertUcpBasic(model);
        } catch (DAOException var3) {
            this.log.error("dao exception:", var3);
            throw new DomainServiceException(var3);
        }

        this.log.debug("insert ucpBasic end");
    }

    @Override
    public UcpBasicModel getUcpBasicByIndex(UcpBasicModel model) {
        return this.dao.getUcpBasicByIndex(model);
    }

    @Override
    public void deleteUcpBasicFile(UcpBasicModel model) {
        dao.deleteUcpBasicFile(model);
    }

    public void updateUcpBasic(UcpBasicModel model) throws DomainServiceException {
        try {
            this.convertTimeParams(model);
            this.dao.updateUcpBasic(model);
        } catch (DAOException var3) {
            this.log.error("dao exception:", var3);
            throw new DomainServiceException(var3);
        }
    }

    @Override
    public List<UcpBasicModel> checkQualificationDoc(String operid) {
        return dao.checkQualificationDoc(operid);
    }

    private void convertTimeParams(UcpBasic ucpBasic) {
        ucpBasic.setChangetime(EspecialCharMgt.conversionDateToStr(new Date(), 14));
    }

}

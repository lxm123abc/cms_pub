package com.zte.cms.notice.service;

import java.util.List;

import org.apache.commons.lang.StringUtils;

import com.zte.cms.common.GlobalConstants;
import com.zte.cms.notice.dao.INoticemapDAO;
import com.zte.cms.notice.model.Noticemap;
import com.zte.purview.access.model.OperInformation;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.SSBBus;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.exception.exceptions.DAOException;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;
import com.zte.umap.common.LoginMgt;
import com.zte.umap.common.dynamicobj.DynamicObjectBaseDS;
import com.zte.umap.common.model.OperInfo;

public class NoticemapService extends DynamicObjectBaseDS implements INoticemapService
{
    // 日志
    private Log log = SSBBus.getLog(getClass());

    private INoticemapDAO dao = null;

    public void setDao(INoticemapDAO dao)
    {
        this.dao = dao;
    }

    @SuppressWarnings("unchecked")
    public TableDataInfo pageInfoQuery(Noticemap noticemap, int start, int pageSize) throws DomainServiceException
    {
        log.info("get noticemap page info by condition starting...");
        PageInfo pageInfo = null;
        try
        {
            pageInfo = dao.pageInfoQuery(noticemap, start, pageSize);
        }
        catch (DAOException daoEx)
        {
            daoEx.printStackTrace();
            log.error("dao exception:", daoEx);
            throw new DomainServiceException(daoEx);
        }
        TableDataInfo tableInfo = new TableDataInfo();
        //判断消息是发送消息（0）还是接受的消息（1）
        List<Noticemap> noticeList=(List<Noticemap>) pageInfo.getResult();
        OperInfo loginUser = LoginMgt.getOperInfo(GlobalConstants.IPTV_SERVICE_KEY);
        // 操作员编码
        String operId = loginUser.getOperid();
        for(int i=0,size=noticeList.size();i<size;i++){
            if(noticeList.get(i).getCreateid().equals(operId)){
                noticeList.get(i).setCreateid("0");
            }else{
                noticeList.get(i).setCreateid("1");
            }
        }
        tableInfo.setData(noticeList);
        tableInfo.setTotalCount((int) pageInfo.getTotalCount());
        log.info("get noticemap page info by condition end");
        return tableInfo;
    }

    @SuppressWarnings("unchecked")
    public TableDataInfo pageInfoQuery(Noticemap noticemap, int start, int pageSize, PageUtilEntity puEntity)
                                                                                                              throws DomainServiceException
    {
        log.info("get noticemap page info by condition starting...");
        PageInfo pageInfo = null;
        try
        {
            pageInfo = dao.pageInfoQuery(noticemap, start, pageSize, puEntity);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            throw new DomainServiceException(daoEx);
        }
        TableDataInfo tableInfo = new TableDataInfo();
        tableInfo.setData((List<Noticemap>) pageInfo.getResult());
        tableInfo.setTotalCount((int) pageInfo.getTotalCount());
        log.info("get noticemap page info by condition end");
        return tableInfo;
    }

    public Noticemap getNoticemap(Noticemap noticemap) throws DomainServiceException
    {
        log.info("get noticemap by pk starting...");
        Noticemap rsObj = null;
        try
        {
            rsObj = dao.getNoticemap(noticemap);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            throw new DomainServiceException(daoEx);
        }
        log.info("get noticemap by pk end");
        return rsObj;
    }
    
    public Noticemap getNoticeLettermap(Noticemap noticemap) throws DomainServiceException
    {
        log.info("get noticemap by pk starting...");
        Noticemap rsObj = null;
        try
        {
            rsObj = dao.getNoticeLettermap(noticemap);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            throw new DomainServiceException(daoEx);
        }
        log.info("get noticemap by pk end");
        return rsObj;
    }

    public Noticemap getCmsOperatorPowerInfo(Noticemap noticemap) throws DomainServiceException
    {
        log.info("getCmsOperatorPowerInfo by pk starting...");
        Noticemap rsObj = null;
        try
        {
            rsObj = dao.getCmsOperatorPowerInfo(noticemap);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            throw new DomainServiceException(daoEx);
        }
        log.info("getCmsOperatorPowerInfo by pk end");
        return rsObj;
    }

    public String insertNoticemap(Noticemap noticemap) throws DomainServiceException
    {
        log.info("insert Noticemap starting...");
        Long messageid = null;
        Long attachid = null;
        try
        {
            messageid = (Long) this.getPrimaryKeyGenerator().getPrimarykey("messageid_index");
            noticemap.setMessageid(Integer.parseInt(messageid + ""));
            dao.insertNoticemap(noticemap);
            
            String attachname = noticemap.getAttachname();
            if(StringUtils.isNotEmpty(attachname)) {
            	attachid = (Long) this.getPrimaryKeyGenerator().getPrimarykey("attachid_index");
            	noticemap.setAttachid(Integer.parseInt(attachid + ""));
            	noticemap.setAttachurl(attachid + attachname.substring(attachname.lastIndexOf(".")));
            	dao.insertAttach(noticemap);
            }
        }
        catch (Exception daoEx)
        {
            log.error("dao exception:", daoEx);
            throw new DomainServiceException(daoEx);
        }
        log.info("insert Noticemap end");
        return messageid + "";
    }

    @Override
    public String insertCmsMessageCp(Integer messageid, String receiverid, Integer receivertype)
                                                                                                 throws DomainServiceException
    {
        log.info("insertCmsMessageCp starting...");
        try
        {
            Noticemap noticemap = new Noticemap();
            noticemap.setMessageid(messageid);
            noticemap.setReceiverid(receiverid);
            noticemap.setReceivertype(receivertype);
            dao.insertCmsMessageCp(noticemap);
        }
        catch (Exception daoEx)
        {
            log.error("dao exception:", daoEx);
            throw new DomainServiceException(daoEx);
        }
        log.info("insertCmsMessageCp end");
        return messageid + "";
    }

    @Override
    public String insertCmsMessageView(String messageid, String cpid, String operatorid) throws DomainServiceException
    {
        log.info("insertCmsMessageView starting...");
        Noticemap noticemap = new Noticemap();
        try
        {
            noticemap.setMessageid(Integer.parseInt(messageid));
            noticemap.setCpid(cpid);
            if(cpid.equals("-1")){
                noticemap.setStatus(1); 
            }
            noticemap.setOperatorid(operatorid);
            dao.insertCmsMessageView(noticemap);
        }
        catch (Exception daoEx)
        {
            log.error("dao exception:", daoEx);
            throw new DomainServiceException(daoEx);
        }
        log.info("insertCmsMessageView end");
        return messageid + "";
    }
    
    public void insertCmsMessageForward(int messageid, int oldMessageid) throws DomainServiceException
    {
        log.info("insertCmsMessageView starting...");
        Noticemap noticemap = new Noticemap();
        noticemap.setMessageid(Integer.valueOf(messageid));
        noticemap.setRefermessageid(Integer.valueOf(oldMessageid));
        try
        {
            dao.insertMessageForward(noticemap);
        }
        catch (Exception daoEx)
        {
            log.error("dao exception:", daoEx);
            throw new DomainServiceException(daoEx);
        }
        log.info("insertCmsMessageView end");
    }
    
    public void deleteNoticemap(Noticemap noticemap) throws DomainServiceException
    {
        log.info("remove cms_message starting..., messageid=" + noticemap.getMessageid());
        try
        {
            dao.deleteNoticemap(noticemap);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            throw new DomainServiceException(daoEx);
        }
        log.info("remove cms_message end");
    }
    
    public void revokeNoticemap(Noticemap noticemap) throws DomainServiceException
    {
        log.info("revoke cms_message starting..., messageid=" + noticemap.getMessageid());
        try
        {
            dao.revokeNoticemap(noticemap);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            throw new DomainServiceException(daoEx);
        }
        log.info("revoke cms_message end");
    }
    public void deleteNoticeMessageCp(Noticemap noticemap) throws DomainServiceException
    {
        log.info("remove cms_message_cp  starting..., messageid=" + noticemap.getMessageid());
        try
        {
            dao.deleteNoticeMessageCp(noticemap);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            throw new DomainServiceException(daoEx);
        }
        log.info("remove cms_message_cp end");
    }

    public void deleteNoticeCmsMessageView(Noticemap noticemap) throws DomainServiceException
    {
        log.info("remove cms_message_view  starting..., messageid=" + noticemap.getMessageid());
        try
        {
            dao.deleteNoticeCmsMessageView(noticemap);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            throw new DomainServiceException(daoEx);
        }
        log.info("remove cms_message_view end");
    }

    public void deleteNoticeCmsReplay(Noticemap noticemap) throws DomainServiceException
    {
        log.info("remove cms_message_replay  starting...,messageid=" + noticemap.getMessageid());
        try
        {
            dao.deleteNoticeCmsReplay(noticemap);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            throw new DomainServiceException(daoEx);
        }
        log.info("remove cms_message_replay end");
    }

    @SuppressWarnings("unchecked")
    public TableDataInfo pageInfoQueryReplay(Noticemap noticemap, int start, int pageSize) throws DomainServiceException
    {
        log.info("get cms_replay info by condition starting...");
        PageInfo pageInfo = null;
        try
        {
            pageInfo = dao.pageInfoQueryReplay(noticemap, start, pageSize);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            throw new DomainServiceException(daoEx);
        }
        TableDataInfo tableInfo = new TableDataInfo();
        tableInfo.setData((List<Noticemap>) pageInfo.getResult());
        tableInfo.setTotalCount((int) pageInfo.getTotalCount());
        log.info("get cms_replay info by condition end");
        return tableInfo;
    }

    @SuppressWarnings("unchecked")
    public TableDataInfo pageInfoQueryReplay(Noticemap noticemap, int start, int pageSize, PageUtilEntity puEntity)
                                                                                                                    throws DomainServiceException
    {
        log.info("get cms_replay info by condition starting...");
        PageInfo pageInfo = null;
        try
        {
            pageInfo = dao.pageInfoQueryReplay(noticemap, start, pageSize, puEntity);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            throw new DomainServiceException(daoEx);
        }
        TableDataInfo tableInfo = new TableDataInfo();
        tableInfo.setData((List<Noticemap>) pageInfo.getResult());
        tableInfo.setTotalCount((int) pageInfo.getTotalCount());
        log.info("get cms_replay info by condition end");
        return tableInfo;
    }

    @SuppressWarnings("unchecked")
    public TableDataInfo pageInfoQueryView(Noticemap noticemap, int start, int pageSize) throws DomainServiceException
    {
        log.info("get cms_message_view info by condition starting...");
        PageInfo pageInfo = null;
        try
        {
            pageInfo = dao.pageInfoQueryView(noticemap, start, pageSize);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            throw new DomainServiceException(daoEx);
        }
        TableDataInfo tableInfo = new TableDataInfo();
        tableInfo.setData((List<Noticemap>) pageInfo.getResult());
        tableInfo.setTotalCount((int) pageInfo.getTotalCount());
        log.info("get cms_message_view info by condition end");
        return tableInfo;
    }

    @SuppressWarnings("unchecked")
    public TableDataInfo pageInfoQueryView(Noticemap noticemap, int start, int pageSize, PageUtilEntity puEntity)
                                                                                                                  throws DomainServiceException
    {
        log.info("get cms_message_view info by condition starting...");
        PageInfo pageInfo = null;
        try
        {
            pageInfo = dao.pageInfoQueryView(noticemap, start, pageSize, puEntity);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            throw new DomainServiceException(daoEx);
        }
        TableDataInfo tableInfo = new TableDataInfo();
        tableInfo.setData((List<Noticemap>) pageInfo.getResult());
        tableInfo.setTotalCount((int) pageInfo.getTotalCount());
        log.info("get cms_message_view info by condition end");
        return tableInfo;
    }

    public void updateNoticemap(Noticemap noticemap) throws DomainServiceException
    {
        log.info("update noticemap by pk starting...");
        try
        {
            dao.updateNoticemap(noticemap);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            throw new DomainServiceException(daoEx);
        }
        log.info("update noticemap by pk end");
    }

    @SuppressWarnings("unchecked")
    public TableDataInfo notificationInfoQuery(Noticemap noticemap, int start, int pageSize)
                                                                                             throws DomainServiceException
    {
        log.info("get Noticemap notification page info by condition starting...");
        PageInfo pageInfo = null;
        try
        {
            pageInfo = dao.notificationInfoQuery(noticemap, start, pageSize);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            throw new DomainServiceException(daoEx);
        }
        TableDataInfo tableInfo = new TableDataInfo();
        tableInfo.setData((List<Noticemap>) pageInfo.getResult());
        tableInfo.setTotalCount((int) pageInfo.getTotalCount());
        log.info("get Noticemap notification page info by condition end");
        return tableInfo;
    }

    public int getNumOfUnView(Noticemap noticemap)
    {
        int rtn = dao.getNumOfUnView(noticemap);
        return rtn;
    }

    public void updateView(Noticemap noticemap) throws DomainServiceException
    {
        log.info("update View starting...");

        try
        {
            dao.updateView(noticemap);
        }
        catch (Exception daoEx)
        {
            log.error("dao exception:", daoEx);
            throw new DomainServiceException(daoEx);
        }
        log.info("update View end");
    }
    public void updateReceiver(Noticemap noticemap) throws DomainServiceException
    {
        log.info("update Receiver starting...");

        try
        {
            dao.updateReceiver(noticemap);
        }
        catch (Exception daoEx)
        {
            log.error("dao exception:", daoEx);
            throw new DomainServiceException(daoEx);
        }
        log.info("update Receiver end");
    }
    
    public Noticemap getNotificationDetail(Noticemap noticemap) throws DomainServiceException
    {
        log.info("get getNotificationDetail by pk starting...");
        Noticemap rsObj = null;
        try
        {
            rsObj = dao.getNotificationDetail(noticemap);
            if (rsObj == null)
            {
                rsObj = dao.getNotificationDetailOnlyMes(noticemap);
            }
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            throw new DomainServiceException(daoEx);
        }
        log.info("get getNotificationDetail by pk end");
        return rsObj;
    }
    public List<Noticemap> getNotificationDetails(Noticemap noticemap) throws DomainServiceException
    {
        log.info("get getNotificationDetail by pk starting...");
        List<Noticemap> rsLists=null;
        try
        {
            rsLists = dao.getNotificationDetails(noticemap);
            if (rsLists==null||rsLists.size()==0)
            {
                rsLists = dao.getNotificationDetailOnlyMess(noticemap);
            }
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            throw new DomainServiceException(daoEx);
        }
        log.info("get getNotificationDetail by pk end");
        return rsLists;
    }
    public void updateReplay(Noticemap noticemap) throws DomainServiceException
    {
        log.info("update Replay starting...");

        try
        {
            dao.updateReplay(noticemap);
        }
        catch (Exception daoEx)
        {
            log.error("dao exception:", daoEx);
            throw new DomainServiceException(daoEx);
        }
        log.info("update Replay end");
    }

    public void setStatusRead(Noticemap noticemap) throws DomainServiceException
    {
        log.info("update cms_message_view status starting...");

        try
        {
            dao.setStatusRead(noticemap);
        }
        catch (Exception daoEx)
        {
            log.error("dao exception:", daoEx);
            throw new DomainServiceException(daoEx);
        }
        log.info("update cms_message_view status end");
    }

    public void setNoticeViewcpnum(Noticemap noticemap) throws DomainServiceException
    {
        log.info("update cms_message_view status starting...");

        try
        {
            dao.setNoticeViewcpnum(noticemap);
        }
        catch (Exception daoEx)
        {
            log.error("dao exception:", daoEx);
            throw new DomainServiceException(daoEx);
        }
        log.info("update cms_message_view status end");
    }

    public String insertReplay(Noticemap noticemap) throws DomainServiceException
    {
        log.info("insert Replay starting...");
        Long replayid = null;
        try
        {
            replayid = (Long) this.getPrimaryKeyGenerator().getPrimarykey("replayid_index");
            noticemap.setReplayid(Integer.parseInt(replayid + ""));
            dao.insertReplay(noticemap);
        }
        catch (Exception daoEx)
        {
            log.error("dao exception:", daoEx);
            throw new DomainServiceException(daoEx);
        }
        log.info("insert Replay end");
        return replayid + "";
    }

    public void deleteReplay(Noticemap noticemap) throws DomainServiceException
    {
        log.info("remove replay_message  starting...");
        try
        {
            dao.deleteReplay(noticemap);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            throw new DomainServiceException(daoEx);
        }
        log.info("remove replay_message end");
    }
    
    public void deleteAttach(Noticemap noticemap) throws DomainServiceException
    {
        log.info("remove attachment starting..., attachid=" + noticemap.getAttachid());
        try
        {
            dao.deleteAttach(noticemap);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            throw new DomainServiceException(daoEx);
        }
        log.info("remove Attachment end");
    }
    
    public void updateAttach(Noticemap noticemap) throws DomainServiceException
    {
        log.info("update Attachment by pk starting...");
        try
        {
        	String fname = noticemap.getAttachname();
            Integer fid = noticemap.getAttachid() ;
            if(fid!=null && StringUtils.isBlank(fname)) {
            	dao.deleteAttach(noticemap);
            }
            else if(fid!=null){
            	int attachid = noticemap.getAttachid();
            	dao.deleteAttach(noticemap);
            	noticemap.setAttachurl(attachid + fname.substring(fname.lastIndexOf(".")));
            	dao.insertAttach(noticemap);
            }
            else if(StringUtils.isNotBlank(fname)){
            	Long attachid = (Long) this.getPrimaryKeyGenerator().getPrimarykey("attachid_index");
            	noticemap.setAttachid(Integer.parseInt(attachid + ""));
            	noticemap.setAttachurl(attachid + fname.substring(fname.indexOf(".")));
            	dao.insertAttach(noticemap);
            }
            //最后一种情况说明fid为null即用户修改公告时没有对附件进行更新（在前台判断fid是否更新并判断是否赋空值给fid）
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            throw new DomainServiceException(daoEx);
        }
        log.info("update attachment by pk end");
    }
    
    public TableDataInfo operInfoQuery(OperInformation operInfo,int start, int pageSize) throws DomainServiceException
    {
    	log.info("get oper page info by condition starting...");
        PageInfo pageInfo = null;
        try
        {
            pageInfo = dao.operInfoQuery(operInfo,start, pageSize);
        }
        catch (DAOException daoEx)
        {
            daoEx.printStackTrace();
            log.error("dao exception:", daoEx);
            throw new DomainServiceException(daoEx);
        }
        TableDataInfo tableInfo = new TableDataInfo();
        tableInfo.setData((List<OperInformation>) pageInfo.getResult());
        tableInfo.setTotalCount((int) pageInfo.getTotalCount());
        log.info("get oper page info by condition end");
        return tableInfo;
    }
    
    public TableDataInfo adminInfoQuery(OperInformation operInfo,int start, int pageSize) throws DomainServiceException
    {
    	log.info("get admin page info by condition starting...");
        PageInfo pageInfo = null;
        try
        {
            pageInfo = dao.adminInfoQuery(operInfo,start, pageSize);
        }
        catch (DAOException daoEx)
        {
            daoEx.printStackTrace();
            log.error("dao exception:", daoEx);
            throw new DomainServiceException(daoEx);
        }
        TableDataInfo tableInfo = new TableDataInfo();
        tableInfo.setData((List<OperInformation>) pageInfo.getResult());
        tableInfo.setTotalCount((int) pageInfo.getTotalCount());
        log.info("get admin page info by condition end");
        return tableInfo;
    }
    
    //更新最后一次回复的信息
    public void updateNoticeReplay(Noticemap noticemap) throws DomainServiceException
    {
        log.info("update notice replay information by pk starting...");
        try {
			dao.updateNoticeReplay(noticemap);
		} catch (DAOException daoEx) {
			log.error("dao exception:", daoEx);
			daoEx.printStackTrace();
		}
        log.info("update notice replay information by pk end");
    }
}

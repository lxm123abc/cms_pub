package com.zte.cms.task.model;

import com.zte.ssb.dynamicobj.DynamicBaseObject;

public class TaskCmsaudit extends DynamicBaseObject
{
    private java.lang.String cpid;
    private java.lang.String operid;
    private java.lang.String files;
    private java.lang.Integer taskid;
    private java.lang.Integer creatorid;
    private java.lang.Integer updaterid;
    private java.lang.Integer lastid;
    private java.lang.Integer status;
    private java.lang.Integer step1;
    private java.lang.Integer step2;
    private java.lang.String taskdesc;
    private java.lang.String auditdesc;
    private java.lang.String audit_desc;
    private java.lang.String createtime;
    private java.lang.String createtimeend;
    private java.lang.String updatetime;
    private java.lang.String attachid;
    private java.lang.String attachname;
    private java.lang.String attachurl;
    private java.lang.Integer funcid;
    private java.lang.Integer parentid;
    
    private java.lang.Integer flag;

    private java.lang.String updatername;
    private java.lang.String lastname;
    private java.lang.String cpname;
    private java.lang.String stepname;
    private java.lang.String creatorname;
    private java.lang.String taskname;
    
    public void initRelation()
    {
        this.addRelation("updatername", "updatername");
    }

	public java.lang.String getCpid() {
		return cpid;
	}

	public void setCpid(java.lang.String cpid) {
		this.cpid = cpid;
	}
	
	public java.lang.String getOperid() {
		return operid;
	}

	public void setOperid(java.lang.String operid) {
		this.operid = operid;
	}

	public java.lang.String getCreatorname() {
		return creatorname;
	}

	public void setCreatorname(java.lang.String creatorname) {
		this.creatorname = creatorname;
	}

	public java.lang.String getUpdatername() {
		return updatername;
	}

	public void setUpdatername(java.lang.String updatername) {
		this.updatername = updatername;
	}

	public java.lang.String getLastname() {
		return lastname;
	}

	public void setLastname(java.lang.String lastname) {
		this.lastname = lastname;
	}

	public java.lang.String getCpname() {
		return cpname;
	}

	public void setCpname(java.lang.String cpname) {
		this.cpname = cpname;
	}

	public java.lang.String getFiles() {
		return files;
	}

	public void setFiles(java.lang.String files) {
		this.files = files;
	}

	public java.lang.Integer getTaskid() {
		return taskid;
	}

	public void setTaskid(java.lang.Integer taskid) {
		this.taskid = taskid;
	}

	public java.lang.Integer getCreatorid() {
		return creatorid;
	}

	public void setCreatorid(java.lang.Integer creatorid) {
		this.creatorid = creatorid;
	}

	public java.lang.Integer getUpdaterid() {
		return updaterid;
	}

	public void setUpdaterid(java.lang.Integer updaterid) {
		this.updaterid = updaterid;
	}

	public java.lang.Integer getLastid() {
		return lastid;
	}

	public void setLastid(java.lang.Integer lastid) {
		this.lastid = lastid;
	}

	public java.lang.Integer getStatus() {
		return status;
	}

	public void setStatus(java.lang.Integer status) {
		this.status = status;
	}

	public java.lang.Integer getStep1() {
		return step1;
	}

	public void setStep1(java.lang.Integer step1) {
		this.step1 = step1;
	}

	public java.lang.Integer getStep2() {
		return step2;
	}

	public void setStep2(java.lang.Integer step2) {
		this.step2 = step2;
	}

	public java.lang.String getTaskname() {
		return taskname;
	}

	public void setTaskname(java.lang.String taskname) {
		this.taskname = taskname;
	}

	public java.lang.String getTaskdesc() {
		return taskdesc;
	}

	public void setTaskdesc(java.lang.String taskdesc) {
		this.taskdesc = taskdesc;
	}

	public java.lang.String getAuditdesc() {
		return auditdesc;
	}

	public void setAuditdesc(java.lang.String auditdesc) {
		this.auditdesc = auditdesc;
	}

	public java.lang.String getCreatetime() {
		return createtime;
	}

	public void setCreatetime(java.lang.String createtime) {
		this.createtime = createtime;
	}

	public java.lang.String getUpdatetime() {
		return updatetime;
	}

	public void setUpdatetime(java.lang.String updatetime) {
		this.updatetime = updatetime;
	}

	public java.lang.String getAttachname() {
		return attachname;
	}

	public void setAttachname(java.lang.String attachname) {
		this.attachname = attachname;
	}

	public java.lang.String getAttachid() {
		return attachid;
	}

	public void setAttachid(java.lang.String attachid) {
		this.attachid = attachid;
	}

	public java.lang.String getAttachurl() {
		return attachurl;
	}

	public void setAttachurl(java.lang.String attachurl) {
		this.attachurl = attachurl;
	}

	public java.lang.Integer getFuncid() {
		return funcid;
	}

	public void setFuncid(java.lang.Integer funcid) {
		this.funcid = funcid;
	}

	public java.lang.String getStepname() {
		return stepname;
	}

	public void setStepname(java.lang.String stepname) {
		this.stepname = stepname;
	}

	public java.lang.Integer getFlag() {
		return flag;
	}

	public void setFlag(java.lang.Integer flag) {
		this.flag = flag;
	}

	public java.lang.Integer getParentid() {
		return parentid;
	}

	public void setParentid(java.lang.Integer parentid) {
		this.parentid = parentid;
	}

	public java.lang.String getCreatetimeend() {
		return createtimeend;
	}

	public void setCreatetimeend(java.lang.String createtimeend) {
		this.createtimeend = createtimeend;
	}

	public java.lang.String getAudit_desc() {
		return audit_desc;
	}

	public void setAudit_desc(java.lang.String audit_desc) {
		this.audit_desc = audit_desc;
	}
	
	
	
}

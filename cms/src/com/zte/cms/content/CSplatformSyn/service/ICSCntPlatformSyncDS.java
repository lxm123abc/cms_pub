package com.zte.cms.content.CSplatformSyn.service;

import java.util.List;

import com.zte.cms.content.CSplatformSyn.model.CSCntPlatformSync;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;

public interface ICSCntPlatformSyncDS
{
    /**
	 * 新增CntPlatformSync对象
	 *
	 * @param cntPlatformSync CntPlatformSync对象
	 * @throws DomainServiceException ds异常
	 */	
	public void insertCntPlatformSync( CSCntPlatformSync cntPlatformSync )throws DomainServiceException;

	/**
     * 新增CntPlatformSync对象
     *
     * @param cntPlatformSync CntPlatformSync对象
     * @throws DomainServiceException ds异常
     */ 
    public Long insertCntPlatformSyncRtn( CSCntPlatformSync cntPlatformSync )throws DomainServiceException;

    /**
	 * 更新CntPlatformSync对象
	 *
	 * @param cntPlatformSync CntPlatformSync对象
	 * @throws DomainServiceException ds异常
	 */
	public void updateCntPlatformSync( CSCntPlatformSync cntPlatformSync )throws DomainServiceException;

    /**
	 * 批量更新CntPlatformSync对象
	 *
	 * @param cntPlatformSync CntPlatformSync对象
	 * @throws DomainServiceException ds异常
	 */
	public void updateCntPlatformSyncList( List<CSCntPlatformSync> cntPlatformSyncList )throws DomainServiceException;



	/**
	 * 删除CntPlatformSync对象
	 *
	 * @param cntPlatformSync CntPlatformSync对象
	 * @throws DomainServiceException ds异常
	 */
	public void removeCntPlatformSync( CSCntPlatformSync cntPlatformSync )throws DomainServiceException;

	/**
	 * 批量删除CntPlatformSync对象
	 *
	 * @param cntPlatformSync CntPlatformSync对象
	 * @throws DomainServiceException ds异常
	 */
	public void removeCntPlatformSyncList( List<CSCntPlatformSync> cntPlatformSyncList )throws DomainServiceException;




	/**
	 * 查询CntPlatformSync对象
	 
	 * @param cntPlatformSync CntPlatformSync对象
	 * @return CntPlatformSync对象
	 * @throws DomainServiceException ds异常 
	 */
	 public CSCntPlatformSync getCntPlatformSync( CSCntPlatformSync cntPlatformSync )throws DomainServiceException; 
	 
	 /**
	  * 根据条件查询CntPlatformSync对象 
	  * 
	  * @param cntPlatformSync CntPlatformSync对象
	  * @return 满足条件的CntPlatformSync对象集
	  * @throws DomainServiceException ds异常
	  */
     public List<CSCntPlatformSync> getCntPlatformSyncByCond( CSCntPlatformSync cntPlatformSync )throws DomainServiceException;

	 /**
	  * 根据条件分页查询CntPlatformSync对象 
	  *
	  * @param cntPlatformSync CntPlatformSync对象，作为查询条件的参数 
	  * @param start 起始行
	  * @param pageSize 页面大小
	  * @param puEntity 排序空置参数@see PageUtilEntity
	  * @return  查询结果
	  * @throws DomainServiceException ds异常
	  */
     public TableDataInfo pageInfoQuery(CSCntPlatformSync cntPlatformSync, int start, int pageSize)throws DomainServiceException;
     
	 /**
	  * 根据条件分页查询CntPlatformSync对象 
	  *
	  * @param cntPlatformSync CntPlatformSync对象，作为查询条件的参数 
	  * @param start 起始行
	  * @param pageSize 页面大小
	  * @param puEntity 排序空置参数@see PageUtilEntity
	  * @return  查询结果
	  * @throws DomainServiceException ds异常
	  */
     public TableDataInfo pageInfoQuery(CSCntPlatformSync cntPlatformSync, int start, int pageSize, PageUtilEntity puEntity)throws DomainServiceException;
     
     /**
      * 根据条件批量删除CntPlatformSync对象
      * 
      * @param cntPlatformSync CntPlatformSync删除条件
      * @throws DomainServiceException ds异常
      */ 
     public void removeCntPlatformSynListByObjindex( List<CSCntPlatformSync> cntTaPlatformSynList )throws DomainServiceException;  

     /**
      * 根据objecttype、objectindex和platform查找CntPlatformSync对象
      * @param cntPlatformSync
      * @return
      * @throws DomainServiceException
      */
     public CSCntPlatformSync getCntPlatformSync4Object( CSCntPlatformSync cntPlatformSync )throws DomainServiceException; 
     
     /**
      * 根据objecttype、objectindex和platform查找CntPlatformSync的对应的子网元数目
      * @param cntPlatformSync
      * @return
      * @throws DomainServiceException
      */
     public int getCntPlatformSyncChildNumber( CSCntPlatformSync cntPlatformSync )throws DomainServiceException; 
 	 public TableDataInfo pageInfoQueryCha(CSCntPlatformSync cntPlatformSync, int start, int pageSize)throws DomainServiceException;
 	 public TableDataInfo pageInfoQuerySche(CSCntPlatformSync cntPlatformSync, int start, int pageSize)throws DomainServiceException;
 	 public TableDataInfo pageInfoQueryCach(CSCntPlatformSync cntPlatformSync, int start, int pageSize)throws DomainServiceException;

}

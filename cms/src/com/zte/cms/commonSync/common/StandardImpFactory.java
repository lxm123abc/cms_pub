package com.zte.cms.commonSync.common;

public class StandardImpFactory
{
    private static StandardImpFactory instance = null;

    private StandardImpFactory()
    {
    }

    public static StandardImpFactory getInstance()
    {
        if (null == instance)
        {
            instance = new StandardImpFactory();
        }
        return instance;

    }

    public IObject2XmlFile create(int platform)
    {
        IObject2XmlFile toXmlFilexImp = null;
        switch (platform)
        {
            case 1:
                toXmlFilexImp = new Object2XmlFile4IPTV20Imp();
                break;
            case 2:
                toXmlFilexImp = new Object2XmlFile4IPTV30Imp();
                break;
            default:
                toXmlFilexImp = null;
        }
        return toXmlFilexImp;
    }
}

package com.zte.cms.ftptask.service;

import java.util.List;

import com.zte.cms.ftptask.dao.ICmsFtptaskDAO;
import com.zte.cms.ftptask.model.CmsFtptask;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.SSBBus;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.exception.exceptions.DAOException;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;
import com.zte.umap.common.dynamicobj.DynamicObjectBaseDS;
import com.zte.cms.ftptask.model.CmsFtptaskAndHis;

public class CmsFtptaskDS extends DynamicObjectBaseDS implements ICmsFtptaskDS
{
    // 日志
    private Log log = SSBBus.getLog(getClass());

    private ICmsFtptaskDAO dao = null;

    public void setDao(ICmsFtptaskDAO dao)
    {
        this.dao = dao;
    }

    public void insertCmsFtptask(CmsFtptask cmsFtptask) throws DomainServiceException
    {
        log.debug("insert cmsFtptask starting...");
        try
        {
            Long taskindex = (Long) this.getPrimaryKeyGenerator().getPrimarykey("cms_ftptask");
            cmsFtptask.setTaskindex(taskindex);
            dao.insertCmsFtptask(cmsFtptask);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("insert cmsFtptask end");
    }

    public void insertCmsFtptaskList(List<CmsFtptask> cmsFtptaskList) throws DomainServiceException
    {
        log.debug("insert cmsFtptaskList by pk starting...");
        if (null == cmsFtptaskList || cmsFtptaskList.size() == 0)
        {
            log.debug("there is no datas in cmsFtptaskList");
            return;
        }
        try
        {
            dao.insertCmsFtptaskList(cmsFtptaskList);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("insert cmsFtptaskList by pk end");
    }

    public void updateCmsFtptask(CmsFtptask cmsFtptask) throws DomainServiceException
    {
        log.debug("update cmsFtptask by pk starting...");
        try
        {
            dao.updateCmsFtptask(cmsFtptask);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("update cmsFtptask by pk end");
    }

    public void updateCmsFtptaskList(List<CmsFtptask> cmsFtptaskList) throws DomainServiceException
    {
        log.debug("update cmsFtptaskList by pk starting...");
        if (null == cmsFtptaskList || cmsFtptaskList.size() == 0)
        {
            log.debug("there is no datas in cmsFtptaskList");
            return;
        }
        try
        {
            dao.updateCmsFtptaskList(cmsFtptaskList);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("update cmsFtptaskList by pk end");
    }

    public void updateCmsFtptaskByCond(CmsFtptask cmsFtptask) throws DomainServiceException
    {
        log.debug("update cmsFtptask by condition starting...");
        try
        {
            dao.updateCmsFtptaskByCond(cmsFtptask);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("update cmsFtptask by condition end");
    }

    public void updateCmsFtptaskListByCond(List<CmsFtptask> cmsFtptaskList) throws DomainServiceException
    {
        log.debug("update cmsFtptaskList by condition starting...");
        if (null == cmsFtptaskList || cmsFtptaskList.size() == 0)
        {
            log.debug("there is no datas in cmsFtptaskList");
            return;
        }
        try
        {
            dao.updateCmsFtptaskListByCond(cmsFtptaskList);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("update cmsFtptaskList by condition end");
    }

    public void removeCmsFtptask(CmsFtptask cmsFtptask) throws DomainServiceException
    {
        log.debug("remove cmsFtptask by pk starting...");
        try
        {
            dao.deleteCmsFtptask(cmsFtptask);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("remove cmsFtptask by pk end");
    }

    public void removeCmsFtptaskList(List<CmsFtptask> cmsFtptaskList) throws DomainServiceException
    {
        log.debug("remove cmsFtptaskList by pk starting...");
        if (null == cmsFtptaskList || cmsFtptaskList.size() == 0)
        {
            log.debug("there is no datas in cmsFtptaskList");
            return;
        }
        try
        {
            dao.deleteCmsFtptaskList(cmsFtptaskList);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("remove cmsFtptaskList by pk end");
    }

    public void removeCmsFtptaskByCond(CmsFtptask cmsFtptask) throws DomainServiceException
    {
        log.debug("remove cmsFtptask by condition starting...");
        try
        {
            dao.deleteCmsFtptaskByCond(cmsFtptask);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("remove cmsFtptask by condition end");
    }

    public void removeCmsFtptaskListByCond(List<CmsFtptask> cmsFtptaskList) throws DomainServiceException
    {
        log.debug("remove cmsFtptaskList by condition starting...");
        if (null == cmsFtptaskList || cmsFtptaskList.size() == 0)
        {
            log.debug("there is no datas in cmsFtptaskList");
            return;
        }
        try
        {
            dao.deleteCmsFtptaskListByCond(cmsFtptaskList);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("remove cmsFtptaskList by condition end");
    }

    public CmsFtptask getCmsFtptask(CmsFtptask cmsFtptask) throws DomainServiceException
    {
        log.debug("get cmsFtptask by pk starting...");
        CmsFtptask rsObj = null;
        try
        {
            rsObj = dao.getCmsFtptask(cmsFtptask);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("get cmsFtptaskList by pk end");
        return rsObj;
    }

    public List<CmsFtptask> getCmsFtptaskByCond(CmsFtptask cmsFtptask) throws DomainServiceException
    {
        log.debug("get cmsFtptask by condition starting...");
        List<CmsFtptask> rsList = null;
        try
        {
            rsList = dao.getCmsFtptaskByCond(cmsFtptask);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("get cmsFtptask by condition end");
        return rsList;
    }

    @SuppressWarnings("unchecked")
    public TableDataInfo pageInfoQuery(CmsFtptask cmsFtptask, int start, int pageSize) throws DomainServiceException
    {
        log.debug("get cmsFtptask page info by condition starting...");
        PageInfo pageInfo = null;
        try
        {
            pageInfo = dao.pageInfoQuery(cmsFtptask, start, pageSize);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        TableDataInfo tableInfo = new TableDataInfo();
        tableInfo.setData((List<CmsFtptask>) pageInfo.getResult());
        tableInfo.setTotalCount((int) pageInfo.getTotalCount());
        log.debug("get cmsFtptask page info by condition end");
        return tableInfo;
    }

    @SuppressWarnings("unchecked")
    public TableDataInfo pageInfoQuery(CmsFtptask cmsFtptask, int start, int pageSize, PageUtilEntity puEntity)
            throws DomainServiceException
    {
        log.debug("get cmsFtptask page info by condition starting...");
        PageInfo pageInfo = null;
        try
        {
            pageInfo = dao.pageInfoQuery(cmsFtptask, start, pageSize, puEntity);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        TableDataInfo tableInfo = new TableDataInfo();
        tableInfo.setData((List<CmsFtptask>) pageInfo.getResult());
        tableInfo.setTotalCount((int) pageInfo.getTotalCount());
        log.debug("get cmsFtptask page info by condition end");
        return tableInfo;
    }

    public void removeCmsFtptaskHis(CmsFtptask cmsFtptask) throws DomainServiceException
    {
        log.debug("remove cmsFtptaskHis by pk starting...");
        try
        {
            dao.deleteCmsFtptaskHis(cmsFtptask);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            throw new DomainServiceException(daoEx);
        }
        log.debug("remove cmsFtptaskHis by pk end");
    }

    public CmsFtptask getCmsFtptaskHis(CmsFtptask cmsFtptask) throws DomainServiceException
    {
        log.debug("get cmsFtptaskHis by pk starting...");
        CmsFtptask rsObj = null;
        try
        {
            rsObj = dao.getCmsFtptaskHis(cmsFtptask);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            throw new DomainServiceException(daoEx);
        }
        log.debug("get cmsFtptaskHis by pk end");
        return rsObj;
    }

    @SuppressWarnings("unchecked")
    public TableDataInfo pageCmsFtptaskHisInfoQuery(CmsFtptaskAndHis cmsFtptaskAndHis, int start, int pageSize)
            throws DomainServiceException
    {
        log.debug("get cmsFtptaskHis page info by condition starting...");
        PageInfo pageInfo = null;
        try
        {
            pageInfo = dao.pageCmsFtptaskHisInfoQuery(cmsFtptaskAndHis, start, pageSize);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            throw new DomainServiceException(daoEx);
        }
        TableDataInfo tableInfo = new TableDataInfo();
        tableInfo.setData((List<CmsFtptaskAndHis>) pageInfo.getResult());
        tableInfo.setTotalCount((int) pageInfo.getTotalCount());
        log.debug("get cmsFtptaskHis page info by condition end");
        return tableInfo;
    }
}

package com.zte.cms.server.ls;

import java.util.List;

import com.zte.cms.common.GlobalConstants;
import com.zte.cms.common.ResourceCodeParser;
import com.zte.cms.server.model.CmsServer;
import com.zte.cms.server.service.ICmsServerDS;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.SSBBus;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.common.log.Log;

/**
 * <p>
 * 文件名称: ServerLS.java
 * </p>
 * <p>
 * 文件描述: 服务器操作类
 * </p>
 * <p>
 * 版权所有: 版权所有(C)2001-2007
 * </p>
 * <p>
 * 公 司: 中兴通讯股份有限公司
 * </p>
 * <p>
 * 内容摘要: 服务器操作类
 * </p>
 * <p>
 * 其他说明: 供定时任务发起时调用
 * </p>
 * <p>
 * 完成日期：2011年1月20日
 * </p>
 * <p>
 * 修改记录1:
 * </p>
 * 
 * <pre>
 *    修改日期：
 *    版 本 号：
 *    修 改 人：
 *    修改内容：
 * </pre>
 * 
 * <p>
 * 修改记录2：
 * </p>
 * 
 * @version 1.0
 * @author 黄杰弟
 */
public class ServerLS implements IServerLS
{

    private ICmsServerDS cmsServerDS;

    private Log log = SSBBus.getLog(getClass());

    private ResourceCodeParser parser = new ResourceCodeParser(ServerConstant.CMS_SERV_RES_FILE_NAME,
            ServerConstant.CMS_SERV_RES_PREFIX);

    public ICmsServerDS getCmsServerDS()
    {
        return cmsServerDS;
    }

    public void setCmsServerDS(ICmsServerDS cmsServerDS)
    {
        this.cmsServerDS = cmsServerDS;
    }

    /**
     * 检查服务器是否还存在
     * 
     * @param String类型对象,是服务器索引
     * @return 返回操作结果
     */
    public String checkExist(String index) throws Exception
    {
        log.debug("checkExist start....................");
        CmsServer tempserver = new CmsServer();
        tempserver.setServerindex(Long.parseLong(index));

        tempserver = cmsServerDS.getCmsServer(tempserver);
        if (tempserver == null)
        {
            log.debug("checkExist end......................");
            return "1:" + parser.getResourceItem(ServerConstant.RES_CMS_SERV_SERVER_NOTEXIST);
        }

        log.debug("checkExist end......................");
        return "0";
    }

    /**
     * 批量删除服务器
     * 
     * @param List<CmsServer> 类型服务器列表
     * @return 返回操作结果
     */
    public String batchDeleCmsServer(List<CmsServer> serverList) throws Exception
    {
        // TODO Auto-generated method stub
        return null;
    }

    /**
     * 删除单个服务器
     * 
     * @param CmsServer类型对象
     * @return 返回操作结果
     */
    public String deleteCmsServer(String index) throws Exception
    {
        // TODO Auto-generated method stub
        log.debug("deleteCmsServer start....................");
        CmsServer tempserver = new CmsServer();
        tempserver.setServerindex(Long.parseLong(index));

        tempserver = cmsServerDS.getCmsServer(tempserver);
        if (tempserver == null)
        {
            return "1:" + parser.getResourceItem(ServerConstant.RES_CMS_SERV_SERVER_NOTEXIST);
        }

        cmsServerDS.removeCmsServer(tempserver);

        // 写业务日志
        ServerLogMgt.insertOperatorLog(parser, String.valueOf(tempserver.getServerindex()), tempserver.getServername(),
                1, 3, 0);

        log.debug("deleteCmsServer end......................");
        return "0:" + parser.getResourceItem(ServerConstant.RES_CMS_SERV_SERVER_OPER_SUCCESSFUL);
    }

    /**
     * 获取服务器信息
     * 
     * @param CmsServer 类型对象的查询条件
     * @return 返回CmsServer类型对象
     */
    public CmsServer getCmsServer(String index) throws Exception
    {
        // TODO Auto-generated method stub
        log.debug("getCmsServer start..........");
        CmsServer server = new CmsServer();

        server.setServerindex(Long.parseLong(index));
        server = cmsServerDS.getCmsServer(server);

        log.debug("getCmsServer end............");
        return server;
    }

    /**
     * 新增一条服务器信息
     * 
     * @param CmsServer类型对象
     * @return 返回操作结果
     */
    public String insertCmsServer(CmsServer cmsServer) throws Exception
    {
        // TODO Auto-generated method stub
        log.debug("insertCmsServer start.................");

        //判断名称是否重复
        CmsServer tempserName = new CmsServer();
        tempserName.setServername(cmsServer.getServername());
        List<CmsServer> servNameList = cmsServerDS.getCmsServerByCond(tempserName);
        if(servNameList.size() > 0)
        {
        	return "1:服务器的名称已经存在";
        }
        
        //判断服务器的IP地址是否重复
        CmsServer tempser = new CmsServer();
        tempser.setInipaddress(cmsServer.getInipaddress());

        List<CmsServer> servlist = null;

        servlist = cmsServerDS.getCmsServerByCond(tempser);

        if (servlist.size() > 0)
        {
            return "1:" + parser.getResourceItem(ServerConstant.RES_CMS_SERV_SERVER_INIP_EXIST);
        }

        cmsServer.setServicekey(GlobalConstants.IPTV_SERVICE_KEY);
        cmsServerDS.insertCmsServer(cmsServer);

        // 写业务日志
        ServerLogMgt.insertOperatorLog(parser, String.valueOf(cmsServer.getServerindex()), cmsServer.getServername(),
                1, 1, 0);

        log.debug("insertCmsServer end...................");
        return "0:" + parser.getResourceItem(ServerConstant.RES_CMS_SERV_SERVER_OPER_SUCCESSFUL);
    }

    /**
     * 分页查询服务器信息
     * 
     * @param CmsServer类型查询条件
     * @param 起始页
     * @param 每页记录大小
     * @return 返回查询结果
     */
    public TableDataInfo pageInfoQuery(CmsServer cmsServer, int start, int pageSize) throws Exception
    {
        // TODO Auto-generated method stub
        return pageInfoQuery(cmsServer, start, pageSize, null);
    }

    /**
     * 分页查询服务器信息
     * 
     * @param CmsServer类型对象的查询条件
     * @param 起始页
     * @param 每页记录数目
     * @param PageUtilEntity类型对象，排序用
     * @return 返回查询结果
     */
    public TableDataInfo pageInfoQuery(CmsServer cmsServer, int start, int pageSize, PageUtilEntity puEntity)
            throws Exception
    {
        // TODO Auto-generated method stub

        return cmsServerDS.pageInfoQuery(cmsServer, start, pageSize, puEntity);
    }

    /**
     * 更新服务器信息
     * 
     * @param CmsServer 类型对象
     * @return 返回操作结果
     */
    public String updateCmsServer(CmsServer cmsServer) throws Exception
    {
        // TODO Auto-generated method stub
        log.debug("updataCmsServer start....................");
        CmsServer tempserver = new CmsServer();
        tempserver.setServerindex(cmsServer.getServerindex());

        tempserver = cmsServerDS.getCmsServer(tempserver);
        if (tempserver == null)
        {
            return "3:" + parser.getResourceItem(ServerConstant.RES_CMS_SERV_SERVER_NOTEXIST);
        }

        //判断修改后的名称是否重复
        CmsServer tempSerName = new CmsServer();
        tempSerName.setServername(cmsServer.getServername());
        List<CmsServer> servNameList = cmsServerDS.getCmsServerByCond(tempSerName);
        if(servNameList.size() > 0 && !(servNameList.get(0).getServerindex().equals(cmsServer.getServerindex())))
        {
        	return "1:已经有相同的服务器名称存在";
        }
        
        // 重复的服务器，根据ip来判断是否重复
        tempserver = new CmsServer();
        tempserver.setInipaddress(cmsServer.getInipaddress());
        List<CmsServer> servlist = null;
        servlist = cmsServerDS.getCmsServerByCond(tempserver);
        if (servlist.size() > 0 && !(servlist.get(0).getServerindex().equals(cmsServer.getServerindex())))
        {
            return "1:" + parser.getResourceItem(ServerConstant.RES_CMS_SERV_SERVER_INIP_EXIST);
        }

        cmsServerDS.updateCmsServer(cmsServer);

        // 写业务日志
        ServerLogMgt.insertOperatorLog(parser, String.valueOf(cmsServer.getServerindex()), cmsServer.getServername(),
                1, 2, 0);

        log.debug("updateCmsServer end......................");
        return "0:" + parser.getResourceItem(ServerConstant.RES_CMS_SERV_SERVER_OPER_SUCCESSFUL);
    }

}

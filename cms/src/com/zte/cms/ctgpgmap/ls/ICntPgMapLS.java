package com.zte.cms.ctgpgmap.ls;

import java.util.List;
import java.util.Map;

import com.zte.cms.content.program.model.CmsProgram;
import com.zte.cms.content.program.programsync.model.ProgramPlatformSyn;
import com.zte.cms.ctgpgmap.model.CategoryProgramMap;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;

/**
 * 
 * @author Administrator
 * 
 */
public interface ICntPgMapLS
{

    /**
     * 
     * @param categoryProgramMap CategoryProgramMap
     * @param start int
     * @param pageSize int
     * @return TableDataInfo
     */
    public TableDataInfo queryCntPgMapList(CategoryProgramMap categoryProgramMap, int start, int pageSize);

    /**
     * 
     * @param categorycdn CategoryProgramMap
     * @return String
     */
    public String insertCtgpgmap(CategoryProgramMap categorycdn);

    /**
     * 
     * @param id Long
     * @return CategoryProgramMap
     */
    public String deletectgpgmap(Long mapindex);

    /**
     * 
     * @param mapid String
     * @return CategoryProgramMap
     */
    public CategoryProgramMap getCmsCategory(String mapid);

    /**
     * 
     * @param pgindex String
     * @return CmsProgram
     */
    public CmsProgram getCmsProgram(String pgindex);

    /**
     * 
     * @param file String
     * @return Map
     */
    public Map<String, String> getpgname(String file);

    /**
     * 
     * @param ctgindex String
     * @return Map
     */
    public Map<String, String> getctgname(String ctgindex);
    /****** publish management *************/
    
    
    
	 /**
	  * 根据条件分页查询CntTargetsysSyn对象 
	  *
	  * @param programPlatformSyn CntTargetsysSyn对象，作为查询条件的参数 
	  * @param start 起始行
	  * @param pageSize 页面大小
	  * @param puEntity 排序空置参数@see PageUtilEntity
	  * @return  查询结果
	  * @throws DomainServiceException ds异常
	  */
    public TableDataInfo pageInfoQueryctprMap(CategoryProgramMap categoryProgramMap, int start, int pageSize) throws Exception;
        
    
	 /**
	  * 点播内容关联栏目针对平台发布
	  * @param categoryProgramMap 发布对象
	  * @param type 发布平台类型
	  * @return  查询结果
	  * @throws DomainServiceException ds异常
	  */
    public String publishCtprMapPlatformSyn(String mapindex, String programid,String categoryid,int type,int synType) throws Exception;

	 /**
	  * 点播内容关联栏目针对平台取消发布
	  * @param categoryProgramMap 发布对象
	  * @param type 发布平台类型
	  * @return  查询结果
	  * @throws DomainServiceException ds异常
	  */
    public String delPublishCtprMapPlatformSyn(String mapindex, String programid,String categoryid,int type,int synType) throws Exception;
    
	 /**
	  * 点播内容关联栏目针对网元发布
	  * @param categoryProgramMap 发布对象
	  * @param type 发布平台类型
	  * @return  查询结果
	  * @throws DomainServiceException ds异常
	  */
    public String publishcatgrProTarget(String mapindex,  String targetindex,int type) throws Exception;
  
    
	 /**
	  * 点播内容关联栏目针对网元发布
	  * @param categoryProgramMap 发布对象
	  * @param type 发布平台类型
	  * @return  查询结果
	  * @throws DomainServiceException ds异常
	  */
    public TableDataInfo pageInfoQueryctprTargetMap(CategoryProgramMap categoryProgramMap, int start, int pageSize) throws Exception;
     
    
 /**
  * 点播内容关联栏目针对网元取消发布
  * @param categoryProgramMap 发布对象
  * @param type 发布平台类型
  * @return  查询结果
  * @throws DomainServiceException ds异常
  */
    public String delpublishcatgrProTarget(String mapindex,  String targetindex,int type) throws Exception;

/**
 * 点播内容关联栏目批量发布或取消发布
 * @param categoryProgramMap 发布对象
 * @param type 发布平台类型
 * @return  查询结果
 * @throws DomainServiceException ds异常
 */
public String batchPublishOrDelPublishcntpg(List<CategoryProgramMap> list,String operType, int type) throws Exception;

   
   
   
   /****** publish management end ********/
}
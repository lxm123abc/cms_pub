package com.zte.cms.cpsp;

public interface LicesConstant
{
    /**
     * 1 CP类型
     */
    public static final int CP_TYPE_1 = 1;
    /**
     * 2 牌照方类型
     */
    public static final int LISCE_TYPE_2 = 2;
    /**
     * 1 牌照方状态 正常
     */
    public static final int STATUS_NORMAL_1 = 1;
    /**
     * 1 牌照方状态 删除
     */
    public static final int STATUS_DEL_4 = 4;
    /**
     * 21 牌照方状态 同步
     */
    public static final int STATUS_SYNING_21 = 21;
    /**
     * 40 牌照方状态 已同步
     */
    public static final int STATUS_SYNED_40 = 40;
    /**
     * 41 牌照方状态 新增同步失败
     */
    public static final int STATUS_NEWSYNFAIL_41 = 41;
    /**
     * 42 牌照方状态 删除同步失败
     */
    public static final int STATUS_DELSYNFAIL_42 = 42;
    /**
     * 0 牌照方ID使用状态 未使用
     */
    public static final int STATUS_ID_UNUSE_0 = 0;
    /**
     * 1 牌照方ID使用状态 已使用
     */
    public static final int STATUS_ID_USED_1 = 1;
    /**
     * 3 牌照方同步类型
     */
    public static final int SYN_LICES_TYPE_3 = 3;
    /**
     * 2 牌照方同步DEVTYPE
     */
    public static final int LICES_DEV_TYPE_2 = 2;
    /**
     * 1 牌照方同步新增 新增类型为1
     */
    public static final int LICES_SYN_OPERTYPE_1 = 1;
    /**
     * 2 牌照方删除同步 删除类型为2
     */
    public static final int LICES_SYN_OPERTYPE_2 = 2;
    /**
     * 3 牌照方修改同步 类型3
     */
    public static final int LICES_SYN_OPERTYPE_3 = 3;
}

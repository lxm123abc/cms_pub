package com.zte.cms.content.movie.dao;

import java.util.List;

import com.zte.cms.content.movie.model.CmsMovie;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DAOException;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;

public interface ICmsMovieDAO
{
    /**
     * 新增CmsMovie对象
     * 
     * @param cmsMovie CmsMovie对象
     * @throws DAOException dao异常
     */
    public void insertCmsMovie(CmsMovie cmsMovie) throws DAOException;

    /**
     * 根据主键更新CmsMovie对象
     * 
     * @param cmsMovie CmsMovie对象
     * @throws DAOException dao异常
     */
    public void updateCmsMovie(CmsMovie cmsMovie) throws DAOException;

    /**
     * 根据条件更新CmsMovie对象
     * 
     * @param cmsMovie CmsMovie更新条件
     * @throws DAOException dao异常
     */
    public void updateCmsMovieByCond(CmsMovie cmsMovie) throws DAOException;

    /**
     * 根据主键删除CmsMovie对象
     * 
     * @param cmsMovie CmsMovie对象
     * @throws DAOException dao异常
     */
    public void deleteCmsMovie(CmsMovie cmsMovie) throws DAOException;

    /**
     * 根据条件删除CmsMovie对象
     * 
     * @param cmsMovie CmsMovie删除条件
     * @throws DAOException dao异常
     */
    public void deleteCmsMovieByCond(CmsMovie cmsMovie) throws DAOException;

    /**
     * 根据主键查询CmsMovie对象
     * 
     * @param cmsMovie CmsMovie对象
     * @return 满足条件的CmsMovie对象
     * @throws DAOException dao异常
     */
    public CmsMovie getCmsMovie(CmsMovie cmsMovie) throws DAOException;

    /**
     * 根据条件查询CmsMovie对象
     * 
     * @param cmsMovie CmsMovie对象
     * @return 满足条件的CmsMovie对象集
     * @throws DAOException dao异常
     */
    public List<CmsMovie> getCmsMovieByCond(CmsMovie cmsMovie) throws DAOException;

    /**
     * 根据条件分页查询CmsMovie对象，作为查询条件的参数
     * 
     * @param cmsMovie CmsMovie对象
     * @param start 起始行
     * @param pageSize 页面大小
     * @return 查询结果
     * @throws DAOException dao异常
     */
    public PageInfo pageInfoQuery(CmsMovie cmsMovie, int start, int pageSize) throws DAOException;

    /**
     * 根据条件分页查询CmsMovie对象，作为查询条件的参数
     * 
     * @param cmsMovie CmsMovie对象
     * @param start 起始行
     * @param pageSize 页面大小
     * @param puEntity 排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws DAOException dao异常
     */
    public PageInfo pageInfoQuery(CmsMovie cmsMovie, int start, int pageSize, PageUtilEntity puEntity)
            throws DAOException;

    /**
     * 根据条件查询CmsMovie对象的数量
     * 
     * @param cmsMovie CmsMovie对象
     * @return 查询结果
     * @throws DAOException
     */
    public int getCmsMovieCount(CmsMovie cmsMovie) throws DAOException;
    /**
     * 根据条件分页查询CmsMovie对象，作为查询条件的参数
     * 
     * @param cmsMovie CmsMovie对象
     * @param start 起始行
     * @param pageSize 页面大小
     * @param puEntity 排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws DAOException dao异常
     */    
    public PageInfo pageInfoQueryEncryption(CmsMovie cmsMovie, int start, int pageSize) throws DAOException;
}

  package com.zte.cms.content.targetSyn.dao;

import java.util.List;

import com.zte.cms.content.targetSyn.dao.ICntTargetSyncDAO;
import com.zte.cms.content.targetSyn.model.CntTargetSync;
import com.zte.ssb.dynamicobj.DynamicBaseObject;
import com.zte.ssb.dynamicobj.DynamicObjectBaseDao;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DAOException;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.SSBBus;

public class CntTargetSyncDAO extends DynamicObjectBaseDao implements ICntTargetSyncDAO
{	
	// 日志
	private Log log = SSBBus.getLog(getClass());
	
    public void insertCntTargetSync( CntTargetSync cntTargetSync )throws DAOException
    {
    	log.debug("insert cntTargetSync starting...");
    	super.insert( "insertCntTargetSync", cntTargetSync );
    	log.debug("insert cntTargetSync end");
    }
     
    public void updateCntTargetSync( CntTargetSync cntTargetSync )throws DAOException
    {
    	log.debug("update cntTargetSync by pk starting...");
       	super.update( "updateCntTargetSync", cntTargetSync );
       	log.debug("update cntTargetSync by pk end");
    }


    public void deleteCntTargetSync( CntTargetSync cntTargetSync )throws DAOException
    {
    	log.debug("delete cntTargetSync by pk starting...");
       	super.delete( "deleteCntTargetSync", cntTargetSync );
       	log.debug("delete cntTargetSync by pk end");
    }


    public CntTargetSync getCntTargetSync( CntTargetSync cntTargetSync )throws DAOException
    {
    	log.debug("query cntTargetSync starting...");
       	CntTargetSync resultObj = (CntTargetSync)super.queryForObject( "getCntTargetSync",cntTargetSync);
       	log.debug("query cntTargetSync end");
       	return resultObj;
    }

	@SuppressWarnings("unchecked")
    public List<CntTargetSync> getCntTargetSyncByCond( CntTargetSync cntTargetSync )throws DAOException
    {
    	log.debug("query cntTargetSync by condition starting...");
    	List<CntTargetSync> rList = (List<CntTargetSync>)super.queryForList( "queryCntTargetSyncListByCond",cntTargetSync);
    	log.debug("query cntTargetSync by condition end");
    	return rList;
    }
	
	@SuppressWarnings("unchecked")
    public List<CntTargetSync> getcntSynnormaltargetListBycond( CntTargetSync cntTargetSync )throws DAOException
    {
    	log.debug("query cntSynnormaltargetListBycond by condition starting...");
    	List<CntTargetSync> rList = (List<CntTargetSync>)super.queryForList( "getcntSynnormaltargetListBycond",cntTargetSync);
    	log.debug("query cntSynnormaltargetListBycond by condition end");
    	return rList;
    }
	
	@SuppressWarnings("unchecked")
    public PageInfo pageInfoQuery(CntTargetSync cntTargetSync, int start, int pageSize)throws DAOException
    {
    	log.debug("page query cntTargetSync by condition starting...");
     	PageInfo pageInfo = null;
    	int totalCnt = ((Integer) super.queryForObject("queryCntTargetSyncListCntByCond",  cntTargetSync)).intValue();
    	if( totalCnt > 0 )
    	{
    		int fetchSize = pageSize > (totalCnt - start)? (totalCnt - start) : pageSize;
    		List<CntTargetSync> rsList = (List<CntTargetSync>)super.pageQuery( "queryCntTargetSyncListByCond" ,  cntTargetSync , start , fetchSize );
    		pageInfo = new PageInfo( start, totalCnt, fetchSize, rsList );
    	}
    	else
    	{
    		pageInfo = new PageInfo();
    	}
    	log.debug("page query cntTargetSync by condition end");
    	return pageInfo;
    }
    
    @SuppressWarnings("unchecked")
    public PageInfo pageInfoQuery(DynamicBaseObject cntTargetSync, int start, int pageSize, PageUtilEntity puEntity)throws DAOException
    {
    	return super.indexPageQuery( "queryCntTargetSyncListByCond", "queryCntTargetSyncListCntByCond", cntTargetSync, start, pageSize, puEntity);
    }

	public PageInfo pageInfoQuery(CntTargetSync cntTargetSync, int start,
			int pageSize, PageUtilEntity puEntity) throws DAOException
	{
		// TODO Auto-generated method stub
		return null;
	}
    
	public void deleteCntTargetSyncByObjindex( CntTargetSync cntTargetSync )throws DAOException
    {
        log.debug("delete cntTargetSync by conditions starting...");
        super.delete( "deleteCntTargetSyncByObjindex", cntTargetSync );
        log.debug("update cntTargetSync by conditions end");
    }

    public int countCntTargetSync4Object(CntTargetSync cntTargetSync) throws DAOException
    {
        // TODO Auto-generated method stub
        return ((Integer) super.queryForObject("queryCntTargetSync4ObjectCntByCond",  cntTargetSync)).intValue();
    }
    
    public List<CntTargetSync> getCommonTarget4ServiceAndProgram(CntTargetSync cntTargetSync) throws DAOException
    {
        // TODO Auto-generated method stub
        log.debug("get common target for service and program by condition starting...");
        List<CntTargetSync> rList = (List<CntTargetSync>)super.queryForList( "getCommonTarget4ServiceAndProgram",cntTargetSync);
        log.debug("get common target for service and program  by condition end");
        return rList;
    }
    public List<CntTargetSync> getCommonTarget4ServiceAndSeries(CntTargetSync cntTargetSync) throws DAOException
    {
        // TODO Auto-generated method stub
        log.debug("get common target for service and series by condition starting...");
        List<CntTargetSync> rList = (List<CntTargetSync>)super.queryForList( "getCommonTarget4ServiceAndSeries",cntTargetSync);
        log.debug("get common target for service and series  by condition end");
        return rList;
    }
    public List<CntTargetSync> getCommonTarget4ServiceAndChannel(CntTargetSync cntTargetSync) throws DAOException
    {
        // TODO Auto-generated method stub
        log.debug("get common target for service and channel by condition starting...");
        List<CntTargetSync> rList = (List<CntTargetSync>)super.queryForList( "getCommonTarget4ServiceAndChannel",cntTargetSync);
        log.debug("get common target for service and channel  by condition end");
        return rList;
    }
} 
package com.zte.cms.channel.wkfw.ls;

import java.util.HashMap;

import com.zte.cms.channel.wkfw.model.ScheduleWkfwCondition;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;

/**
 * 
 * @author Administrator
 * 
 */
public interface IScheduleWkfwQueryLS
{
    /**
     * 
     * @param channelWkfwTaskForMeCondition ChannelWkfwCondition
     * @return int
     * @throws DomainServiceException DomainServiceException
     */
    public abstract int queryScheduleWkfwTaskForMeListCnt(ScheduleWkfwCondition scheduleWkfwTaskForMeCondition)
            throws DomainServiceException;

    /**
     * 
     * @param channelWkfwTaskForMeCondition ChannelWkfwCondition
     * @param rangeStart int
     * @param fetchSize int
     * @return TableDataInfo
     * @throws DomainServiceException DomainServiceException
     */
    public abstract TableDataInfo queryScheduleWkfwTaskForMe(ScheduleWkfwCondition scheduleWkfwTaskForMeCondition,
            int rangeStart, int fetchSize) throws DomainServiceException;

    /**
     * 
     * @param channelWkfwTaskForMeCondition ChannelWkfwCondition
     * @param rangeStart int
     * @param fetchSize int
     * @param puEntity PageUtilEntity
     * @return TableDataInfo
     * @throws DomainServiceException DomainServiceException
     */
    public abstract TableDataInfo queryScheduleWkfwTaskForMe(ScheduleWkfwCondition scheduleWkfwTaskForMeCondition,
            int rangeStart, int fetchSize, PageUtilEntity puEntity) throws DomainServiceException;

    /**
     * 
     * @param channelWkfwTaskForMeCondition ChannelWkfwCondition
     * @return int
     * @throws DomainServiceException DomainServiceException
     */
    public abstract int queryScheduleWkfwProcessWithMeListCnt(ScheduleWkfwCondition scheduleWkfwTaskForMeCondition)
            throws DomainServiceException;

    /**
     * 
     * @param channelWkfwTaskForMeCondition ChannelWkfwCondition
     * @param rangeStart int
     * @param fetchSize int
     * @return TableDataInfo
     * @throws DomainServiceException DomainServiceException
     */
    public abstract TableDataInfo queryScheduleWkfwProcessWithMe(ScheduleWkfwCondition scheduleWkfwTaskForMeCondition,
            int rangeStart, int fetchSize) throws DomainServiceException;

    /**
     * 
     * @param channelWkfwTaskForMeCondition ChannelWkfwCondition
     * @param rangeStart int
     * @param fetchSize int
     * @param puEntity PageUtilEntity
     * @return TableDataInfo
     * @throws DomainServiceException DomainServiceException
     */
    public abstract TableDataInfo queryScheduleWkfwProcessWithMe(ScheduleWkfwCondition scheduleWkfwTaskForMeCondition,
            int rangeStart, int fetchSize, PageUtilEntity puEntity) throws DomainServiceException;

    /**
     * 
     * @param selectedValue String
     * @param needSelect Boolean
     * @return HashMap *
     */
    public HashMap<String, Object> getWorkflowTypeList(String selectedValue, Boolean needSelect);

    /**
     * 
     * @return HashMap
     */
    public HashMap<String, String> getWorkflowTypeMap();
}
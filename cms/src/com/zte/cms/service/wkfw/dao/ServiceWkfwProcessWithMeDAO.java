package com.zte.cms.service.wkfw.dao;

import java.util.List;

import com.zte.cms.service.wkfw.model.ServiceWkfwCondition;
import com.zte.cms.service.wkfw.model.ServiceWkfwProcessWithMe;
import com.zte.ssb.dynamicobj.DynamicObjectBaseDao;
import com.zte.ssb.framework.SSBBus;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.exception.exceptions.DAOException;

public class ServiceWkfwProcessWithMeDAO extends DynamicObjectBaseDao implements IServiceWkfwProcessWithMeDAO
{
    // 日志
    private Log log = SSBBus.getLog(getClass());

    /*
     * (non-Javadoc)
     * 
     * @see com.zte.cms.strategy.wkfw.common.dao.IStrategyWkfwProcessWithMeDAO#queryStrategyWkfwProcessWithMeListByCond(com.zte.cms.strategy.wkfw.common.model.StrategyWkfwCondition)
     */
    @SuppressWarnings("unchecked")
    public List<ServiceWkfwProcessWithMe> queryServiceWkfwProcessWithMeListByCond(
            ServiceWkfwCondition serviceWkfwProcessWithMeCondition) throws DAOException
    {
        log.debug("query serviceWkfwProcessWithMeList by condition starting...");
        List<ServiceWkfwProcessWithMe> rList = (List<ServiceWkfwProcessWithMe>) super.queryForList(
                "queryServiceWkfwProcessWithMeListByCond", serviceWkfwProcessWithMeCondition);
        log.debug("query serviceWkfwProcessWithMeList by condition end");
        return rList;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.zte.cms.strategy.wkfw.common.dao.IStrategyWkfwProcessWithMeDAO#queryStrategyWkfwProcessWithMeListCntByCond(com.zte.cms.strategy.wkfw.common.model.StrategyWkfwCondition)
     */
    @SuppressWarnings("unchecked")
    public int queryServiceWkfwProcessWithMeListCntByCond(ServiceWkfwCondition serviceWkfwProcessWithMeCondition)
            throws DAOException
    {
        log.debug("query serviceWkfwProcessWithMeList count by condition starting...");
        int totalCnt = ((Integer) super.queryForObject("queryServiceWkfwProcessWithMeListCntByCond",
                serviceWkfwProcessWithMeCondition)).intValue();
        log.debug("query serviceWkfwProcessWithMeList count by condition end");
        return totalCnt;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.zte.cms.strategy.wkfw.common.dao.IStrategyWkfwProcessWithMeDAO#pageInfoQuery(com.zte.cms.strategy.wkfw.common.model.StrategyWkfwCondition,
     *      int, int)
     */
    @SuppressWarnings("unchecked")
    public PageInfo pageInfoQuery(ServiceWkfwCondition serviceWkfwProcessWithMeCondition, int start, int pageSize)
            throws DAOException
    {
        log.debug("page query serviceWkfwProcessWithMeList by condition starting...");
        PageInfo pageInfo = null;
        int totalCnt = ((Integer) super.queryForObject("queryServiceWkfwProcessWithMeListCntByCond",
                serviceWkfwProcessWithMeCondition)).intValue();
        if (totalCnt > 0)
        {
            int fetchSize = pageSize > (totalCnt - start) ? (totalCnt - start) : pageSize;
            List<ServiceWkfwProcessWithMe> rsList = (List<ServiceWkfwProcessWithMe>) super.pageQuery(
                    "queryServiceWkfwProcessWithMeListByCond", serviceWkfwProcessWithMeCondition, start, fetchSize);
            pageInfo = new PageInfo(start, totalCnt, fetchSize, rsList);
        }
        else
        {
            pageInfo = new PageInfo();
        }
        log.debug("page query strategyWkfwProcessWithMeList by condition end");
        return pageInfo;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.zte.cms.strategy.wkfw.common.dao.IStrategyWkfwProcessWithMeDAO#pageInfoQuery(com.zte.cms.strategy.wkfw.common.model.StrategyWkfwCondition,
     *      int, int, com.zte.ssb.framework.base.util.PageUtilEntity)
     */
    public PageInfo pageInfoQuery(ServiceWkfwCondition serviceWkfwProcessWithMeCondition, int start, int pageSize,
            PageUtilEntity puEntity) throws DAOException
    {
        return super.indexPageQuery("queryServiceWkfwProcessWithMeListByCond",
                "queryServiceWkfwProcessWithMeListCntByCond", serviceWkfwProcessWithMeCondition, start, pageSize,
                puEntity);
    }
}
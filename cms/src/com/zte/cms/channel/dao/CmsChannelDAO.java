package com.zte.cms.channel.dao;

import java.util.List;

import com.zte.cms.channel.dao.ICmsChannelDAO;
import com.zte.cms.channel.model.CmsChannel;
import com.zte.ssb.dynamicobj.DynamicObjectBaseDao;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DAOException;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.SSBBus;

public class CmsChannelDAO extends DynamicObjectBaseDao implements ICmsChannelDAO
{
    // 日志
    private Log log = SSBBus.getLog(getClass());

    public void insertCmsChannel(CmsChannel cmsChannel) throws DAOException
    {
        log.debug("insert cmsChannel starting...");

        super.insert("insertCmsChannel", cmsChannel);
        log.debug("insert cmsChannel end");
    }

    public void updateCmsChannel(CmsChannel cmsChannel) throws DAOException
    {
        log.debug("update cmsChannel by pk starting...");
        super.update("updateCmsChannel", cmsChannel);
        log.debug("update cmsChannel by pk end");
    }

    public void deleteCmsChannel(CmsChannel cmsChannel) throws DAOException
    {
        log.debug("delete cmsChannel by pk starting...");
        super.delete("deleteCmsChannel", cmsChannel);
        log.debug("delete cmsChannel by pk end");
    }

    public CmsChannel getCmsChannel(CmsChannel cmsChannel) throws DAOException
    {
        log.debug("query cmsChannel starting...");
        CmsChannel resultObj = (CmsChannel) super.queryForObject("getCmsChannel", cmsChannel);
        log.debug("query cmsChannel end");
        return resultObj;
    }

    @SuppressWarnings("unchecked")
    public PageInfo pageInfoQuery(CmsChannel cmsChannel, int start, int pageSize) throws DAOException
    {
        log.debug("page query cmsChannel by condition starting...");
        PageInfo pageInfo = null;
        int totalCnt = ((Integer) super.queryForObject("getChannelContentCnt", cmsChannel)).intValue();
        if (totalCnt > 0)
        {
            int fetchSize = pageSize > (totalCnt - start) ? (totalCnt - start) : pageSize;
            List<CmsChannel> rsList = (List<CmsChannel>) super.pageQuery("pageInfoChannelContent", cmsChannel, start,
                    fetchSize);
            pageInfo = new PageInfo(start, totalCnt, fetchSize, rsList);
        }
        else
        {
            pageInfo = new PageInfo();
        }
        log.debug("page query cmsChannel by condition end");
        return pageInfo;
    }

    @SuppressWarnings("unchecked")
    public PageInfo pageInfoQuery(CmsChannel cmsChannel, int start, int pageSize, PageUtilEntity puEntity)
            throws DAOException
    {
        return super.indexPageQuery("queryCmsChannelListByCond", "queryCmsChannelListCntByCond", cmsChannel, start,
                pageSize, puEntity);
    }

    public List getChannelByCond(CmsChannel channel)
    {
        return (List) super.queryForList("getChannelByCond", channel);
    }

    public CmsChannel getCmsChannelById(CmsChannel cmsChannel) throws DAOException
    {
        log.debug("query getCmsChannelById starting...");
        CmsChannel resultObj = (CmsChannel) super.queryForObject("getCmsChannelById", cmsChannel);
        log.debug("query getCmsChannelById end");
        return resultObj;
    }

}
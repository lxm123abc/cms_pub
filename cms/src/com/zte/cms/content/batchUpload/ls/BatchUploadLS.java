package com.zte.cms.content.batchUpload.ls;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.zte.cms.common.Generator;
import com.zte.cms.common.GlobalConstants;
import com.zte.cms.common.ResourceManager;
import com.zte.cms.common.log.CommonLogConstant;
import com.zte.cms.common.log.CommonLogUtil;
import com.zte.cms.content.batchUpload.common.BatchUploadConstants;
import com.zte.cms.content.movie.model.CmsMovie;
import com.zte.cms.content.movie.service.ICmsMovieDS;
import com.zte.cms.content.program.model.CmsProgram;
import com.zte.cms.content.program.service.ICmsProgramDS;
import com.zte.cms.cpsp.model.CmsLpopNoaudit;
import com.zte.cms.cpsp.service.ICmsLpopNoauditDS;
import com.zte.cms.ftptask.model.CmsFtptask;
import com.zte.cms.ftptask.service.ICmsFtptaskDS;
import com.zte.cms.storageArea.ls.ICmsStorageareaLS;
import com.zte.ismp.common.util.DbUtil;
import com.zte.ismp.common.util.FtpUtil;
import com.zte.sapi.ServiceAccess;
import com.zte.sapi.protocol.FTPService;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;
import com.zte.ssb.servicecontainer.business.server.RIAContext;
import com.zte.ssb.servicecontainer.business.util.ServiceConsts;
import com.zte.umap.common.ResourceMgt;
import com.zte.umap.common.dynamicobj.DynamicObjectBaseDS;
import com.zte.umap.cpsp.common.model.UcpBasic;
import com.zte.umap.cpsp.common.service.IUcpBasicDS;

/**
 * 内容批量上载处理接口实现类
 * 
 */
public class BatchUploadLS extends DynamicObjectBaseDS implements IBatchUploadLS
{
    /**
     * 日志记录器
     */
    private Log log = LogFactory.getLog(getClass());

    /**
     * UCMP资源文件
     */
    private static final String RESOURCE_UCMP_CONTENT = "ucmp_content_resource";

    /**
     * 操作成功提示KEY
     */
    private static final String RESOURCE_OPERATION_SUCCESS = "operation.success";
    /**
     * 未找到上载FTP提示KEY
     */
    private static final String RESOURCE_FTP_NOTFOUND = "err.ftp.notfound";

    private static final String BATCHID = "ucdn_task_batch_id";// 任务批次号

    private static final String LANGUAGE = "zh";

    /**
     * 最大行数
     */
    private final int MAX_ROW_NUMBER = 100;// 内容名称最大长度
    /**
     * 列数
     */
    private final int COLMNE_NUMBER = 6;// 列数
    /**
     * 内容名称最大长度
     */
    private final int MAX_CNTNAME_LENGTH = 128;// 内容名称最大长度
    /**
     * 文件路径最大长度
     */
    private final int MAX_FILEURL_LENGTH = 1024;// 文件路径最大长度
    /**
     * 播放时长最大长度
     */
    private final int MAX_DURATION_LENGTH = 8;// 播放时长最大长度

    private ICmsStorageareaLS cmsStorageareaLS = null;

    private ICmsProgramDS cmsProgramDS = null;

    private ICmsMovieDS cmsMovieDS = null;

    private ICmsFtptaskDS cmsFtptaskDS = null;

    private IUcpBasicDS ucpBasicDS = null;
    
    private ICmsLpopNoauditDS noAuditDS;

    private FtpUtil ftpUtil=null;
    FTPService ftpService=null;
    BatchUploadLS()
    {
        ResourceMgt.addDefaultResourceBundle(RESOURCE_UCMP_CONTENT);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.zte.cms.content.batchUpload.ls.IBatchUploadLS#addBatchContents(java.lang.Long, java.lang.String,
     *      java.lang.String, int, java.lang.String, java.lang.String, java.lang.String, java.lang.Integer)
     */
    @SuppressWarnings("unchecked")
    public String addBatchContents(Long cpindex, String cpid, String ip, String port, String user, String pass,
            String file, String licenseid, Integer contentType, Integer uploadType, String programType)
            throws DomainServiceException
    {
        int infoCnt = 0;
        int errorCount = 0;// 失败个数
        int continuecount = 0; // 空行数
        int rowNum = 0;// 文件的行数
        
        String specialCharMask = ",!@#$%^&*;|?()[]{}<>'\""; //特殊字符串 
        Pattern regexDuration = Pattern
                   .compile("(\\d{2})[0-5][0-9][0-5](\\d{3})");//视频的时间正则表达式
        OperateInfo operateInfo = null;
        ReturnInfoForExp rtnInfo = new ReturnInfoForExp();

        // 获取在线区地址
        String onlineArea = null;
        try
        {
            onlineArea = cmsStorageareaLS.getAddress(BatchUploadConstants.STORAGEAREA_ONLINE); // 2：在线区
        }
        catch (Exception e)
        {
            log.error("Error occurred in getAddress method", e);
            throw new DomainServiceException(ResourceManager.getResourceText(e));
        }

        if (null == onlineArea || BatchUploadConstants.BATCHUPLOAD_GET_STORAGEAREA_ERROR.equals(onlineArea))
        {// 1056020:获取在线区地址失败
            rtnInfo.setFlag(GlobalConstants.FAIL);
            rtnInfo.setReturnMessage(ResourceManager
                    .getResourceTextByCode(BatchUploadConstants.BATCHUPLOAD_ONLINEAREA_NOTFOUND)); // "100001"
            return rtnInfo.toString();
        }
        
        try
        {
            if(uploadType==3){
             // 判断FTP是否连接正常
                ftpService = ServiceAccess.getFTPService();
                if (!ftpService.connect(ip, Integer.parseInt(port), user, pass))
                {// 连接失败            
                    rtnInfo.setFlag(GlobalConstants.FAIL);
                    rtnInfo.setReturnMessage("FTP服务器连接失败,请重新配置FTP信息");
                    return rtnInfo.toString();
                }                
            }            
        }
        catch (Exception e)
        {
            log.debug("FTP connection failed, please check network configuration", e);
            rtnInfo.setFlag(GlobalConstants.FAIL);
            rtnInfo.setReturnMessage("FTP服务器连接失败,请重新配置FTP信息");
            return rtnInfo.toString();
        }

        try
        {
            RIAContext context = RIAContext.getCurrentInstance();
            List<FileItem> fileList = (List<FileItem>) context.getRequest().getAttribute(ServiceConsts.FILEITEM_LIST);

            log.debug("fileList:" + fileList.size());
            if (fileList.size() != 1)
            {
                return "1:" + ResourceMgt.findDefaultText("err.notuploadfile");
            }
            FileItem fileItem = null;

            // 检查后缀是否正确
            Iterator<FileItem> iter = fileList.iterator();

            fileItem = iter.next();
            log.debug(fileItem.getName());
            String ext = fileItem.getName().substring(fileItem.getName().lastIndexOf('.') + 1);
            if (!ext.equalsIgnoreCase("xls") && !ext.equalsIgnoreCase("xlsx"))
            {
                rtnInfo.setFlag(GlobalConstants.FAIL);
                rtnInfo.setReturnMessage(ResourceManager
                        .getResourceTextByCode(BatchUploadConstants.BATCHUPLOAD_WRONG_FORMAT));
                return rtnInfo.toString();
            }

            Workbook workbook = null;
            try
            {
                if (ext.equalsIgnoreCase("xls"))
                {
                    workbook = new HSSFWorkbook(fileItem.getInputStream());
                }
                else if (ext.equalsIgnoreCase("xlsx"))
                {
                    // 构造 XSSFWorkbook 对象，strPath 传入文件路径
                    workbook = new XSSFWorkbook(fileItem.getInputStream());
                }
            }
            catch (IOException e)
            {
                rtnInfo.setFlag(GlobalConstants.FAIL);
                rtnInfo.setReturnMessage(ResourceManager
                        .getResourceTextByCode(BatchUploadConstants.BATCHUPLOAD_WRONG_FORMAT));
                return rtnInfo.toString();
            }
            Sheet sheet = workbook.getSheetAt(0);
            rowNum = sheet.getLastRowNum();
            Row rowData = sheet.getRow(sheet.getFirstRowNum());

            // 检查列数
            int colNum = 0;
            if (null != rowData)
            {
                colNum = rowData.getLastCellNum();
            }
            if (colNum != COLMNE_NUMBER)
            {
                rtnInfo.setFlag(GlobalConstants.FAIL);
                rtnInfo.setReturnMessage(ResourceManager
                        .getResourceTextByCode(BatchUploadConstants.BATCHUPLOAD_WRONG_FORMAT));
                return rtnInfo.toString();
            }
            if (rowNum > MAX_ROW_NUMBER)
            {
                rtnInfo.setFlag(GlobalConstants.FAIL);
                rtnInfo.setReturnMessage(ResourceManager
                        .getResourceTextByCode(BatchUploadConstants.BATCHUPLOAD_WRONG_ROW));
                return rtnInfo.toString();
            }

            // 格式校验
            String contentName = null;
            String platform = null;
            String fileurl = null;
            String duration = null;
            String videoprofile = null;
            String bitrateType = null;
            for (int row = sheet.getFirstRowNum() + 1; row <= rowNum; row++)
            {
                rowData = sheet.getRow(row);

                if (rowData == null || emptyRow(rowData, COLMNE_NUMBER))
                {
                    continuecount++;
                    continue;
                }

                contentName = getCellDate(rowData, 0);
                platform = getCellDate(rowData, 1);
                fileurl = getCellDate(rowData, 2);
                duration = getCellDate(rowData, 3);
                videoprofile = getCellDate(rowData, 4);
                bitrateType = getCellDate(rowData, 5);              
                // 内容名称校验
                if (contentName != null && contentName.length() > 0)
                {
                    if (!checkCharInMask(contentName, specialCharMask, false))
                    {
                        errorCount++;
                        operateInfo = new OperateInfo(String.valueOf(++infoCnt), "第" + Integer.toString(row) + "行",
                                "解析失败", "内容名称不能包含特殊字符");
                        rtnInfo.appendOperateInfo(operateInfo);
                        continue;
                    }else if (exlen(contentName, MAX_CNTNAME_LENGTH, LANGUAGE))
                    {
                        errorCount++;
                        operateInfo = new OperateInfo(String.valueOf(++infoCnt), "第" + Integer.toString(row) + "行",
                                "解析失败", "内容名称超长");
                        rtnInfo.appendOperateInfo(operateInfo);
                        continue;
                    }
                }
                else
                {
                    errorCount++;
                    operateInfo = new OperateInfo(String.valueOf(++infoCnt), ResourceMgt.findDefaultText("cnt.bu.number") + Integer.toString(row) + ResourceMgt.findDefaultText("cnt.bu.line"), ResourceMgt.findDefaultText("cnt.bu.resolve.fail"),//解析失败
                    		ResourceMgt.findDefaultText("cnt.bu.cnt.name.notbevoid"));//内容名称不能为空
                    rtnInfo.appendOperateInfo(operateInfo);
                    continue;
                }

                // 业务平台类型校验
                if (platform != null && platform.length() > 0)
                {
                    if (platform.length() != 1 || "1234".indexOf(platform) == -1)
                    {
                        errorCount++;
                        operateInfo = new OperateInfo(String.valueOf(++infoCnt), ResourceMgt.findDefaultText("cnt.bu.number") + Integer.toString(row) + ResourceMgt.findDefaultText("cnt.bu.line"),
                        		ResourceMgt.findDefaultText("cnt.bu.resolve.fail"), ResourceMgt.findDefaultText("cnt.bu.service.platform.between1and4"));//解析失败   业务平台必须填入一个1到4之间的数字
                        rtnInfo.appendOperateInfo(operateInfo);
                        continue;
                    }
                }
                else
                {
                    errorCount++;
                    operateInfo = new OperateInfo(String.valueOf(++infoCnt),  ResourceMgt.findDefaultText("cnt.bu.number")+ Integer.toString(row) + ResourceMgt.findDefaultText("cnt.bu.line"), ResourceMgt.findDefaultText("cnt.bu.resolve.fail"),//解析失败
                    		ResourceMgt.findDefaultText("cnt.bu.service.platform.cannot.void"));//业务平台不能为空
                    rtnInfo.appendOperateInfo(operateInfo);
                    continue;
                }

                // 文件路径校验
                if (fileurl != null && fileurl.length() > 0)
                {
                	int fileNameBeg = fileurl.lastIndexOf("/");
                	String filename = fileurl.substring(fileNameBeg+1, fileurl.length()).toUpperCase();
                	if(!filename.endsWith("TS") && !filename.endsWith("3GP")&& !filename.endsWith("MPG4")&& !filename.endsWith("MP4")&& !filename.endsWith("WMV")
                	        && !filename.endsWith("MPEG")&& !filename.endsWith("FVI")&& !filename.endsWith("MPG")&& !filename.endsWith("MPE")&& !filename.endsWith("AVI")
                	        && !filename.endsWith("MPEG1")&& !filename.endsWith("MPEG2")&& !filename.endsWith("MPEG4")&& !filename.endsWith("RTP")
                	        && !filename.endsWith("WMV9")&& !filename.endsWith("H264"))
                	{
                	    errorCount++;
                        operateInfo = new OperateInfo(String.valueOf(++infoCnt), "第" + Integer.toString(row) + "行", "解析失败",
                        "视频文件格式不支持。支持的视频文件格式为：MPG4,MP4,WMV,AVI,MPE,MPG,FVI,MPEG,MPEG1,MPEG2,MPEG4,3GP,TS,RTP,H264,WMV9");
                        rtnInfo.appendOperateInfo(operateInfo);
                        continue;
                	}
                    
                	
                	if (!checkCharInMask(fileurl, specialCharMask, false))
                    {
                        errorCount++;
                        operateInfo = new OperateInfo(String.valueOf(++infoCnt), "第" + Integer.toString(row) + "行",
                                "解析失败", "文件路径不能包含特殊字符");
                        rtnInfo.appendOperateInfo(operateInfo);
                        continue;
                    }
                	
                	if (exlen(fileurl, MAX_FILEURL_LENGTH, LANGUAGE))
                    {
                        errorCount++;
                        operateInfo = new OperateInfo(String.valueOf(++infoCnt), ResourceMgt.findDefaultText("cnt.bu.number") + Integer.toString(row) + ResourceMgt.findDefaultText("cnt.bu.line"),
                        		ResourceMgt.findDefaultText("cnt.bu.resolve.fail"),ResourceMgt.findDefaultText("cnt.bu.file.path.toolong"));//解析失败   "文件路径超长"
                        rtnInfo.appendOperateInfo(operateInfo);
                        continue;
                    }
                    // else if (fileurl.lastIndexOf(".") == -1 || fileurl.lastIndexOf("/") == -1)
                    // {
                    // errorCount++;
                    // operateInfo = new OperateInfo(String.valueOf(++infoCnt),  + Integer.toString(row) + "行",
                    // "解析失败", "文件路径格式错误");
                    // rtnInfo.appendOperateInfo(operateInfo);
                    // continue;
                    // }
                }
                else
                {
                    errorCount++;
                    operateInfo = new OperateInfo(String.valueOf(++infoCnt), ResourceMgt.findDefaultText("cnt.bu.number") + Integer.toString(row) + ResourceMgt.findDefaultText("cnt.bu.line"), ResourceMgt.findDefaultText("cnt.bu.resolve.fail"),//解析失败
                            ResourceMgt.findDefaultText("cnt.bu.file.path.cannot.void"));//"文件路径不能为空"
                    rtnInfo.appendOperateInfo(operateInfo);
                    continue;
                }
                
                if(uploadType==3){//FTP方式上传文件时验证文件是否存在
                    String[] names = ftpService.listNames(fileurl);
                    if (null == names || names.length <= 0)
                    {   // 源文件不存在
                        errorCount++;
                        operateInfo = new OperateInfo(String.valueOf(++infoCnt), "第" + Integer.toString(row) + "行", "解析失败",
                                "源文件不存在");
                        rtnInfo.appendOperateInfo(operateInfo);
                        continue;
                    }       
                }else{//已上载方式上传文件时验证文件是否存在
                    String mountpoint = GlobalConstants.getMountPoint(); // 获取挂载点
                    File f =new File(mountpoint+fileurl);
                    if(!f.exists()){
                     // 源文件不存在
                        errorCount++;
                        operateInfo = new OperateInfo(String.valueOf(++infoCnt), "第" + Integer.toString(row) + "行", "解析失败",
                                "源文件不存在");
                        rtnInfo.appendOperateInfo(operateInfo);
                        continue;
                    }   
                }                           

                // 播放时长校验
                if (duration != null && duration.length() > 0)
                {
                    if (exlen(duration, MAX_DURATION_LENGTH, LANGUAGE))
                    {
                        errorCount++;
                        operateInfo = new OperateInfo(String.valueOf(++infoCnt), ResourceMgt.findDefaultText("cnt.bu.number") + Integer.toString(row) + ResourceMgt.findDefaultText("cnt.bu.line"),
                        		ResourceMgt.findDefaultText("cnt.bu.resolve.fail"), ResourceMgt.findDefaultText("cnt.bu.play.time.toolong"));//解析失败   "播放时长超长"
                        rtnInfo.appendOperateInfo(operateInfo);
                        continue;
                    }
                    
                    Matcher m = regexDuration.matcher(duration);
                    boolean b = m.matches();
                    

                    if ((!b)||("00000000".equals(duration)))
                    {
                        errorCount++;
                        operateInfo = new OperateInfo(String.valueOf(++infoCnt),ResourceMgt.findDefaultText("cnt.bu.number")  + Integer.toString(row) + ResourceMgt.findDefaultText("cnt.bu.line"),
                        		ResourceMgt.findDefaultText("cnt.bu.resolve.fail"), ResourceMgt.findDefaultText("cnt.bu.play.time.form.wrong") );//解析失败   "播放时长格式不符"
                        rtnInfo.appendOperateInfo(operateInfo);
                        continue;                    	
                    }

                }
                else
                {
                    errorCount++;
                    operateInfo = new OperateInfo(String.valueOf(++infoCnt), ResourceMgt.findDefaultText("cnt.bu.number") + Integer.toString(row) + ResourceMgt.findDefaultText("cnt.bu.line"), ResourceMgt.findDefaultText("cnt.bu.resolve.fail"),
                            ResourceMgt.findDefaultText("cnt.bu.play.time.cannot.void"));//"播放时长不能为空"
                    rtnInfo.appendOperateInfo(operateInfo);
                    continue;
                }

                // 视频规格类型校验
                if (videoprofile != null && videoprofile.length() > 0)
                {
                    if (videoprofile.length() != 1 || "123456".indexOf(videoprofile) == -1)
                    {
                        errorCount++;
                        operateInfo = new OperateInfo(String.valueOf(++infoCnt), ResourceMgt.findDefaultText("cnt.bu.number") + Integer.toString(row) + ResourceMgt.findDefaultText("cnt.bu.line"),
                        		ResourceMgt.findDefaultText("cnt.bu.resolve.fail"), ResourceMgt.findDefaultText("cnt.bu.video.between1and6"));//解析失败   "视频规格必须填入一个1到6之间的数字"
                        rtnInfo.appendOperateInfo(operateInfo);
                        continue;
                    }
                }
                else
                {
                    errorCount++;
                    operateInfo = new OperateInfo(String.valueOf(++infoCnt), ResourceMgt.findDefaultText("cnt.bu.number") + Integer.toString(row) + ResourceMgt.findDefaultText("cnt.bu.line"), ResourceMgt.findDefaultText("cnt.bu.resolve.fail"),//解析失败
                            ResourceMgt.findDefaultText("cnt.bu.video.cannot.void") );//"视频规格不能为空"
                    rtnInfo.appendOperateInfo(operateInfo);
                    continue;
                }

                // 码流速率类型校验
                if (bitrateType != null && bitrateType.length() > 0)
                {
                    if (bitrateType.length() != 1 || "34567".indexOf(bitrateType) == -1)
                    {
                        errorCount++;
                        operateInfo = new OperateInfo(String.valueOf(++infoCnt), ResourceMgt.findDefaultText("cnt.bu.number") + Integer.toString(row) + ResourceMgt.findDefaultText("cnt.bu.line"),
                        		ResourceMgt.findDefaultText("cnt.bu.resolve.fail"), ResourceMgt.findDefaultText("cnt.bu.code.stream.rate.between3and7"));//解析失败   码流速率必须填入一个3到7之间的数字
                        rtnInfo.appendOperateInfo(operateInfo);
                        continue;
                    }
                }
                else
                {
                    errorCount++;
                    operateInfo = new OperateInfo(String.valueOf(++infoCnt),  ResourceMgt.findDefaultText("cnt.bu.number")+ Integer.toString(row) + ResourceMgt.findDefaultText("cnt.bu.line"), ResourceMgt.findDefaultText("cnt.bu.resolve.fail"),//解析失败
                    		ResourceMgt.findDefaultText("cnt.bu.code.stream.rate.cannot.void"));
                    rtnInfo.appendOperateInfo(operateInfo);
                    continue;
                }
            }

            // 写入数据
            if (errorCount == 0)
            {
                UcpBasic ucpBasic = ucpBasicDS.getUcpBasicByIndex(cpindex);

                // 查Licenseindex
                Long licenseIndex = null;
                UcpBasic license = new UcpBasic();
                license.setCpid(cpid);
                license.setCptype(2);// 一个牌照方
                List<UcpBasic> licenseList = ucpBasicDS.getUcpBasicByCond(license);

                if (null != licenseList && licenseList.size() > 0)
                {
                    licenseIndex = licenseList.get(0).getCpindex();
                }

                for (int row = sheet.getFirstRowNum() + 1; row <= rowNum; row++)
                {
                    rowData = sheet.getRow(row);

                    if (rowData == null || emptyRow(rowData, COLMNE_NUMBER))
                    {
                        continue;
                    }

                    contentName = getCellDate(rowData, 0);
                    platform = getCellDate(rowData, 1);
                    fileurl = getCellDate(rowData, 2);
                    duration = getCellDate(rowData, 3);
                    videoprofile = getCellDate(rowData, 4);
                    bitrateType = getCellDate(rowData, 5);

                    // 写入program数据
                    CmsProgram cmsProgram = new CmsProgram();
                    Long programIndex = (Long) this.getPrimaryKeyGenerator().getPrimarykey("cms_program_index");
                    cmsProgram.setProgramindex(programIndex);
                    String programId = Generator.getContentId(Long.parseLong(cpid), "PROG");
                    cmsProgram.setProgramid(programId);
                    cmsProgram.setCpindex(cpindex);
                    cmsProgram.setCpid(cpid);
                    cmsProgram.setProvid(ucpBasic.getBusinessscope());// 区域ID
                    cmsProgram.setNamecn(contentName);
                    cmsProgram.setPlatform(Integer.valueOf(platform));
                    if (uploadType == 3)
                    {
                        cmsProgram.setStatus(100);// 初始状态 (免审CP时状态由接口机修改未‘审核通过’)
                    }
                    else if (uploadType == 2)
                    {
                        if(noAudit(cpid))
                        {
                            cmsProgram.setStatus(0);// 审核通过
                        }
                        else
                        {
                            cmsProgram.setStatus(110);// 待审核
                        }
                    }
                    cmsProgram.setLicenseid(licenseid);
                    cmsProgram.setLicenseindex(licenseIndex);
                    cmsProgram.setSeriesflag(contentType);
                    cmsProgram.setProgramtype(programType);
                    cmsProgramDS.insertCmsProgram(cmsProgram);

                    // 写入movie数据
                    Date date = new Date();
                    DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
                    String currentTime = dateFormat.format(date);// 当前时间

                    String filepath = fileurl.substring(0, fileurl.lastIndexOf('/') + 1);
                    String oldFileName = fileurl.substring(fileurl.lastIndexOf('/') + 1);
                    // 新文件名
                    StringBuffer newFileName = new StringBuffer(String.valueOf(date.getTime()));
                    newFileName.append("_");
                    newFileName.append(oldFileName);
                    // 新全路径
                    StringBuffer newPath = new StringBuffer(onlineArea);
                    newPath.append('/');
                    newPath.append(currentTime);
                    newPath.append('/');
                    newPath.append(newFileName);

                    Long movieIndex = (Long) this.getPrimaryKeyGenerator().getPrimarykey("cms_movie_index");
                    CmsMovie cmsMovie = new CmsMovie();
                    cmsMovie.setProgramindex(programIndex);
                    cmsMovie.setProgramid(programId);
                    cmsMovie.setMovieindex(movieIndex);
                    cmsMovie.setMovieid(Generator.getPhysicalContentId("MOVI"));
                    cmsMovie.setMovietype(1);// 正片
                    if (uploadType == 3)
                    {
                        cmsMovie.setFileurl(newPath.toString());
                    }
                    else if (uploadType == 2)
                    {
                        cmsMovie.setFileurl(fileurl);
                    }
                    cmsMovie.setDuration(duration);
                    cmsMovie.setFilesize(0L);// 文件大小由接口机更新
                    cmsMovie.setBitratetype(Integer.valueOf(bitrateType));
                    cmsMovie.setNamecn(newFileName.toString());
                    if (uploadType == 3)
                    {
                        cmsMovie.setStatus(100);// 初始状态  (免审CP时状态由接口机修改未‘审核通过’)
                    }
                    else if (uploadType == 2)
                    {
                        if(noAudit(cpid))
                        {
                            cmsMovie.setStatus(0);// 审核通过                          
                        }
                        else
                        {
                            cmsMovie.setStatus(110);// 待审核
                        }
                    }
                    cmsMovie.setVideoprofile(Integer.valueOf(videoprofile));
                    cmsMovieDS.insertCmsMovie(cmsMovie);

                    // 上传类型为FTP时，需要插入FTP任务
                    if (uploadType == 3)
                    {
                        CmsFtptask cmsFtptask = new CmsFtptask();
                        cmsFtptask.setTasktype(1L);// 1.普通类型 2.母片 3.发布迁移
                        cmsFtptask.setFiletype(1);// 迁移文件类型 1.movie 2.picture
                        cmsFtptask.setContentindex(programIndex);
                        cmsFtptask.setFileindex(movieIndex);
                        cmsFtptask.setUploadtype(uploadType);

                        StringBuffer ftp = new StringBuffer("ftp://");
                        ftp.append(user).append(":");
                        ftp.append(pass).append("@");
                        ftp.append(ip).append(":");
                        ftp.append(port);
                        ftp.append(fileurl);
                        cmsFtptask.setSrcaddress(ftp.toString());

                        cmsFtptask.setDestaddress(newPath.toString());
                        cmsFtptask.setStatus(0);// 待上载
                        cmsFtptask.setSrcfilehandle(0);// 源文件不处理

                        cmsFtptaskDS.insertCmsFtptask(cmsFtptask);
                    }
                }
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
            throw new DomainServiceException(e);
        }finally{
            try
            {
                if(uploadType==3&&ftpService.isConnected()){
                    ftpService.disconnect();
                }               
            }
            catch (IOException e)
            {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }

        int successCnt = rowNum - errorCount - continuecount;
        if (errorCount > 0)
        {
            if (errorCount == rowNum - continuecount)
            {
                rtnInfo.setFlag("1");
                rtnInfo.setReturnMessage(ResourceMgt.findDefaultText("fail.count") + errorCount);
            }
            else
            {
                rtnInfo.setFlag("2");
                rtnInfo.setReturnMessage(ResourceMgt.findDefaultText("success.count") + successCnt + " " + ResourceMgt.findDefaultText("fail.count") + errorCount);
            }

        }
        else
        {
            rtnInfo.setFlag("0");
            rtnInfo.setReturnMessage(ResourceMgt.findDefaultText("success.count") + successCnt);
            CommonLogUtil.insertOperatorLog(cpid, CommonLogConstant.MGTTYPE_CONTENT,
                    CommonLogConstant.OPERTYPE_CONTENT_BATCHIMPORT,
                    CommonLogConstant.OPERTYPE_CONTENT_BATCHIMPORT_INFO, CommonLogConstant.RESOURCE_OPERATION_SUCCESS);
        }
        return rtnInfo.toString();
    }

    private boolean exlen(String content, int len, String language)
    {
        boolean result = false;
        int temp = 0;
        if ("en".equals(language))
        {
            for (int i = 0; i < content.length(); i++)
            {
                int value = content.codePointAt(i);
                if (value < 0x080)
                {
                    temp += 1;
                }
                else if (value < 0x0800)
                {
                    temp += 2;
                }
                else if (value < 0x010000)
                {
                    temp += 3;
                }
                else
                {
                    temp += 4;
                }
            }
        }
        else
        {
            for (int i = 0; i < content.length(); i++)
            {
                int value = content.codePointAt(i);
                if (value < 0x080)
                {
                    temp += 1;
                }
                else if (value < 0x0800)
                {
                    temp += 2;
                }
                else if (value < 0x010000)
                {
                    temp += 2;
                }
                else
                {
                    temp += 4;
                }
            }
        }

        if (temp > len)
        {
            result = true;
        }
        return result;
    }

    private boolean emptyRow(Row row, int colnum)
    {
        boolean result = true;
        for (int i = 0; i < colnum; i++)
        {
            if (null != getCellDate(row, i) && getCellDate(row, i).length() > 0)
            {
                result = false;
            }
        }
        return result;
    }

    private String getCellDate(Row row, int colnum)
    {
        String cellStr;
        Cell cell = row.getCell(colnum);
        if (cell == null)
        {
            return null;
        }
        int type = cell.getCellType();

        switch (type)
        {
            case 0:
                cell.setCellType(HSSFCell.CELL_TYPE_STRING);
                cellStr = cell.toString().trim();
                break;
            case 1:
                cellStr = cell.toString().trim();
                break;
            default:
                cellStr = "";
                break;
        }
        return cellStr;
    }
    
    /**
     * 检查字符串1中的字符是否在字符串2中。
     * 
     * @param str 字符串1
     * @param mask 字符串2(模式串）
     * @param isIn： true--检查字符串1中的字符是否在字符串2中，如果都在返回true，否在返回false
     *            false--检查字符串1中的字符是否不在字符串2中，如果都不在返回true，如果有一个或多个字符在字符串2中，返回false
     * @return boolean类型
     */
    private boolean checkCharInMask(String str, String mask, boolean isIn)
    {
        boolean result = true;
        for (int i = 0; i < str.length(); i++)
        {
            if (isIn)
            {
                if (mask.indexOf(str.charAt(i)) == -1)
                {
                    result = false;
                }
            }
            else
            {
                if (mask.indexOf(str.charAt(i)) != -1)
                {
                    result = false;
                    break;
                }
            }
        }

        return result;
    }
    
    /**
     *  判断该cp是否为免审
     * @param cpid
     * @return 
     */
    private boolean noAudit(String cpid)
    {
        boolean result = false;
        DbUtil dbUtil = new DbUtil();
        String sql = "select count(1) from zxdbm_cms.cms_lpop_noaudit where cpid='" + cpid + "' and lptype='4'";
 
        try
        {
            if(dbUtil.queryForCount(sql) > 0)
            {
                result = true;
            }          
        }
        catch (Exception e)
        {
            result = false;
        }
        return result;
    }

    public void setCmsProgramDS(ICmsProgramDS cmsProgramDS)
    {
        this.cmsProgramDS = cmsProgramDS;
    }

    public void setCmsMovieDS(ICmsMovieDS cmsMovieDS)
    {
        this.cmsMovieDS = cmsMovieDS;
    }

    public void setCmsStorageareaLS(ICmsStorageareaLS cmsStorageareaLS)
    {
        this.cmsStorageareaLS = cmsStorageareaLS;
    }

    public void setCmsFtptaskDS(ICmsFtptaskDS cmsFtptaskDS)
    {
        this.cmsFtptaskDS = cmsFtptaskDS;
    }

    public void setUcpBasicDS(IUcpBasicDS ucpBasicDS)
    {
        this.ucpBasicDS = ucpBasicDS;
    }
    
    public void setNoAuditDS(ICmsLpopNoauditDS noAuditDS)
    {
        this.noAuditDS = noAuditDS;
    }
    
}

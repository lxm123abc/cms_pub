package com.zte.cms.castrolemap.crmsync.ls;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.zte.cms.cast.model.Castcdn;
import com.zte.cms.cast.service.ICastcdnDS;
import com.zte.cms.castrolemap.crmsync.model.CrmPlatformSync;
import com.zte.cms.castrolemap.crmsync.service.ICrmPlatformSyncDS;
import com.zte.cms.castrolemap.model.Castrolemap;
import com.zte.cms.castrolemap.model.CastrolemapConstant;
import com.zte.cms.castrolemap.service.ICastrolemapDS;
import com.zte.cms.cntSyncTask.model.CntSyncTask;
import com.zte.cms.cntSyncTask.service.ICntSyncTaskDS;
import com.zte.cms.common.DateUtil2;
import com.zte.cms.common.GlobalConstants;
import com.zte.cms.common.log.CommonLogConstant;
import com.zte.cms.common.log.CommonLogUtil;
import com.zte.cms.commonSync.common.StandardImpFactory;
import com.zte.cms.commonSync.common.StatusTranslator;
import com.zte.cms.content.batchUpload.ls.OperateInfo;
import com.zte.cms.content.batchUpload.ls.ReturnInfo;
import com.zte.cms.content.common.CntConstants;
import com.zte.cms.content.objectSynRecord.model.ObjectSyncRecord;
import com.zte.cms.content.objectSynRecord.service.IObjectSyncRecordDS;
import com.zte.cms.content.platformSyn.model.CntPlatformSync;
import com.zte.cms.content.platformSyn.service.ICntPlatformSyncDS;
import com.zte.cms.content.targetSyn.model.CntTargetSync;
import com.zte.cms.content.targetSyn.service.ICntTargetSyncDS;
import com.zte.cms.content.util.CntUtils;
import com.zte.cms.programcrmmap.model.ProgramCrmMap;
import com.zte.cms.programcrmmap.service.IProgramCrmMapDS;
import com.zte.cms.seriescrmmap.model.SeriesCrmMap;
import com.zte.cms.seriescrmmap.service.ISeriesCrmMapDS;
import com.zte.cms.storageArea.ls.ICmsStorageareaLS;
import com.zte.cms.targetSystem.model.Targetsystem;
import com.zte.cms.targetSystem.service.ITargetsystemDS;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.umap.common.EspecialCharMgt;
import com.zte.umap.common.ResourceMgt;
import com.zte.umap.common.dynamicobj.DynamicObjectBaseDS;
import com.zte.umap.common.log.Logger;
import com.zte.umap.common.log.LoggerFactory;
import com.zte.umap.common.log.LoggerModel;
import com.zte.umap.sys.model.UsysConfig;
import com.zte.umap.sys.service.IUsysConfigDS;

public class CrmPlatformSyncLS extends DynamicObjectBaseDS implements ICrmPlatformSyncLS
{
    private Logger log = LoggerFactory.getLogger(LoggerModel.MODEL_CPSP, getClass());
    private ICrmPlatformSyncDS crmPlatformSyncDS;
    private ICastrolemapDS castrolemapDS;
    private ICntPlatformSyncDS cntPlatformSyncDS;
    private ICntTargetSyncDS cntTargetSyncDS;
    private ITargetsystemDS targetSystemDS;
    private ICmsStorageareaLS cmsStorageareaLS;
    private ICntSyncTaskDS cntsyncTaskDS;
    private IObjectSyncRecordDS objectSyncRecordDS;
    private ICastcdnDS castds;
    private IProgramCrmMapDS programCrmMapDS;
    private ISeriesCrmMapDS seriesCrmMapDS;
    
    private Map map = new HashMap<String, List>();
    public final static String CNTSYNCXML = "xmlsync";
    public static final String TEMPAREA_ID = "1"; // 临时区ID
    public static final String MEDIA_ID = "2"; // 在线区ID
    public static final String TEMPAREA_ERRORSIGN = "1056020"; // 获取临时区目录失败
    public static final String CORRELATEID = "ucdn_task_correlate_id";// 任务流水号
    
    public void setCastrolemapDS(ICastrolemapDS castrolemapDS)
    {
        this.castrolemapDS = castrolemapDS;
    }

    public void setCntPlatformSyncDS(ICntPlatformSyncDS cntPlatformSyncDS)
    {
        this.cntPlatformSyncDS = cntPlatformSyncDS;
    }

    public void setCntTargetSyncDS(ICntTargetSyncDS cntTargetSyncDS)
    {
        this.cntTargetSyncDS = cntTargetSyncDS;
    }

    public void setTargetSystemDS(ITargetsystemDS targetSystemDS)
    {
        this.targetSystemDS = targetSystemDS;
    }

    public void setCmsStorageareaLS(ICmsStorageareaLS cmsStorageareaLS)
    {
        this.cmsStorageareaLS = cmsStorageareaLS;
    }

    public void setCntsyncTaskDS(ICntSyncTaskDS cntsyncTaskDS)
    {
        this.cntsyncTaskDS = cntsyncTaskDS;
    }

    public void setObjectSyncRecordDS(IObjectSyncRecordDS objectSyncRecordDS)
    {
        this.objectSyncRecordDS = objectSyncRecordDS;
    }
    
    public void setCastds(ICastcdnDS castds)
    {
        this.castds = castds;
    }

    public void setProgramCrmMapDS(IProgramCrmMapDS programCrmMapDS)
    {
        this.programCrmMapDS = programCrmMapDS;
    }

    public void setSeriesCrmMapDS(ISeriesCrmMapDS seriesCrmMapDS)
    {
        this.seriesCrmMapDS = seriesCrmMapDS;
    }

    public String batchPublishCrmToPlatform(List<Castrolemap> crmList, int platform ,int syncType) throws Exception
    {
        ReturnInfo returnInfo = new ReturnInfo();
        OperateInfo operateInfo = null;
        int successCount = 0;
        int totalCastindex = 0;
        int failCount = 0;
        int num = 1;
        
        try{
            for(int i = 0; i < crmList.size(); i++)  
            {
                totalCastindex ++;
                String result = null;
                String operdesc = null;
                String proStatusStr = null;
                
                Castrolemap crmObj = new Castrolemap();
                crmObj.setCastrolemapindex(crmList.get(i).getCastrolemapindex());
                crmObj = castrolemapDS.getCastrolemap(crmObj);
                if (crmObj == null ){
                   proStatusStr = ResourceMgt.findDefaultText("cast.role.not.exist");
                   failCount++;
                   result = CntUtils.getResourceText(CntConstants.CNT_FAIL);
                   operateInfo = new OperateInfo(String.valueOf(num), crmList.get(i).getCastrolemapid(), 
                           result.substring(2, result.length()), proStatusStr);
                   returnInfo.appendOperateInfo(operateInfo);  
                }else{
                    operdesc = publishCrmToPlatform(crmList.get(i).getCastrolemapindex(),platform,syncType);  
                    if (operdesc.substring(0, 1).equals("0"))  
                    {
                        successCount++;
                        result = ResourceMgt.findDefaultText(CommonLogConstant.RESOURCE_OPERATION_SUCCESS);
                        operateInfo = new OperateInfo(String.valueOf(num), crmObj.getCastrolemapid(), 
                                result.substring(2, result.length()), operdesc.substring(2, operdesc.length()));
                        returnInfo.appendOperateInfo(operateInfo);
                    }
                    else  //内容关联运营平台失败
                    {
                        failCount++;
                        result = CntUtils.getResourceText(CntConstants.CNT_FAIL);
                        operateInfo = new OperateInfo(String.valueOf(num), crmObj.getCastrolemapid(), 
                                result.substring(2, result.length()), operdesc.substring(2, operdesc.length()));
                        returnInfo.appendOperateInfo(operateInfo);
                            
                    }
                }
                num++;
            }
        }catch(Exception e){
            log.error("cmsProgramLS exception:" + e.getMessage());
        }
        if ((successCount) == totalCastindex)
        {
            returnInfo.setReturnMessage(ResourceMgt.findDefaultText("success.count") + successCount);
            returnInfo.setFlag("0");
        }
        else
        {
            if ((failCount) == totalCastindex)
            {
                returnInfo.setReturnMessage(ResourceMgt.findDefaultText("fail.count") + failCount);
                returnInfo.setFlag("1");
            }
            else
            {
                returnInfo.setReturnMessage(ResourceMgt.findDefaultText("success.count") + successCount + 
                        ResourceMgt.findDefaultText("fail.count") + failCount);
                returnInfo.setFlag("2");
            }
        }

        return returnInfo.toString();   
        
    }
    
    /**
     * 角色发布到平台
     *
     * @param castrolemapindex 角色index 
     * @param platform 平台类型： 1:2.0平台，2:3.0平台
     * @param syncType 同步类型：1-REGIST，2-UPDATE，3-DELETE
     * @return  操作结果
     * @throws Exception ds异常
     */
    public String publishCrmToPlatform(Long castrolemapindex, int platform ,int syncType) throws Exception
    {
        log.debug("publishCrmToPlatform start ");
        List<Castrolemap> castrolemapList = new ArrayList<Castrolemap>();
        Castrolemap castrolemap = new Castrolemap();
        List<Castcdn> castcodeList = new ArrayList<Castcdn>();
        Castcdn cast = new Castcdn();
        StringBuffer resultStr = new StringBuffer();
        boolean sucess = false;
        String rtnStr; 
        Long batchid = (Long)this.getPrimaryKeyGenerator().getPrimarykey("ucdn_task_batch_id");
        
        try{
            castrolemap.setCastrolemapindex(castrolemapindex);
            castrolemap = castrolemapDS.getCastrolemap(castrolemap);
            if(castrolemap == null){
                return "1:角色不存在";
            }else{
                cast.setCastindex(castrolemap.getCastindex());
                cast = castds.getCastcdn(cast);
                castrolemapList.add(castrolemap);
                castcodeList.add(cast);
                map.put("castrolemapList", castrolemapList);
                map.put("castcodeList", castcodeList);
            }
            
            Long relateindex = null;
            CntPlatformSync crmPlatform = new CntPlatformSync();
            CntTargetSync target = new CntTargetSync();
            
            crmPlatform.setObjecttype(12);
            crmPlatform.setObjectid(castrolemap.getCastrolemapid());
            crmPlatform.setPlatform(platform);
            List<CntPlatformSync> crmPlatformList = cntPlatformSyncDS.getCntPlatformSyncByCond(crmPlatform);
            if(crmPlatformList != null && crmPlatformList.size() > 0){
                int castSyncStatus = crmPlatformList.get(0).getStatus();
                relateindex = crmPlatformList.get(0).getSyncindex();
                if(syncType == 1){ //新增同步
                    //0：待发布，400：发布失败
                    if(castSyncStatus != 0 && castSyncStatus!= 400){
                        return "1:角色发布状态不正确";
                    }
                }else{
                    if(castSyncStatus != 300 && castSyncStatus!= 500 && castSyncStatus!= 400){
                        return "1:角色发布状态不正确";
                    }
                }
            }else
            {
                return "1:发布记录不存在";
            }
            
            target.setRelateindex(relateindex);
            List<CntTargetSync> crmTargetList = cntTargetSyncDS.getCntTargetSyncByCond(target);
            if(syncType == 1){
                if(crmTargetList != null && crmTargetList.size() > 0 )
                {
                    for(CntTargetSync crmTargetSync:crmTargetList){
                        Long crmtargetindex = crmTargetSync.getTargetindex();
                        CntTargetSync casttarget = new CntTargetSync();
                        casttarget.setObjecttype(11);
                        casttarget.setObjectid(castrolemap.getCastid());
                        casttarget.setTargetindex(crmtargetindex);
                        List<CntTargetSync> castTargetList = cntTargetSyncDS.getCntTargetSyncByCond(casttarget);
                        if(castTargetList != null && castTargetList.size() > 0 )
                        {
                            int castSyncStatus = castTargetList.get(0).getStatus();
                            if(castSyncStatus!=300&&castSyncStatus!=500){
                                return "1:人物发布状态不正确";
                            }
                        }
                    }
                        
                }
            }else if(syncType == 3)
            {
                CntTargetSync othTarget = new CntTargetSync();
                ProgramCrmMap programCrmMap = new ProgramCrmMap();
                programCrmMap.setCastrolemapindex(castrolemapindex);
                List<ProgramCrmMap> pcrmMapList = programCrmMapDS.getProgramCrmMapByCond(programCrmMap);
                if(pcrmMapList != null && pcrmMapList.size() > 0 ){
                    for(ProgramCrmMap pcrm:pcrmMapList){
                        crmPlatform.setObjecttype(34);
                        crmPlatform.setObjectid(pcrm.getMappingid());
                        crmPlatform.setPlatform(platform);
                        List<CntPlatformSync> pcrmPlatformList = cntPlatformSyncDS.getCntPlatformSyncByCond(crmPlatform);
                        if(pcrmPlatformList != null && pcrmPlatformList.size() > 0 )
                        {
                            othTarget.setRelateindex(pcrmPlatformList.get(0).getSyncindex());
                            List<CntTargetSync> otherTargetList = cntTargetSyncDS.getCntTargetSyncByCond(othTarget);
                            if(otherTargetList != null && otherTargetList.size() > 0 )
                            {
                                for(CntTargetSync otherTargetSync:otherTargetList){
                                    if(otherTargetSync.getStatus() == 300 || otherTargetSync.getStatus()== 500 || otherTargetSync.getStatus()== 200){
                                        return "1:该角色与点播内容的关联关系已经发布，请先取消发布的关联关系";
                                    }
                                }
                            }
                        }
                    }
                }
                
                SeriesCrmMap seriesCrmMap = new SeriesCrmMap();
                seriesCrmMap.setCastrolemapindex(castrolemapindex);
                List<SeriesCrmMap> scrmMapList = seriesCrmMapDS.getSeriesCrmMapByCond(seriesCrmMap);
                if(scrmMapList != null && scrmMapList.size() > 0 ){
                    for(SeriesCrmMap scrm:scrmMapList){
                        crmPlatform.setObjecttype(37);
                        crmPlatform.setObjectid(scrm.getMappingid());
                        crmPlatform.setPlatform(platform);
                        List<CntPlatformSync> scrmPlatformList = cntPlatformSyncDS.getCntPlatformSyncByCond(crmPlatform);
                        if(scrmPlatformList != null && scrmPlatformList.size() > 0 )
                        {
                            othTarget.setRelateindex(scrmPlatformList.get(0).getSyncindex());
                            List<CntTargetSync> otherTargetList = cntTargetSyncDS.getCntTargetSyncByCond(othTarget);
                            if(otherTargetList != null && otherTargetList.size() > 0 )
                            {
                                for(CntTargetSync otherTargetSync:otherTargetList){
                                    if(otherTargetSync.getStatus() == 300 || otherTargetSync.getStatus()== 500 || otherTargetSync.getStatus()== 200){
                                        return "1:该角色与连续剧内容的关联关系已经发布，请先取消发布的关联关系";
                                    }
                                }
                            }
                        }
                    }
                }
                
            }
            
            String desPath = cmsStorageareaLS.getAddress(TEMPAREA_ID);
            if (null == desPath || (TEMPAREA_ERRORSIGN).equals(desPath)) // 获取临时区地址失败
            {
                return "1:操作失败,获取临时区地址失败";
            }
            else
            {
                File makePath = new File(GlobalConstants.getMountPoint() + desPath + File.separator);
                if (!makePath.exists())
                {// 找不到目标地址，请检查存储区绑定
                    return "1:操作失败,找不到临时区目标地址，请检查存储区绑定";
                }             
            }
            
            int[] crmTargetStatus = new int[crmTargetList.size()];
            if(crmTargetList != null && crmTargetList.size() > 0 )
            {
                for(CntTargetSync crmTargetSync:crmTargetList)
                { 
                    String correlateid = this.getPrimaryKeyGenerator().getPrimarykey(CORRELATEID).toString();
                    Long index = (Long)this.getPrimaryKeyGenerator().getPrimarykey("cnt_sync_task"); //获取taskindex
                    Long syncindex = (Long)this.getPrimaryKeyGenerator().getPrimarykey("object_sync_record_index");
                    String wg18xml = "";
                    String castrolemapxml = "";
                    int operresult = 0;
                    int targetStatus = 0;
                    Targetsystem tagertsystem = new Targetsystem();
                    tagertsystem.setTargetindex(crmTargetSync.getTargetindex());
                    tagertsystem = targetSystemDS.getTargetsystem(tagertsystem);
                    if(tagertsystem.getStatus()==1 || tagertsystem.getStatus()==2)
                    {
                        resultStr.append("目标系统"+tagertsystem.getTargetid()+"状态为暂停或者已注销  ");
                        continue;
                    }
                    
                    if((syncType == 1 && crmTargetSync.getStatus() == 0) || (syncType == 1 &&crmTargetSync.getStatus() == 400))
                    {
                        if(platform ==1) //2.0
                        { 
                            wg18xml = StandardImpFactory.getInstance().create(1).createXmlFile(map, tagertsystem.getTargettype(), 12, 1);
                            if(wg18xml == null)
                            {
                                resultStr.append("目标系统"+tagertsystem.getTargetid()+"生成同步xml文件失败  ");
                                continue;
                            }
                            insertSyncTask(index,wg18xml,correlateid,crmTargetSync.getTargetindex(),batchid);  // 插入同步任务
                            insertObjectRecord( syncindex, index, castrolemapindex, castrolemap.getCastrolemapid(), 12
                                    ,crmTargetSync.getTargetindex(), syncType, castrolemap.getCastrolemapcode(),2);
                        }else if(platform ==2) //3.0
                        {
                            castrolemapxml = StandardImpFactory.getInstance().create(2).createXmlFile(map, tagertsystem.getTargettype(), 12, 1);
                            if(castrolemapxml == null)
                            {
                                resultStr.append("目标系统"+tagertsystem.getTargetid()+"生成同步xml文件失败  ");
                                continue;
                            }
                            insertSyncTask(index,castrolemapxml,correlateid,crmTargetSync.getTargetindex(),batchid);  // 插入同步任务
                            insertObjectRecord( syncindex, index, castrolemapindex,  castrolemap.getCastrolemapid(),12, 
                                    crmTargetSync.getTargetindex(), syncType,"",1);
                        }
                        
                        //更新网元状态
                        operresult = 10;
                        targetStatus = StatusTranslator.getTargetSyncStatus(operresult);
                        crmTargetSync.setStatus(targetStatus);
                        crmTargetSync.setOperresult(operresult);
                        cntTargetSyncDS.updateCntTargetSync(crmTargetSync);
                        
                        CommonLogUtil.insertOperatorLog( castrolemap.getCastrolemapid(), CastrolemapConstant.MGTTYPE_CASTROLEMAP_SYNC,CastrolemapConstant.OPERTYPE_CASTROLEMAP_PUBLISH,
                                CastrolemapConstant.OPERTYPE_CASTROLEMAP_PUBLISH_INFO,CommonLogConstant.RESOURCE_OPERATION_SUCCESS);
                        
                        resultStr.append("目标系统"+tagertsystem.getTargetid()+"操作成功"+"  ");
                        sucess = true;
                        
                    }
                    else if((syncType == 3 && crmTargetSync.getStatus() == 300) || (syncType == 3 && crmTargetSync.getStatus() == 500))
                    {
                        if(platform ==1) //2.0
                        { 
                            wg18xml = StandardImpFactory.getInstance().create(1).createXmlFile(map, tagertsystem.getTargettype(), 12, 3);
                            if(wg18xml == null)
                            {
                                resultStr.append("目标系统"+tagertsystem.getTargetid()+"生成同步xml文件失败  ");
                                continue;
                            }
                            insertSyncTask(index,wg18xml,correlateid,crmTargetSync.getTargetindex(),batchid);  // 插入同步任务
                            insertObjectRecord( syncindex, index, castrolemapindex,  castrolemap.getCastrolemapid(), 12,
                                    crmTargetSync.getTargetindex(), syncType,castrolemap.getCastrolemapcode(),2);

                        }else if(platform ==2) //3.0
                        {
                            castrolemapxml = StandardImpFactory.getInstance().create(2).createXmlFile(map, tagertsystem.getTargettype(), 12, 3);
                            if(castrolemapxml == null)
                            {
                                resultStr.append("目标系统"+tagertsystem.getTargetid()+"生成同步xml文件失败  ");
                                continue;
                            }
                            insertSyncTask(index,castrolemapxml,correlateid,crmTargetSync.getTargetindex(),batchid);  // 插入同步任务
                            insertObjectRecord( syncindex, index, castrolemapindex,  castrolemap.getCastrolemapid(), 12,crmTargetSync.getTargetindex(), syncType,"",1);
                        }
                        
                        //更新网元状态
                        operresult = 70;
                        targetStatus = StatusTranslator.getTargetSyncStatus(operresult);
                        crmTargetSync.setStatus(targetStatus);
                        crmTargetSync.setOperresult(operresult);
                        cntTargetSyncDS.updateCntTargetSync(crmTargetSync);
                        
                        CommonLogUtil.insertOperatorLog( castrolemap.getCastrolemapid(), CastrolemapConstant.MGTTYPE_CASTROLEMAP_SYNC,CastrolemapConstant.OPERTYPE_CASTROLEMAP_CANCELPUBLISH,
                                CastrolemapConstant.OPERTYPE_CASTROLEMAP_CANCELPUBLISH__INFO,CommonLogConstant.RESOURCE_OPERATION_SUCCESS);
                        
                        resultStr.append("目标系统"+tagertsystem.getTargetid()+"操作成功"+"  ");
                        sucess = true;
                    }
                    else if(syncType == 3 && crmTargetSync.getStatus() == 400)
                    {
                        //更新网元状态
                        crmTargetSync.setStatus(0);
                        crmTargetSync.setOperresult(80);
                        cntTargetSyncDS.updateCntTargetSync(crmTargetSync);
                        
                        CommonLogUtil.insertOperatorLog( castrolemap.getCastrolemapid(), CastrolemapConstant.MGTTYPE_CASTROLEMAP_SYNC,CastrolemapConstant.OPERTYPE_CASTROLEMAP_CANCELPUBLISH,
                                CastrolemapConstant.OPERTYPE_CASTROLEMAP_CANCELPUBLISH__INFO,CommonLogConstant.RESOURCE_OPERATION_SUCCESS);
                        
                        resultStr.append("目标系统"+tagertsystem.getTargetid()+"操作成功"+"  ");
                        sucess = true;
                    }
                }
                if(crmTargetList != null && crmTargetList.size() > 0){
                    for(int i = 0; i< crmTargetList.size(); i++){
                        crmTargetStatus[i] = crmTargetList.get(i).getStatus();
                    }
                }
                //更新平台状态
                int platformStatus = StatusTranslator.getPlatformSyncStatus(crmTargetStatus);
                crmPlatform = crmPlatformList.get(0);
                crmPlatform.setStatus(platformStatus);
                cntPlatformSyncDS.updateCntPlatformSync(crmPlatform);
            }
            
        }
        catch (Exception e)
        {
            log.error("crmPlatformSyncLS exception:" + e.getMessage());
            return "1:操作失败";
        }
        
        log.debug("publishCrmToPlatform end ");
        rtnStr = sucess==false ? "1:"+resultStr.toString():"0:"+resultStr.toString();
        return rtnStr;
    }

    public TableDataInfo pageInfoQuery(CrmPlatformSync crmPlatformSync, int start, int pageSize) throws Exception
    {
        TableDataInfo dataInfo = null;
        try
        {
            log.debug("pageInfoQuery starting...");
     
            // 格式化内容带有时间的字段
            crmPlatformSync.setCreateStarttime(DateUtil2.get14Time(crmPlatformSync.getCreateStarttime()));
            crmPlatformSync.setCreateEndtime(DateUtil2.get14Time(crmPlatformSync.getCreateEndtime()));
            crmPlatformSync.setOnlinetimeStartDate(DateUtil2.get14Time(crmPlatformSync.getOnlinetimeStartDate()));
            crmPlatformSync.setOnlinetimeEndDate(DateUtil2.get14Time(crmPlatformSync.getOnlinetimeEndDate()));
            crmPlatformSync.setCntofflinestarttime(DateUtil2.get14Time(crmPlatformSync.getCntofflinestarttime()));
            crmPlatformSync.setCntofflineendtime(DateUtil2.get14Time(crmPlatformSync.getCntofflineendtime()));
            
            if(crmPlatformSync.getCastrolemapid()!=null&&!"".equals(crmPlatformSync.getCastrolemapid())){
                crmPlatformSync.setCastrolemapid(EspecialCharMgt.conversion(crmPlatformSync.getCastrolemapid()));
            }
            if(crmPlatformSync.getCastrole()!=null&&!"".equals(crmPlatformSync.getCastrole())){
                crmPlatformSync.setCastrole(EspecialCharMgt.conversion(crmPlatformSync.getCastrole()));
            }
            dataInfo = crmPlatformSyncDS.pageInfoQuery(crmPlatformSync, start, pageSize);
        }
        catch (Exception e)
        {
            throw e;
        }
        
        return dataInfo;
    }

    /**
     * 
     * @param taskindex 同步任务index
     * @param xmlAddress 同步xml文件地址
     * @param correlateid 流水号
     * @param targetindex 目标系统index
     */
    private void insertSyncTask( Long taskindex,  String xmlAddress, String correlateid ,Long targetindex,Long batchid)
    {
        CntSyncTask cntSyncTask = new CntSyncTask();
        SimpleDateFormat dateformat = new SimpleDateFormat("yyyyMMddHHmmss");
        try
        {
            cntSyncTask.setTaskindex(taskindex);
            cntSyncTask.setStatus(1);
            cntSyncTask.setPriority(5);
            cntSyncTask.setRetrytimes(3);
            cntSyncTask.setCorrelateid(correlateid);
            cntSyncTask.setContentmngxmlurl(xmlAddress);
            cntSyncTask.setDestindex(Long.valueOf(targetindex));
            cntSyncTask.setSource(1);
            cntSyncTask.setStarttime(dateformat.format(new Date()));
            cntSyncTask.setBatchid(batchid);
            cntsyncTaskDS.insertCntSyncTask(cntSyncTask);
        }
        catch (Exception e)
        {
            log.error("cmsProgramLS exception:" + e.getMessage());
        }
    }
 
    /**
     * 
     * @param syncindex 对象发布记录index
     * @param taskindex 同步任务index
     * @param objectindex 对象index
     * @param objectid 对象编码
     * @param objecttype 对象类型：1-Service，2-Category，3-Program，
     * @param targetindex 目标系统index
     * @param synType 同步类型：1-REGIST，2-UPDATE，3-DELETE
     * @param objectcode 发布到2.0平台时对象的文广code
     * @param platType 平台类型： 1:3.0平台，2:3.0平台
     */
    private void insertObjectRecord( Long syncindex,  Long taskindex,Long objectindex,String objectid, int objecttype, Long targetindex,int synType,String objectcode,int platType )
    {
        ObjectSyncRecord objectSyncRecord = new ObjectSyncRecord();
        SimpleDateFormat dateformat = new SimpleDateFormat("yyyyMMddHHmmss");
        try
        {
            objectSyncRecord.setSyncindex(syncindex);
            objectSyncRecord.setTaskindex(taskindex);
            objectSyncRecord.setObjectindex(objectindex);
            objectSyncRecord.setObjectid(objectid);
            objectSyncRecord.setDestindex(targetindex);
            objectSyncRecord.setObjecttype(objecttype);
            objectSyncRecord.setActiontype(synType);
            if (platType==2)
            {
                objectSyncRecord.setObjectcode(objectcode);
            }
            objectSyncRecord.setStarttime(dateformat.format(new Date()));
            objectSyncRecordDS.insertObjectSyncRecord(objectSyncRecord);
        }
        catch (Exception e)
        {
            log.error("cmsProgramLS exception:" + e.getMessage());
        }
    } 
    
    public void setLog(Logger log)
    {
        this.log = log;
    }

    public void setCrmPlatformSyncDS(ICrmPlatformSyncDS crmPlatformSyncDS)
    {
        this.crmPlatformSyncDS = crmPlatformSyncDS;
    }

}

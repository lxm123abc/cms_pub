package com.zte.cms.service.dao;

import com.zte.cms.service.model.Service;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DAOException;

public interface IServiceContentBindDAO
{

    /**
     * 查询服务和内容绑定关系的列表，根据传入的内容的类型不同，查询不同的sql语句，其中不同的sql语句对应不同的服务内容绑定表
     * 
     * @param service，
     * @param start
     * @param pageSize
     * @param puEntity
     * @return
     * @throws DAOException
     */
    public PageInfo pageInfoQueryCntBindService(Service service, int start, int pageSize, PageUtilEntity puEntity)
            throws DAOException;

    /**
     * 查询可以绑定服务的内容
     * 
     * @param service
     * @param start
     * @param pageSize
     * @param puEntity
     * @return
     * @throws DAOException
     */
    public PageInfo pageInfoQueryContentForServiceBind(Service service, int start, int pageSize, PageUtilEntity puEntity)
            throws DAOException;

    /**
     * 查询服务和栏目的关联关系
     * 
     * @param service Service
     * @param start int
     * @param pageSize int
     * @param puEntity PageUtilEntity
     * @return TableDataInfo
     * @throws DAOException DAOException
     */
    public PageInfo pageInfoQueryServiceCategoryMap(Service service, int start, int pageSize, 
            PageUtilEntity puEntity)throws DAOException;
    
    /**
     * 
     * @param service Service
     * @param start int
     * @param pageSize int
     * @param puEntity PageUtilEntity
     * @return PageInfo
     * @throws DAOException DAOException
     */
    public PageInfo serviceCntMapQuery(Service service, int start, int pageSize, PageUtilEntity puEntity)
        throws DAOException;
    
    //查询服务是否绑定内容
    public boolean isBingContent(Service service) throws DAOException;

}
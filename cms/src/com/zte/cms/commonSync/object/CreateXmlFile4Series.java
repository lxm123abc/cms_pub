package com.zte.cms.commonSync.object;

import java.io.File;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.dom4j.Document;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;

import com.zte.cms.common.GlobalConstants;
import com.zte.cms.commonSync.common.AbstractCreateXmlFile4Object;
import com.zte.cms.content.series.model.CmsSeries;
import com.zte.ismp.common.util.StrUtil;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;

public class CreateXmlFile4Series extends AbstractCreateXmlFile4Object
{

    public String createXmlFile4WG18(Map<String, List> objectMap, int targetType, int syncType)
    {
        
        
        Date date = new Date();
        String fileName = null;
       
        // 获取发布Action类型
        String action = "";
        if (syncType == 1)
        {
            action = "REGIST";
        }else if (syncType == 2)
        {
            action = "UPDATE";
        }
        else if (syncType == 3)
        {
            action = "DELETE";
        }else
        {
            return null;
        }
        if (targetType == 0)
        {
            fileName = "series_20wg_"+action.toLowerCase().replace(" ", "")+"_"+date.getTime()+".xml";
        }else
        {
            return null;
        }
        // 创建XML文件
        String tempFilePath = mountPoint + tempAreaPath + File.separator + CNTSYNCXML + File.separator + getCurrentTime()
                + File.separator + "series" + File.separator;
        String retPath = GlobalConstants.filterSlashStr(tempAreaPath + File.separator + CNTSYNCXML + File.separator + getCurrentTime()
                + File.separator + "series" + File.separator + fileName);
        File file = new File(tempFilePath);
        if (!file.exists())
        {
            file.mkdirs();
        }

        String fileFullName = tempFilePath + fileName;
        Document dom = DocumentHelper.createDocument();
        Element adiElement = dom.addElement("ADI");
        Element objectsElement = adiElement.addElement("Objects");
     
        List<CmsSeries> seriesList = (List<CmsSeries>)objectMap.get("seriesList");
        if (null == seriesList || 0 == seriesList.size())
        {
            return null;
        }
        
        for (CmsSeries series : seriesList)
        {
            Element objectElement = null;
            Element seriesElement = null;
            // 元素节点
            objectElement = objectsElement.addElement("Object");
            objectElement.addAttribute("ElementType", "Series");
            objectElement.addAttribute("ID", series.getCpcontentid());
            objectElement.addAttribute("Action", action);
            objectElement.addAttribute("Code", series.getCpcontentid());
            writeElement(objectElement, seriesElement, "Name", series.getNamecn(),true);// 插入连续剧名字
            writeElement(objectElement, seriesElement, "OrderNumber", series.getOrdernumber(),false);// 插入订购编号
            writeElement(objectElement, seriesElement, "OriginalName", series.getOriginalnamecn(),true); // 插入原名
            writeElement(objectElement, seriesElement, "SortName", series.getSortname(),true); // 插入排序名称
            writeElement(objectElement, seriesElement, "SearchName", series.getSearchname(),true); // 插入索引名称
            writeElement(objectElement, seriesElement, "OrgAirDate", series.getOrgairdate(),false); // 插入首播日期yyyyMMdd
            writeElement(objectElement, seriesElement, "LicensingWindowStart", series.getLicensingstart(),false); // 有效订购开始时间(YYYYMMDDHH24MiSS)
            writeElement(objectElement, seriesElement, "LicensingWindowEnd", series.getLicensingend(),false); // 有效订购结束时间(YYYYMMDDHH24MiSS)
            writeElement(objectElement, seriesElement, "DisplayAsNew", series.getNewcomedays(),false);// 新到天数
            writeElement(objectElement, seriesElement, "DisplayAsLastChance", series.getRemainingdays(),false); // 剩余天数
            writeElement(objectElement, seriesElement, "Macrovision", series.getMacrovision(),false);// 拷贝保护标志(0/1)
            writeElement(objectElement, seriesElement, "Price", series.getPricetaxin(),false); // 含税定价
            writeElement(objectElement, seriesElement, "VolumnCount", series.getVolumncount(),false); // 总集数
            writeElement(objectElement, seriesElement, "Status", series.getIsvalid(),false); // 生效失效
            writeElement(objectElement, seriesElement, "Description", series.getDesccn(),true); // 描述信息
            writeElement(objectElement, seriesElement, "Type", "连续剧",true); // 描述信息
            writeElement(objectElement, seriesElement, "KeyWords", series.getKeywords(),true); // 关键字
            writeElement(objectElement, seriesElement, "Tags", series.getContenttags(),true); // 标签
            writeElement(objectElement, seriesElement, "Reserve1", series.getReserve1(),false);// 保留字段
            writeElement(objectElement, seriesElement, "Reserve2", series.getReserve1(),false); // 保留字段
            writeElement(objectElement, seriesElement, "Reserve3", series.getReserve1(),false);// 保留字段
            writeElement(objectElement, seriesElement, "Reserve4", series.getReserve1(),false); // 保留字段
            writeElement(objectElement, seriesElement, "Reserve5", series.getReserve1(),false);// 保留字段
            
        }
        
        writeDom(dom,fileFullName);
        
        return retPath;
    }

    public String createXmlFile4IPTV30(Map<String, List> objectMap, int targetType, int syncType)
    {
        Date date = new Date();
        String fileName = null;
        String action = "";
        // 获取发布Action类型
        if (syncType == 1 || syncType == 2)
        {
            action = "REGIST or UPDATE";
        }
        else if (syncType == 3)
        {
            action = "DELETE";
        }
        if (targetType == 1)
        {
            fileName = "series_bms_"+action.toLowerCase().replace(" ", "")+"_"+date.getTime()+".xml";
        }
        else if (targetType == 2)
        {
            fileName = "series_epg_"+action.toLowerCase().replace(" ", "")+"_"+date.getTime()+".xml";
        }
        else if (targetType == 3)
        {
            fileName = "series_cdn_"+action.toLowerCase().replace(" ", "")+"_"+date.getTime()+".xml";
        }else
        {
            return null;
        }
        // 创建XML文件
        String tempFilePath = mountPoint + tempAreaPath + File.separator + CNTSYNCXML + File.separator + getCurrentTime()
                + File.separator + "series" + File.separator;
        String retPath = GlobalConstants.filterSlashStr(tempAreaPath + File.separator + CNTSYNCXML + File.separator + getCurrentTime()
                + File.separator + "series" + File.separator + fileName);
        File file = new File(tempFilePath);
        if (!file.exists())
        {
            file.mkdirs();
        }

        String fileFullName = tempFilePath + fileName;
        Document dom = DocumentHelper.createDocument();
        Element adiElement = dom.addElement("ADI");
        Element objectsElement = adiElement.addElement("Objects");
       
        
        List<CmsSeries> seriesList = (List<CmsSeries>)objectMap.get("seriesList");
        if (null == seriesList || 0 == seriesList.size())
        {
            return null;
        }
        
        for (CmsSeries series : seriesList)
        {
            Element objectElement = null;
            Element seriesElement = null;
            // 元素节点
            objectElement = objectsElement.addElement("Object");
            objectElement.addAttribute("ElementType", "Series");
            objectElement.addAttribute("ContentID", series.getSeriesid());
            
            objectElement.addAttribute("Action", action);
            String unicontentid = series.getUnicontentid();
            if (unicontentid == null)
            {
                unicontentid = "";
            }
            writeElement(objectElement, seriesElement, "CPContentID", series.getCpcontentid(),false);
            writeElement(objectElement, seriesElement, "UniContentId", unicontentid,false);           
            writeElement(objectElement, seriesElement, "Name", series.getNamecn(),true);// 插入连续剧名字
            writeElement(objectElement, seriesElement, "OrderNumber", series.getOrdernumber(),false);// 插入订购编号
            writeElement(objectElement, seriesElement, "OriginalName", series.getOriginalnamecn(),true); // 插入原名
            writeElement(objectElement, seriesElement, "SortName", series.getSortname(),true); // 插入排序名称
            writeElement(objectElement, seriesElement, "SearchName", series.getSearchname(),true); // 插入索引名称
            writeElement(objectElement, seriesElement, "OrgAirDate", series.getOrgairdate(),false); // 插入首播日期yyyyMMdd
            writeElement(objectElement, seriesElement, "LicensingWindowStart", series.getLicensingstart(),false); // 有效订购开始时间(YYYYMMDDHH24MiSS)
            writeElement(objectElement, seriesElement, "LicensingWindowEnd", series.getLicensingend(),false); // 有效订购结束时间(YYYYMMDDHH24MiSS)
            writeElement(objectElement, seriesElement, "DisplayAsNew", series.getNewcomedays(),false);// 新到天数
            writeElement(objectElement, seriesElement, "DisplayAsLastChance", series.getRemainingdays(),false); // 剩余天数
            writeElement(objectElement, seriesElement, "Macrovision", series.getMacrovision(),false);// 拷贝保护标志(0/1)
            writeElement(objectElement, seriesElement, "Price", series.getPricetaxin(),false); // 含税定价
            writeElement(objectElement, seriesElement, "VolumnCount", series.getVolumncount(),false); // 总集数
            writeElement(objectElement, seriesElement, "Status", series.getIsvalid(),false); // 生效失效
            writeElement(objectElement, seriesElement, "Description", series.getDesccn(),true); // 描述信息

            writeElement(objectElement, seriesElement, "ContentProvider", series.getCpid(),false);// 内容提供商
            writeElement(objectElement, seriesElement, "KeyWords", series.getKeywords(),true); // 关键字
            writeElement(objectElement, seriesElement, "Tags", series.getContenttags(),true); // 标签
            writeElement(objectElement, seriesElement, "ViewPoint", series.getViewpoint(),true);// 看点
            writeElement(objectElement, seriesElement, "StarLevel", series.getRecommendstart(),false);// 推荐星级
            writeElement(objectElement, seriesElement, "Rating", series.getRating(),false);// 限制类别
            writeElement(objectElement, seriesElement, "Awards", series.getAwards(),true);// 所含奖项
            writeElement(objectElement, seriesElement, "Reserve1", series.getReserve1(),false);// 保留字段
            writeElement(objectElement, seriesElement, "Reserve2", series.getReserve1(),false); // 保留字段
            
        }
        
        writeDom(dom,fileFullName);
        
        return retPath;
    }

    @Override
    public String createXmlFile4YS26(Map<String, List> objectMap, int targetType, int syncType)
            throws DomainServiceException
    {
        Date date = new Date();
        String fileName = null;
       
        // 获取发布Action类型
        String action = "";
        if (syncType == 1)
        {
            action = "REGIST";
        }else if (syncType == 2)
        {
            action = "UPDATE";
        }
        else if (syncType == 3)
        {
            action = "DELETE";
        }else
        {
            return null;
        }
        if (targetType == 10)
        {
            fileName = "series_20ys_"+action.toLowerCase().replace(" ", "")+"_"+date.getTime()+".xml";
        }else
        {
            return null;
        }
        // 创建XML文件
        String tempFilePath = mountPoint + tempAreaPath + File.separator + CNTSYNCXML + File.separator + getCurrentTime()
                + File.separator + "series" + File.separator;
        String retPath = GlobalConstants.filterSlashStr(tempAreaPath + File.separator + CNTSYNCXML + File.separator + getCurrentTime()
                + File.separator + "series" + File.separator + fileName);
        File file = new File(tempFilePath);
        if (!file.exists())
        {
            file.mkdirs();
        }

        String fileFullName = tempFilePath + fileName;
        Document dom = DocumentHelper.createDocument();
        Element adiElement = dom.addElement("ADI");
        Element objectsElement = adiElement.addElement("Objects");
     
        List<CmsSeries> seriesList = (List<CmsSeries>)objectMap.get("seriesList");
        if (null == seriesList || 0 == seriesList.size())
        {
            return null;
        }
        /**处理插入连续剧与角色的相关的人物名称  begin***/
        List<CmsSeries> castnameTypeList = (List<CmsSeries>)objectMap.get("castnameTypeList");
        StringBuffer kpeopleStrBuffer = new StringBuffer("") ;
        StringBuffer directorStrBuffer = new StringBuffer("") ;
        if (null != castnameTypeList && castnameTypeList.size() > 0)
        {
            for (int j = 0; j < castnameTypeList.size(); j++)
            {
                String castname = castnameTypeList.get(j).getCastname();
                int roletype = castnameTypeList.get(j).getRoletype();
                String tempStr;
                if (roletype == 1)
                {   tempStr = directorStrBuffer.toString() + " " + castname;
                    if (StrUtil.exlen(tempStr.trim(), "en") <= 50)
                    {
                        directorStrBuffer.append(" ").append(castname);
                    }  
                }
                else
                {
                    tempStr = kpeopleStrBuffer.toString() + " " + castname;
                    if (StrUtil.exlen(tempStr.trim(),"en") <= 150)
                    {
                        kpeopleStrBuffer.append(" ").append(castname);
                    } 
                }
            }           
        }        
        for (CmsSeries series : seriesList)
        {
            Element objectElement = null;
            Element seriesElement = null;
            // 元素节点
            objectElement = objectsElement.addElement("Object");
            objectElement.addAttribute("ElementType", "Series");
            objectElement.addAttribute("ID", series.getCpcontentid());
            objectElement.addAttribute("Action", action);
            objectElement.addAttribute("Code", series.getCpcontentid());
            writeElement(objectElement, seriesElement, "Name", series.getNamecn(),true);// 插入连续剧名字
            writeElement(objectElement, seriesElement, "OrderNumber", series.getOrdernumber(),false);// 插入订购编号
            writeElement(objectElement, seriesElement, "OriginalName", series.getOriginalnamecn(),true); // 插入原名
            writeElement(objectElement, seriesElement, "SortName", series.getSortname(),true); // 插入排序名称
            writeElement(objectElement, seriesElement, "SearchName", series.getSearchname(),true); // 插入索引名称
            writeElement(objectElement, seriesElement, "OrgAirDate", series.getOrgairdate(),false); // 插入首播日期yyyyMMdd
            writeElement(objectElement, seriesElement, "LicensingWindowStart", series.getLicensingstart(),false); // 有效订购开始时间(YYYYMMDDHH24MiSS)
            writeElement(objectElement, seriesElement, "LicensingWindowEnd", series.getLicensingend(),false); // 有效订购结束时间(YYYYMMDDHH24MiSS)
            writeElement(objectElement, seriesElement, "DisplayAsNew", series.getNewcomedays(),false);// 新到天数
            writeElement(objectElement, seriesElement, "DisplayAsLastChance", series.getRemainingdays(),false); // 剩余天数
            writeElement(objectElement, seriesElement, "Macrovision", series.getMacrovision(),false);// 拷贝保护标志(0/1)
            writeElement(objectElement, seriesElement, "Price", series.getPricetaxin(),false); // 含税定价
            writeElement(objectElement, seriesElement, "VolumnCount", series.getVolumncount(),false); // 总集数
            writeElement(objectElement, seriesElement, "Status", series.getIsvalid(),false); // 生效失效
            writeElement(objectElement, seriesElement, "Description", series.getDesccn(),true); // 描述信息
            //央视2.6新增字段
            writeElement(objectElement, seriesElement, "Kpeople", series.getKpeople() != null && !series.getKpeople().equals("") ? series.getKpeople() : kpeopleStrBuffer.toString().trim(),true); // 主要人物
            writeElement(objectElement, seriesElement, "Director", series.getDirectors() != null && !series.getDirectors().equals("") ? series.getDirectors() : directorStrBuffer.toString().trim(),true); // 导演
            writeElement(objectElement, seriesElement, "ScriptWriter", series.getScriptwriter(),true); // 编剧
            writeElement(objectElement, seriesElement, "Compere", series.getCompere(),true); // 节目主持人
            writeElement(objectElement, seriesElement, "Guest", series.getGuest(),true); // 受访者
            writeElement(objectElement, seriesElement, "Reporter", series.getReporter(),true); // 记者
            writeElement(objectElement, seriesElement, "OPIncharge", series.getOpincharge(),true); // 其他责任人
            
        }
        
        writeDom(dom,fileFullName);
        
        return retPath;
    }

}

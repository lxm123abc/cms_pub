package com.zte.cms.content.model;

import com.zte.ssb.dynamicobj.DynamicBaseObject;

public class CmsTasksourceMap extends DynamicBaseObject
{
    private java.lang.Long srcmapindex;
    private java.lang.Long taskindex;
    private java.lang.Long fileindex;
    private java.lang.String mediasrcxmlpath;

    public java.lang.Long getSrcmapindex()
    {
        return srcmapindex;
    }

    public void setSrcmapindex(java.lang.Long srcmapindex)
    {
        this.srcmapindex = srcmapindex;
    }

    public java.lang.Long getTaskindex()
    {
        return taskindex;
    }

    public void setTaskindex(java.lang.Long taskindex)
    {
        this.taskindex = taskindex;
    }

    public java.lang.Long getFileindex()
    {
        return fileindex;
    }

    public void setFileindex(java.lang.Long fileindex)
    {
        this.fileindex = fileindex;
    }

    public java.lang.String getMediasrcxmlpath()
    {
        return mediasrcxmlpath;
    }

    public void setMediasrcxmlpath(java.lang.String mediasrcxmlpath)
    {
        this.mediasrcxmlpath = mediasrcxmlpath;
    }

    public void initRelation()
    {
        this.addRelation("srcmapindex", "SRCMAPINDEX");
        this.addRelation("taskindex", "TASKINDEX");
        this.addRelation("fileindex", "FILEINDEX");
        this.addRelation("mediasrcxmlpath", "MEDIASRCXMLPATH");
    }
}

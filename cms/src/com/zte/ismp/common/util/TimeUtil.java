package com.zte.ismp.common.util;

import java.util.*;
import java.text.*;

/**
 * <p>
 * 文件名称: TimeUtil.java
 * </p>
 * <p>
 * 文件描述: 时间操作类
 * </p>
 * <p>
 * 版权所有: 版权所有(C)2001-2004
 * </p>
 * <p>
 * 公 司: 中兴通讯股份有限公司
 * </p>
 * <p>
 * 内容摘要: 时间操作类
 * </p>
 * <p>
 * 其他说明:
 * </p>
 * <p>
 * 完成日期：2006年7月12日
 * </p>
 * <p>
 * 修改记录1:
 * </p>
 * 
 * <pre>
 *    修改日期：
 *    版 本 号：
 *    修 改 人：
 *    修改内容：
 * </pre>
 * 
 * <p>
 * 修改记录2：
 * </p>
 * 
 * @version 1.0
 * @author 孔建华
 */

public class TimeUtil
{
    // ****** 代码段: 常量定义 *******************************************************************************/

    // ****** 代码段: 变量定义 *******************************************************************************/
    private Date time;
    private Date timem;
    private String strtime;
    private SimpleDateFormat format;
    private SimpleDateFormat format1;

    // ****** 代码段: 构造方法 *******************************************************************************/
    public TimeUtil()
    {
        time = new Date();
        strtime = "";
        format = null;
    }

    // ****** 代码段: 公共方法 *******************************************************************************/
    /**
     * 取得年份
     * 
     * @return int
     */
    public int getYear()
    {
        format = new SimpleDateFormat("yyyy", Locale.getDefault());
        strtime = format.format(time);
        return Integer.parseInt(strtime);
    }

    /**
     * 取得月份
     * 
     * @return int
     */
    public int getMonth()
    {
        format = new SimpleDateFormat("MM", Locale.getDefault());
        strtime = format.format(time);
        return Integer.parseInt(strtime);
    }

    /**
     * 取得日份
     * 
     * @return int
     */
    public int getSun()
    {
        format = new SimpleDateFormat("dd", Locale.getDefault());
        strtime = format.format(time);
        return Integer.parseInt(strtime);
    }

    /**
     * 取得星期
     * 
     * @return int
     */
    // public int getWeek()
    // {
    // return time.getDay();
    // }
    /**
     * 取得小时
     * 
     * @return int
     */
    public int getHour()
    {
        format = new SimpleDateFormat("HH", Locale.getDefault());
        strtime = format.format(time);
        return Integer.parseInt(strtime);
    }

    /**
     * 取得分钟
     * 
     * @return int
     */
    public int getMinu()
    {
        format = new SimpleDateFormat("mm", Locale.getDefault());
        strtime = format.format(time);
        return Integer.parseInt(strtime);
    }

    /**
     * 取得秒数
     * 
     * @return int
     */
    public int getSecond()
    {
        format = new SimpleDateFormat("ss", Locale.getDefault());
        strtime = format.format(time);
        return Integer.parseInt(strtime);
    }

    /**
     * 取得年-月-日
     * 
     * @return String
     */
    public String getYMD()
    {
        format = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
        Date currentdate = new Date();
        strtime = format.format(currentdate);
        return strtime;
    }

    /**
     * 取得时:分:秒
     * 
     * @return String
     */
    public String getHMS()
    {
        format = new SimpleDateFormat("HH:mm:ss", Locale.getDefault());
        strtime = format.format(time);
        return strtime;
    }

    /**
     * 取得年-月-日 时:分:秒
     * 
     * @return String
     */
    public String getYMDHMS()
    {
        format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        strtime = format.format(time);
        return strtime;
    }

    /**
     * 根据类型定制
     * 
     * @param dateformat String
     * @return String
     */
    public String getYMDHMS(String dateformat)
    {
        format = new SimpleDateFormat(dateformat, Locale.getDefault());
        strtime = format.format(time);
        return strtime;
    }

    /**
     * 获小时间差,输入的时间减现在的时间
     * 
     * @param in String
     * @return int
     */
    public int getMinH(String in)
    {
        format = new SimpleDateFormat("HH:mm:ss", Locale.getDefault());
        format1 = new SimpleDateFormat("HH", Locale.getDefault());
        try
        {
            timem = format.parse(in);
            strtime = format1.format(timem);
            System.err.print(strtime);
        }
        catch (ParseException e)
        {
            e.printStackTrace();
        }
        return Integer.parseInt(strtime) - this.getHour();
    }

    /**
     * 获分钟差,输入的时间减现在的时间
     * 
     * @param in String
     * @return int
     */
    public int getMinM(String in)
    {
        format = new SimpleDateFormat("HH:mm:ss", Locale.getDefault());
        format1 = new SimpleDateFormat("mm", Locale.getDefault());
        try
        {
            timem = format.parse(in);
            strtime = format1.format(timem);
        }
        catch (ParseException e)
        {
            e.printStackTrace();
        }
        return Integer.parseInt(strtime) - this.getMinu();
    }

    /**
     * 获秒差,输入的时间减现在的时间
     * 
     * @param in String
     * @return int
     */
    public int getMinS(String in)
    {
        format = new SimpleDateFormat("HH:mm:ss", Locale.getDefault());
        format1 = new SimpleDateFormat("ss", Locale.getDefault());
        try
        {
            timem = format.parse(in);
            strtime = format1.format(timem);
        }
        catch (ParseException e)
        {
            e.printStackTrace();
        }
        return Integer.parseInt(strtime) - this.getSecond();
    }

    /**
     * 根据开始时间和时间差获取结束时间
     * 
     * @param starttime String 开始时间 格式HH:mm:ss
     * @param timespan int 时间差 秒
     * @return String
     */
    public String getTimeBySpan(String starttime, int timespan, boolean after)
    {
        format = new SimpleDateFormat("HH:mm:ss", Locale.getDefault());
        try
        {
            timem = format.parse(starttime);
            if (after)
            {
                time = new Date(timem.getTime() + timespan * 1000);
            }
            else
            {
                time = new Date(timem.getTime() - timespan * 1000);
            }
            strtime = format.format(time);
        }
        catch (ParseException e)
        {
            e.printStackTrace();
        }
        return strtime;
    }

    /**
     * 根据开始时间和时间差获取结束时间
     * 
     * @param starttime String 开始时间 格式yyyy-MM-dd HH:mm:ss
     * @param timespan int 时间差 秒
     * @return String
     */
    public String getFullTimeBySpan(String starttime, int timespan, boolean after)
    {
        format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        try
        {
            timem = format.parse(starttime);
            if (after)
            {
                time = new Date(timem.getTime() + timespan * 1000);
            }
            else
            {
                time = new Date(timem.getTime() - timespan * 1000);
            }
            strtime = format.format(time);
        }
        catch (ParseException e)
        {
            e.printStackTrace();
        }
        return strtime;
    }

    /**
     * 根据开始日期和日期差获取结束日期
     * 
     * @param starttime String 开始日期 格式yyyy-MM-dd
     * @param timespan int 日期差 天
     * @param after boolean 往前或往后
     * @return String
     */
    public String getDateBySpan(String starttime, int timespan, boolean after)
    {
        format = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
        try
        {
            timem = format.parse(starttime);
            if (after)
            {
                time = new Date(timem.getTime() + timespan * 24 * 60 * 60 * 1000);
            }
            else
            {
                time = new Date(timem.getTime() - timespan * 24 * 60 * 60 * 1000);
            }
            strtime = format.format(time);
        }
        catch (ParseException e)
        {
            e.printStackTrace();
        }
        return strtime;
    }

}

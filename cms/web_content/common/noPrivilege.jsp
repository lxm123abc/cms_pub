<%@ page language="java" pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title><fmt:message key='common.FrmTimeOut.title'/></title>
<link href="<%=request.getContextPath()%>/jam/css/cssCn.css" rel="stylesheet" type="text/css">
<style type="text/css">

-->
</style>
</head>

<body>
<table align=center cellPadding=0 cellSpacing=0 id=tb_content>
  <tr>
    <td><br>
      <br>
      <table width="350" height="171" border="0" align="center" cellpadding="0" cellspacing="0" class="tb_prompt1" >
        <tr>
          <td class="tr_Ptitle" ><fmt:message key='common.noPrivilege.title'/></td>
        </tr>
        <tr>
          <td height="114" colspan="2" style="button">
            <table width="250"  border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="36" align="right"><img src="<%=request.getContextPath()%>/jam/images/4.gif" width="36" height="36"></td>
                <td width="88%"><fmt:message key='common.noPrivilege.text'/></td>
              </tr>
          </table></td>
        </tr>
        <tr>
          <td height="27" colspan="2"><input type="button" onclick="window.history.back(); return false;" value="<fmt:message key='popmessage.009.name'/>" class="button"></td>
        </tr>
        <tr>
          <td colspan="2">&nbsp;</td>
        </tr>
      </table>
    <div align="center"></div></td>
  </tr>
</table>
</body>
</html>

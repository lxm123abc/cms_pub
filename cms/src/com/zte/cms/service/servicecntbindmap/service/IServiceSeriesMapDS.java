package com.zte.cms.service.servicecntbindmap.service;

import java.util.List;
import com.zte.cms.service.servicecntbindmap.model.ServiceSeriesMap;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;

public interface IServiceSeriesMapDS
{
    /**
     * 新增ServiceSeriesMap对象
     * 
     * @param serviceSeriesMap ServiceSeriesMap对象
     * @throws DomainServiceException ds异常
     */
    public void insertServiceSeriesMap(ServiceSeriesMap serviceSeriesMap) throws DomainServiceException;

    /**
     * 更新ServiceSeriesMap对象
     * 
     * @param serviceSeriesMap ServiceSeriesMap对象
     * @throws DomainServiceException ds异常
     */
    public void updateServiceSeriesMap(ServiceSeriesMap serviceSeriesMap) throws DomainServiceException;

    /**
     * 批量更新ServiceSeriesMap对象
     * 
     * @param serviceSeriesMap ServiceSeriesMap对象
     * @throws DomainServiceException ds异常
     */
    public void updateServiceSeriesMapList(List<ServiceSeriesMap> serviceSeriesMapList) throws DomainServiceException;

    /**
     * 删除ServiceSeriesMap对象
     * 
     * @param serviceSeriesMap ServiceSeriesMap对象
     * @throws DomainServiceException ds异常
     */
    public void removeServiceSeriesMap(ServiceSeriesMap serviceSeriesMap) throws DomainServiceException;

    /**
     * 批量删除ServiceSeriesMap对象
     * 
     * @param serviceSeriesMap ServiceSeriesMap对象
     * @throws DomainServiceException ds异常
     */
    public void removeServiceSeriesMapList(List<ServiceSeriesMap> serviceSeriesMapList) throws DomainServiceException;

    /**
     * 查询ServiceSeriesMap对象
     * 
     * @param serviceSeriesMap ServiceSeriesMap对象
     * @return ServiceSeriesMap对象
     * @throws DomainServiceException ds异常
     */
    public ServiceSeriesMap getServiceSeriesMap(ServiceSeriesMap serviceSeriesMap) throws DomainServiceException;

    /**
     * 根据条件查询ServiceSeriesMap对象
     * 
     * @param serviceSeriesMap ServiceSeriesMap对象
     * @return 满足条件的ServiceSeriesMap对象集
     * @throws DomainServiceException ds异常
     */
    public List<ServiceSeriesMap> getServiceSeriesMapByCond(ServiceSeriesMap serviceSeriesMap)
            throws DomainServiceException;

    /**
     * 根据条件分页查询ServiceSeriesMap对象
     * 
     * @param serviceSeriesMap ServiceSeriesMap对象，作为查询条件的参数
     * @param start 起始行
     * @param pageSize 页面大小
     * @param puEntity 排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws DomainServiceException ds异常
     */
    public TableDataInfo pageInfoQuery(ServiceSeriesMap serviceSeriesMap, int start, int pageSize)
            throws DomainServiceException;

    /**
     * 根据条件分页查询ServiceSeriesMap对象
     * 
     * @param serviceSeriesMap ServiceSeriesMap对象，作为查询条件的参数
     * @param start 起始行
     * @param pageSize 页面大小
     * @param puEntity 排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws DomainServiceException ds异常
     */
    public TableDataInfo pageInfoQuery(ServiceSeriesMap serviceSeriesMap, int start, int pageSize,
            PageUtilEntity puEntity) throws DomainServiceException;
}

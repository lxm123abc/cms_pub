package com.zte.cms.ctgsrmap.dao;

import java.util.List;

import com.zte.cms.ctgsrmap.model.CategorySeriesMap;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DAOException;

public interface ICategorySeriesMapDAO
{
    /**
     * 新增CategorySeriesMap对象
     * 
     * @param categorySeriesMap CategorySeriesMap对象
     * @throws DAOException dao异常
     */
    public void insertCategorySeriesMap(CategorySeriesMap categorySeriesMap) throws DAOException;

    /**
     * 根据主键更新CategorySeriesMap对象
     * 
     * @param categorySeriesMap CategorySeriesMap对象
     * @throws DAOException dao异常
     */
    public void updateCategorySeriesMap(CategorySeriesMap categorySeriesMap) throws DAOException;

    /**
     * 根据主键删除CategorySeriesMap对象
     * 
     * @param categorySeriesMap CategorySeriesMap对象
     * @throws DAOException dao异常
     */
    public void deleteCategorySeriesMap(CategorySeriesMap categorySeriesMap) throws DAOException;

    /**
     * 根据主键查询CategorySeriesMap对象
     * 
     * @param categorySeriesMap CategorySeriesMap对象
     * @return 满足条件的CategorySeriesMap对象
     * @throws DAOException dao异常
     */
    public CategorySeriesMap getCategorySeriesMap(CategorySeriesMap categorySeriesMap) throws DAOException;

    /**
     * 根据条件查询CategorySeriesMap对象
     * 
     * @param categorySeriesMap CategorySeriesMap对象
     * @return 满足条件的CategorySeriesMap对象集
     * @throws DAOException dao异常
     */
    public List<CategorySeriesMap> getCategorySeriesMapByCond(CategorySeriesMap categorySeriesMap) throws DAOException;

    /**
     * 根据条件分页查询CategorySeriesMap对象，作为查询条件的参数
     * 
     * @param categorySeriesMap CategorySeriesMap对象
     * @param start 起始行
     * @param pageSize 页面大小
     * @return 查询结果
     * @throws DAOException dao异常
     */
    public PageInfo pageInfoQuery(CategorySeriesMap categorySeriesMap, int start, int pageSize) throws DAOException;

    /**
     * 根据条件分页查询CategorySeriesMap对象，作为查询条件的参数
     * 
     * @param categorySeriesMap CategorySeriesMap对象
     * @param start 起始行
     * @param pageSize 页面大小
     * @param puEntity 排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws DAOException dao异常
     */
    public PageInfo pageInfoQuery(CategorySeriesMap categorySeriesMap, int start, int pageSize, PageUtilEntity puEntity)
            throws DAOException;

}
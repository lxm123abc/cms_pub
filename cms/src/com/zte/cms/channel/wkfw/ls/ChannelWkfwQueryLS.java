package com.zte.cms.channel.wkfw.ls;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.zte.cms.channel.ls.ChannelConstants;
import com.zte.cms.channel.wkfw.model.ChannelWkfwCondition;
import com.zte.cms.channel.wkfw.service.IChannelWkfwProcessWithMeDS;
import com.zte.cms.channel.wkfw.service.IChannelWkfwTaskForMeDS;

import com.zte.ssb.exportExcel.tableModel.TableDataInfo;

import com.zte.ssb.framework.base.util.PageUtilEntity;

import com.zte.ssb.framework.exception.exceptions.DomainServiceException;
import com.zte.umap.common.LoginMgt;
import com.zte.umap.common.ResourceMgt;
import com.zte.umap.common.model.OperInfo;

/**
 * 
 * @author Administrator
 * 
 */
public class ChannelWkfwQueryLS implements IChannelWkfwQueryLS
{
    protected IChannelWkfwTaskForMeDS channelWkfwTaskForMeDS = null;
    protected IChannelWkfwProcessWithMeDS channelWkfwProcessWithMeDS = null;

    /**
     * 
     * @return IChannelWkfwTaskForMeDS
     */
    public IChannelWkfwTaskForMeDS getChannelWkfwTaskForMeDS()
    {
        return channelWkfwTaskForMeDS;
    }

    /**
     * 
     * @param channelWkfwTaskForMeDS IChannelWkfwTaskForMeDS
     */
    public void setChannelWkfwTaskForMeDS(IChannelWkfwTaskForMeDS channelWkfwTaskForMeDS)
    {
        this.channelWkfwTaskForMeDS = channelWkfwTaskForMeDS;
    }

    /**
     * 
     * @return IChannelWkfwProcessWithMeDS
     */
    public IChannelWkfwProcessWithMeDS getChannelWkfwProcessWithMeDS()
    {
        return channelWkfwProcessWithMeDS;
    }

    /**
     * 
     * @param channelWkfwProcessWithMeDS IChannelWkfwProcessWithMeDS
     */
    public void setChannelWkfwProcessWithMeDS(IChannelWkfwProcessWithMeDS channelWkfwProcessWithMeDS)
    {
        this.channelWkfwProcessWithMeDS = channelWkfwProcessWithMeDS;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.zte.cms.strategy.wkfw.common.ls.IStrategyWkfwQueryLS
     *      #queryStrategyWkfwTaskForMeListCnt(com.zte.cms.strategy.wkfw.common.model.StrategyWkfwCondition)
     */
    /**
     * @param channelWkfwTaskForMeCondition ChannelWkfwCondition
     * @return int
     * @throws DomainServiceException DomainServiceException
     */
    public int queryChannelWkfwTaskForMeListCnt(ChannelWkfwCondition channelWkfwTaskForMeCondition)
            throws DomainServiceException
    {
        if (null == channelWkfwTaskForMeCondition.getPboType()
                || ("").equals(channelWkfwTaskForMeCondition.getPboType()))
        {
            channelWkfwTaskForMeCondition.setPboType(ChannelConstants.WKFWTYPE_CHANNEL_ALL);
        }
        channelWkfwTaskForMeCondition.setExecutorId(ChannelConstants.getUserid());
        channelWkfwTaskForMeCondition.setTaskStatus("N");
        return channelWkfwTaskForMeDS.queryChannelWkfwTaskForMeListCntByCond(channelWkfwTaskForMeCondition);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.zte.cms.strategy.wkfw.common.ls.IStrategyWkfwQueryLS
     *      #queryStrategyWkfwTaskForMe(com.zte.cms.strategy.wkfw.common.model.StrategyWkfwCondition, int, int)
     */
    /**
     * @param channelWkfwTaskForMeCondition ChannelWkfwCondition
     * @param rangeStart int
     * @param fetchSize int
     * @return TableDataInfo
     * @throws DomainServiceException DomainServiceException
     */
    public TableDataInfo queryChannelWkfwTaskForMe(ChannelWkfwCondition channelWkfwTaskForMeCondition, int rangeStart,
            int fetchSize) throws DomainServiceException
    {

        TableDataInfo tableInfo = null;
        PageUtilEntity puEntity = new PageUtilEntity();
        puEntity.setIsAlwaysSearchSize(true);
        puEntity.setIsAsc(false);
        puEntity.setOrderByColumn("processStartTime");
        tableInfo = queryChannelWkfwTaskForMe(channelWkfwTaskForMeCondition, rangeStart, fetchSize, puEntity);
        return tableInfo;
    }

    /**
     * @param channelWkfwTaskForMeCondition ChannelWkfwCondition
     * @param rangeStart int
     * @param fetchSize int
     * @param puEntity PageUtilEntity
     * @return TableDataInfo
     * @throws DomainServiceException DomainServiceException
     */
    public TableDataInfo queryChannelWkfwTaskForMe(ChannelWkfwCondition channelWkfwTaskForMeCondition, int rangeStart,
            int fetchSize, PageUtilEntity puEntity) throws DomainServiceException
    {
        if (null == channelWkfwTaskForMeCondition.getPboType()
                || ("").equals(channelWkfwTaskForMeCondition.getPboType()))
        {
            channelWkfwTaskForMeCondition.setPboType(ChannelConstants.WKFWTYPE_CHANNEL_ALL);
        }
        channelWkfwTaskForMeCondition.setExecutorId(ChannelConstants.getUserid());
        channelWkfwTaskForMeCondition.setTaskStatus("N");

        TableDataInfo tableInfo = null;
        OperInfo operinfo = LoginMgt.getOperInfo("CMS");
        channelWkfwTaskForMeCondition.setOperid(operinfo.getOperid());
        tableInfo = channelWkfwTaskForMeDS
                .pageInfoQuery(channelWkfwTaskForMeCondition, rangeStart, fetchSize, puEntity);
        return tableInfo;
    }

    /**
     * @param channelWkfwTaskForMeCondition ChannelWkfwCondition
     * @return int
     * @throws DomainServiceException DomainServiceException
     */
    public int queryChannelWkfwProcessWithMeListCnt(ChannelWkfwCondition channelWkfwTaskForMeCondition)
            throws DomainServiceException
    {
        if (null == channelWkfwTaskForMeCondition.getPboType()
                || ("").equals(channelWkfwTaskForMeCondition.getPboType()))
        {
            channelWkfwTaskForMeCondition.setPboType(ChannelConstants.WKFWTYPE_CHANNEL_ALL);
        }
        channelWkfwTaskForMeCondition.setOpername(ChannelConstants.getUserid());
        channelWkfwTaskForMeCondition.setUserid(ChannelConstants.getUserid());
        return channelWkfwProcessWithMeDS.queryChannelWkfwProcessWithMeListCntByCond(channelWkfwTaskForMeCondition);
    }

    /**
     * @param channelWkfwTaskForMeCondition ChannelWkfwCondition
     * @param rangeStart rangeStart
     * @param fetchSize int
     * @return TableDataInfo
     * @throws DomainServiceException DomainServiceException
     */
    public TableDataInfo queryChannelWkfwProcessWithMe(ChannelWkfwCondition channelWkfwTaskForMeCondition,
            int rangeStart, int fetchSize) throws DomainServiceException
    {
        PageUtilEntity puEntity = new PageUtilEntity();
        puEntity.setIsAlwaysSearchSize(true);
        puEntity.setIsAsc(false);

        puEntity.setOrderByColumn("processStartTime");
        return queryChannelWkfwProcessWithMe(channelWkfwTaskForMeCondition, rangeStart, fetchSize, puEntity);
    }

    /**
     * @param channelWkfwTaskForMeCondition ChannelWkfwCondition
     * @param rangeStart int
     * @param fetchSize int
     * @param puEntity PageUtilEntity
     * @return TableDataInfo
     * @throws DomainServiceException DomainServiceException
     */
    public TableDataInfo queryChannelWkfwProcessWithMe(ChannelWkfwCondition channelWkfwTaskForMeCondition,
            int rangeStart, int fetchSize, PageUtilEntity puEntity) throws DomainServiceException
    {
        if (null == channelWkfwTaskForMeCondition.getPboType()
                || ("").equals(channelWkfwTaskForMeCondition.getPboType()))
        {
            channelWkfwTaskForMeCondition.setPboType(ChannelConstants.WKFWTYPE_CHANNEL_ALL);
        }
        channelWkfwTaskForMeCondition.setOpername(ChannelConstants.getUserid());
        channelWkfwTaskForMeCondition.setUserid(ChannelConstants.getUserid());
        OperInfo operinfo = LoginMgt.getOperInfo("CMS");
        channelWkfwTaskForMeCondition.setOperid(operinfo.getOperid());
        return channelWkfwProcessWithMeDS.pageInfoQuery(channelWkfwTaskForMeCondition, rangeStart, fetchSize, puEntity);
    }

    /**
     * @param selectedValue String
     * @param needSelect Boolean
     * @return HashMap
     * 
     */
    public HashMap<String, Object> getWorkflowTypeList(String selectedValue, Boolean needSelect)
    {
        List<String> textList = new ArrayList<String>();
        List<String> valueList = new ArrayList<String>();

        ResourceMgt.addDefaultResourceBundle(ChannelConstants.RESOURCE_UCDN_CHANNEL);

        if (needSelect)
        {
            textList.add(ResourceMgt.findDefaultText(ChannelConstants.RES_CMS_CHANNEL_OPTION_PLEASESELECT));
            valueList.add("CMS_channel_%");
        }

        textList.add(ResourceMgt.findDefaultText(ChannelConstants.RES_CMS_STRA_WKFWTYPENAME_CHANNEL_ADD));
        valueList.add(ChannelConstants.WKFWTYPE_CHANNEL_ADD);

        // textList.add(ResourceMgt.findDefaultText(StrategyConstant.RES_CMS_STRA_WKFWTYPENAME_STRATEGY_DROPOUT));
        // valueList.add(StrategyConstant.WKFWTYPE_STRATEGY_DROPOUT);

        HashMap<String, Object> map = new HashMap<String, Object>();
        map.put("codeText", textList);
        map.put("codeValue", valueList);
        map.put("defaultValue", selectedValue);
        return map;
    }

    /**
     * @return HashMap
     */
    public HashMap<String, String> getWorkflowTypeMap()
    {
        ResourceMgt.addDefaultResourceBundle(ChannelConstants.RESOURCE_UCDN_CHANNEL);
        HashMap<String, String> map = new HashMap<String, String>();
        map.put(ChannelConstants.WKFWTYPE_CHANNEL_ADD, ResourceMgt
                .findDefaultText(ChannelConstants.RES_CMS_STRA_WKFWTYPENAME_CHANNEL_ADD));

        return map;
    }
}

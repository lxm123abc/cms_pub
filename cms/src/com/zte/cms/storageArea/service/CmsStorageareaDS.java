package com.zte.cms.storageArea.service;

import java.util.List;
import com.zte.cms.storageArea.model.CmsStoragearea;
import com.zte.cms.storageArea.dao.ICmsStorageareaDAO;
import com.zte.cms.storageArea.service.ICmsStorageareaDS;
import com.zte.cms.storageManage.model.CmsStoragemanage;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;
import com.zte.ssb.framework.exception.exceptions.DAOException;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.SSBBus;
import com.zte.umap.common.dynamicobj.DynamicObjectBaseDS;

public class CmsStorageareaDS extends DynamicObjectBaseDS implements ICmsStorageareaDS
{
    // 日志
    private Log log = SSBBus.getLog(getClass());

    private ICmsStorageareaDAO dao = null;

    public void setDao(ICmsStorageareaDAO dao)
    {
        this.dao = dao;
    }

    public void insertCmsStoragearea(CmsStoragearea cmsStoragearea) throws DomainServiceException
    {
        log.debug("insert cmsStoragearea starting...");
        try
        {
            Long id = (Long) this.getPrimaryKeyGenerator().getPrimarykey("cms_storageArea_areaIndex");
            System.out.println(id);
            cmsStoragearea.setAreaindex(id);
            dao.insertCmsStoragearea(cmsStoragearea);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("insert cmsStoragearea end");
    }

    public void updateCmsStoragearea(CmsStoragearea cmsStoragearea) throws DomainServiceException
    {
        log.debug("update cmsStoragearea by pk starting...");
        try
        {
            dao.updateCmsStoragearea(cmsStoragearea);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("update cmsStoragearea by pk end");
    }

    public void updateCmsStorageareaList(List<CmsStoragearea> cmsStorageareaList) throws DomainServiceException
    {
        log.debug("update cmsStorageareaList by pk starting...");
        if (null == cmsStorageareaList || cmsStorageareaList.size() == 0)
        {
            log.debug("there is no datas in cmsStorageareaList");
            return;
        }
        try
        {
            for (CmsStoragearea cmsStoragearea : cmsStorageareaList)
            {
                dao.updateCmsStoragearea(cmsStoragearea);
            }
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("update cmsStorageareaList by pk end");
    }

    public void removeCmsStoragearea(CmsStoragearea cmsStoragearea) throws DomainServiceException
    {
        log.debug("remove cmsStoragearea by pk starting...");
        try
        {
            dao.deleteCmsStoragearea(cmsStoragearea);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("remove cmsStoragearea by pk end");
    }

    public void removeCmsStorageareaList(List<CmsStoragearea> cmsStorageareaList) throws DomainServiceException
    {
        log.debug("remove cmsStorageareaList by pk starting...");
        if (null == cmsStorageareaList || cmsStorageareaList.size() == 0)
        {
            log.debug("there is no datas in cmsStorageareaList");
            return;
        }
        try
        {
            for (CmsStoragearea cmsStoragearea : cmsStorageareaList)
            {
                dao.deleteCmsStoragearea(cmsStoragearea);
            }
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("remove cmsStorageareaList by pk end");
    }

    public CmsStoragearea getCmsStoragearea(CmsStoragearea cmsStoragearea) throws DomainServiceException
    {
        log.debug("get cmsStoragearea by pk starting...");
        CmsStoragearea rsObj = null;
        try
        {
            rsObj = dao.getCmsStoragearea(cmsStoragearea);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("get cmsStorageareaList by pk end");
        return rsObj;
    }

    public List<CmsStoragearea> getCmsStorageareaByCond(CmsStoragearea cmsStoragearea) throws DomainServiceException
    {
        log.debug("get cmsStoragearea by condition starting...");
        List<CmsStoragearea> rsList = null;
        try
        {
            rsList = dao.getCmsStorageareaByCond(cmsStoragearea);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("get cmsStoragearea by condition end");
        return rsList;
    }

    @SuppressWarnings("unchecked")
    public TableDataInfo pageInfoQuery(CmsStoragearea cmsStoragearea, int start, int pageSize)
            throws DomainServiceException
    {
        log.debug("get cmsStoragearea page info by condition starting...");
        PageInfo pageInfo = null;
        try
        {
            pageInfo = dao.pageInfoQuery(cmsStoragearea, start, pageSize);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        TableDataInfo tableInfo = new TableDataInfo();
        tableInfo.setData((List<CmsStoragearea>) pageInfo.getResult());
        tableInfo.setTotalCount((int) pageInfo.getTotalCount());
        log.debug("get cmsStoragearea page info by condition end");
        return tableInfo;
    }

    @SuppressWarnings("unchecked")
    public TableDataInfo pageInfoQuery(CmsStoragearea cmsStoragearea, int start, int pageSize, PageUtilEntity puEntity)
            throws DomainServiceException
    {
        log.debug("get cmsStoragearea page info by condition starting...");
        PageInfo pageInfo = null;
        try
        {
            pageInfo = dao.pageInfoQuery(cmsStoragearea, start, pageSize, puEntity);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        TableDataInfo tableInfo = new TableDataInfo();
        tableInfo.setData((List<CmsStoragearea>) pageInfo.getResult());
        tableInfo.setTotalCount((int) pageInfo.getTotalCount());
        log.debug("get cmsStoragearea page info by condition end");
        return tableInfo;
    }

}

package com.zte.cms.content.ls;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.dom4j.io.XMLWriter;

import com.zte.cms.common.GlobalConstants;
import com.zte.cms.content.model.CmsSplittask;
import com.zte.cms.content.model.CmsTasksourceMap;
import com.zte.cms.content.model.IcmSourcefile;
import com.zte.cms.content.service.ICmsSplittaskDS;
import com.zte.cms.content.service.ICmsTasksourceMapDS;
import com.zte.cms.content.service.IIcmSourcefileDS;
import com.zte.cms.storageArea.ls.ICmsStorageareaLS;
import com.zte.cms.storageManage.model.CmsDeviceWindiskidBind;
import com.zte.cms.storageManage.service.ICmsDeviceWindiskidBindDS;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.SSBBus;
import com.zte.ssb.framework.base.IPrimaryKeyGenerator;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;
import com.zte.umap.common.EspecialCharMgt;
import com.zte.umap.common.LoginMgt;
import com.zte.umap.common.dynamicobj.DynamicObjectBaseDS;
import com.zte.umap.common.model.OperInfo;
import com.zte.umap.sys.model.UsysConfig;
import com.zte.umap.sys.service.IUsysConfigDS;

public class IcmSplitfileLS extends DynamicObjectBaseDS implements IIcmSplitfileLS
{
    private Log log = SSBBus.getLog(getClass());

    /**
     * ICmsSplittaskDS组件
     */
    private ICmsSplittaskDS cmSplitfiletaskDS;

    /**
     * CmsTasksourceMapDS组件
     */
    private ICmsTasksourceMapDS cmsTasksourceMapDS;

    /**
     * IIcmSourcefileDS组件
     */
    private IIcmSourcefileDS cmSourcefileDS;

    /**
     * ICmsDeviceWindiskidBindDS组件
     */
    private ICmsDeviceWindiskidBindDS cmsDeviceWindiskidBindDS;

    /**
     * IUsysConfigDS组件
     */
    private IUsysConfigDS usysConfigDS = null;

    /**
     * IPrimaryKeyGenerator组件
     */
    private IPrimaryKeyGenerator primaryKeyGenerator = null;

    public void setCmSplitfiletaskDS(ICmsSplittaskDS cmSplitfiletaskDS)
    {
        this.cmSplitfiletaskDS = cmSplitfiletaskDS;
    }

    public void setCmsTasksourceMapDS(ICmsTasksourceMapDS cmsTasksourceMapDS)
    {
        this.cmsTasksourceMapDS = cmsTasksourceMapDS;
    }

    public void setCmSourcefileDS(IIcmSourcefileDS cmSourcefileDS)
    {
        this.cmSourcefileDS = cmSourcefileDS;
    }

    public void setCmsDeviceWindiskidBindDS(ICmsDeviceWindiskidBindDS cmsDeviceWindiskidBindDS)
    {
        this.cmsDeviceWindiskidBindDS = cmsDeviceWindiskidBindDS;
    }

    public void setUsysConfigDS(IUsysConfigDS usysConfigDS)
    {
        this.usysConfigDS = usysConfigDS;
    }

    public void setPrimaryKeyGenerator(IPrimaryKeyGenerator primaryKeyGenerator)
    {
        this.primaryKeyGenerator = primaryKeyGenerator;
    }

    /**
     * 拆条：多个小段内容简单合为一个视频内容
     * 
     * @param soureFileIndexs 素材文件主键列表
     * @return
     * @throws Exception
     */
    public String addMultiMergeToOneSplitTask(List<String> soureFileIndexs, String cpid) throws Exception
    {
        if (soureFileIndexs != null && soureFileIndexs.size() > 0)
        {
            Long taskindex = (Long) primaryKeyGenerator.getPrimarykey("cms_splittask");
            // 获取制作区路径
            ICmsStorageareaLS storageareaLs = (ICmsStorageareaLS) SSBBus.findDomainService("cmsStorageareaLS");
            // String stroageareaPath = storageareaLs.getAddress("4");//制作区
            String deviceid = storageareaLs.getDeviceID("4");// 制作区设备ID
            String spaceid = storageareaLs.getSpaceID("4");// 制作区空间ID
            String stroageareaPath = null;
            if (deviceid.equalsIgnoreCase("1056020") || spaceid.equalsIgnoreCase("1056020"))
            {

                return "1:获取制作区地址失败";
            }
            else
            // 加上日期目录，成为制作区按日期存放路径
            {
                CmsDeviceWindiskidBind cond = new CmsDeviceWindiskidBind();
                cond.setDeviceid(deviceid);
                CmsDeviceWindiskidBind cmsDeviceWindiskidBind = cmsDeviceWindiskidBindDS
                        .getCmsDeviceWindiskidBind(cond);
                if (cmsDeviceWindiskidBind == null || cmsDeviceWindiskidBind.getWindiskid() == null
                        || cmsDeviceWindiskidBind.getWindiskid() == "")
                {
                    return "1:获取设备盘符绑定关系失败";
                }
                // SimpleDateFormat sdfDate = new SimpleDateFormat("yyyyMMdd");
                // String dateStr = sdfDate.format(new Date());
                stroageareaPath = cmsDeviceWindiskidBind.getWindiskid() + spaceid;// + dateStr;
            }

            for (int i = 0; i < soureFileIndexs.size(); i++)
            {
                IcmSourcefile srcfile = new IcmSourcefile();
                srcfile.setFileindex(Long.parseLong(soureFileIndexs.get(i)));
                srcfile = cmSourcefileDS.getIcmSourcefile(srcfile);
                String xmlfilertn = createMediaSrcXMLFile(srcfile);
                if (xmlfilertn.equalsIgnoreCase("1"))
                {
                    return "1:获取临时区路径失败";
                }
                else if (xmlfilertn.equalsIgnoreCase("2"))
                {
                    return "1:生成媒资数据XML文件出错";
                }
                else if (xmlfilertn.equalsIgnoreCase("3"))
                {
                    return "1:素材状态不正确";
                }
                else if (xmlfilertn.equalsIgnoreCase("4"))
                {
                    return "1:获取素材存放路径失败";
                }
                else if (xmlfilertn.equalsIgnoreCase("5"))
                {
                    return "1:获取WINDOWS共享访问点失败";
                }
                else if (xmlfilertn.equalsIgnoreCase("6"))
                {
                    return "1:选择的视频文件不存在";
                }
                else if (xmlfilertn.equalsIgnoreCase("7"))
                {
                    return "1:获取设备盘符绑定关系失败";
                }
                {
                    CmsTasksourceMap tasksourceMap = new CmsTasksourceMap();
                    tasksourceMap.setFileindex(Long.parseLong(soureFileIndexs.get(i)));
                    tasksourceMap.setTaskindex(taskindex);
                    tasksourceMap.setMediasrcxmlpath(xmlfilertn);
                    cmsTasksourceMapDS.insertCmsTasksourceMap(tasksourceMap);
                }
            }
            CmsSplittask splittask = new CmsSplittask();
            splittask.setCpid(cpid);
            splittask.setTaskindex(taskindex);
            splittask.setTasktype(2);// 合并
            splittask.setStatus(1);// 待执行
            splittask.setDestaddress(stroageareaPath);
            OperInfo operinfo = LoginMgt.getOperInfo("CMS");
            splittask.setOpername(operinfo.getUserId());
            cmSplitfiletaskDS.insertCmsSplittask(splittask);

        }
        else
        {
            return "1:素材列表为空";
        }

        return "0:操作成功";
    }

    /**
     * 拆条：将一个内容切割为多个视频
     * 
     * @param soureFileIndex 素材文件主键
     * @return
     * @throws Exception
     */
    public String addOneSplitToMutiSplitTask(String soureFileIndex) throws Exception
    {
        if (soureFileIndex != null)
        {
            Long taskindex = (Long) primaryKeyGenerator.getPrimarykey("cms_splittask");
            // 获取制作区路径
            ICmsStorageareaLS storageareaLs = (ICmsStorageareaLS) SSBBus.findDomainService("cmsStorageareaLS");
            // String stroageareaPath = storageareaLs.getAddress("4");//制作区
            String deviceid = storageareaLs.getDeviceID("4");// 制作区设备ID
            String spaceid = storageareaLs.getSpaceID("4");// 制作区空间ID
            String stroageareaPath = null;
            if (deviceid.equalsIgnoreCase("1056020") || spaceid.equalsIgnoreCase("1056020"))
            {

                return "1:获取制作区地址失败";
            }
            else
            // 加上日期目录，成为在线区按日期存放路径
            {
                CmsDeviceWindiskidBind cond = new CmsDeviceWindiskidBind();
                cond.setDeviceid(deviceid);
                CmsDeviceWindiskidBind cmsDeviceWindiskidBind = cmsDeviceWindiskidBindDS
                        .getCmsDeviceWindiskidBind(cond);
                if (cmsDeviceWindiskidBind == null || cmsDeviceWindiskidBind.getWindiskid() == null
                        || cmsDeviceWindiskidBind.getWindiskid() == "")
                {
                    return "1:获取设备盘符绑定关系失败";
                }
                // SimpleDateFormat sdfDate = new SimpleDateFormat("yyyyMMdd");
                // String dateStr = sdfDate.format(new Date());
                stroageareaPath = cmsDeviceWindiskidBind.getWindiskid() + spaceid;// + dateStr;
            }

            IcmSourcefile srcfile = new IcmSourcefile();
            srcfile.setFileindex(Long.parseLong(soureFileIndex));
            srcfile = cmSourcefileDS.getIcmSourcefile(srcfile);
            String xmlfilertn = createMediaSrcXMLFile(srcfile);
            if (xmlfilertn.equalsIgnoreCase("1"))
            {
                return "1:获取临时区路径失败";
            }
            else if (xmlfilertn.equalsIgnoreCase("2"))
            {
                return "1:生成媒资数据XML文件出错";
            }
            else if (xmlfilertn.equalsIgnoreCase("3"))
            {
                return "1:素材状态不正确";
            }
            else if (xmlfilertn.equalsIgnoreCase("4"))
            {
                return "1:获取素材存放路径失败";
            }
            else if (xmlfilertn.equalsIgnoreCase("5"))
            {
                return "1:获取WINDOWS共享访问点失败";
            }
            else if (xmlfilertn.equalsIgnoreCase("6"))
            {
                return "1:选择的视频文件不存在";
            }
            else if (xmlfilertn.equalsIgnoreCase("7"))
            {
                return "1:获取设备盘符绑定关系失败";
            }
            {
                CmsTasksourceMap tasksourceMap = new CmsTasksourceMap();
                tasksourceMap.setFileindex(Long.parseLong(soureFileIndex));
                tasksourceMap.setTaskindex(taskindex);
                tasksourceMap.setMediasrcxmlpath(xmlfilertn);
                cmsTasksourceMapDS.insertCmsTasksourceMap(tasksourceMap);
            }

            CmsSplittask splittask = new CmsSplittask();
            splittask.setTaskindex(taskindex);
            splittask.setTasktype(1);// 拆分
            splittask.setStatus(1);// 待执行
            splittask.setCpid(srcfile.getCpid());
            splittask.setDestaddress(stroageareaPath);
            OperInfo operinfo = LoginMgt.getOperInfo("CMS");
            splittask.setOpername(operinfo.getUserId());
            cmSplitfiletaskDS.insertCmsSplittask(splittask);
        }
        else
        {
            return "1:素材列表为空";
        }

        return "0:操作成功";
    }

    /**
     * 生成媒资数据XML文件
     * 
     * @param src 素材
     * @return "1"-获取在线区路径失败 "2"-生成媒资数据XML文件失败 "3"-素材状态不正确 "4"获取素材存放路径失败 "5"-获取WINDOWS共享访问点失败 "6"-选择的视频文件不存在
     *         成功-生成媒资数据XML文件路径
     */
    private String createMediaSrcXMLFile(IcmSourcefile src) throws Exception
    {
        // 获取临时路径
        /*
         * String stroageareaPath; try { ICmsStorageareaLS storageareaLs =
         * (ICmsStorageareaLS)SSBBus.findDomainService("cmsStorageareaLS"); stroageareaPath =
         * storageareaLs.getAddress("1");//在线区 if (stroageareaPath.equalsIgnoreCase("1056020")) {
         * 
         * return "1";//获取 } else//加上日期目录，成为在线区按日期存放路径 { SimpleDateFormat sdfDate = new SimpleDateFormat("yyyyMMdd");
         * String dateStr = sdfDate.format(new Date()); stroageareaPath = stroageareaPath + "/" + dateStr; } } catch
         * (DomainServiceException e1) { e1.printStackTrace(); return "1"; }
         */

        if (src != null && src.getStatus() != 12)
        {
            return "3";
        }
        else if (src.getAddresspath() == null || src.getAddresspath().equalsIgnoreCase(""))
        {
            return "4";
        }

        Document dom = DocumentHelper.createDocument();
        Element rootElemt = dom.addElement("ObjectData");
        Element level2Elemt1 = rootElemt.addElement("Object");
        level2Elemt1.addAttribute("ObjectID", src.getFileindex().toString());
        level2Elemt1.addAttribute("ObjectName", src.getNamecn());
        level2Elemt1.addAttribute("ParentID", "");
        level2Elemt1.addAttribute("RootID", src.getFileindex().toString());
        level2Elemt1.addAttribute("CCID", "视音频类节目");
        level2Elemt1.addAttribute("InPoint", "0");
        level2Elemt1.addAttribute("OutPoint", "125");
        Element level3Elemt2_1_1 = level2Elemt1.addElement("MetaData");
        level3Elemt2_1_1.addAttribute("MetaDataCount", "1");
        Element level4Elemt3_1_1 = level3Elemt2_1_1.addElement("sAttributeGroups");
        Element level4Elemt3_1_2 = level3Elemt2_1_1.addElement("sAttribute");
        level4Elemt3_1_2.addAttribute("enumType", "0");
        level4Elemt3_1_2.addAttribute("strName", "正题名");
        level4Elemt3_1_2.addText("素材");
        Element level3Elemt2_1_2 = level2Elemt1.addElement("Content");
        Element level4Elemt3_2_1 = level3Elemt2_1_2.addElement("ContentFile");
        level4Elemt3_2_1.addAttribute("strName", src.getFilename());

        // 路径设置为盘符+空间ID+文件形式
        String deviceid = src.getAddresspath().substring(0, src.getAddresspath().indexOf("/"));
        CmsDeviceWindiskidBind cond = new CmsDeviceWindiskidBind();
        cond.setDeviceid(deviceid);
        CmsDeviceWindiskidBind cmsDeviceWindiskidBind = cmsDeviceWindiskidBindDS.getCmsDeviceWindiskidBind(cond);
        if (cmsDeviceWindiskidBind == null || cmsDeviceWindiskidBind.getWindiskid() == null
                || cmsDeviceWindiskidBind.getWindiskid() == "")
        {
            return "7";
        }

        String addresspathname = src.getAddresspath().substring(src.getAddresspath().indexOf("/") + 1);

        String winpath = cmsDeviceWindiskidBind.getWindiskid()
                + addresspathname.substring(0, addresspathname.lastIndexOf("/") + 1);
        winpath = winpath.replaceAll("/", "\\\\");// 用 "\" 替换 "/"
        level4Elemt3_2_1.addAttribute("strPath", winpath);
        level4Elemt3_2_1.addAttribute("enumType", "2");
        level4Elemt3_2_1.addAttribute("dwLength", "0");

        String filepathname = null;
        String returnpath = null;
        try
        {
            // SimpleDateFormat sdfDate = new SimpleDateFormat("yyyyMMddHHmmss");
            // String dateStr = sdfDate.format(new Date());
            Long time = new Date().getTime();
            // String filename = src.getNamecn() + "_" + time.toString() + ".xml";
            String icmSourceFileName = src.getFilename();
            if (icmSourceFileName.indexOf(".") > 0)
            {
                icmSourceFileName = icmSourceFileName.substring(0, icmSourceFileName.indexOf("."));
            }
            String filename = icmSourceFileName + "_" + time.toString() + ".xml";
            // 获取WINDOWS共享访问点
            UsysConfig usysConfig = new UsysConfig();
            String windowShareddir = null;
            log.debug("UsysConfigDS: getUsysConfigByCfgkey starting...");
            usysConfig = usysConfigDS.getUsysConfigByCfgkey("cms.windows.share");
            log.debug("UsysConfigDS: getUsysConfigByCfgkey end");
            if (null == usysConfig && null == usysConfig.getCfgvalue() && (usysConfig.getCfgvalue().endsWith("")))
            {
                return "5";
            }
            else
            {
                windowShareddir = usysConfig.getCfgvalue();
            }
            String path = src.getAddresspath().substring(0, src.getAddresspath().lastIndexOf("/"));
            String mountPoint = GlobalConstants.getMountPoint();
            File Vediofile = new File(mountPoint + src.getAddresspath());
            if (!Vediofile.exists())
            {
                return "6";
            }
            filepathname = mountPoint + path + "/" + filename;
            returnpath = windowShareddir + path + "/" + filename;
            returnpath = returnpath.replaceAll("/+", "\\\\");
            String xmlstr = dom.asXML();
            File xmlfile = new File(filepathname);
            if (!Vediofile.exists())
            {
                xmlfile.delete();
            }
            XMLWriter writer = new XMLWriter(new FileOutputStream(filepathname));
            writer.write(dom);
            log.debug(xmlstr);
            writer.close();
        }
        catch (DomainServiceException e)
        {
            // TODO Auto-generated catch block

            return "2";
        }
        catch (IOException e)
        {
            e.printStackTrace();
            return "2";
        }
        return returnpath;
    }

    public TableDataInfo pageInfoQuery(CmsSplittask cmsSplittask, int start, int pageSize)
    {

        // 先进行时间格式的转换
        String starttime = cmsSplittask.getStarttime();
        if (!starttime.equals(""))
        {
            String starttime1 = starttime.substring(0, 4) + starttime.substring(5, 7) + starttime.substring(8, 10)
                    + starttime.substring(11, 13) + starttime.substring(14, 16) + starttime.substring(17, 19);
            // System.out.println(starttime1);
            cmsSplittask.setStarttime(starttime1);
        }
        String starttime2 = cmsSplittask.getEndtime();
        if (!starttime2.equals(""))
        {
            String starttime3 = starttime2.substring(0, 4) + starttime2.substring(5, 7) + starttime2.substring(8, 10)
                    + starttime2.substring(11, 13) + starttime2.substring(14, 16) + starttime2.substring(17, 19);
            // System.out.println(starttime3);
            cmsSplittask.setEndtime(starttime3);
        }

        if (null != cmsSplittask.getCpid() && !cmsSplittask.getCpid().equals(""))
        {
            String cpid = EspecialCharMgt.conversion(cmsSplittask.getCpid());
            cmsSplittask.setCpid(cpid);
        }

        TableDataInfo dataInfo = null;
        try
        {
            dataInfo = cmSplitfiletaskDS.pageInfoQuerySplit(cmsSplittask, start, pageSize);
        }
        catch (Exception e)
        {

        }
        return dataInfo;
    }
}

function divalert(loadingObj)
{
	var parentObj = getWindowParent();
	var msgw,msgh,bordercolor;
	msgw=400; 
	msgh=100; 
	titleheight=25; 
	bordercolor="#336699"; 
	titlecolor="#99CCFF"; 
	var sWidth,sHeight;
	
	var bgObj=parentObj.document.createElement("div"); 
	bgObj.setAttribute('id','bgDiv');
	bgObj.style.position="absolute";
	bgObj.style.top="0";
	//bgObj.style.background="#777";
	bgObj.style.filter="progid:DXImageTransform.Microsoft.Alpha(style=3,opacity=25,finishOpacity=75";
	bgObj.style.opacity="0.6";
	bgObj.style.left="0";
	//bgObj.style.width=sWidth + "px";
	//bgObj.style.height=sHeight + "px";
	bgObj.style.zIndex = "10000";
	
	parentObj.document.body.appendChild(bgObj); 
	//wanghao
	fixWindow();
	parentObj.onresize = fixWindow;
	
	var msgObj=parentObj.document.createElement("div"); 
	msgObj.setAttribute("id","msgDiv");
	
	msgObj.style.background="white";//颜色
	msgObj.style.border="1px solid " + bordercolor;
	msgObj.style.position = "absolute";
	msgObj.style.left = "50%";//
	msgObj.style.top = "50%";
	msgObj.style.font="12px/1.6em Verdana, Geneva, Arial, Helvetica, sans-serif";
	
	msgObj.style.marginLeft = "-225px" ;
	msgObj.style.marginTop = -75+parentObj.document.documentElement.scrollTop+"px";
	/*msgObj.style.width = msgw + "px";
	msgObj.style.height =msgh + "px";
	msgObj.style.textAlign = "center";
	msgObj.style.lineHeight ="25px";
	*/
	msgObj.style.zIndex = "10001";
	var title=parentObj.document.createElement("h4"); 
	title.setAttribute("id","msgTitle");
	title.setAttribute("align","right");
	title.style.filter="progid:DXImageTransform.Microsoft.Alpha(startX=20, startY=20, finishX=100, finishY=100,style=1,opacity=75,finishOpacity=100);";
	title.style.opacity="0.75";
	/*      title.style.margin="0";
	title.style.padding="3px";
	title.style.background=bordercolor;     
	title.style.border="1px solid " + bordercolor;
	title.style.height="18px";
	title.style.font="12px Verdana, Geneva, Arial, Helvetica, sans-serif";
	title.style.color="white";
	title.style.cursor="pointer";
	*/
	//title.innerHTML=$res_entry("ria.loading.title","\u6b63\u5728\u88c5\u8f7d\u4e2d\u8bf7\u7a0d\u5019......");
	
	 var image = parentObj.document.createElement("IMG");// 
	 var imgscr = ""; 
	 var width = "";  
	 var height =""; 
	 if(loadingObj)
	 {
	     imgscr = loadingObj.img;
	     width = loadingObj.width;
	     height = loadingObj.height;
	 }
	 
	//wanghao22
	image.setAttribute("id","msgImage");        
	image.setAttribute("src",imgscr);
	image.setAttribute("align","middle");
	image.setAttribute("width","100%");
	image.setAttribute("height","100%");
	//wanghao22
	/*var cancel =  document.createElement("div");// 
	cancel.innerHTML = "<a onclick='removeObj()' href='javascript:void(0)'>Cancel</a>";
	    */
	parentObj.document.body.appendChild(msgObj); 
	//document.getElementById("msgDiv").appendChild(title);// 
	parentObj.document.getElementById("msgDiv").appendChild(image);// 
	//document.getElementById("msgDiv").appendChild(cancel);// 
}

function removeObj()
{
	var parentObj = getWindowParent();
    //\u70b9\u51fb\u6807\u9898\u680f\u89e6\u53d1\u7684\u4e8b\u4ef6
    var msgObj=parentObj.document.getElementById("msgDiv");//\u83b7\u53d6\u4e00\u4e2adiv\u5bf9\u8c61\uff08\u63d0\u793a\u6846\u5c42\uff09
    var title=parentObj.document.getElementById("msgTitle");//\u83b7\u53d6\u4e00\u4e2ah4\u5bf9\u8c61\uff08\u63d0\u793a\u6846\u6807\u9898\u680f\uff09
    var bgObj=parentObj.document.getElementById("bgDiv");//\u83b7\u53d6\u4e00\u4e2adiv\u5bf9\u8c61\uff08\u80cc\u666f\u5c42\uff09 
    
    //wanghao22
    parentObj.onresize = null;
    
    //wanghao22
    if(bgObj)
       parentObj.document.body.removeChild(bgObj);//\u5220\u9664\u80cc\u666f\u5c42Div
    if(msgObj && title)
       parentObj.document.getElementById("msgDiv").removeChild(title);//\u5220\u9664\u63d0\u793a\u6846\u7684\u6807\u9898\u680f
    if(msgObj)
       parentObj.document.body.removeChild(msgObj);//\u5220\u9664\u63d0\u793a\u6846\u5c42

	$(parentObj.document).unbind("keydown", disableKeyWhenProcessing).unbind("keyup", disableKeyWhenProcessing);
	disableKeyWhenProcessing.isRuning = false;
}

function getWindowParent()
{
	var parentObj = window.parent;
	var indexUrl = parentObj.location.href;
	var count = 0;
	while(indexUrl != undefined && indexUrl !=null && indexUrl.indexOf('/uiloader/index.html') == -1)
	{
		count++;
		parentObj = parentObj.parent;
		indexUrl = parentObj.location.href;
		if(count>20)
		{
			break;
		}
	}
	count = 0;
	return parentObj;
}

function fixWindow()
{
	var parentObj = getWindowParent();
    var bgObj = parentObj.document.getElementById('bgDiv');
    if(parentObj.document.body.offsetWidth > parentObj.document.body.scrollWidth)
        bgObj.style.width = parentObj.document.body.offsetWidth + "px";
    else
        bgObj.style.width = parentObj.document.body.scrollWidth + "px";
        
    if(parentObj.document.body.scrollHeight > parentObj.document.body.offsetHeight)
        bgObj.style.height = parentObj.document.body.scrollHeight + "px";
    else
        bgObj.style.height = parentObj.document.body.offsetHeight + "px";
}

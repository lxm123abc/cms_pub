package com.zte.ucm;

public class Constants
{
  public static final String CONFIGFILENAME = "cms-web-config-client.xml";
  public static String CONFIGFILEPATH = "D://";

  public static String CONFIGAPPLYITEMSXML = "config_apply_items.xml";

  public static String LOGPATH = "D://log";

  public static String ERRORFILENAME = "update_config.log";
  public static final String CHKRESULTFILENAME = "config_check_result.log";
}
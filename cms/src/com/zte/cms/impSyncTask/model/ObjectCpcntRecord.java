
  package com.zte.cms.impSyncTask.model;

import com.zte.ssb.dynamicobj.DynamicBaseObject;

public class ObjectCpcntRecord extends DynamicBaseObject
{
	private java.lang.Long cpcntindex;
	
	private java.lang.String taskindex;//新增taskindex
	
	private java.lang.String correlateid;
	private java.lang.String objecttype;
	private java.lang.String actiontype;
	private java.lang.String objectid;
	private java.lang.String objectcode;
	private java.lang.String parentid;
	private java.lang.String parentcode;
	private java.lang.String starttime;
	private java.lang.String endtime;
	private java.lang.Integer resultcode;
	private java.lang.String resultdesc;
    private java.lang.String starttime1;
    private java.lang.String starttime2;
    
    public java.lang.Long getCpcntindex()
    {
        return cpcntindex;
    } 
         
    public void setCpcntindex(java.lang.Long cpcntindex) 
    {
        this.cpcntindex = cpcntindex;
    }
    
    public java.lang.String getCorrelateid()
    {
        return correlateid;
    } 
         
    public void setCorrelateid(java.lang.String correlateid) 
    {
        this.correlateid = correlateid;
    }
    
    public java.lang.String getObjecttype()
    {
        return objecttype;
    } 
         
    public void setObjecttype(java.lang.String objecttype) 
    {
        this.objecttype = objecttype;
    }
    
    public java.lang.String getActiontype()
    {
        return actiontype;
    } 
         
    public void setActiontype(java.lang.String actiontype) 
    {
        this.actiontype = actiontype;
    }
    
    public java.lang.String getObjectid()
    {
        return objectid;
    } 
         
    public void setObjectid(java.lang.String objectid) 
    {
        this.objectid = objectid;
    }
    
    public java.lang.String getObjectcode()
    {
        return objectcode;
    } 
         
    public void setObjectcode(java.lang.String objectcode) 
    {
        this.objectcode = objectcode;
    }
    
    public java.lang.String getParentid()
    {
        return parentid;
    } 
         
    public void setParentid(java.lang.String parentid) 
    {
        this.parentid = parentid;
    }
    
    public java.lang.String getParentcode()
    {
        return parentcode;
    } 
         
    public void setParentcode(java.lang.String parentcode) 
    {
        this.parentcode = parentcode;
    }
    
    public java.lang.String getStarttime()
    {
        return starttime;
    } 
         
    public void setStarttime(java.lang.String starttime) 
    {
        this.starttime = starttime;
    }
    
    public java.lang.String getEndtime()
    {
        return endtime;
    } 
         
    public void setEndtime(java.lang.String endtime) 
    {
        this.endtime = endtime;
    }
    
    public java.lang.Integer getResultcode()
    {
        return resultcode;
    } 
         
    public void setResultcode(java.lang.Integer resultcode) 
    {
        this.resultcode = resultcode;
    }
    
    public java.lang.String getResultdesc()
    {
        return resultdesc;
    } 
         
    public void setResultdesc(java.lang.String resultdesc) 
    {
        this.resultdesc = resultdesc;
    }
    
    public java.lang.String getStarttime1()
    {
        return starttime1;
    }

    public void setStarttime1(java.lang.String starttime1)
    {
        this.starttime1 = starttime1;
    }

    public java.lang.String getStarttime2()
    {
        return starttime2;
    }

    public void setStarttime2(java.lang.String starttime2)
    {
        this.starttime2 = starttime2;
    }
    

    public java.lang.String getTaskindex()
    {
        return taskindex;
    }

    public void setTaskindex(java.lang.String taskindex)
    {
        this.taskindex = taskindex;
    }

    public void initRelation()
    {
        this.addRelation("cpcntindex","CPCNTINDEX");   
        this.addRelation("correlateid","CORRELATEID");   
        
        this.addRelation("taskindex","TASKINDEX");
        
        this.addRelation("objecttype","OBJECTTYPE");   
        this.addRelation("actiontype","ACTIONTYPE");   
        this.addRelation("objectid","OBJECTID");   
        this.addRelation("objectcode","OBJECTCODE");   
        this.addRelation("parentid","PARENTID");   
        this.addRelation("parentcode","PARENTCODE");   
        this.addRelation("starttime","STARTTIME");   
        this.addRelation("endtime","ENDTIME");   
        this.addRelation("resultcode","RESULTCODE");   
        this.addRelation("resultdesc","RESULTDESC");  
        this.addRelation("starttime1", "STARTTIME1");
        this.addRelation("starttime2", "STARTTIME2");
    } 
}

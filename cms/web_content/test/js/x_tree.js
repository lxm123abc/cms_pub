
var treeObject = {};
var pageObject = {};
var pageInfo = [];
var tree;

function buildTreeHTMLByFSO() {
	tree = new dhtmlXTreeObject("dhtmltree","100%","100%",0);
	tree.setImagePath("imgs/");
	tree.enableCheckBoxes(0);
	tree.setOnClickHandler(readFile);
	tree.enableDragAndDrop(true, false);
	tree.setDragHandler(moveFile);
	treeObject = {};
	callUrl("fileSystemManagerDS/getTagNodeList.ssm", null, 'treeObject', true);
	if (isEmpty(treeObject) || isEmpty(treeObject.rtnValue)) {
		return;
	}
	var jsObj = treeObject.rtnValue;
	for (var obj in jsObj) {
		var item = jsObj[obj];
		if (item.isdir) {
			tree.insertNewItem(item.pid, item.id, item.name, "", "folderClosed.gif");
		}
		else {
			tree.insertNewItem(item.pid, item.id, item.name);
		}
	}
}

function createFolder() {
	var sid = tree.getSelectedItemId();
	if (!sid || sid.endWith(".txt;.bpel;.run;.xml")) {
		alert("请选择一个目录");
		return;
	}
	$("#itemName").show();
	$("#itemName").find("input[id='txtItemActor']").val("folder");
}

function createBpel() {
	var sid = tree.getSelectedItemId();
	if (!sid || sid.endWith(".txt;.bpel;.run;.xml")) {
		alert("请选择一个目录");
		return;
	}
	$("#itemName").show();
	$("#itemName").find("input[id='txtItemActor']").val("bpel");
}

function createRunnable() {
	var sid = tree.getSelectedItemId();
	if (!sid || sid.endWith(".txt;.bpel;.run;.xml")) {
		alert("请选择一个目录");
		return;
	}
	$("#itemName").show();
	$("#itemName").find("input[id='txtItemActor']").val("runnable");
}

function createService() {
	var sid = tree.getSelectedItemId();
	if (!sid || sid.endWith(".txt;.bpel;.run;.xml")) {
		alert("请选择一个目录");
		return;
	}
	$("#itemName").show();
	$("#itemName").find("input[id='txtItemActor']").val("service");
} 

function createFolderOrFile() {
	var sid = tree.getSelectedItemId();
	if (!sid || sid.endWith(".txt;.bpel;.run;.xml")) {
		alert("请选择一个目录");
		return;
	}
	var actor = $("#itemName").find("input[id='txtItemActor']").val();
	var name = $("#itemName").find("input[id='txtItemName']").val();
	if (isEmpty(name)) {
		alert("请输入目录名或者不带扩展名的文件名");
		return;
	}
	$("#itemName").hide();
	treeObject = {};
	if (actor == "folder") {
		callUrl("fileSystemManagerDS/createFolder.ssm", '[' + sid + ',' + name + ']', 'treeObject', true);
		if (isEmpty(treeObject) || isEmpty(treeObject.rtnValue) || treeObject.rtnValue == "null") {
			alert("创建目录失败");
			return;
		}
		tree.insertNewItem(sid, treeObject.rtnValue, name, "", "folderClosed.gif");
		return;
	}
	if (actor == "bpel") {
		name = name + ".bpel";
		parent.frames['xmain'].saveBpelObject();
	}
	if (actor == "runnable") {
		name = name + ".run";
		parent.frames['xmain'].saveRunnableObject();
	}
	if (actor == "service") {
		name = name + ".txt";
		parent.frames['xmain'].savePageObject();
	}
	pageObject = {};
	pageObject.jsonstr = $("#itemName").find("input[id='txtItemJsonStr']").val();
	callUrl("fileSystemManagerDS/createFile.ssm", '[' + sid + ',' + name + ',pageObject]', 'treeObject', true);
	if (isEmpty(treeObject) || isEmpty(treeObject.rtnValue) || treeObject.rtnValue == "null") {
		alert("创建文件失败");
		return;
	}
	tree.insertNewItem(sid, treeObject.rtnValue, name);
}

function deleteFile() {
	var sid = tree.getSelectedItemId();
	if (!sid) {
		alert("请选择一个目录或者文件");
		return;
	}
	if (!confirm("确认将(" + sid + ")删除?")) {
		return;
	}
	treeObject = {};
	callUrl("fileSystemManagerDS/deleteFile.ssm", sid, 'treeObject', true);
	if (isEmpty(treeObject) || isEmpty(treeObject.rtnValue) || treeObject.rtnValue == "null") {
		alert("删除失败");
		return;
	}
	tree.deleteItem(sid);
}

function saveFile(actor) {
	var sid = tree.getSelectedItemId();
	if (!sid) {
		alert("请选择一个目录或文件");
		return;
	}
	var filesep = (sid.indexOf("/") != -1) ? "/" : "\\";
	var pid = sid;
	var name = "";
	var isfile = sid.endWith(".txt;.bpel;.run;.xml");
	if (actor == "bpel") {
		if (isfile) {
			if (sid.endWith(".txt;.run;.xml")) {
				alert("文件类型不匹配");
				return;
			}
			if (!confirm("确认覆盖(" + sid + ")文件?")) {
				return;
			}
		}
		else {
			name = "新建文件" + getDT("yyyymmddhhmmssms") + ".bpel";
			sid = sid + filesep + name;
		}
		parent.frames['xmain'].saveBpelObject();
	}
	else if (actor == "runnable") {
		if (isfile) {
			if (sid.endWith(".txt;.bpel;.xml")) {
				alert("文件类型不匹配");
				return;
			}
			if (!confirm("确认覆盖(" + sid + ")文件?")) {
				return;
			}
		}
		else {
			name = "新建文件" + getDT("yyyymmddhhmmssms") + ".run";
			sid = sid + filesep + name;
		}
		parent.frames['xmain'].saveRunnableObject();
	}
	else if (actor == "service") {
		if (isfile) {
			if (sid.endWith(".bpel;.run;.xml")) {
				alert("文件类型不匹配");
				return;
			}
			if (!confirm("确认覆盖(" + sid + ")文件?")) {
				return;
			}
		}
		else {
			name = "新建文件" + getDT("yyyymmddhhmmssms") + ".txt";
			sid = sid + filesep + name;
		}
		parent.frames['xmain'].savePageObject();
	}
	pageObject = {};
	pageObject.jsonstr = $("#itemName").find("input[id='txtItemJsonStr']").val();
	callUrl("fileSystemManagerDS/saveFile.ssm", '[' + sid + ',pageObject]', 'treeObject', true);
	if (isEmpty(treeObject) || isEmpty(treeObject.rtnValue) || treeObject.rtnValue == "null") {
		alert("保存文件失败");
		return;
	}
	if (!isfile) tree.insertNewItem(pid, treeObject.rtnValue, name);
}

//@callback
function readFile() {
	$("#itemName").hide();
	var sid = tree.getSelectedItemId();
	if (!sid || !sid.endWith(".txt;.bpel;.run;.xml")) {
		return;
	}
	var divid = parent.frames['xmain'].getDivId();
	if (!((divid == "bpel" && sid.endWith(".bpel")) 
		|| (divid == "runnable" && sid.endWith(".run"))
		|| (divid == "service" && sid.endWith(".txt")))) {
		return;
	}
	treeObject = {};
	callUrl("fileSystemManagerDS/readFile.ssm", sid, 'treeObject', true);
	if (isEmpty(treeObject) || isEmpty(treeObject.rtnValue) || isEmpty(treeObject.rtnValue.jsonstr)) {
		alert("文件读取失败");
		return;
	}
	$("#itemName").find("input[id='txtItemJsonStr']").val(treeObject.rtnValue.jsonstr);
	if (divid == "bpel") {
		parent.frames['xmain'].readBpelObject();
	}
	else if (divid == "runnable") {
		parent.frames['xmain'].readRunnableObject();
	}
	else if (divid == "service") {
		parent.frames['xmain'].readPageObject();
	}
}

//@callback
function moveFile(sid, pid) {
	if (!sid || !sid.endWith(".txt;.bpel;.run;.xml")) {
		return false;
	}
	if (!pid || !pid.endWith(".txt;.bpel;.run;.xml")) {
		return false;
	}
	treeObject = {};
	callUrl("fileSystemManagerDS/moveFile.ssm", '[' + sid + ',' + pid + ']', 'treeObject', true);
	if (isEmpty(treeObject) || isEmpty(treeObject.rtnValue) || treeObject.rtnValue == "null") {
		alert("移动文件失败");
		return false;
	}
	tree.changeItemId(sid,treeObject.rtnValue);
	return true;
}

function exportFolderOrFile() {
	$("#itemName").hide();
	var sid = tree.getSelectedItemId();
	if (!sid || sid.endWith(".bepl;.run;.xml")) {
		alert("请选择一个目录或者接口测试文件");
		return;
	}
	if (sid.endWith(".txt")) {
		exportFile(sid);
	}
	else {
		exportFolder(sid);
	}
	alert("导出完成");
}

function exportFolder(sid) {
	var childrenstr = tree.getSubItems(sid);
	if (isEmpty(childrenstr)) {
		return;
	}
	var children = childrenstr.split(",");
	for (var i=0; i<children.length; i++) {
		if (children[i].endWith(".bpel;.run;.xml")) {
			continue;
		}
		if (children[i].endWith(".txt")) {
			exportFile(children[i]);
			continue;
		}
		exportFolder(children[i]);
	}
}

function exportFile(sid) {
	treeObject = {};
	callUrl("fileSystemManagerDS/readFile.ssm", sid, 'treeObject', true);
	if (isEmpty(treeObject) || isEmpty(treeObject.rtnValue) || isEmpty(treeObject.rtnValue.jsonstr)) {
		alert("文件(" + sid + ")读取失败");
		return;
	}
	var filesep = (sid.indexOf("/") != -1) ? "/" : "\\";
	sid = sid.replace(filesep + "tag" + filesep, filesep + "tst" + filesep).replace(".txt", ".html");
	var title = sid.substring(sid.lastIndexOf(filesep) + 1, sid.lastIndexOf("."));
	var obj = getJsonVal(treeObject.rtnValue.jsonstr);
	pageInfo = [];
	if (obj.sltDatabaseType == undefined) return;
	var chkBeanUserDefined = obj.chkBeanUserDefined;
	var chkQueryTable = obj.chkQueryTable;
	var sltComponent = obj.sltComponent;
	var sltBean = obj.sltBean;
	var sltBeanUd = obj.sltBeanUd;
	var sltParam = obj.sltParam;
	var params = sltParam;
	if (params.indexOf(")") != -1) params = params.substring(params.indexOf("(") + 1, params.indexOf(")"));
	var qrkchk = obj.qrychk;
	var querycond = obj.querycond;
	var k = 0;
	pageInfo[k++] = "测试目的：" + title + "。";
	pageInfo[k++] = "测试输入：" + (isEmpty(params) ? "无。" : ("入参为" + params + "。"));
	pageInfo[k++] = "测试输出：页面响应速度不大于2秒。";
	pageInfo[k++] = "约束条件：" + (isEmpty(qrkchk) ? "无。"  : (qrkchk + "为必填属性。"));
	pageInfo[k++] = "前置条件：无。";
	pageInfo[k++] = "测试步骤：";
	if (chkBeanUserDefined) {
		var m = 1;
		pageInfo[k++] = (m++) + ".选择自定义服务，输入服务名称：" + sltBeanUd + "；";
		if (querycond.length > 2) {
			var args = "(";
			var vals = "";
			for (var i=2; i<querycond.length; i++) {
				args = args + "arg" + (i-2) + ",";
				vals = vals + "，" + "arg" + (i-2) + ":" + createJsonStr(querycond[i]);
			}
			args = args.substring(0, args.length - 1) + ")";
			pageInfo[k++] = (m++) + ".点击加载接口，选择接口：" + sltParam.substring(0, sltParam.indexOf("(")) + args + "；";
			pageInfo[k++] = (m++) + ".点击加载参数，输入参数" + vals + "；";
		}
		else {
			pageInfo[k++] = (m++) + ".点击加载接口，选择接口：" + sltParam + "；";
			pageInfo[k++] = (m++) + ".点击加载参数；"
		}
		pageInfo[k++] = (m++) + ".点击调用服务按钮，执行成功。";
	}
	else {
		var m = 1;
		pageInfo[k++] = (m++) + ".选择组件名称：" + initComponentList(sltComponent) + "；";
		pageInfo[k++] = (m++) + ".选择服务名称：" + initBeanList(sltComponent, sltBean) + "；";
		pageInfo[k++] = (m++) + ".选择实体名称：" + initParamList(sltBean, sltParam) + 
			(chkQueryTable ? "，并勾选是否转化为表结构" : "") + "；";
		var vals = "";
		for (var tid in querycond[0]) {
			vals = vals + "，" + tid + ":" + querycond[0][tid];
		}
		pageInfo[k++] = (m++) + ".点击加载实体，输入参数" + vals + "；";
		if (!isEmpty(querycond[1])) {
			for (var tid in querycond[1]) {
				pageInfo[k++] = (m++) + ".点击创建对象，对象名称输入：" + tid + "，值输入：" + querycond[1][tid] + "；";
			}
		}
		pageInfo[k++] = (m++) + ".点击按钮，执行成功。";
	}
	callUrl("fileSystemManagerDS/exportFile.ssm", '[' + sid + ',' + title + ',pageInfo]', 'treeObject', true);
	if (isEmpty(treeObject) || isEmpty(treeObject.rtnValue)) {
		alert("文件(" + sid + ")导出失败");
	}
}
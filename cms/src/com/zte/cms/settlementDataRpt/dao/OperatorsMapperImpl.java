package com.zte.cms.settlementDataRpt.dao;

import com.zte.cms.settlementDataRpt.model.OperInformation;
import com.zte.ssb.dynamicobj.DynamicObjectBaseDao;
import com.zte.ssb.framework.base.util.PageInfo;

import java.util.List;

/**
 * @Author: Liangxiaomin
 * @Date Created in 15:02 2019/6/27
 * @Description:
 */
public class OperatorsMapperImpl extends DynamicObjectBaseDao implements OperatorsMapper {
    @Override
    public PageInfo selectOperators(OperInformation operInformation) {
        int pageSize = operInformation.getPageSize();
        int start = operInformation.getStart();
        PageInfo pageInfo = null;

        int totalCnt = ((Integer) super.queryForObject("selectOperatorsInfoCount", operInformation)).intValue();
        if (totalCnt > 0) {
            int fetchSize = pageSize > (totalCnt - start) ? (totalCnt - start) : pageSize;
            List<OperInformation> rsList = (List<OperInformation>) super.pageQuery("selectOperatorsInfo",
                    operInformation, start, fetchSize);
            pageInfo = new PageInfo(start, totalCnt, fetchSize, rsList);
        } else {
            pageInfo = new PageInfo();
        }


        return pageInfo;
    }
}

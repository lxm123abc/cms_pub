
//\u5224\u65ad\u8f93\u5165\u5b57\u7b26\u4e32\u662f\u5426\u4e3a\u5408\u6cd5\u7684\u65f6\u95f4\u683c\u5f0f(hh:mm:ss)
function isTime(str)
{
	var a = str.match(/^(\d{1,2})(:)?(\d{1,2})\2(\d{1,2})$/);
   	if (a == null) {return false;}
   	if (a[1]>=24 || a[3]>=60 || a[4]>=60)
    {
    	return false
     }
     return true;
}
function isTime2(str)
{
	var a = str.match(/^(\d{2})(:)?(\d{2})\2(\d{2})$/);
   	if (a == null) {return false;}
   	if (a[1]>=24 || a[3]>=60 || a[4]>=60)
    {
    	return false
     }
     return true;
}
// \u5224\u65ad\u8f93\u5165\u5b57\u7b26\u4e32\u662f\u5426\u4e3a\u5408\u6cd5\u7684\u65e5\u671f\u683c\u5f0f(YYYY-MM-DD)
function isDate(str)
{
 	var r = str.match(/^(\d{1,4})(-)(\d{1,2})\2(\d{1,2})$/);
   	if(r==null)return false;
    var d= new Date(r[1], r[3]-1, r[4]);
    return (d.getFullYear()==r[1]&&(d.getMonth()+1)==r[3]&&d.getDate()==r[4]);
}

// (2003-12-05 13:04:06)
function isDateTime(str)
{
	var reg = /^(\d{1,4})(-|\/)(\d{1,2})\2(\d{1,2}) (\d{1,2}):(\d{1,2}):(\d{1,2})$/;
    var r = str.match(reg);
    if(r==null)return false;
    var d= new Date(r[1], r[3]-1,r[4],r[5],r[6],r[7]);
    return (d.getFullYear()==r[1]&&(d.getMonth()+1)==r[3]&&d.getDate()==r[4]&&d.getHours()==r[5]&&d.getMinutes()==r[6]&&d.getSeconds()==r[7]);
}

// (20031205)
function isShortDate(str)
{
  	if (!str.match(/\d{8}/)){
     	return false;
  	}
    var d= new Date(str.substr(0,4), str.substr(4,2)-1, str.substr(6,2));
    return (d.getFullYear()==str.substr(0,4)&&(d.getMonth()+1)==str.substr(4,2)&&d.getDate()==str.substr(6,2));
}

//date1\u662f\u5426\u7b49\u4e8e\u6216\u5728date2\u4e4b\u524d\u3002\u5982\u679c\u65e5\u671f\u683c\u5f0f\u4e0d\u7b26\u5408\u8981\u6c42\u4e5f\u5c06\u8fd4\u56defalse
function isDateBefore(date1,date2)
{
	if(!isDate(date1)||!isDate(date2))
		return false;
	var r1 = date1.match(/^(\d{1,4})(-|\/)(\d{1,2})\2(\d{1,2})$/);
    var d1= new Date(r1[1], r1[3]-1, r1[4]);
	var r2 = date2.match(/^(\d{1,4})(-|\/)(\d{1,2})\2(\d{1,2})$/);
    var d2= new Date(r2[1], r2[3]-1, r2[4]);
	if(d1<=d2)
		return true;
	else
		return false;
}

//\u5224\u65ad
function isShortDateBefore(date1,date2){
	if(!isShortDate(date1)||!isShortDate(date2))
		return false;
	if(date1<=date2)
	return true;
	else
	return false;
}

//\u5224\u65ad\u8f93\u5165\u5b57\u7b26\u4e32\u662f\u5426\u4e3a\u624b\u673a\u53f7\u7801
function isMdn(value){
	if (value.length<6||value.length>15)
	return false;
	if(value.match(/^\d+$/))
		return true;
	else
		return false;
}

/*

\u6ee4\u6389\u8f93\u5165\u5b57\u7b26\u4e32\u524d\u540e\u7684\u7a7a\u683c


*/


function trim(input){
    return input.replace(/(^\s*)|(\s*$)/g, "");
}
/*
//\u5224\u65ad\u8f93\u5165\u5b57\u7b26\u4e32\u662f\u5426\u4e3a\u7a7a
//\u5982\u679c\u8f93\u5165\u4e3a\u7a7a\uff0c\u6216\u53ea\u6709\u7a7a\u683c\u5219\u8fd4\u56detrue,\u5426\u5219\u8fd4\u56defalse

*/
function isEmpty(input){
	if(trim(input)=="") return true;
	else return false;
}
/*

//\u68c0\u67e5\u5f85\u6d4b\u5b57\u7b26\u4e32sCheck\u662f\u5426\u662f\u683c\u5f0f\u6b63\u786e\u7684\u7535\u8bdd\u53f7\u7801
//showalert = true  \u663e\u793a\u8be6\u7ec6\u8b66\u544a\u4fe1\u606f
//showalert = false \u5173\u95ed\u8be6\u7ec6\u8b66\u544a\u4fe1\u606f

*/
function isValidPstnNum(sCheck,showalert){
  if (isEmpty(sCheck)){
    if(showalert)
      alert("\u7535\u8bdd\u53f7\u7801\u4e0d\u80fd\u4e3a\u7a7a\uff01");
    return false;
  }
  if(sCheck.length < 7){
    if(showalert)
      alert("\u7535\u8bdd\u53f7\u7801\u957f\u5ea6\u5e94\u8be5\u81f3\u5c117\u4f4d\uff01");
    return false;
  }
  if(!isValidChars (sCheck, "0123456789-()")){
    if(showalert)
    /*
    alert("\u7535\u8bdd\u53f7\u7801\u53ea\u80fd\u5305\u542b\u4ee5\u4e0b\u5b57\u7b26:" + "\r\n" + "0123456789-()\n");
    */
    alert("PostNumber should only contain the following chars:"+"\r\n"+"012345678-()\n");
    return false;
  }
  return true;
}
function isValidPstnNumWithLength(sCheck,showalert,slength){
  if (isEmpty(sCheck)){
    if(showalert)
      alert("\u7535\u8bdd\u53f7\u7801\u4e0d\u80fd\u4e3a\u7a7a\uff01");
    return false;
  }
  if(sCheck.length < slength){
    if(showalert)
      alert("\u7535\u8bdd\u53f7\u7801\u957f\u5ea6\u5e94\u8be5\u81f3\u5c117\u4f4d\uff01");
    return false;
  }
  if(!isValidChars (sCheck, "0123456789-()")){
    if(showalert)
    /*
    alert("\u7535\u8bdd\u53f7\u7801\u53ea\u80fd\u5305\u542b\u4ee5\u4e0b\u5b57\u7b26:" + "\r\n" + "0123456789-()\n");
    */
    alert("PostNumber should only contain the following chars:"+"\r\n"+"012345678-()\n");
    return false;
  }
  return true;
}

/*
//\u5224\u65ad\u5f85\u6d4b\u5b57\u7b26cCheck\u662f\u5426\u662f\u6570\u5b57

*/
function isDigit(cCheck) {
  return (('0'<=cCheck) && (cCheck<='9'));
}

/*
//\u5224\u65ad\u5f85\u6d4b\u5b57\u7b26cCheck\u662f\u5426\u662f\u5927\u5c0f\u5199\u5b57\u6bcd
*/
function isAlpha(cCheck) {
  return ((('a'<=cCheck) && (cCheck<='z')) || (('A'<=cCheck) && (cCheck<='Z')))
}
/*
//\u5224\u65ad\u5f85\u6d4b\u5b57\u7b26cCheck\u662f\u5426\u662f\u53ef\u89c1\u5b57\u7b26
//32\u662f\u7a7a\u683c\uff0c126\u662f~
*/
function isVisibleChar(cCheck) {
  return ((33<=cCheck) && (cCheck<=126));
}

/*
//\u5224\u65ad\u5f85\u6d4b\u5b57\u7b26\u4e32sCheck\u662f\u5426\u662f\u7eaf\u6570\u5b57\u4e32
*/
function isNumeric(sCheck){
  var regexp= /^\d+$/;
  //must be large than 0
  
  return sCheck.match(regexp)&&parseInt(sCheck,10)>0;
  /*
  for (nIndex=0; nIndex<sCheck.length; nIndex++)
  {
     cCheck = sCheck.charAt(nIndex);
        if (!(isDigit(cCheck)))
           return false;
  }
  return true;
  */
}
/*
//\u5224\u65ad\u5f85\u6d4b\u5b57\u7b26\u4e32sCheck\u662f\u5426\u662f\u53ef\u89c1\u5b57\u7b26
*/
function isVisibleChars(sCheck){
  for (nIndex=0; nIndex<sCheck.length; nIndex++)
  {
     cCheck = sCheck.charAt(nIndex);
        if (!(isVisibleChar(cCheck)))
           return false;
  }
  return true;
}
/*
//\u5224\u65ad\u5f85\u6d4b\u5b57\u7b26\u4e32sCheck\u662f\u5426\u5305\u542b\u4e2d\u6587\u5b57\u7b26
*/
function hasChineseChars(sCheck){
  for (nIndex=0; nIndex<sCheck.length; nIndex++)
  {
     cCheck = sCheck.charCodeAt(nIndex);
        if (cCheck > 255)
           return true;
  }
  return false;
}

/*

//\u5224\u65ad\u5f85\u6d4b\u5b57\u7b26\u4e32sCheck\u662f\u5426\u5305\u542b\u7a7a\u683c


*/
function hasWhitespace (sCheck){
  var whitespace = " \t\n\r";
  var i;
  for (i = 0; i < sCheck.length; i++){
    var c = sCheck.charAt(i);
    if (whitespace.indexOf(c) >= 0){
      return true;
    }
  }
  return false;
}
/*
//\u68c0\u67e5\u5f85\u6d4b\u5b57\u7b26\u4e32\u4e2d\u7684\u5b57\u7b26\u662f\u5426\u5168\u662f\u5408\u6cd5\u5b57\u7b26
//ValidChars\u6307\u5b9a\u6709\u6548\u5b57\u7b26\u96c6\u5408\uff0c\u5982"0123456789;*"
*/
function isValidChars(sCheck,ValidChars)
{
   var i,j;
   if (sCheck.length== 0)
       return false
   for (i=0;i<sCheck.length;i++)
   {
      j = ValidChars.indexOf(sCheck.charAt(i));
      if (j==-1)
        return false;
   }
   return true;
}

/*
//\u68c0\u67e5\u5f85\u6d4b\u5b57\u7b26\u4e32sCheck\u662f\u5426\u662f\u683c\u5f0f\u6b63\u786e\u7684\u90ae\u653f\u7f16\u7801
//showalert = true  \u663e\u793a\u8be6\u7ec6\u8b66\u544a\u4fe1\u606f
//showalert = false \u5173\u95ed\u8be6\u7ec6\u8b66\u544a\u4fe1\u606f
*/
function isValidZipCode(sCheck,showalert){
  if (isEmpty(sCheck)){
    if(showalert)
      alert("\u90ae\u653f\u7f16\u7801\u4e0d\u80fd\u4e3a\u7a7a\uff01");
    return false;
  }
  if(!isValidChars (sCheck, "0123456789")){
    if(showalert)
      alert("\u90ae\u653f\u7f16\u7801\u5fc5\u987b\u4e3a\u7eaf\u6570\u5b57\uff01");
    return false;
  }
  if(sCheck.length != 6){
    if(showalert)
      alert("\u90ae\u653f\u7f16\u7801\u957f\u5ea6\u5e94\u8be5\u4e3a6\u4f4d\uff01");
    return false;
  }
  return true;
}
/*
//\u68c0\u67e5\u5f85\u6d4b\u5b57\u7b26\u4e32sCheck\u662f\u5426\u662f\u683c\u5f0f\u6b63\u786e\u7684SPID
//showalert = true  \u663e\u793a\u8be6\u7ec6\u8b66\u544a\u4fe1\u606f
//showalert = false \u5173\u95ed\u8be6\u7ec6\u8b66\u544a\u4fe1\u606f
*/
function isValidSpid(sCheck,showalert){
  if(!isNumeric(sCheck)||sCheck.length>8||sCheck.length<4){
    if(showalert)
      alert("\u4f01\u4e1a\u4ee3\u7801\u5fc5\u987b\u5927\u4e8e\u7b49\u4e8e4\u4f4d\u6570\u5b57\u6216\u5c0f\u4e8e\u7b49\u4e8e8\u4f4d\u6570\u5b57\uff01");
    return false;
  }
  return true;
}

/*
//\u68c0\u67e5\u5f85\u6d4b\u5b57\u7b26\u4e32sCheck\u662f\u5426\u662f\u683c\u5f0f\u6b63\u786e\u7684\u7528\u6237\u53e3\u4ee4
//showalert = true  \u663e\u793a\u8be6\u7ec6\u8b66\u544a\u4fe1\u606f
//showalert = false \u5173\u95ed\u8be6\u7ec6\u8b66\u544a\u4fe1\u606f

*/
function isValidPassword(sCheck,showalert){
  if (isEmpty(sCheck)){
    if(showalert)
      alert("\u53e3\u4ee4\u4e0d\u80fd\u4e3a\u7a7a\uff01");
    return false;
  }
  if(sCheck.length < 6){
    if(showalert)
      alert("\u53e3\u4ee4\u957f\u5ea6\u81f3\u5c11\u9700\u89816\u4f4d\uff01");
    return false;
  }
  var validChars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789.-_@!~`#^&()+={}[]:;\"'<>,.?$*";
  if(!isValidChars (sCheck, validChars)){
    if(showalert)
    /*
      alert("\u53e3\u4ee4\u53ea\u80fd\u5305\u542b\u4ee5\u4e0b\u5b57\u7b26:     " + "\r\n" + validChars + "\n");
    */
    alert("only can containt the following chars:"+"\r\n"+validChars+"\n");
    return false;
  }
  return true;
}
/*
//\u68c0\u67e5\u5f85\u6d4b\u5b57\u7b26\u4e32sCheck\u662f\u5426\u662f\u683c\u5f0f\u6b63\u786e\u7684Email\u5730\u5740
//showalert = true  \u663e\u793a\u8be6\u7ec6\u8b66\u544a\u4fe1\u606f
//showalert = false \u5173\u95ed\u8be6\u7ec6\u8b66\u544a\u4fe1\u606f


*/
function isValidEmail(sCheck,showalert){
  if (isEmpty(sCheck)){
    if(showalert)
      alert("Email address need be input!");
    return false;
  }
  if (hasWhitespace(sCheck)){
    if(showalert)
      alert("Email address is invalid");
    return false;
  }
  var i   = 1;
  if (sCheck.length > 40){
    if(showalert)
      alert("Email address is less than 40 characters");
    return false;
  }
  var validChars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789.-_@";
  if ( !isValidChars(sCheck,validChars)){
    if(showalert)
     /*
      alert("Email\u5730\u5740\u4e2d\u53ea\u80fd\u5305\u542b\u4ee5\u4e0b\u5b57\u7b26:" + "\r\n" + validChars +"\n");
     */
     alert("Email address is invalid");
    return false;
  }

  var posa1 = sCheck.indexOf("@");
  var posb1 = sCheck.indexOf(".");
  var posa2 = sCheck.lastIndexOf("@");
  var posb2 = sCheck.lastIndexOf(".");

  if ( (posa1 == -1 ||posb1 == -1) //\u7f3a\u5c11'@'\u6216'.'
     ||(posa1 != posa2) //\u4e0d\u6b62\u4e00\u4e2a'@'
     ){
      if(showalert)
        alert("Email Address is invalid");
      return false;
  }
  return true;
}

//\u8fd4\u56de\u4e00\u4e2a\u5b57\u7b26\u4e32\u7684\u5b9e\u9645\u957f\u5ea6\uff0c
//\u4e00\u4e2a\u6c49\u5b57\u957f\u5ea6\u4e3a2
function strlen(str)
{
  var len;
  var i;
  len = 0;
  for (i=0;i<str.length;i++)
  {
    if (str.charCodeAt(i)>255) len+=3; else len++;
  }
  return len;
}

// \u589e\u52a0\u4e00\u4e2a\u540d\u4e3a trim \u7684\u51fd\u6570\u4f5c\u4e3a
// String \u6784\u9020\u51fd\u6570\u7684\u539f\u578b\u5bf9\u8c61\u7684\u4e00\u4e2a\u65b9\u6cd5\u3002
String.prototype.trim = function()
{
    // \u7528\u6b63\u5219\u8fbe\u5f0f\u5c06\u524d\u540e\u7a7a\u683c
    // \u7528\u7a7a\u5b57\u7b26\u4e32\u66ff\u4ee3\u3002
    return this.replace(/(^\s*)|(\s*$)/g, "");
}

//\u68c0\u67e5\u5f85\u6d4b\u5b57\u7b26\u4e32sCheck\u662f\u5426\u662f\u683c\u5f0f\u6b63\u786e\u7684\u8d85\u94fe\u63a5
//showalert = true  \u663e\u793a\u8be6\u7ec6\u8b66\u544a\u4fe1\u606f
//showalert = false \u5173\u95ed\u8be6\u7ec6\u8b66\u544a\u4fe1\u606f
function isValidateURL(sCheck,showalert){
  var regexp = /^http:\/\//;
  var result = sCheck.match(regexp);
  if(!result){
    if(showalert)
      alert("Please input the correct url\uff08http://\uff09\uff01");
	return false;
  }
  return true;
}


//\u68c0\u67e5\u8f93\u5165\u5b57\u7b26\u4e32\u662f\u5426\u5305\u542b\u6307\u5b9a\u7684\u5b57\u7b26,\u5e76\u544a\u8b66
//str \u8f93\u5165\u5b57\u7b26\u4e32
//schar \u6307\u5b9a\u5b57\u7b26
//message \u544a\u8b66\u4fe1\u606f
function containChar(schar,str){
  if(isEmpty(schar)){
    schar="'";
  }
  if(str.value.indexOf(schar)>=0){
    str.focus();
    return true;
  }
  else {
    return false;
  }
}
/*
//\u68c0\u67e5\u8f93\u5165\u5b57\u7b26\u4e32\u662f\u5426\u662f\u6570\u5b57\u8868\u8fbe\u5f0f
//\u4f4623.32\u8fd9\u79cd\u5e26\u5c0f\u6570\u7684\u683c\u5f0f\u6b63\u786e

*/
function isNum(sCheck){
  var regexp= /^\d+$/;
  var regexq=/^(\d+)(\.)(\d+)$/;
  return sCheck.match(regexp)||sCheck.match(regexq);
}

function isNumPrice(sCheck){
  var regexp= /^\d+$/;
  var regexq=/^(\d+)(\.)(\d\d)$/;
  var regexr=/^(\d+)(\.)(\d)$/;
  var flag1 = sCheck.match(regexp)||sCheck.match(regexq)||sCheck.match(regexr);
  //must be larger than 0
  var flag2 = false;
  if(flag1){
    flag2 =sCheck*100.0>0.0;
  }
  return flag1&&flag2;
}

/*
//\u68c0\u67e5\u5bf9\u8c61\u7684\u503c\u662f\u5426\u662f\u6570\u5b57\u8868\u8fbe\u5f0f
//\u4f4623.32\u8fd9\u79cd\u5e26\u5c0f\u6570\u7684\u683c\u5f0f\u6b63\u786e

*/
function isNumObject(obj){
  var regexp= /^\d+$/;
  var regexq=/^(\d+)(\.)(\d+)$/;
  if(!(obj.value.match(regexp)||obj.value.match(regexq))){
     obj.focus();
     return false;
    }
    return true;
}

/*
//\u5224\u65ad\u5bf9\u8c61\u503c\u662f\u5426\u4e3a\u7a7a
//\u5982\u679c\u4e3a\u7a7a\uff0c\u6216\u53ea\u6709\u7a7a\u683c\u5219\u8fd4\u56detrue,\u5e76\u83b7\u5f97\u7126\u70b9,\u5426\u5219\u8fd4\u56defalse


*/
function isEmptyObject(obj){
	if(trim(obj.value)==""){
          obj.focus();
          return true;
	}
       return false;
}

function checkip(ip){

	
var iplength = ip.length;

var Letters = "1234567890.";
var scount=0;
for (var j=0; j < ip.length; j++)
  {
   var CheckChar = ip.charAt(j);
   if (Letters.indexOf(CheckChar) == -1)
   {
    return false;
   }
  }

for (var j = 0;j<iplength;j++){
 (ip.substr(j,1)==".")?scount++:scount;
}
if(scount!=3){

return false;}

var  first = ip.indexOf(".");  //\u7b2c\u4e00\u6b21\u51fa\u73b0"."\u7684\u5b57\u7b26\u4f4d\u7f6e\u3002

var last = ip.lastIndexOf(".");  //"."\u7b26\u4e32\u6700\u540e\u51fa\u73b0\u7684\u4f4d\u7f6e
var strIP = ip.substring(first+1,last);//\u53d6\u5f97\u9664\u53bb\u5f00\u5934\u548c\u7ed3\u5c3e\u7684\u57df
var lenStrIP = strIP.length;
var str1 = ip.substring(0,first);//\u53d6\u5f97\u7b2c1\u4e2aip\u6bb5

var second = strIP.indexOf(".");
var str2 = strIP.substring(0,second);//\u53d6\u5f97\u7b2c2\u4e2aip\u6bb5

var str3 = strIP.substring(second+1,lenStrIP);//\u53d6\u5f97\u7b2c3\u4e2aip\u6bb5
var str4 = ip.substring(last+1,iplength); //\u53d6\u5f97\u7b2c4\u4e2aip\u6bb5

 if (parseInt(str1,10)<= parseInt(0) || parseInt(str1,10) >parseInt(255)||str1=="" || str1.length > 3)
       { 
     
        return false;
    }else if (parseInt(str2,10)< parseInt(0) || parseInt(str2,10) >parseInt(255)||str2=="" || str2.length > 3)
    {
 
    return false;
  
    }else if (parseInt(str3,10)< parseInt(0) || parseInt(str3,10) >parseInt(255)||str3=="" || str3.length > 3)
    {
  
    return false;
    }else if(parseInt(str4,10) < parseInt(0)|| parseInt(str4,10) >parseInt(255)||str4=="" || str4.length > 3)
    {
  
    return false;
    }else{
return true;}
}
function checkipsize(ip1,ip2)
{
	//beginIP
var  first1 = ip1.indexOf(".");  //\u7b2c\u4e00\u6b21\u51fa\u73b0"."\u7684\u5b57\u7b26\u4f4d\u7f6e\u3002
var iplength1 = ip1.length
var last1 = ip1.lastIndexOf(".");  //"."\u7b26\u4e32\u6700\u540e\u51fa\u73b0\u7684\u4f4d\u7f6e
var strIP1 = ip1.substring(first1+1,last1);//\u53d6\u5f97\u9664\u53bb\u5f00\u5934\u548c\u7ed3\u5c3e\u7684\u57df
var lenStrIP1 = strIP1.length;
var Bstr1 = ip1.substring(0,first1);//\u53d6\u5f97\u7b2c1\u4e2aip\u6bb5

var second1 = strIP1.indexOf(".");
var Bstr2 = strIP1.substring(0,second1);//\u53d6\u5f97\u7b2c2\u4e2aip\u6bb5

var Bstr3 = strIP1.substring(second1+1,lenStrIP1);//\u53d6\u5f97\u7b2c3\u4e2aip\u6bb5
var Bstr4 = ip1.substring(last1+1,iplength1); //\u53d6\u5f97\u7b2c4\u4e2aip\u6bb5

//endIP
var  first2 = ip2.indexOf(".");  //\u7b2c\u4e00\u6b21\u51fa\u73b0"."\u7684\u5b57\u7b26\u4f4d\u7f6e\u3002
var iplength2 = ip2.length
var last2 = ip2.lastIndexOf(".");  //"."\u7b26\u4e32\u6700\u540e\u51fa\u73b0\u7684\u4f4d\u7f6e
var strIP2 = ip2.substring(first2+1,last2);//\u53d6\u5f97\u9664\u53bb\u5f00\u5934\u548c\u7ed3\u5c3e\u7684\u57df
var lenStrIP2 = strIP2.length;
var Estr1 = ip2.substring(0,first2);//\u53d6\u5f97\u7b2c1\u4e2aip\u6bb5

var second2 = strIP2.indexOf(".");
var Estr2 = strIP2.substring(0,second2);//\u53d6\u5f97\u7b2c2\u4e2aip\u6bb5

var Estr3 = strIP2.substring(second2+1,lenStrIP2);//\u53d6\u5f97\u7b2c3\u4e2aip\u6bb5
var Estr4 = ip2.substring(last2+1,iplength2); //\u53d6\u5f97\u7b2c4\u4e2aip\u6bb5

//check
if(parseInt(Bstr1,10)>parseInt(Estr1,10)){
	
	return false;
}
if(Bstr1==Estr1){
	
	if(parseInt(Bstr2,10)>parseInt(Estr2,10)){
		
		return false;
	}
	if(Bstr2==Estr2){
		
		if(parseInt(Bstr3,10)>parseInt(Estr3,10)){
			
			return false;
		}
		if(Bstr3==Estr3){
			
			if(parseInt(Bstr4,10)>=parseInt(Estr4,10)){
			
				return false;
			}
		}
	}
}
}

function is_include1(str, test) {
	var i = 0;
	for (i=0; i<str.length; i++) {
		if (test.indexOf(str.charAt(i)) == -1) return false;
	}
	return true;
}

function checkCode(inputStr)
{
	
   var myReg = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789_@.^-";
	return is_include1(inputStr, myReg);
}
function checkCodestr(inputStr)
{
	
   var myReg = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
	return is_include1(inputStr, myReg);
}
function getStrLength(str)
{
	var s = str;
	var num = 0;
	for(var i=0;i<s.length;i++)
	{
		num++;
		if(s.charCodeAt(i)>255)
		{
			num++;
		}
	}
	return num;
}

function isInaz(str){
	var s = str;
	for(var i=0;i<s.length;i++)
	{
		if(s.charCodeAt(i)<48||s.charCodeAt(i)>122||(s.charCodeAt(i)>57&&s.charCodeAt(i)<97))
		{
			return true;
		}
	}
	return false;
}
function isInaze(str){
	var s = str;
	for(var i=0;i<s.length;i++)
	{
	 if((s.charCodeAt(i)<48||s.charCodeAt(i)>1103||((s.charCodeAt(i)>57&&s.charCodeAt(i)<97)||(s.charCodeAt(i)>122&&s.charCodeAt(i)<1040)))&&(s.charCodeAt(i)!=1025&&s.charCodeAt(i)!=1105))
		{
			return true;
		}
	}
	return false;
}
function isInazObject(obj){
	var s = obj.value;
	for(var i=0;i<s.length;i++)
	{
		if(s.charCodeAt(i)<48||s.charCodeAt(i)>122||(s.charCodeAt(i)>57&&s.charCodeAt(i)<97))
		{			
			obj.focus();
			return true;
		}
	}
	return false;
}
function isInazeObject(obj){
	var s = obj.value;
	for(var i=0;i<s.length;i++)
	{
          if((s.charCodeAt(i)<48||s.charCodeAt(i)>1103||((s.charCodeAt(i)>57&&s.charCodeAt(i)<97)||(s.charCodeAt(i)>122&&s.charCodeAt(i)<1040)))&&(s.charCodeAt(i)!=1025&&s.charCodeAt(i)!=1105))
		{
			obj.focus();
			return true;
		}
	}
	return false;
}
//
function isIPAddr(value){
	var rep = /^([01]?\d\d?|2[0-4]\d|25[0-5])(\.([01]?\d\d?|2[0-4]\d|25[0-5])){3}$/;
	return value.match(rep);
}
//00:00:89
function getTimeDuration(vstart,vend){
	var _e = parseInt(vend.substr(0,2),10)*3600+parseInt(vend.substr(3,2),10)*60+parseInt(vend.substr(6,2),10);
	var _s = parseInt(vstart.substr(0,2),10)*3600+parseInt(vstart.substr(3,2),10)*60+parseInt(vstart.substr(6,2),10);
	return _e-_s;
		
}

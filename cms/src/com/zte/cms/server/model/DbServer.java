package com.zte.cms.server.model;

public class DbServer
{

    private java.lang.String usedsize;

    private java.lang.String freesize;

    private java.lang.String totalconnect;

    private java.lang.String freeconnect;

    private java.lang.String extendsize;

    public java.lang.String getExtendsize()
    {
        return extendsize;
    }

    public void setExtendsize(java.lang.String extendsize)
    {
        this.extendsize = extendsize;
    }

    public java.lang.String getUsedsize()
    {
        return usedsize;
    }

    public void setUsedsize(java.lang.String usedsize)
    {
        this.usedsize = usedsize;
    }

    public java.lang.String getFreesize()
    {
        return freesize;
    }

    public void setFreesize(java.lang.String freesize)
    {
        this.freesize = freesize;
    }

    public java.lang.String getTotalconnect()
    {
        return totalconnect;
    }

    public void setTotalconnect(java.lang.String totalconnect)
    {
        this.totalconnect = totalconnect;
    }

    public java.lang.String getFreeconnect()
    {
        return freeconnect;
    }

    public void setFreeconnect(java.lang.String freeconnect)
    {
        this.freeconnect = freeconnect;
    }

}

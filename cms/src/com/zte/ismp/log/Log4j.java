package com.zte.ismp.log;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.input.SAXBuilder;

import com.zte.ssb.framework.SSBBus;
import com.zte.ssb.framework.common.log.Log;

public class Log4j
{
    private static Log log = SSBBus.getLog(Log4j.class);
    private static String LEVEL_BEBUG = "debug";
    private static String LEVEL_INFO = "info";
    private static String LEVEL_WARN = "warn";
    private static String LEVEL_ERROR = "error";
    private static String LEVEL_FATAL = "fatal";
    private static String LEVEL_RESET = "reset";

    private static String ROOT_LOGGER_NAME = "root";

    /**
     * 修改日志级别
     * 
     * @param level
     */
    public static void setLevel(String level)
    {
        if (LEVEL_RESET.equalsIgnoreCase(level))
        {
            // LogManager.shutdown();
            // DOMConfigurator.configure(Log4jTest.class.getResource("/log4j.xml"));
            resetConfiguration(null);
        }
        else
        {
            resetConfiguration(level);
        }

    }

    /**
     * 重置log4j的配置
     */
    public static void resetConfiguration(String level)
    {
        List loggerlist = getLoggersFromFile();

        for (Object obj : loggerlist)
        {
            String loggerlevel = level;
            LoggerInfo loggerinfo = (LoggerInfo) obj;
            if (level == null)
            {
                loggerlevel = loggerinfo.getLoggerLevel();
            }
            if (!ROOT_LOGGER_NAME.equalsIgnoreCase(loggerinfo.getLoggerName()))
            {
                setLoggerLevel(Logger.getLogger(loggerinfo.getLoggerName()), loggerlevel);
            }
            else
            {
                setLoggerLevel(Logger.getRootLogger(), loggerlevel);
            }
        }
    }

    /**
     * 修改日志级别
     * 
     * @param list
     */
    public static void changeLoggerLevel(List<LoggerInfo> list)
    {
        if (list != null)
        {
            for (Object obj : list)
            {
                LoggerInfo loggerInfo = (LoggerInfo) obj;
                if (ROOT_LOGGER_NAME.equalsIgnoreCase(loggerInfo.getLoggerName()))
                {
                    Logger rootLogger = Logger.getRootLogger();
                    setLoggerLevel(rootLogger, loggerInfo.getLoggerLevel());
                }
                else
                {
                    Logger l = Logger.getLogger(loggerInfo.getLoggerName());
                    setLoggerLevel(l, loggerInfo.getLoggerLevel());
                }

            }
        }
    }

    /**
     * 修改日志记录器的级别
     * 
     * @param logger
     * @param level
     */
    public static void setLoggerLevel(Logger logger, String level)
    {
        if (logger != null)
        {
            if (LEVEL_BEBUG.equalsIgnoreCase(level))
            {
                logger.setLevel(Level.DEBUG);
            }
            else if (LEVEL_INFO.equalsIgnoreCase(level))
            {
                logger.setLevel(Level.INFO);
            }
            else if (LEVEL_WARN.equalsIgnoreCase(level))
            {
                logger.setLevel(Level.WARN);
            }
            else if (LEVEL_ERROR.equalsIgnoreCase(level))
            {
                logger.setLevel(Level.ERROR);
            }
            else if (LEVEL_FATAL.equalsIgnoreCase(level))
            {
                logger.setLevel(Level.FATAL);
            }
            else
            {
                logger.setLevel(Level.ERROR);
            }
        }
    }

    /**
     * 读取log4j.xml文件
     * 
     * @return
     */
    public static Element readLog4jXml()
    {
        Element root = null;
        try
        {
            SAXBuilder builder = new SAXBuilder();
            Document doc = builder.build(Log4j.class.getResource("/log4j.xml"));
            root = doc.getRootElement();
        }
        catch (Exception e)
        {
            log.error("readLog4jXml : ", e);
        }
        return root;
    }

    /**
     * 得到log4j.xml文件中的所有 logger
     * 
     * @return
     */
    public static List getLoggersFromMemory()
    {
        List list = new ArrayList();
        try
        {

            Element root = readLog4jXml();
            if (root != null)
            {
                List children = root.getChildren("logger");
                Logger rootLogger = Logger.getRootLogger();
                String rootLevel = "";
                if (rootLogger != null)
                {
                    rootLevel = rootLogger.getLevel().toString();
                    LoggerInfo rootLoggerInfo = new LoggerInfo();
                    rootLoggerInfo.setLoggerName(rootLogger.getName());
                    rootLoggerInfo.setLoggerLevel(rootLevel.toLowerCase());
                    list.add(rootLoggerInfo);

                    for (Object obj : children)
                    {
                        Element child = (Element) obj;
                        String loggerName = child.getAttributeValue("name");

                        Logger logger = Logger.getLogger(loggerName);
                        String loggerLevel = rootLevel;
                        if (logger != null)
                        {
                            loggerLevel = logger.getLevel().toString();
                        }
                        if (loggerLevel != null && loggerLevel != "")
                        {
                            LoggerInfo loggerInfo = new LoggerInfo();
                            loggerLevel = loggerLevel.toLowerCase();
                            loggerInfo.setLoggerName(loggerName);
                            loggerInfo.setLoggerLevel(loggerLevel);
                            list.add(loggerInfo);
                        }
                        System.out.println("logger name=" + loggerName + "logger level=" + loggerLevel);
                    }
                }
            }
        }
        catch (Exception e)
        {
            log.error("getLoggersFromMemory", e);
        }
        return list;
    }

    public static List getLoggersFromFile()
    {
        List list = new ArrayList();
        try
        {

            Element root = readLog4jXml();
            if (root != null)
            {
                List children = root.getChildren("logger");
                for (Object obj : children)
                {
                    Element child = (Element) obj;
                    String loggerName = child.getAttributeValue("name");
                    Element levelElem = child.getChild("level");
                    String loggerLevel = null;
                    if (levelElem != null)
                    {
                        loggerLevel = levelElem.getAttributeValue("value");
                    }

                    LoggerInfo loggerInfo = new LoggerInfo();
                    loggerInfo.setLoggerName(loggerName);
                    loggerInfo.setLoggerLevel(loggerLevel);
                    list.add(loggerInfo);
                }

                Logger rootlogger = Logger.getRootLogger();
                Element rootElem = root.getChild("root");
                if (rootElem != null)
                {
                    Element priority = rootElem.getChild("priority");
                    String rootlevel = null;
                    if (priority != null)
                    {
                        rootlevel = priority.getAttributeValue("value");
                    }
                    LoggerInfo loggerInfo = new LoggerInfo();
                    loggerInfo.setLoggerName(rootlogger.getName());
                    loggerInfo.setLoggerLevel(rootlevel);
                    list.add(loggerInfo);
                }
                else
                {
                    LoggerInfo loggerInfo = new LoggerInfo();
                    loggerInfo.setLoggerName(rootlogger.getName());
                    loggerInfo.setLoggerLevel(rootlogger.getLevel().toString());
                    list.add(loggerInfo);
                }
            }

        }
        catch (Exception e)
        {
            log.error("getLoggersFromFile", e);
        }
        return list;
    }

    /**
     * 得到日志级别
     * 
     * @param level
     * @return
     */
    public static Level getLevel(String level)
    {
        if (LEVEL_BEBUG.equalsIgnoreCase(level))
        {
            return Level.DEBUG;
        }
        if (LEVEL_INFO.equalsIgnoreCase(level))
        {
            return Level.INFO;
        }
        if (LEVEL_WARN.equalsIgnoreCase(level))
        {
            return Level.WARN;
        }
        if (LEVEL_ERROR.equalsIgnoreCase(level))
        {
            return Level.ERROR;
        }
        if (LEVEL_FATAL.equalsIgnoreCase(level))
        {
            return Level.FATAL;
        }
        else
        {
            return Level.ERROR;
        }
    }

    /**
     * 得到当前rootlogger的日志级别
     * 
     * @return
     */
    public static String getCurrentLevel()
    {
        String level = LEVEL_BEBUG;
        String levelName = Logger.getRootLogger().getLevel().toString();
        if (LEVEL_BEBUG.equalsIgnoreCase(levelName))
        {
            level = LEVEL_BEBUG;
        }
        else if (LEVEL_INFO.equalsIgnoreCase(levelName))
        {
            level = LEVEL_INFO;
        }
        else if (LEVEL_WARN.equalsIgnoreCase(levelName))
        {
            level = LEVEL_WARN;
        }
        else if (LEVEL_ERROR.equalsIgnoreCase(levelName))
        {
            level = LEVEL_ERROR;
        }
        else if (LEVEL_FATAL.equalsIgnoreCase(levelName))
        {
            level = LEVEL_FATAL;
        }
        return level;
    }

    /**
     * @param args
     */
    public static void main(String[] args)
    {
        // setLevel(3);
        log.debug("debug");
        log.info("info");
        log.warn("warn");
        log.error("error");
        getLoggersFromMemory();
        // getCurrentLevel();
    }

}

package com.zte.cms.content.series.dao;

import java.util.List;

import com.zte.cms.content.series.dao.ICmsSeriesDAO;
import com.zte.cms.content.series.model.CmsSeries;
import com.zte.ssb.dynamicobj.DynamicObjectBaseDao;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DAOException;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.SSBBus;

public class CmsSeriesDAO extends DynamicObjectBaseDao implements ICmsSeriesDAO
{
    // 日志
    private Log log = SSBBus.getLog(getClass());

    public void insertCmsSeries(CmsSeries cmsSeries) throws DAOException
    {
        log.debug("insert cmsSeries starting...");
        super.insert("insertCmsSeries", cmsSeries);
        log.debug("insert cmsSeries end");
    }

    public void updateCmsSeries(CmsSeries cmsSeries) throws DAOException
    {
        log.debug("update cmsSeries by pk starting...");
        super.update("updateCmsSeries", cmsSeries);
        log.debug("update cmsSeries by pk end");
    }

    public void deleteCmsSeries(CmsSeries cmsSeries) throws DAOException
    {
        log.debug("delete cmsSeries by pk starting...");
        super.delete("deleteCmsSeries", cmsSeries);
        log.debug("delete cmsSeries by pk end");
    }

    public CmsSeries getCmsSeries(CmsSeries cmsSeries) throws DAOException
    {
        log.debug("query cmsSeries starting...");
        CmsSeries resultObj = (CmsSeries) super.queryForObject("getCmsSeries", cmsSeries);
        log.debug("query cmsSeries end");
        return resultObj;
    }

    @SuppressWarnings("unchecked")
    public List<CmsSeries> getCmsSeriesByCond(CmsSeries cmsSeries) throws DAOException
    {
        log.debug("query cmsSeries by condition starting...");
        List<CmsSeries> rList = (List<CmsSeries>) super.queryForList("queryCmsSeriesListByCond", cmsSeries);
        log.debug("query cmsSeries by condition end");
        return rList;
    }

    @SuppressWarnings("unchecked")
    public PageInfo pageInfoQuery(CmsSeries cmsSeries, int start, int pageSize) throws DAOException
    {
        log.debug("page query cmsSeries by condition starting...");
        PageInfo pageInfo = null;
        int totalCnt = ((Integer) super.queryForObject("queryCmsSeriesListCntByCond", cmsSeries)).intValue();
        if (totalCnt > 0)
        {
            int fetchSize = pageSize > (totalCnt - start) ? (totalCnt - start) : pageSize;
            List<CmsSeries> rsList = (List<CmsSeries>) super.pageQuery("queryCmsSeriesListByCond", cmsSeries, start,
                    fetchSize);
            pageInfo = new PageInfo(start, totalCnt, fetchSize, rsList);
        }
        else
        {
            pageInfo = new PageInfo();
        }
        log.debug("page query cmsSeries by condition end");
        return pageInfo;
    }

    @SuppressWarnings("unchecked")
    public PageInfo pageInfoQuery(CmsSeries cmsSeries, int start, int pageSize, PageUtilEntity puEntity)
            throws DAOException
    {
        return super.indexPageQuery("queryCmsSeriesListByCond", "queryCmsSeriesListCntByCond", cmsSeries, start,
                pageSize, puEntity);
    }

    @SuppressWarnings("unchecked")
    public PageInfo pageInfoQueryForPage(CmsSeries cmsSeries, int start, int pageSize) throws DAOException
    {
        log.debug("pageInfoQueryForPage starting...");
        PageInfo pageInfo = null;
        int totalCnt = ((Integer) super.queryForObject("queryCmsSeriesPageListCnt", cmsSeries)).intValue();
        if (totalCnt > 0)
        {
            int fetchSize = pageSize > (totalCnt - start) ? (totalCnt - start) : pageSize;
            List<CmsSeries> rsList = (List<CmsSeries>) super.pageQuery("queryCmsSeriesPageList", cmsSeries, start,
                    fetchSize);
            pageInfo = new PageInfo(start, totalCnt, fetchSize, rsList);
        }
        else
        {
            pageInfo = new PageInfo();
        }
        log.debug("pageInfoQueryForPage end");
        return pageInfo;
    }

    public CmsSeries getCmsSeriesById(CmsSeries cmsSeries) throws DAOException
    {
        log.debug("getCmsSeriesById starting...");
        CmsSeries resultObj = (CmsSeries) super.queryForObject("getCmsSeriesById", cmsSeries);
        log.debug("getCmsSeriesById end");
        return resultObj;
    }
    
    public PageInfo pageInfoQueryPlatformSync(CmsSeries cmsSeries, int start, int pageSize) throws DAOException
    {
        log.debug("pageInfoQueryPlatformSync starting...");
        PageInfo pageInfo = null;
        int totalCnt = ((Integer) super.queryForObject("queryCmsSeriesPlatformSyncListCnt", cmsSeries)).intValue();
        if (totalCnt > 0)
        {
            int fetchSize = pageSize > (totalCnt - start) ? (totalCnt - start) : pageSize;
            List<CmsSeries> rsList = (List<CmsSeries>) super.pageQuery("queryCmsSeriesPlatformSyncList", cmsSeries, start,
                    fetchSize);
            pageInfo = new PageInfo(start, totalCnt, fetchSize, rsList);
        }
        else
        {
            pageInfo = new PageInfo();
        }
        log.debug("pageInfoQueryPlatformSync end");
        return pageInfo;
    }
    
    public PageInfo pageInfoQueryTargetSync(CmsSeries cmsSeries, int start, int pageSize) throws DAOException
    {
        log.debug("pageInfoQueryTargetSync starting...");
        PageInfo pageInfo = null;
        int totalCnt = ((Integer) super.queryForObject("queryCmsSeriesTargetSyncListCnt", cmsSeries)).intValue();
        if (totalCnt > 0)
        {
            int fetchSize = pageSize > (totalCnt - start) ? (totalCnt - start) : pageSize;
            List<CmsSeries> rsList = (List<CmsSeries>) super.pageQuery("queryCmsSeriesTargetSyncList", cmsSeries, start,
                    fetchSize);
            pageInfo = new PageInfo(start, totalCnt, fetchSize, rsList);
        }
        else
        {
            pageInfo = new PageInfo();
        }
        log.debug("pageInfoQueryTargetSync end");
        return pageInfo;
    }

    public PageInfo pageInfoQueryCatSerMapPlatform(CmsSeries cmsSeries, int start, int pageSize) throws DAOException
    {
        log.debug("pageInfoQueryCatSerMapPlatform starting...");
        PageInfo pageInfo = null;
        int totalCnt = ((Integer) super.queryForObject("queryCategorySeriesMapPlatformCntByCond", cmsSeries)).intValue();
        if (totalCnt > 0)
        {
            int fetchSize = pageSize > (totalCnt - start) ? (totalCnt - start) : pageSize;
            List<CmsSeries> rsList = (List<CmsSeries>) super.pageQuery("queryCategoryServiceMapPlatformByCond", cmsSeries, start,
                    fetchSize);
            pageInfo = new PageInfo(start, totalCnt, fetchSize, rsList);
        }
        else
        {
            pageInfo = new PageInfo();
        }
        log.debug("pageInfoQueryCatSerMapPlatform end");
        return pageInfo;
    }
    public PageInfo pageInfoQueryCatSerMapTarget(CmsSeries cmsSeries, int start, int pageSize) throws DAOException
    {
        log.debug("pageInfoQueryCatSerMapTarget starting...");
        PageInfo pageInfo = null;
        int totalCnt = ((Integer) super.queryForObject("queryCategorySeriesMapTargetCntByCond", cmsSeries)).intValue();
        if (totalCnt > 0)
        {
            int fetchSize = pageSize > (totalCnt - start) ? (totalCnt - start) : pageSize;
            List<CmsSeries> rsList = (List<CmsSeries>) super.pageQuery("queryCategorySeriesMapTargetByCond", cmsSeries, start,
                    fetchSize);
            pageInfo = new PageInfo(start, totalCnt, fetchSize, rsList);
        }
        else
        {
            pageInfo = new PageInfo();
        }
        log.debug("pageInfoQueryCatSerMapTarget end");
        return pageInfo;
    }
    public PageInfo pageInfoQuerySerCastMapPlatform(CmsSeries cmsSeries, int start, int pageSize) throws DAOException
    {
        log.debug("pageInfoQuerySerCastMapPlatform starting...");
        PageInfo pageInfo = null;
        int totalCnt = ((Integer) super.queryForObject("querySeriesCastMapPlatformCntByCond", cmsSeries)).intValue();
        if (totalCnt > 0)
        {
            int fetchSize = pageSize > (totalCnt - start) ? (totalCnt - start) : pageSize;
            List<CmsSeries> rsList = (List<CmsSeries>) super.pageQuery("queryServiceCastMapPlatformByCond", cmsSeries, start,
                    fetchSize);
            pageInfo = new PageInfo(start, totalCnt, fetchSize, rsList);
        }
        else
        {
            pageInfo = new PageInfo();
        }
        log.debug("pageInfoQuerySerCastMapPlatform end");
        return pageInfo;
    }
    public PageInfo pageInfoQuerySerCastMapTarget(CmsSeries cmsSeries, int start, int pageSize) throws DAOException
    {
        log.debug("pageInfoQuerySerCastMapTarget starting...");
        PageInfo pageInfo = null;
        int totalCnt = ((Integer) super.queryForObject("querySeriesCastMapTargetCntByCond", cmsSeries)).intValue();
        if (totalCnt > 0)
        {
            int fetchSize = pageSize > (totalCnt - start) ? (totalCnt - start) : pageSize;
            List<CmsSeries> rsList = (List<CmsSeries>) super.pageQuery("querySeriesCastMapTargetByCond", cmsSeries, start,
                    fetchSize);
            pageInfo = new PageInfo(start, totalCnt, fetchSize, rsList);
        }
        else
        {
            pageInfo = new PageInfo();
        }
        log.debug("pageInfoQuerySerCastMapTarget end");
        return pageInfo;
    }

    public List<CmsSeries> getSeriesCastTypeName(CmsSeries cmsSeries) throws DAOException
    {
        log.debug("query roletype and castname starting...");
        List<CmsSeries> rList = (List<CmsSeries>) super.queryForList("getSeriesCastTypeName", cmsSeries);
        log.debug("query roletype and castname  end");
        return rList;
    }

    
}
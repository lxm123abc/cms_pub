package com.zte.cms.EMBtask.dao;

import java.util.List;

import com.zte.cms.EMBtask.model.CmsEmbtask;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DAOException;

public interface ICmsEmbtaskDAO
{
    /**
     * 新增CmsEmbtask对象
     * 
     * @param cmsEmbtask CmsEmbtask对象
     * @throws DAOException dao异常
     */
    public void insertCmsEmbtask(CmsEmbtask cmsEmbtask) throws DAOException;

    /**
     * 根据主键更新CmsEmbtask对象
     * 
     * @param cmsEmbtask CmsEmbtask对象
     * @throws DAOException dao异常
     */
    public void updateCmsEmbtask(CmsEmbtask cmsEmbtask) throws DAOException;

    /**
     * 根据主键删除CmsEmbtask对象
     * 
     * @param cmsEmbtask CmsEmbtask对象
     * @throws DAOException dao异常
     */
    public void deleteCmsEmbtask(CmsEmbtask cmsEmbtask) throws DAOException;

    /**
     * 根据主键查询CmsEmbtask对象
     * 
     * @param cmsEmbtask CmsEmbtask对象
     * @return 满足条件的CmsEmbtask对象
     * @throws DAOException dao异常
     */
    public CmsEmbtask getCmsEmbtask(CmsEmbtask cmsEmbtask) throws DAOException;

    /**
     * 根据条件查询CmsEmbtask对象
     * 
     * @param cmsEmbtask CmsEmbtask对象
     * @return 满足条件的CmsEmbtask对象集
     * @throws DAOException dao异常
     */
    public List<CmsEmbtask> getCmsEmbtaskByCond(CmsEmbtask cmsEmbtask) throws DAOException;

    /**
     * 根据条件分页查询CmsEmbtask对象，作为查询条件的参数
     * 
     * @param cmsEmbtask CmsEmbtask对象
     * @param start 起始行
     * @param pageSize 页面大小
     * @return 查询结果
     * @throws DAOException dao异常
     */
    public PageInfo pageInfoQuery(CmsEmbtask cmsEmbtask, int start, int pageSize) throws DAOException;

    /**
     * 根据条件分页查询CmsEmbtask对象，作为查询条件的参数
     * 
     * @param cmsEmbtask CmsEmbtask对象
     * @param start 起始行
     * @param pageSize 页面大小
     * @param puEntity 排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws DAOException dao异常
     */
    public PageInfo pageInfoQuery(CmsEmbtask cmsEmbtask, int start, int pageSize, PageUtilEntity puEntity)
            throws DAOException;

    public PageInfo pageInfoQuerySub(CmsEmbtask cmsEmbtask, int start, int pageSize) throws DAOException;

    public PageInfo pageInfoQuerySub(CmsEmbtask cmsEmbtask, int start, int pageSize, PageUtilEntity puEntity)
            throws DAOException;
    public PageInfo pageInfoQueryEncryption(CmsEmbtask cmsEmbtask, int start, int pageSize, PageUtilEntity puEntity)
            throws DAOException;
}
package com.zte.cms.basicdata.ls;

import com.zte.cms.basicdata.model.CmsProgramtype;
import com.zte.cms.basicdata.model.CmsProgramtypecpMap;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;

public interface ICmsProgramtypecpMapLS
{

    /**
     * 列表
     */
    public abstract TableDataInfo queryCPByTypid(CmsProgramtypecpMap cmsProgramtypecpMap, int start, int pageSize);

    /**
     * 按照cpid查询所有与之关联的节目分类信息
     * 
     * @param cmsProgramtypecpMap
     * @param start
     * @param pageSize
     * @return
     */
    public TableDataInfo queryCmsProgramtypeByCpId(CmsProgramtypecpMap cmsProgramtypecpMap, int start, int pageSize);

    public Integer queryCPByTypidCount(CmsProgramtypecpMap cmsProgramtypecpMap);
}